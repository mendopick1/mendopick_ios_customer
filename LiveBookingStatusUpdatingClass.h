//
//  LiveBookingStatusUpdatingClass.h
//  DeliveryPlus
//
//  Created by Rahul Sharma on 21/11/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol updateLiveBookingStatusDelegate  <NSObject>

@required

-(void)updateLiveBookingStatusResponse:(NSInteger)status;

@end

@protocol noNeedToUpdateStatus  <NSObject>

@required

-(void)noNeedToUpdateBookingStatus:(NSInteger)status;

@end

@protocol gotoMyOrder  <NSObject>

@required

-(void)gotoMyOrderVC:(NSInteger)status;

@end


@interface LiveBookingStatusUpdatingClass : NSObject

@property(assign,nonatomic) id <updateLiveBookingStatusDelegate> updateDelegate;
@property(assign,nonatomic) id <noNeedToUpdateStatus> noNeedToUpdateDelegate;
@property(assign,nonatomic) id <gotoMyOrder> gotoMyOrderDelegate;
@property (strong,nonatomic) NSString *bid;
@property (strong,nonatomic) NSString *status;
@property (assign, nonatomic) CancelBookingReasons cancelReason;



+(instancetype)sharedInstance;
-(void)updateLiveBookingStatusAndShowVC:(NSDictionary *)appointmentResponse;

@end
