//
//  LiveBookingTable.m
//  DeliveryPlus
//
//  Created by Rahul Sharma on 19/11/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import "LiveBookingTable.h"

@implementation LiveBookingTable

@dynamic bid;
@dynamic status;
@dynamic passengerEmail;
@dynamic passengerMobile;
@dynamic pickupAddress;
@dynamic pickupLat;
@dynamic pickupLong;
@dynamic appointmentDate;
@dynamic driverFirstName;
@dynamic driverLastName;
@dynamic driverPpic;

@end
