//
//  RecepientDetails.h
//
//  Created by Rahul Sharma on 03/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface RecepientDetails : NSManagedObject


@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phoneNumber;
@property (nonatomic, retain) NSString * passengerEmail;
@property (nonatomic, retain) NSString * recepientEmail;
@property (nonatomic, retain) NSString * address;
@property (nonatomic ,retain) NSNumber * randomNumber;

@property (nonatomic, retain) NSString * landMark;
@property (nonatomic, retain) NSString * zipCode;
@property (nonatomic, retain) NSString * dropLatitude;
@property (nonatomic, retain) NSString * dropLongitude;
@property (nonatomic, retain) NSString * flatNumber;
@property (nonatomic, retain) NSString * city;

@end
