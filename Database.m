//
//  Database.m
//  privMD
//
//  Created by Rahul Sharma on 20/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "Database.h"
#import "Entity.h"
#import "PatientAppDelegate.h"
#import "SourceAddress.h"
#import "DestinationAddress.h"
#import "UserAddress.h"
#import "ShipmentDetails.h"
#import "LiveBookingTable.h"
#import "LiveBookingShipmentDetails.h"
#import "LiveBookingOrderTracking.h"
#import "RecepientDetails.h"

@implementation Database

#pragma mark -
#pragma  mark Card Details Methods  -

-(void)makeDataBaseEntry:(NSDictionary *)dictionary
{
//	TELogInfo(@"DATABASE dictionary : %@",dictionary);
	NSError *error;
	PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	NSManagedObjectContext *context = [appDelegate managedObjectContext];
	Entity *entity = [NSEntityDescription insertNewObjectForEntityForName:@"Entity" inManagedObjectContext:context];
    
    [entity setExpMonth:flStrForObj([dictionary objectForKey:@"exp_month"])];
    [entity setExpYear:flStrForObj([dictionary objectForKey:@"exp_year"])];
    [entity setIdCard:flStrForObj([dictionary objectForKey:@"id"])];
    [entity setCardtype:flStrForObj([dictionary objectForKey:@"type"])];
   // [entity setLast4:[NSString stringWithFormat:@"%ld",dictionary objectForKey:@"type"]];
    [entity setLast4:flStrForObj([dictionary objectForKey:@"last4"])];

    
	BOOL isSaved = [context save:&error];
	if (isSaved)
    {
	//	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Campaign added to favourite list" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	//	[alertView show];
    }
	else
    {
	//	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Saving failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	//	[alertView show];
	}
    
}
+ (NSArray *)getCardDetails;
{
    
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Entity" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
	//	[fetchRequest release];
   // TELogInfo(@"resutl : %@",result);
    return result;
    
    return nil;
}

+ (BOOL)DeleteCard:(NSString*)Campaign_id
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"Entity" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"idCard == %@", Campaign_id];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    
    if ([context save:&error]) {
        return YES;
    }
    else {
        return NO;
    }
    return NO;
}


/**
 *  On logout it will delete all cards
 */

+ (void)DeleteAllCard
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"Entity" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    //  NSPredicate *pred = [NSPredicate predicateWithFormat:@"idCard == %@", Campaign_id];
    //[fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}

/**
 *  Adding Address to the Database and managing that
 */

#pragma mark -
#pragma  mark Source Address Details Methods  -

-(void)addSourceAddressInDataBase:(NSDictionary *)dictionary
{
    NSArray *addressArr = [[NSMutableArray alloc] initWithArray:[Database getSourceAddressFromDataBase]];
    if(addressArr.count)
    {
        for (int arrCount = 0; arrCount < addressArr.count;  arrCount++)
        {
            id address =  addressArr[arrCount];
            SourceAddress *add = (SourceAddress*)address;
            if([add.keyId isEqualToString:dictionary[(@"place_id")]] )
            {
                return;
            }
        }
    }
    NSDictionary *location = dictionary[@"geometry"][@"location"];
    NSError *error;
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    SourceAddress *entity = [NSEntityDescription insertNewObjectForEntityForName:@"SourceAddress" inManagedObjectContext:context];
    
    NSString *add1 = [NSString stringWithFormat:@"%@",flStrForStr(dictionary[@"name"])];
    NSString *add2 = @"";
    NSString *zipCode =@"";
    NSArray *arr = [dictionary objectForKey:@"address_components"];
    for (int addrCount=1 ; addrCount< [arr count]; addrCount++)
    {
        NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
        if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
        {
            add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
        }
        if([[arrCountryType objectAtIndex:0] isEqualToString:@"postal_code"])
        {
            zipCode = flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"]);
        }
    }
    if ([add2 hasPrefix:@", "]) {
        add2 = [add2 substringFromIndex:2];
    }
    
    if ([add2 hasSuffix:@", "]) {
        add2 = [add2 substringToIndex:[add2 length]];
    }
    if (add1.length == 0)
    {
        NSArray *arr = dictionary[@"address_components"];
        NSString *add2 =@"";
        for (int addrCount=0 ; addrCount< [arr count]; addrCount++)
        {
            NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
            if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
            {
                add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
            }
        }
        
        if ([add2 hasPrefix:@","]) {
            add2 = [add2 substringFromIndex:1];
        }
        
        if ([add2 hasSuffix:@", "]) {
            add2 = [add2 substringToIndex:[add2 length]-2];
        }
        if(add2.length)
        {
            add1 = [NSString stringWithFormat:@"%@, %@",add1, add2];
            add2 = @"";
        }
        else
        {
            add1 = [NSString stringWithFormat:@"%@ %@",add1, add2];
            add2 = @"";
        }
    }
//        [entity setSrcAddress:flStrForStr(dictionary[@"name"])];
//        [entity setSrcAddress2:flStrForStr(dictionary[@"formatted_address"])];

    [entity setSrcAddress:flStrForStr(add1)];
    [entity setSrcAddress2:flStrForStr(add2)];
    [entity setSrcLatitude:[NSNumber numberWithDouble:[[location objectForKey:@"lat"] doubleValue]]];
    [entity setSrcLongitude:[NSNumber numberWithDouble:[[location objectForKey:@"lng"] doubleValue]]];
    [entity setKeyId:flStrForStr(dictionary[(@"place_id")])];
    [entity setZipCode:flStrForStr(zipCode)];
    BOOL isSaved = [context save:&error];
    if (isSaved) {
    }
}

+(NSArray *)getSourceAddressFromDataBase
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SourceAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
	return result;
    
}

+ (void)deleteAllSourceAddress
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"SourceAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}


#pragma mark -
#pragma  mark Destination Address Details Methods  -

-(void)addDestinationAddressInDataBase:(NSDictionary *)dictionary
{
    NSArray *addressArr = [[NSMutableArray alloc] initWithArray:[Database getDestinationAddressFromDataBase]];
    if(addressArr.count)
    {
        for (int arrCount = 0; arrCount < addressArr.count;  arrCount++)
        {
            id address =  addressArr[arrCount];
            SourceAddress *add = (SourceAddress*)address;
            if([add.keyId isEqualToString:dictionary[(@"place_id")]] )
            {
                return;
            }
        }
    }
    NSDictionary *location = dictionary[@"geometry"][@"location"];
    NSError *error;
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    DestinationAddress *entity = [NSEntityDescription insertNewObjectForEntityForName:@"DestinationAddress" inManagedObjectContext:context];
    
    NSString *add1 = [NSString stringWithFormat:@"%@",flStrForStr(dictionary[@"name"])];
    NSString *add2 = @"";
    NSString *zipCode =@"";
    NSArray *arr = [dictionary objectForKey:@"address_components"];
    for (int addrCount=1 ; addrCount< [arr count]; addrCount++)
    {
        NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
        if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
        {
            add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
        }
        if([[arrCountryType objectAtIndex:0] isEqualToString:@"postal_code"])
        {
            zipCode = flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"]);
        }
    }
    if ([add2 hasPrefix:@", "]) {
        add2 = [add2 substringFromIndex:2];
    }
    
    if ([add2 hasSuffix:@", "]) {
        add2 = [add2 substringToIndex:[add2 length]];
    }
    if (add1.length == 0)
    {
        NSArray *arr = dictionary[@"address_components"];
        NSString *add2 =@"";
        for (int addrCount=0 ; addrCount< [arr count]; addrCount++)
        {
            NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
            if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
            {
                add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
            }
        }
        
        if ([add2 hasPrefix:@","]) {
            add2 = [add2 substringFromIndex:1];
        }
        
        if ([add2 hasSuffix:@", "]) {
            add2 = [add2 substringToIndex:[add2 length]-2];
        }
        if(add2.length)
        {
            add1 = [NSString stringWithFormat:@"%@, %@",add1, add2];
            add2 = @"";
        }
        else
        {
            add1 = [NSString stringWithFormat:@"%@ %@",add1, add2];
            add2 = @"";
        }
    }
    
//    [entity setDesAddress:flStrForStr(dictionary[@"name"])];
//    [entity setDesAddress2:flStrForStr(dictionary[@"formatted_address"])];
    
    [entity setDesAddress:flStrForStr(add1)];
    [entity setDesAddress2:flStrForStr(add2)];
    [entity setDesLatitude:[NSNumber numberWithDouble:[[location objectForKey:@"lat"] doubleValue]]];
    [entity setDesLongitude:[NSNumber numberWithDouble:[[location objectForKey:@"lng"] doubleValue]]];
    [entity setKeyId:flStrForStr(dictionary[@"place_id"])];
    [entity setZipCode:flStrForStr(dictionary[@"zipCode"])];
    BOOL isSaved = [context save:&error];
    if (isSaved)
    {
    }
}

+(NSArray *)getDestinationAddressFromDataBase
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DestinationAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
	return result;
}


+ (BOOL)DeleteDestinationAddress:(NSString*)addressTag
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"DestinationAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"idCard == %@", addressTag];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    
    if ([context save:&error]) {
        return YES;
    }
    else {
        return NO;
    }
    return NO;
}

+ (void)deleteAllDestinationAddress
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"DestinationAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}



#pragma mark -
#pragma  mark User Address (My Address) Details Methods  -

-(BOOL)makeDataBaseEntryForAddress:(NSDictionary *)dictionary
{
    NSError *error;
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    UserAddress *entity1=[NSEntityDescription insertNewObjectForEntityForName:@"UserAddress" inManagedObjectContext:context];
    
    [entity1 setEmail:flStrForObj([dictionary objectForKey:@"email"])];
    [entity1 setAddressLine1:flStrForObj([dictionary objectForKey:@"add_line1"])];
    [entity1 setTagAddress:flStrForObj([dictionary objectForKey:@"tagAddress"])];
    [entity1 setIdentity:[NSNumber numberWithInteger:[[dictionary objectForKey:@"identity"] integerValue]]];
    [entity1 setCity:flStrForObj([dictionary objectForKey:@"city"])];
    [entity1 setFlatNumber:flStrForObj([dictionary objectForKey:@"flatNumber"])];
    [entity1 setLatitude:[NSNumber numberWithFloat:[[dictionary objectForKey:@"app_latitude"] floatValue]]];
    [entity1 setLongitude:[NSNumber numberWithFloat:[[dictionary objectForKey:@"app_longitude"] floatValue]]];
    NSInteger randomNumber = [[dictionary objectForKey:@"randomNumber"] integerValue];
    [entity1 setRandomNumber:[NSNumber numberWithInteger:randomNumber]];
    BOOL isSaved = [context save:&error];
    if (isSaved)
    {
        
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Saving failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
   
    return isSaved;
}

+ (NSArray *)getParticularUserAddressDetails:(NSString *)email and:(NSNumber *)identity
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"UserAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity1];
    
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"email == %@",email];
    
    //fetch ShipmentDetails Depending on Shipment id in the above result
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"identity == %@",identity];
    //[fetch setPredicate:pred2];
    
    NSArray *subPredicates = [NSArray arrayWithObjects:pred1, pred2, nil];
    NSPredicate *orPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    [fetch setPredicate:orPredicate];

    
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *result = [context executeFetchRequest:fetch error:&fetchError];
    
    
    return result;
}

+ (NSArray *)getAddressDetails
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"UserAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity1];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
    
    return result;
}

+ (BOOL)DeleteAddress:(NSNumber *)addKey
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"UserAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity1];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"randomNumber == %@", addKey];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts = [context executeFetchRequest:fetch error:&fetchError];
    //    NSManagedObject *products = [fetchedProducts objectAtIndex:0];
    for (NSManagedObject *product in fetchedProducts)
    {
        [context deleteObject:product];
        break;
    }
    
    if ([context save:&error])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}

+ (void)deleteAllAddressFromDataBase
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"UserAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}


#pragma mark -
#pragma  mark Shipment Details Methods  -

-(BOOL)makeDataBaseEntryForShipments:(NSDictionary *)dictionary
{
    NSError *error;
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    ShipmentDetails *entity1=[NSEntityDescription insertNewObjectForEntityForName:@"ShipmentDetails" inManagedObjectContext:context];
    NSInteger randomNumber = [[dictionary objectForKey:@"randomNumber"] integerValue];
    
    [entity1 setName:flStrForObj([dictionary objectForKey:@"name"])];
    [entity1 setAddress:flStrForObj([dictionary objectForKey:@"address"])];
    [entity1 setShipmentDetails:flStrForObj([dictionary objectForKey:@"shipmentDetails"])];
    [entity1 setEmail:flStrForObj([dictionary objectForKey:@"email"])];
    [entity1 setWeight:flStrForObj([dictionary objectForKey:@"weight"])];
    [entity1 setQuantity:[NSNumber numberWithInteger:[[dictionary objectForKey:@"quantity"] integerValue]]];
    [entity1 setPhoneNumber:flStrForObj([dictionary objectForKey:@"phoneNumber"])];
    [entity1 setRandomNumber:[NSNumber numberWithInteger:randomNumber]];
    [entity1 setImage:[dictionary objectForKey:@"image"]];
    [entity1 setCity:[dictionary objectForKey:@"city"]];
    [entity1 setLandMark:[dictionary objectForKey:@"landMark"]];
    [entity1 setZipCode:[dictionary objectForKey:@"zipCode"]];
    [entity1 setFlatNumber:[dictionary objectForKey:@"flatNumber"]];
    [entity1 setDropLatitude:[dictionary objectForKey:@"dropLatitude"]];
    [entity1 setDropLongitude:[dictionary objectForKey:@"dropLongitude"]];
    [entity1 setCost:[dictionary objectForKey:@"shipmentvalue"]];
    
    
    BOOL isSaved = [context save:&error];
    if (!isSaved)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Saving failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];

    }
    
    return isSaved;
}

+ (NSArray *)getShipmentDetails
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"ShipmentDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity1];
    
    
    NSSortDescriptor *timeDescriptor = [[NSSortDescriptor alloc] initWithKey:@"miles" ascending:YES selector:@selector(compare:)];
    [fetchRequest setSortDescriptors:@[timeDescriptor]];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
    
   
    return result;
}

+ (NSArray *)getParticularShipmentDetail:(NSNumber *)addKey and:(NSDictionary *)dict
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"ShipmentDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity1];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"randomNumber == %@",addKey];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *result = [context executeFetchRequest:fetch error:&fetchError];
    
    ShipmentDetails *sd = result[0];
    [sd setCost:flStrForObj(dict[@"ApproxFare"])];
    double distance = [dict[@"dist"] doubleValue];
    [sd setMiles:[NSNumber numberWithDouble:distance]];
    
    return result;
}


+ (BOOL)DeleteShipmentDetails:(NSNumber *)addKey
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"ShipmentDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity1];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"randomNumber == %@", addKey];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts = [context executeFetchRequest:fetch error:&fetchError];
    //    NSManagedObject *products = [fetchedProducts objectAtIndex:0];
    for (NSManagedObject *product in fetchedProducts)
    {
        [context deleteObject:product];
        break;
    }
    
    if ([context save:&error])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


+ (void)deleteAllShipmentDetails
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"ShipmentDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}


#pragma mark -
#pragma  mark Live Booking Details Methods  -

-(BOOL)makeDataBaseEntryForLiveBooking:(NSDictionary *)dictionary {

    NSError *error;
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    LiveBookingTable *entity1=[NSEntityDescription insertNewObjectForEntityForName:@"LiveBookingTable" inManagedObjectContext:context];
    [entity1 setBid:flStrForObj([dictionary objectForKey:@"bid"])];
    [entity1 setAppointmentDate:flStrForObj([dictionary objectForKey:@"appntDate"])];
    [entity1 setStatus:flStrForObj([dictionary objectForKey:@"status"])];
    [entity1 setPassengerEmail:flStrForObj([dictionary objectForKey:@"passengerEmail"])];
    [entity1 setPassengerMobile:flStrForObj([dictionary objectForKey:@"passengerMobile"])];
    [entity1 setPickupAddress:flStrForObj([dictionary objectForKey:@"pickupAddress"])];
    [entity1 setPickupLat:flStrForObj([dictionary objectForKey:@"pickupLat"])];
    [entity1 setPickupLong:flStrForObj([dictionary objectForKey:@"pickupLong"])];
    [entity1 setDriverFirstName:flStrForObj([dictionary objectForKey:@"driverName"])];
    [entity1 setDriverLastName:flStrForObj([dictionary objectForKey:@"laterbooking"])];
    
    
    BOOL isSaved = [context save:&error];
    if (!isSaved) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Saving failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
   
    return isSaved;
    
}

+ (NSArray *)getLiveBookingDetails:(NSString *)bid
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"LiveBookingTable" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity1];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"bid == %@",bid];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *result = [context executeFetchRequest:fetch error:&fetchError];
    
    
    return result;
}



+ (NSArray *)getAllLiveBookingDetails
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"LiveBookingTable" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity1];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
    
    
    return result;
}


+ (BOOL)DeleteLiveBookingDetails:(NSString *)bid
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"LiveBookingTable" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity1];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"bid == %@", bid];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts = [context executeFetchRequest:fetch error:&fetchError];
    //    NSManagedObject *products = [fetchedProducts objectAtIndex:0];
    for (NSManagedObject *product in fetchedProducts)
    {
        [context deleteObject:product];
        break;
    }
    
    if ([context save:&error])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


+ (void)deleteAllLiveBookingDetails
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"LiveBookingTable" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}


#pragma mark -
#pragma  mark Live Booking Each Shipment Details Methods  -

-(BOOL)makeDataBaseEntryForLiveBookingEachShipment:(NSDictionary *)dictionary
{
    NSError *error;
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    LiveBookingShipmentDetails *entity1=[NSEntityDescription insertNewObjectForEntityForName:@"LiveBookingShipmentDetails" inManagedObjectContext:context];
    
    
    [entity1 setBid:flStrForObj([dictionary objectForKey:@"bid"])];
    [entity1 setShipmentStatus:flStrForObj([dictionary objectForKey:@"shipmentStatus"])];
    [entity1 setShipmentId:flStrForObj([dictionary objectForKey:@"shipmentId"])];
    [entity1 setShipmentPhoneNumber:flStrForObj([dictionary objectForKey:@"shipmentPhoneNumber"])];
    [entity1 setShipmentImage:flStrForObj([dictionary objectForKey:@"shipmentImage"])];
    [entity1 setShipmentAddress:flStrForObj([dictionary objectForKey:@"shipmentAddress"])];
    [entity1 setDropLatitude:flStrForObj([dictionary objectForKey:@"dropLatitude"])];
    [entity1 setDropLongitude:flStrForObj([dictionary objectForKey:@"dropLongitude"])];
    [entity1 setRecepientName:flStrForObj([dictionary objectForKey:@"name"])];

    BOOL isSaved = [context save:&error];
    if (isSaved)
    {
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Saving failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
    
    return isSaved;
    
}

+ (NSArray *)getLiveBookingEachShipmentDetails:(NSString *)bid shipmentId:(NSString *)shipmentId
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"LiveBookingShipmentDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    //fetch all Shipment Details
    [fetch setEntity:entity1];
    
    //fetch ShipmentDetails Depending on Bid
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"bid == %@",bid];
    //[fetch setPredicate:pred1];
    
     //fetch ShipmentDetails Depending on Shipment id in the above result
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"shipmentId == %@",shipmentId];
    //[fetch setPredicate:pred2];

    NSArray *subPredicates = [NSArray arrayWithObjects:pred1, pred2, nil];
    NSPredicate *orPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    [fetch setPredicate:orPredicate];
    
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *result = [context executeFetchRequest:fetch error:&fetchError];
    
    
    return result;
}

+ (NSArray *)getLiveBookingEachShipmentDetails:(NSString *)bid
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"LiveBookingShipmentDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    //fetch all Shipment Details
    [fetch setEntity:entity1];
    
    //fetch ShipmentDetails Depending on Bid
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"bid == %@",bid];
    [fetch setPredicate:pred1];
    
    
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *result = [context executeFetchRequest:fetch error:&fetchError];
    
    
    return result;
}


+ (NSArray *)getAllLiveBookingShipmentDetails
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"LiveBookingShipmentDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    
    
   // NSSortDescriptor *isHomeTeam = [[NSSortDescriptor alloc] initWithKey:@"bid" ascending:YES];

    
    [fetchRequest setEntity:entity1];
    
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"bid"
                                                                   ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];

    
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
    
    
    return result;
}


+ (BOOL)DeleteLiveBookingShipmentDetails:(NSString *)bid shipmentId:(NSString *)shipmentId
{
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"LiveBookingShipmentDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    //fetch all Shipment Details
    [fetch setEntity:entity1];
    
    //fetch ShipmentDetails Depending on Bid
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"bid == %@",bid];
    //[fetch setPredicate:pred1];
    
    //fetch ShipmentDetails Depending on Shipment id in the above result
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"shipmentId == %@",shipmentId];
    //[fetch setPredicate:pred2];
    
    NSArray *subPredicates = [NSArray arrayWithObjects:pred1, pred2, nil];
    NSPredicate *orPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    [fetch setPredicate:orPredicate];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts = [context executeFetchRequest:fetch error:&fetchError];
    //    NSManagedObject *products = [fetchedProducts objectAtIndex:0];
    for (NSManagedObject *product in fetchedProducts)
    {
        [context deleteObject:product];
        break;
    }
    
    if ([context save:&error])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}

+ (BOOL)DeleteWholeLiveBookingShipmentDetails:(NSString *)bid
{
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"LiveBookingShipmentDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity1];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"bid == %@", bid];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts = [context executeFetchRequest:fetch error:&fetchError];
    //    NSManagedObject *products = [fetchedProducts objectAtIndex:0];
    for (NSManagedObject *product in fetchedProducts)
    {
        [context deleteObject:product];
        break;
    }
    
    if ([context save:&error])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}


+ (void)deleteAllLiveBookingShipmentDetails
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"LiveBookingShipmentDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}


+ (NSArray *)getLiveBookingEachOrderTraking:(NSString *)bid shipmentId:(NSString *)shipmentId
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"LiveBookingOrderTrackingTimes" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    //fetch all Shipment Details
    [fetch setEntity:entity1];
    
    //fetch ShipmentDetails Depending on Bid
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"bid == %@",bid];
    //[fetch setPredicate:pred1];
    
    //fetch ShipmentDetails Depending on Shipment id in the above result
    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"shipmentId == %@",shipmentId];
    //[fetch setPredicate:pred2];
    
    NSArray *subPredicates = [NSArray arrayWithObjects:pred1, pred2, nil];
    NSPredicate *orPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    [fetch setPredicate:orPredicate];
    
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *result = [context executeFetchRequest:fetch error:&fetchError];
    
    
    return result;
}


#pragma mark -
#pragma  mark Recepients Details Methods  -

-(BOOL)makeDataBaseEntryForRecepients:(NSDictionary *)dictionary
{
    NSError *error;
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    RecepientDetails *entity1=[NSEntityDescription insertNewObjectForEntityForName:@"RecepientDetails" inManagedObjectContext:context];
    
    [entity1 setPassengerEmail:flStrForObj([dictionary objectForKey:@"passengerEmail"])];
    [entity1 setAddress:flStrForObj([dictionary objectForKey:@"address"])];
    [entity1 setName:flStrForObj([dictionary objectForKey:@"name"])];
    [entity1 setRecepientEmail:flStrForObj([dictionary objectForKey:@"recepientEmail"])];
    [entity1 setPhoneNumber:flStrForObj([dictionary objectForKey:@"phoneNumber"])];
    
    [entity1 setFlatNumber:flStrForObj([dictionary objectForKey:@"flatNumber"])];
    [entity1 setCity:flStrForObj([dictionary objectForKey:@"city"])];
    [entity1 setLandMark:flStrForObj([dictionary objectForKey:@"landMark"])];
    [entity1 setZipCode:flStrForObj([dictionary objectForKey:@"zipCode"])];
    [entity1 setDropLatitude:flStrForObj([dictionary objectForKey:@"dropLatitude"])];
    [entity1 setDropLongitude:flStrForObj([dictionary objectForKey:@"dropLongitude"])];
    

    NSInteger randomNumber = [[dictionary objectForKey:@"randomNumber"] integerValue];
    [entity1 setRandomNumber:[NSNumber numberWithInteger:randomNumber]];
    
    BOOL isSaved = [context save:&error];
    if (isSaved)
    {
        
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Saving failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
    return isSaved;
}

+ (NSArray *)getParticularRecepientsDetails:(NSString *)email
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"RecepientDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity1];
    
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"passengerEmail == %@",email];
    
//    //fetch ShipmentDetails Depending on Shipment id in the above result
//    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"identity == %@",identity];
//    //[fetch setPredicate:pred2];
//    
//    NSArray *subPredicates = [NSArray arrayWithObjects:pred1, pred2, nil];
//    NSPredicate *orPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    [fetch setPredicate:pred1];
    
    
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *result = [context executeFetchRequest:fetch error:&fetchError];
    
    
    return result;
}

+ (NSArray *)getParticularRecepientsDetailsUsingMobile:(NSString *)mobile
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"RecepientDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity1];
    
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"phoneNumber == %@",mobile];
    
    //    //fetch ShipmentDetails Depending on Shipment id in the above result
    //    NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"identity == %@",identity];
    //    //[fetch setPredicate:pred2];
    //
    //    NSArray *subPredicates = [NSArray arrayWithObjects:pred1, pred2, nil];
    //    NSPredicate *orPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    [fetch setPredicate:pred1];
    
    
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *result = [context executeFetchRequest:fetch error:&fetchError];
    
    
    return result;
}



+ (NSArray *)getRecepientsDetails
{
    
    //used for retrieve data from store
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"RecepientDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity1];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
    
    return result;
}

+ (BOOL)DeleteRecepientDetails:(NSNumber *)addKey
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"RecepientDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:entity1];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"randomNumber == %@", addKey];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts = [context executeFetchRequest:fetch error:&fetchError];
    //    NSManagedObject *products = [fetchedProducts objectAtIndex:0];
    for (NSManagedObject *product in fetchedProducts)
    {
        [context deleteObject:product];
        break;
    }
    
    if ([context save:&error])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    return NO;
}

+ (void)deleteAllRecepientsDetailsFromDataBase
{
    PatientAppDelegate *appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"RecepientDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    [context save:&error];
}



@end
