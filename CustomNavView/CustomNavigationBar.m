//
//  CustomNavigationBar.m
//  privMD
//
//  Created by Surender Rathore on 15/04/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "CustomNavigationBar.h"
#import "Helper.h"
#import "LanguageManager.h"

@interface CustomNavigationBar()

@end

@implementation CustomNavigationBar

@synthesize labelTitle;
@synthesize rightbarButton;
@synthesize leftbarButton;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"home_navigation_bar"]]];
        
        //title
        labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, [[UIScreen mainScreen] bounds].size.width, 40)];
        labelTitle.textAlignment = NSTextAlignmentCenter;
        [Helper setToLabel:labelTitle Text:@"" WithFont:Hind_Medium FSize:17 Color:UIColorFromRGB(0xffffff)];
        labelTitle.numberOfLines = 2;
        [self addSubview:labelTitle];
        
        leftbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        leftbarButton.frame = CGRectMake(10, 25, 30, 30);
        
       // NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
        
        if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
              leftbarButton.frame = CGRectMake(ScreenWidth - 40 , 25, 30, 30);
        }
        
       // leftbarButton.backgroundColor = [UIColor redColor];
        
        leftbarButton.titleLabel.font = [UIFont fontWithName:OpenSans_Regular size:11];
        
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"home_menu_icon_off"] forState:UIControlStateNormal];
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"home_menu_icon_on"] forState:UIControlStateHighlighted];
        [leftbarButton addTarget:self action:@selector(leftBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:leftbarButton];
        [self createRightBarButton];
        
    }
    return self;
}
-(void)createRightBarButton
{
    
    UIImage *buttonImageOff = [UIImage imageNamed:@"search_btn_off"];
    UIImage *buttonImageOn = [UIImage imageNamed:@"search_btn_on"];
    rightbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightbarButton.frame = CGRectMake(ScreenWidth-70, 20,50, 44);
    
  //  NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
    
 if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        rightbarButton.frame = CGRectMake(20, 20,50, 44);
    }
    
    rightbarButton.titleLabel.font = [UIFont fontWithName:Hind_Regular size:17];
    [rightbarButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [rightbarButton setTitleColor:UIColorFromRGB(0xffcc00) forState:UIControlStateSelected];
    [rightbarButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [rightbarButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    [rightbarButton addTarget:self action:@selector(rightBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:rightbarButton];
}

-(void)addTitleButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"home_mendo_pick_logo"];
    _titleImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_titleImageButton setUserInteractionEnabled:NO];
    _titleImageButton.frame = CGRectMake(0,64,37,40);
  
    
    NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
    
    if([language isEqualToString:ArabicLang])
    {
        
    }
    [_titleImageButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    _titleImageButton.tag = 100;
    [self addSubview:_titleImageButton];
    [self bringSubviewToFront:_titleImageButton];
    [_titleImageButton setHidden:NO];
    [labelTitle setHidden:YES];
    
}


-(void)hideTitleButton:(BOOL)toHide
{
    if (toHide)
    {
        [_titleImageButton setHidden:YES];
        [labelTitle setHidden:NO];
    }
    else
    {
        [_titleImageButton setHidden:NO];
        [labelTitle setHidden:YES];
    }
}

-(void)hideLeftMenuButton:(BOOL)toHide {
    
    if (toHide) {
        [leftbarButton setBackgroundImage:nil forState:UIControlStateNormal];
        [leftbarButton setBackgroundImage:nil forState:UIControlStateHighlighted];;
        
    }
    else {
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"home_menu_icon_off"] forState:UIControlStateNormal];
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"home_menu_icon_on"] forState:UIControlStateHighlighted];
    }
}

-(void)setCustomTitle:(NSMutableAttributedString *)title{
    
    labelTitle.numberOfLines = 2;
    
    labelTitle.attributedText = title;
    CGRect bounds = labelTitle.frame;
    bounds.size.height = labelTitle.frame.size.height +12;
    bounds.origin.y = labelTitle.frame.origin.y-4 ;
    labelTitle.frame = bounds;
    
    [_titleImageButton setHidden:YES];
    
   // labelTitle.backgroundColor = [UIColor redColor];
    
    [labelTitle setHidden:NO];
    
}

-(void)setTitle:(NSString*)title{
    
    labelTitle.text = NSLocalizedString(title, title);
    labelTitle.textColor = UIColorFromRGB(0x565656);
    labelTitle.font = [UIFont fontWithName:Hind_Medium size:17];
    [_titleImageButton setHidden:YES];
    
    [labelTitle setHidden:NO];
}


-(void)setRightBarButtonTitle:(NSString*)title{
    
    [rightbarButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [rightbarButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
    
    [rightbarButton setTitleColor:UIColorFromRGB(0xd53e30) forState: UIControlStateNormal];
    [rightbarButton setTitle:NSLocalizedString(title, title) forState:UIControlStateNormal];
    
}

-(void)setLeftBarButtonTitle:(NSString*)title
{
    [leftbarButton setTitle:title forState:UIControlStateNormal];
}

-(void)setleftBarButtonImage:(UIImage*)imageOn :(UIImage *)imageOff
{
    [leftbarButton setImage:imageOn forState:UIControlStateHighlighted];
    [leftbarButton setImage:imageOff forState:UIControlStateNormal];
}

-(void)rightBarButtonClicked:(UIButton*)sender{
    if (delegate && [delegate respondsToSelector:@selector(rightBarButtonClicked:)]) {
        [delegate rightBarButtonClicked:sender];
    }
}
-(void)leftBarButtonClicked:(UIButton*)sender {
    if (delegate && [delegate respondsToSelector:@selector(leftBarButtonClicked:)]) {
        [delegate leftBarButtonClicked:sender];
    }
}

-(void)setRightBarButtonImage:(NSDictionary*)images{
    
    UIImage *buttonImageOn = [images objectForKey:@"ImgOn"];
    UIImage *buttonImageOff = [images objectForKey:@"ImgOff"];
    
    [rightbarButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [rightbarButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
}

-(void)hideRightBarButton:(BOOL)hide {
    
    [rightbarButton setHidden:hide];
}

-(void)addButtonRight {
    
    if(rightbarButton)
    {
        [rightbarButton removeFromSuperview];
        rightbarButton = nil;
    }
    rightbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightbarButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-100, 20, 70,44);
    [rightbarButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [Helper setButton:rightbarButton Text:@"" WithFont:Hind_Regular FSize:17 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [rightbarButton setTitleColor:[UIColor colorWithWhite:0.384 alpha:1.000] forState:UIControlStateHighlighted];
    [rightbarButton addTarget:self action:@selector(rightBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:rightbarButton];
}

-(void)removeRightBarButton {
    
    [rightbarButton setHidden:YES];
    [rightbarButton removeFromSuperview];
}


@end
