//
//  CustomNavigationBar.h
//  privMD
//
//  Created by Surender Rathore on 15/04/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CustomNavigationBarDelegate;
@interface CustomNavigationBar : UIView
@property(nonatomic,weak)id <CustomNavigationBarDelegate> delegate;


@property(nonatomic,strong)UILabel *labelTitle;
@property(nonatomic,strong)UIButton *rightbarButton;
@property(nonatomic,strong)UIButton *titleImageButton;
@property(nonatomic,strong)UIButton *leftbarButton;

-(void)setCustomTitle:(NSMutableAttributedString*)title;
-(void)setTitle:(NSString*)title;
-(void)setRightBarButtonTitle:(NSString*)title;
-(void)setLeftBarButtonTitle:(NSString*)title;
-(void)setleftBarButtonImage:(UIImage*)imageOn :(UIImage *)imageOff;
-(void)createRightBarButton;
-(void)hideTitleButton:(BOOL)toHide;
-(void)hideRightBarButton:(BOOL)hide;
-(void)addTitleButton;
-(void)hideLeftMenuButton:(BOOL)toHide;
//-(void)setTitleForRightBarButton:(NSString *)title;
-(void)removeRightBarButton;
-(void)addButtonRight;

@end

@protocol CustomNavigationBarDelegate <NSObject>

@optional
-(void)rightBarButtonClicked:(UIButton*)sender;
-(void)leftBarButtonClicked:(UIButton*)sender;

@end
