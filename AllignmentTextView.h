//
//  AllignmentTextView.h
//  MenDoPick
//
//  Created by Rahul Sharma on 28/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllignmentTextView : UITextView<UITextInput>

@end
