//
//  RightAlignedLabels.m
//  Jaiecom
//
//  Created by Rahulsharma on 06/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "RightAlignedLabels.h"
#import "LanguageManager.h"

@implementation RightAlignedLabels

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    [self setAlignmentForLabel];
}
- (void)setAlignmentForLabel {
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [self setTextAlignment:NSTextAlignmentLeft];
    }
    else {
        [self setTextAlignment:NSTextAlignmentRight];
    }
}
@end
