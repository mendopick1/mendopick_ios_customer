//
//  AllignmentTextView.m
//  MenDoPick
//
//  Created by Rahul Sharma on 28/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "AllignmentTextView.h"
#import "LanguageManager.h"


@implementation AllignmentTextView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setTextAlignmentForLeftAlignedTextView];
}

- (void)setTextAlignmentForLeftAlignedTextView {
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [self setTextAlignment:NSTextAlignmentRight];
    }
    else {
        [self setTextAlignment:NSTextAlignmentLeft];
    }
}

- (CGRect)caretRectForPosition:(UITextPosition *)position
{
    CGRect originalRect = [super caretRectForPosition:position];
    originalRect.size.height = 18.;
    return originalRect;
    
}



@end
