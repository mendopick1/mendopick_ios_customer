//
//  RotateButton.m
//  
//
//  Created by Rahulsharma on 05/12/16.
//
//

#import "RotateButton.h"
#import "LanguageManager.h"

@implementation RotateButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
-(void)awakeFromNib {
    [super awakeFromNib];
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang])
    {
        self.imageView.transform = CGAffineTransformMakeScale(-1, 1);
    }
    else
    {
        
    }
}

@end
