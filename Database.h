//
//  Database.h
//  privMD
//
//  Created by Rahul Sharma on 20/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Database : NSObject

//Card Details
+ (NSArray *)getCardDetails;
- (void)makeDataBaseEntry:(NSDictionary *)dictionary;
+ (BOOL)DeleteCard:(NSString*)Campaign_id;
+ (void)DeleteAllCard;


//SourceAddress
-(void)addSourceAddressInDataBase:(NSDictionary *)dictionary;
+(NSArray *)getSourceAddressFromDataBase;
+ (void)deleteAllSourceAddress;


//Destination Address
-(void)addDestinationAddressInDataBase:(NSDictionary *)dictionary;
+(NSArray *)getDestinationAddressFromDataBase;
+ (void)deleteAllDestinationAddress;


//User Address (My Address)
-(BOOL)makeDataBaseEntryForAddress:(NSDictionary *)dictionary;
+ (NSArray *)getAddressDetails;
+ (NSArray *)getParticularUserAddressDetails:(NSString *)email and:(NSNumber *)identity;
+ (BOOL)DeleteAddress:(NSNumber*)addKey;
+(void)deleteAllAddressFromDataBase;


//Shipment Details
+ (NSArray *)getShipmentDetails;
+ (NSArray *)getParticularShipmentDetail:(NSNumber *)addKey and:(NSDictionary *)dict;
+ (BOOL)DeleteShipmentDetails:(NSNumber*)addKey;
-(BOOL)makeDataBaseEntryForShipments:(NSDictionary *)dictionary;
+(void)deleteAllShipmentDetails;


//Live Booking
+ (NSArray *)getLiveBookingDetails:(NSString *)bid;
+ (BOOL)DeleteLiveBookingDetails:(NSString *)bid;
-(BOOL)makeDataBaseEntryForLiveBooking:(NSDictionary *)dictionary;
+ (void)deleteAllLiveBookingDetails;
+ (NSArray *)getAllLiveBookingDetails;



//Live Booking Shipment Details
+ (NSArray *)getLiveBookingEachShipmentDetails:(NSString *)bid shipmentId:(NSString *)shipmentId;
+ (BOOL)DeleteLiveBookingShipmentDetails:(NSString *)bid shipmentId:(NSString *)shipmentId;
-(BOOL)makeDataBaseEntryForLiveBookingEachShipment:(NSDictionary *)dictionary;
+ (void)deleteAllLiveBookingShipmentDetails;
+ (NSArray *)getAllLiveBookingShipmentDetails;
+ (NSArray *)getLiveBookingEachShipmentDetails:(NSString *)bid;
+ (BOOL)DeleteWholeLiveBookingShipmentDetails:(NSString *)bid;

+ (NSArray *)getLiveBookingEachOrderTraking:(NSString *)bid shipmentId:(NSString *)shipmentId;

//Recepient Details
-(BOOL)makeDataBaseEntryForRecepients:(NSDictionary *)dictionary;
+ (NSArray *)getParticularRecepientsDetails:(NSString *)email;
+ (NSArray *)getParticularRecepientsDetailsUsingMobile:(NSString *)mobile;
+ (NSArray *)getRecepientsDetails;
+ (BOOL)DeleteRecepientDetails:(NSNumber *)addKey;
+ (void)deleteAllRecepientsDetailsFromDataBase;

@end
