//
//  ContactTableViewController.m
//  Speedy
//
//  Created by Rahul Sharma on 21/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ContactTableViewController.h"
#import "ContactsTableViewCell.h"
#import <Contacts/Contacts.h>
#import "CountryPicker.h"

@interface ContactTableViewController ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray * contactList;
    NSMutableArray *indexFirstLetter;
    NSArray *searchResults;
    NSInteger *selectedTag;
    BOOL isFiltered;
}
@end

@implementation ContactTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    contactList = [[NSMutableArray alloc] init];
    indexFirstLetter = [[NSMutableArray alloc] init];
    searchResults = [[NSArray alloc] init];
//    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didTap)];
//    [self.view addGestureRecognizer:gesture];
    isFiltered = NO;
    
    self.title = LS(@"Contacts");
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0x565656e)}];
    [self getContact];
}

-(void)didTap
{
    [self.view endEditing:YES];
}

-(void)getContact
{
    
    [[ProgressIndicator sharedInstance] showPIOnWindow:WINDOW withMessge:@""];
    
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES) {
            //keys with fetching properties
            NSArray *keys = @[CNContactFamilyNameKey,CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey, CNContactEmailAddressesKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error) {
                NSLog(@"error fetching contacts %@", error);
            } else {
                NSString *phone;
                NSString *fullName;
                NSString *firstName;
                NSString *lastName;
                UIImage *profileImage;
                NSMutableArray *emailArray;
                NSString *countryCode;
                NSString* email = @"";
                for (CNContact *contact in cnContacts) {
                    // copy data to my custom Contacts class.
                    firstName = contact.givenName;
                    lastName = contact.familyName;
                    if (lastName == nil) {
                        fullName=[NSString stringWithFormat:@"%@",firstName];
                    }else if (firstName == nil){
                        fullName=[NSString stringWithFormat:@"%@",lastName];
                    }
                    else{
                        fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    }
                    if ([[fullName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
                        fullName = @"";
                    }
                    
                    UIImage *image = [UIImage imageWithData:contact.imageData];
                    if (image != nil) {
                        profileImage = image;
                    }else{
                        profileImage = [UIImage imageNamed:@"placeholder.png"];
                    }
                    for (CNLabeledValue *label in contact.phoneNumbers) {
                        
                        NSLog(@"%@", [label.value valueForKey:@"countryCode"]);
                        
                        
                      countryCode   = [NSString stringWithFormat:@"%@", [label.value valueForKey:@"countryCode"]];
                        
                        countryCode = [countryCode uppercaseString];
                        
                        phone = [label.value stringValue];
                        if ([phone length] > 0) {
                            
                            
                        }
                    }
                    //Get all E-Mail addresses from contacts
                    for (CNLabeledValue *label in contact.emailAddresses) {
                        email = label.value;
                        if ([email length] > 0) {
                            [emailArray addObject:email];
                        }
                    }
                    NSDictionary* personDict = [[NSDictionary alloc] initWithObjectsAndKeys: fullName,@"fullName",profileImage,@"userImage",phone,@"PhoneNumbers",email,@"userEmailId",countryCode,@"CountryCode", nil];
                    [contactList addObject:personDict];
                }
                dispatch_async(dispatch_get_main_queue(), ^{                    
                    if (contactList.count != 0) {
                        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                        [pi hideProgressIndicator];
                        
                        [self indexNamesLetter:contactList];
                        [_tableviewContact reloadData];
                    }
                    else
                    {
                        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                        [pi hideProgressIndicator];
                        UILabel *messagelabel = [[UILabel alloc]initWithFrame: _tableviewContact.frame];
                        [Helper setToLabel:messagelabel Text:LS(@"No Contact Available...") WithFont:Hind_Regular FSize:15 Color:UIColorFromRGB(0x333333)];
                        _tableviewContact.backgroundView = messagelabel;
                        
                    }
                    
                });
            }
        }
    }];

}

-(void)indexNamesLetter :(NSArray *)contactDetails
{
    for (NSDictionary *details in contactDetails)
    {
        NSString *fullName = details[@"fullName"];
        
        //check added....
        if (fullName.length>0) {
            NSString *firstCharacter = [[fullName substringToIndex:1] uppercaseString];
            
            if(![indexFirstLetter containsObject:firstCharacter])
            {
                [indexFirstLetter addObject:firstCharacter];
            }
        }
    }
    [indexFirstLetter sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}


- (NSArray *)shortArray:(NSInteger)index{
    
    NSString *charater = indexFirstLetter[index];
    
    NSPredicate *samplePredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"fullName beginswith[c] '%@'",charater]];
    NSArray *listOfCharacters = [contactList filteredArrayUsingPredicate:samplePredicate];
    
    if ([charater isEqualToString:@"J"]) {

    }
    
    return listOfCharacters;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonAction:(id)sender
{
   [self.view endEditing:YES];
   [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if(isFiltered == YES)
    {
        return nil;
    }

    return indexFirstLetter;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [indexFirstLetter indexOfObject:title];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (isFiltered==YES)
    {
        return 1;
    }
    
    return indexFirstLetter.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if(isFiltered == YES)
    {
        return [searchResults count];
    }
    
    return [[self shortArray:(NSInteger)section] count];;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(isFiltered == YES){
        return  nil;
    }
    
    UIView *view =[[UIView alloc]initWithFrame:CGRectMake(16.0,0,self.view.frame.size.width-16,50)];
    view.backgroundColor = UIColorFromRGB(0xf8f8f8);
    UILabel *customLabel = [[UILabel alloc]initWithFrame:CGRectMake(16.0,0.0,self.view.frame.size.width-15,30)];
    customLabel.text = [indexFirstLetter objectAtIndex:section];
    customLabel.textColor = [UIColor colorWithRed:(48.0/225.0) green:(201.0/255.0) blue:(232.0/255.0) alpha:1];
    customLabel.font = [UIFont fontWithName:Hind_Regular size:15];
    [view addSubview:customLabel];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactsTableViewCell" forIndexPath:indexPath];
    
    if (isFiltered == YES)
    {
        cell.labelName.text = searchResults[indexPath.row][@"fullName"];
        cell.labelMobileNo.text = searchResults[indexPath.row][@"PhoneNumbers"];
        cell.imageviewProfile.image = searchResults[indexPath.row][@"userImage"];
        cell.btnContactSelect.tag = indexPath.section;
    }
    else
    {
        NSArray *sampleArray =[[self shortArray:(NSInteger)indexPath.section] mutableCopy];
        cell.labelName.text = sampleArray[indexPath.row][@"fullName"];
        cell.labelMobileNo.text = sampleArray[indexPath.row][@"PhoneNumbers"];
        cell.imageviewProfile.image = sampleArray[indexPath.row][@"userImage"];
        cell.btnContactSelect.tag = indexPath.row;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if (_oncomplete)
    {
        if (isFiltered == YES)
        {
            _oncomplete(searchResults[indexPath.row]);
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            NSInteger uin = indexPath.row;
            
            NSLog(@"%ld",uin);
            
            NSArray *sampleArray =[[self shortArray:indexPath.section] mutableCopy];
            _oncomplete(sampleArray[indexPath.row]);
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }

}

#pragma mark - Autocomplete SearchBar methods -

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{

}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
        
    if(searchText.length==0)
    {
        isFiltered = NO;
    }
    else{
        isFiltered = YES;
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.fullName contains[cd] %@",searchText];
       searchResults = [contactList filteredArrayUsingPredicate:resultPredicate];
    }
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        [self.tableView reloadData];
    }];
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
   
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
    return YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
