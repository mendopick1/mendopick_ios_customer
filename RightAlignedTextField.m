//
//  RightAlignedTextField.m
//  Jaiecom
//
//  Created by Rahulsharma on 13/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "RightAlignedTextField.h"
#import "LanguageManager.h"


@implementation RightAlignedTextField

- (void)awakeFromNib {
    [super awakeFromNib];
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [self setTextAlignment:NSTextAlignmentLeft];
    }
    else {
        [self setTextAlignment:NSTextAlignmentRight];
    }
}
//- (UITextInputMode *) textInputMode {
//    for (UITextInputMode *tim in [UITextInputMode activeInputModes]) {
//        if ([[Utilities langFromLocale:userDefinedKeyboardLanguage] isEqualToString:[Utilities langFromLocale:tim.primaryLanguage]]) return tim;
//    }
//    return [super textInputMode];
//}

@end
