//
//  RotateImageView.m
//  Jaiecom
//
//  Created by Rahulsharma on 05/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "RotateImageView.h"
#import "LanguageManager.h"

@implementation RotateImageView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    
//    if([CurrentLanguage isLanguageChanged]) {
        if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
            self.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        }
//        else {
//            self.transform = CGAffineTransformMakeScale(1.0, 1.0);
//        }
//    }
    
}

@end
