//
//  LiveBookingOrderTracking.m
//  DeliveryPlus
//
//  Created by Rahul Sharma on 26/11/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import "LiveBookingOrderTracking.h"

@implementation LiveBookingOrderTracking

@dynamic shipmentId;
@dynamic bid;
@dynamic onthewayTime;
@dynamic pickedupTime;
@dynamic jobcompletedTime;
@dynamic jobstartedTime;

@end
