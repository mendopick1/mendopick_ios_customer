//
//  RecepientDetails.m
//
//  Created by Rahul Sharma on 03/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "RecepientDetails.h"

@implementation RecepientDetails

@dynamic name;
@dynamic phoneNumber;
@dynamic address;
@dynamic passengerEmail;
@dynamic recepientEmail;
@dynamic randomNumber;
@dynamic flatNumber;
@dynamic landMark;
@dynamic zipCode;
@dynamic city;
@dynamic dropLatitude;
@dynamic dropLongitude;

@end
