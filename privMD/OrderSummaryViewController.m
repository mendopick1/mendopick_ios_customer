//
//  OrderSummaryViewController.m
//  MenDoPick
//
//  Created by Rahul Sharma on 21/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "OrderSummaryViewController.h"
#import "Database.h"
#import "SDWebImageDownloader.h"
#import <AWSCore/AWSCore.h>
#import "AWSS3Model.h"
#import <AWSCore/AWSNetworking.h>
#import <AWSCore/AWSModel.h>
#import "AmazonTransfer.h"
#import "UIImageView+WebCache.h"
#import "Update&SubscribeToPubNub.h"
#import "RecepientListViewController.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "ShipmentBookingViewController.h"
#import "LanguageManager.h"
#import "OrderStatusTableViewCell.h"
#import "LiveBookingStatusUpdatingClass.h"
#import "OnTheWayViewController.h"

#define onlinedrivertext NSLocalizedString(@"Online Driver", @"Online Driver")

#define statusMessagePending NSLocalizedString(@"Customer Confirmation Pending", @"Customer Confirmation Pending")

#define statusMessageTitle1 NSLocalizedString(@"Customer Confirmed", @"Customer Confirmed")

#define statusMessageTitle2 NSLocalizedString(@"Order accepted", @"Order accepted")

#define statusMessageTitle3 NSLocalizedString(@"On the way to pickup", @"On the way to pickup")

#define statusMessageTitle4 NSLocalizedString(@"Arrived at pickup location", @"Arrived at pickup location")

#define statusMessageTitle5 NSLocalizedString(@"On the way to deliver", @"On the way to deliver")

#define statusMessageTitle6 NSLocalizedString(@"Arrived at drop location", @"Arrived at drop location")

#define statusMessageTitle7 NSLocalizedString(@"Order completed", @"Order completed")


#define statusMessage1 NSLocalizedString(@"The order has been confirmed by customer.", @"The order has been confirmed by customer.")

#define statusMessage2 NSLocalizedString(@"your order has been accepted by", @"your order has been accepted by")

#define statusMessage3 NSLocalizedString(@"is on the way to pickup your order.", @"is on the way to pickup your order.")

#define statusMessage4 NSLocalizedString(@"is arrived at pickup location.", @"is arrived at pickup location.")

#define statusMessage5 NSLocalizedString(@"is on the way to deliver your order.", @"is on the way to deliver your order.")

#define statusMessage6 NSLocalizedString(@"has arrived at drop location and unloaded the vehicle.", @"has arrived at drop location and unloaded the vehicle.")

#define statusMessage7 NSLocalizedString(@"Order Completed", @"Order Completed")

#define cancelReason1 NSLocalizedString(@"I Booked by mistake",@"I Booked by mistake")
#define cancelReason2 NSLocalizedString(@"I Changed my mind",@"I Changed my mind")
#define cancelReason3 NSLocalizedString(@"Its Taking too long",@"Its Taking too long")
#define cancelReason4 NSLocalizedString(@"Other",@"Other")

#define MINIMUMHEIGHTCONTENTVIEW 760

#define MINIMUMHEIGHTTABLEVIEW 105


@interface OrderSummaryViewController ()<updateAndSubscribeToPubNubDelegate,ReceipentListDelegate,UIApplicationDelegate,UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>
{
    NSUserDefaults *ud;
    LiveBookingStatusUpdatingClass *liveBookingStatusUpdateVC;
    UIButton *navNextButton;
    
    NSString *additionalNotes;
    NSInteger tp;
    NSString *driverEmail;
    NSString *driverName;
    NSMutableArray * onlinedriver;
    NSString *deliveryfee;
    NSString *shipmentValue;
    NSString *shipmentImageUrl;
    NSString * apointmentTime;
    
    
    NSArray *trackingImages;
    NSArray *trackingImagesSelected;
    NSMutableArray *tracImage;
    NSArray *statusTitle;
    NSArray *statusTitleMessage;
    NSMutableArray *StatusTitleColorCode;
    
    NSString * customerConfirmedTime;
    NSString * acceptedTime;
    NSString * driverOnTheWayTime;
    NSString * arrivedTime;
    NSString *pickedupTime;
    NSString * dropTime;
    NSString * completedTime;
    NSMutableArray *timeStatusArray;
    
    NSString * dropoffAddress;
    NSString * landMark;
    NSString * flatNumber;
    NSString *  zipCode;
    Update_SubscribeToPubNub *Update_SubscribeToPubNubSharedObject;
    UILabel *navTitle;
    BOOL isFromLiveTrack;
}


@property(nonatomic,strong) NSString *patientPubNubChannel;
@property(nonatomic,strong) NSString *serverPubNubChannel;
@property(nonatomic,strong) NSString *patientEmail;
@property(nonatomic,strong) NSTimer *timer;

@end

static OrderSummaryViewController *showOnGoingBookingVC = nil;

@implementation OrderSummaryViewController

+ (instancetype) getSharedInstance
{
    return showOnGoingBookingVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    showOnGoingBookingVC = self;
    isFromLiveTrack = NO;
    if (_jobStatus == KPending)
    {
        _constraintTableviewHeight.constant = MINIMUMHEIGHTTABLEVIEW*2;
        _constraintHeightContentview.constant = (_constraintTableviewHeight.constant + _constraintHeightContentview.constant)-80;
    }
    else if(_jobStatus == KConfirm)
    {
        _constraintTableviewHeight.constant = MINIMUMHEIGHTTABLEVIEW*2;
        _constraintHeightContentview.constant = (_constraintTableviewHeight.constant + _constraintHeightContentview.constant);
        [self regiesterForNotification];
    }
    else
    {
        [self regiesterForNotification];
        _constraintTableviewHeight.constant = MINIMUMHEIGHTTABLEVIEW*7;
        _constraintHeightContentview.constant = (_constraintTableviewHeight.constant + _constraintHeightContentview.constant)-80;
    }
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self customizeUiAndInitialseArray];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(bookingConfirmed:)
                                                 name:BOOKINGCONFIRMED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dropaddressChangedNotification:)
                                                 name:DROPADDRESSCHANGED object:nil];

    if (_jobStatus != KPending)
    {
        Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
        [Update_SubscribeToPubNubSharedObject subscribeToPassengerChannel];
        Update_SubscribeToPubNubSharedObject.delegate = self;
        liveBookingStatusUpdateVC = [LiveBookingStatusUpdatingClass sharedInstance];
    }
    [self addCustomNavigationBar];
    [self createNavLeftButton];
    [_tableviewOrderStatus reloadData];
}


#pragma mark - NotificationMethods - 
-(void)dropaddressChangedNotification:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    
    NSInteger bid1 = [userInfo[@"aps"][@"bid"] intValue];
    if (bid1 == _bid)
    {
      _labelDropoffAddress.text = userInfo[@"aps"][@"adr"] ;
    }
}

-(void)bookingConfirmed:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    
    NSInteger bid1 = [userInfo[@"aps"][@"bid"] intValue];
    if (bid1 == _bid)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)addCustomNavigationBar
{
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    [[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0x565656)}];
    
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80,-10, 200, 55)];
    navTitle = [[UILabel alloc]initWithFrame:CGRectMake(0,0, 200, 55)];
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.numberOfLines = 2;
    navTitle.font = [UIFont fontWithName:Hind_Medium size:17];
    
    NSString *str = LS(@"Job Id");
    NSString *BID = [NSString stringWithFormat:@"%@ \n",str];
    if (_shipmentDetailsBookingList[@"bookingId"] == nil || [_shipmentDetailsBookingList[@"bookingId"] isEqualToString:@""]) {
        BID = [BID stringByAppendingString:[NSString stringWithFormat:@"%@",_shipmentDetailsBookingList[@"bid"]]];
    }
    else
    {
        BID = [BID stringByAppendingString:[NSString stringWithFormat:@"%@",_shipmentDetailsBookingList[@"bookingId"]]];
    }
    
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc]initWithString:BID];
    [title addAttribute: NSForegroundColorAttributeName value:UIColorFromRGB(0x565656) range:NSMakeRange(0,BID.length)];
    
    navTitle.attributedText = title;
    
    [navView addSubview:navTitle];
    
    self.navigationItem.titleView = navView;
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self deRegiesterForNotification];
    [_timer invalidate];
    _timer=nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BOOKINGCONFIRMED object:nil];
    Update_SubscribeToPubNubSharedObject.delegate = nil;
}

-(void)regiesterForNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subscribePubNub:) name:@"notificationToSubscribe" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unSubscribePubNub:) name:@"notificationToUnSubscribe" object:nil];
}

-(void)deRegiesterForNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"notificationToSubscribe" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"notificationToUnSubscribe" object:nil];
}

-(void)subscribePubNub:(NSNotification *)notice
{
    if (_jobStatus !=KPending && _jobStatus != KConfirm) {
        
        [self sendRequestToGetAppointmentDetails];
    }
    
    [self initialisePubnub];
}

-(void)unSubscribePubNub:(NSNotification *)notice
{
    [_timer invalidate];
    Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
    [Update_SubscribeToPubNubSharedObject unSubsCribeToPubNubChannel:[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientPubNubChannelkey]];
}

#pragma -mark - CustomizeUIAnd Intialise Data -

-(void)customizeUiAndInitialseArray
{
    ud = [NSUserDefaults standardUserDefaults];
    
    _patientEmail = [ud objectForKey:kNSUPatientEmailAddressKey];
    _patientPubNubChannel = [ud objectForKey:kNSUPatientPubNubChannelkey];
    _serverPubNubChannel = kPMDPublishStreamChannel;
    
    driverEmail = _shipmentDetailsBookingList[@"email"];
    driverName = _shipmentDetailsBookingList[@"fname"];
    
    if (driverName==nil || [driverName isEqualToString:@""]) {
        
        driverName = _shipmentDetailsBookingList[@"fName"];
    }
        
    switch (_jobStatus) {
        case KPending:
            trackingImages = @[@"not_confirmed_icon",@"live_tracking_accepted_grey_icon"];
            
            statusTitle = @[statusMessagePending,
                            statusMessageTitle2,
                            ];
            
            _constraintVehicleDetailViewHeight.constant = 0;
            _constraintTopfirstView.constant = 10;
            _viewVehicleDetails.hidden = YES;
             [Helper setButton:_btnSendToCustomer Text:NSLocalizedString(@"Cancel Order", @"Cancel Order") WithFont:Hind_Bold FSize:16 TitleColor:WHITE_COLOR ShadowColor:CLEAR_COLOR];
            [self getShipmentDetails];
            break;
            
        case KConfirm:
            
            [self initialisePubnub];
             onlinedriver = [[NSMutableArray alloc]init];
            [Helper setButton:_btnSendToCustomer Text:NSLocalizedString(@"Dispatch Job", @"Dispatch Job") WithFont:Hind_Bold FSize:16 TitleColor:WHITE_COLOR ShadowColor:CLEAR_COLOR];
            
            trackingImages = @[@"live_tracking_confirmed_icon",@"live_tracking_accepted_grey_icon",@"live_tracking_pickup_grey_icon",@"live_tracking_arrived_location_grey_icon",@"live_tracking_deliver_grey_icon",@"live_tracking_dropped_product_grey_icon",@"live_tracking_accepted_grey_icon"];
            
            trackingImagesSelected = @[@"live_tracking_confirmed_icon",@"live_tracking_accepted_icon",@"live_tracking_pickup_icon",@"live_tracking_arrived_location_green_icon",@"live_tracking_deliver_icon",@"live_tracking_dropped_product_green_icon",@"live_tracking_accepted_icon"];
            
            
            tracImage = [[NSMutableArray alloc] initWithArray:trackingImages];

            statusTitle = @[statusMessageTitle1,
                            statusMessageTitle2,
                            statusMessageTitle3,
                            statusMessageTitle4,
                            statusMessageTitle5,
                            statusMessageTitle6,
                            statusMessageTitle7];
            
            statusTitleMessage = @[statusMessage1,[NSString stringWithFormat:@"%@ %@",statusMessage2,driverName],[NSString stringWithFormat:@"%@ %@",driverName,statusMessage3],[NSString stringWithFormat:@"%@ %@",driverName,statusMessage4],[NSString stringWithFormat:@"%@ %@",driverName,statusMessage5],[NSString stringWithFormat:@"%@ %@",driverName,statusMessage6],[NSString stringWithFormat:@"%@ %@",driverName,statusMessage7]];
            
            [self getShipmentDetails];
            break;
        default:
            [self viewStatusForOngoingBooking];
            [self sendRequestToGetAppointmentDetails];
            break;
    }
    
    
   }

#pragma mark - custom methods -

-(void)viewStatusForOngoingBooking
{
    _bid = [_shipmentDetailsBookingList[@"bid"] integerValue];
    _btnSendToCustomer.hidden = YES;
    _constraintScrollViewBottom.constant = 0;
    _constraintVehicleDetailViewHeight.constant = 0;
    _constraintTopfirstView.constant = 10;
    _viewVehicleDetails.hidden = YES;
    
    trackingImages = @[@"live_tracking_confirmed_icon",@"live_tracking_accepted_grey_icon",@"live_tracking_pickup_grey_icon",@"live_tracking_arrived_location_grey_icon",@"live_tracking_deliver_grey_icon",@"live_tracking_dropped_product_grey_icon",@"live_tracking_accepted_grey_icon"];
    
    trackingImagesSelected = @[@"live_tracking_confirmed_icon",@"live_tracking_accepted_icon",@"live_tracking_pickup_icon",@"live_tracking_pickup_icon",@"live_tracking_deliver_icon",@"live_tracking_dropped_product_green_icon",@"live_tracking_accepted_icon"];
    
    StatusTitleColorCode = [[NSMutableArray alloc] initWithArray:@[@"1",@"0",@"0",@"0",@"0",@"0",@"0"]];
    
    
    acceptedTime = @"";
    driverOnTheWayTime = @"";
    arrivedTime =  @"";
    pickedupTime = @"";
    dropTime = @"";
    completedTime = @"";
    
    timeStatusArray =  [[NSMutableArray alloc] initWithArray:@[@"",acceptedTime,driverOnTheWayTime,arrivedTime,pickedupTime,dropTime,completedTime]];
    
    tracImage = [[NSMutableArray alloc] initWithArray:trackingImages];
    
    [self changeUIAccordingToStatus:_BookingStatus];
    
    statusTitle = @[statusMessageTitle1,
                    statusMessageTitle2,
                    statusMessageTitle3,
                    statusMessageTitle4,
                    statusMessageTitle5,
                    statusMessageTitle6,
                    statusMessageTitle7];
    
    statusTitleMessage = @[[NSString stringWithFormat:@"%@ %@",driverName,statusMessage1],[NSString stringWithFormat:@"%@ %@",statusMessage2,driverName],[NSString stringWithFormat:@"%@ %@",driverName,statusMessage3],[NSString stringWithFormat:@"%@ %@",driverName,statusMessage4],[NSString stringWithFormat:@"%@ %@",driverName,statusMessage5],[NSString stringWithFormat:@"%@ %@",driverName,statusMessage6],[NSString stringWithFormat:@"%@ %@",driverName,statusMessage7]];
    
    [self getOrderDetails];
}

-(void)changeUIAccordingToStatus:(NSInteger)status
{
    switch (status) {
        case kNotificationTypeBookingAccept:

            [tracImage replaceObjectAtIndex:1 withObject:trackingImagesSelected[1]];
            [StatusTitleColorCode replaceObjectAtIndex:1 withObject:@"1"];
            
            [timeStatusArray replaceObjectAtIndex:1 withObject:flStrForObj([Helper togetTimeDifference:acceptedTime])];
            
            [self createNavRightButton:LS(@"Cancel")];
            break;
        case kNotificationTypeBookingOnMyWay:
            
            [tracImage replaceObjectAtIndex:1 withObject:trackingImagesSelected[1]];
             [tracImage replaceObjectAtIndex:2 withObject:trackingImagesSelected[2]];
            
            [timeStatusArray replaceObjectAtIndex:1 withObject:flStrForObj([Helper togetTimeDifference:acceptedTime])];
            [timeStatusArray replaceObjectAtIndex:2 withObject:flStrForObj([Helper togetTimeDifference:driverOnTheWayTime])];

            
            [StatusTitleColorCode replaceObjectAtIndex:1 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:2 withObject:@"1"];
            [self createNavRightButton:LS(@"Live")];
            
            break;
        case kNotificationTypeBookingReachedLocation:
            
            [tracImage replaceObjectAtIndex:1 withObject:trackingImagesSelected[1]];
            [tracImage replaceObjectAtIndex:2 withObject:trackingImagesSelected[2]];
            [tracImage replaceObjectAtIndex:3 withObject:trackingImagesSelected[3]];
            
            [timeStatusArray replaceObjectAtIndex:1 withObject:flStrForObj([Helper togetTimeDifference:acceptedTime])];
            [timeStatusArray replaceObjectAtIndex:2 withObject:flStrForObj([Helper togetTimeDifference:driverOnTheWayTime])];
            [timeStatusArray replaceObjectAtIndex:3 withObject:flStrForObj([Helper togetTimeDifference:arrivedTime])];
            
            [StatusTitleColorCode replaceObjectAtIndex:1 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:2 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:3 withObject:@"1"];
            [self createNavRightButton:LS(@"Live")];
            break;
        case kNotificationTypeBookingStarted:
            
            [tracImage replaceObjectAtIndex:1 withObject:trackingImagesSelected[1]];
            [tracImage replaceObjectAtIndex:2 withObject:trackingImagesSelected[2]];
            [tracImage replaceObjectAtIndex:3 withObject:trackingImagesSelected[3]];
            [tracImage replaceObjectAtIndex:4 withObject:trackingImagesSelected[4]];
            
            [timeStatusArray replaceObjectAtIndex:1 withObject:flStrForObj([Helper togetTimeDifference:acceptedTime])];
            [timeStatusArray replaceObjectAtIndex:2 withObject:flStrForObj([Helper togetTimeDifference:driverOnTheWayTime])];
            [timeStatusArray replaceObjectAtIndex:3 withObject:flStrForObj([Helper togetTimeDifference:arrivedTime])];
            [timeStatusArray replaceObjectAtIndex:4 withObject:flStrForObj([Helper togetTimeDifference:pickedupTime])];
            [timeStatusArray replaceObjectAtIndex:5 withObject:flStrForObj([Helper togetTimeDifference:dropTime])];
            
            
            [StatusTitleColorCode replaceObjectAtIndex:1 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:2 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:3 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:4 withObject:@"1"];

            [self createNavRightButton:LS(@"Live")];
            break;
        case  kNotificationTypeIndividualBooking:
            
            [tracImage replaceObjectAtIndex:1 withObject:trackingImagesSelected[1]];
            [tracImage replaceObjectAtIndex:2 withObject:trackingImagesSelected[2]];
            [tracImage replaceObjectAtIndex:3 withObject:trackingImagesSelected[3]];
            [tracImage replaceObjectAtIndex:4 withObject:trackingImagesSelected[4]];
            [tracImage replaceObjectAtIndex:5 withObject:trackingImagesSelected[5]];
            
            [timeStatusArray replaceObjectAtIndex:1 withObject:flStrForObj([Helper togetTimeDifference:acceptedTime])];
            [timeStatusArray replaceObjectAtIndex:2 withObject:flStrForObj([Helper togetTimeDifference:driverOnTheWayTime])];
            [timeStatusArray replaceObjectAtIndex:3 withObject:flStrForObj([Helper togetTimeDifference:arrivedTime])];
            [timeStatusArray replaceObjectAtIndex:4 withObject:flStrForObj([Helper togetTimeDifference:pickedupTime])];
            [timeStatusArray replaceObjectAtIndex:5 withObject:flStrForObj([Helper togetTimeDifference:dropTime])];
            
            [StatusTitleColorCode replaceObjectAtIndex:1 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:2 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:3 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:4 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:5 withObject:@"1"];
            [self createNavRightButton:LS(@"Live")];
            break;
            
        case kNotificationTypeBookingComplete:
            
            navNextButton.hidden = YES;
            [tracImage replaceObjectAtIndex:1 withObject:trackingImagesSelected[1]];
            [tracImage replaceObjectAtIndex:2 withObject:trackingImagesSelected[2]];
            [tracImage replaceObjectAtIndex:3 withObject:trackingImagesSelected[3]];
            [tracImage replaceObjectAtIndex:4 withObject:trackingImagesSelected[4]];
            [tracImage replaceObjectAtIndex:5 withObject:trackingImagesSelected[5]];
            [tracImage replaceObjectAtIndex:6 withObject:trackingImagesSelected[6]];
            
//            [timeStatusArray replaceObjectAtIndex:1 withObject:[Helper togetTimeDifference:acceptedTime]];
//            [timeStatusArray replaceObjectAtIndex:2 withObject:[Helper togetTimeDifference:driverOnTheWayTime]];
//            [timeStatusArray replaceObjectAtIndex:3 withObject:[Helper togetTimeDifference:arrivedTime]];
//            [timeStatusArray replaceObjectAtIndex:4 withObject:[Helper togetTimeDifference:pickedupTime]];
//            [timeStatusArray replaceObjectAtIndex:5 withObject:[Helper togetTimeDifference:dropTime]];
//            [timeStatusArray replaceObjectAtIndex:6 withObject:[Helper togetTimeDifference:completedTime]];

            
            [StatusTitleColorCode replaceObjectAtIndex:1 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:2 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:3 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:4 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:5 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:6 withObject:@"1"];
            break;
        case kNotificationTypeBookingClosed:
            
            navNextButton.hidden = YES;
            [tracImage replaceObjectAtIndex:1 withObject:trackingImagesSelected[1]];
            [tracImage replaceObjectAtIndex:2 withObject:trackingImagesSelected[2]];
            [tracImage replaceObjectAtIndex:3 withObject:trackingImagesSelected[3]];
            [tracImage replaceObjectAtIndex:4 withObject:trackingImagesSelected[4]];
            [tracImage replaceObjectAtIndex:5 withObject:trackingImagesSelected[5]];
            [tracImage replaceObjectAtIndex:6 withObject:trackingImagesSelected[6]];
            
            [StatusTitleColorCode replaceObjectAtIndex:1 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:2 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:3 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:4 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:5 withObject:@"1"];
            [StatusTitleColorCode replaceObjectAtIndex:6 withObject:@"1"];
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case kNotificationTypeBookingCancelled:
            
            [self.navigationController popViewControllerAnimated:YES];
            break;

        default:
            break;
    }
    
}

// this method is called after coming from order scree

-(void)getOrderDetails
{
    [self downloadShipmentImage:_shipmentDetailsBookingList[@"shipment_details"][0][@"photo"]];
    _labelPickupAddress.text = _shipmentDetailsBookingList[@"shipment_details"][0][@"PickupAddress"];
    _labelDropoffAddress.text = _shipmentDetailsBookingList[@"shipment_details"][0][@"address"];
    _labelRecepientname.text = _shipmentDetailsBookingList[@"shipment_details"][0][@"name"];;
    _labelRecepientMobile.text = [Helper appendToString:_shipmentDetailsBookingList[@"shipment_details"][0][@"mobile"]];
    _labelRecepientMail.text = _shipmentDetailsBookingList[@"shipment_details"][0][@"email"];
    
    // if no email means to reduce height of label.
    if (_labelRecepientMail.text.length == 0) {
        _constraintEmailHeight.constant = 0;
        _constraintHeightSeondview.constant = _constraintHeightSeondview.constant - 20;
    }
    
    _labelWeghtDetails.text = [NSString stringWithFormat:@"%@ : %@ %@ : %@",weight1,_shipmentDetailsBookingList[@"shipment_details"][0][@"weight"],quantity1,_shipmentDetailsBookingList[@"shipment_details"][0][@"quantity"]];
    _labelShipmentItemName.text = _shipmentDetailsBookingList[@"productname"];
    
    if (_shipmentDetailsBookingList[@"productname"] == nil) {
        
         _labelShipmentItemName.text = _shipmentDetailsBookingList[@"shipment_details"][0][@"productname"];
    }
    
    _labelAditionalDetailTitle.text = LS(@"Additional Details");
    
    
    if ([_labelShipmentItemName.text length] == 0)
    {
        _labelShipmentItemName.text = @"";
        _labelAditionalDetailTitle.text = @"";
        _constraintAdditiionalDetails.constant = 0;
    }
    
    float cost = [_shipmentDetailsBookingList[@"shipment_details"][0][@"shipementValue"] floatValue];
    _labelShipmentValue.text =[NSString stringWithFormat:@"%@", [PatientGetLocalCurrency getCurrencyLocal:cost]];
    shipmentValue = _shipmentDetailsBookingList[@"shipementValue"];
    float cost1 = [_shipmentDetailsBookingList[@"shipment_details"][0][@"ApproxFare"] floatValue];
    _labelDeliveryFee.text =[NSString stringWithFormat:@"%@", [PatientGetLocalCurrency getCurrencyLocal:cost1]];
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    
}

// this method is called after coming from BookingList screen
-(void)getShipmentDetails
{
    tp = [_shipmentDetailsBookingList[@"vehicleType"][@"id"] integerValue];    
    _labelVehicleType.text = _shipmentDetailsBookingList[@"vehicleType"][@"typename"];
    
    _bid = [_shipmentDetailsBookingList[@"bookingId"] integerValue];
    [self downloadShipmentImage:_shipmentDetailsBookingList[@"photo"]];
    
    _labelPickupAddress.text = _shipmentDetailsBookingList[@"PickupAddress"];
    _labelDropoffAddress.text = _shipmentDetailsBookingList[@"address"];
    _labelRecepientname.text = _shipmentDetailsBookingList[@"name"];;
    _labelRecepientMobile.text = [Helper appendToString:_shipmentDetailsBookingList[@"mobile"]];
    _labelRecepientMail.text = _shipmentDetailsBookingList[@"email"];
    
    // if no email means to reduce height of label.
    if (_labelRecepientMail.text.length == 0) {
        _constraintEmailHeight.constant = 0;
        _constraintHeightSeondview.constant = _constraintHeightSeondview.constant - 20;
    }
    
    _labelWeghtDetails.text = [NSString stringWithFormat:@"%@ : %@ %@ : %@",weight1,_shipmentDetailsBookingList[@"weight"],quantity1,_shipmentDetailsBookingList[@"quantity"]];
    _labelShipmentItemName.text = _shipmentDetailsBookingList[@"productname"];
    
    if ([_labelShipmentItemName.text length] == 0)
    {
         _labelShipmentItemName.text = @"";
        _labelAditionalDetailTitle.text = @"";
        _constraintAdditiionalDetails.constant = 0;
    }
    
    float cost = [_shipmentDetailsBookingList[@"shipementValue"] floatValue];
    _labelShipmentValue.text =[NSString stringWithFormat:@"%@", [PatientGetLocalCurrency getCurrencyLocal:cost]];
    shipmentValue = _shipmentDetailsBookingList[@"shipementValue"];
    float cost1 = [_shipmentDetailsBookingList[@"ApproxFare"] floatValue];
    _labelDeliveryFee.text =[NSString stringWithFormat:@"%@", [PatientGetLocalCurrency getCurrencyLocal:cost1]];
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
}

-(void)downloadShipmentImage:(NSString *)imgUrl
{
    [_activityindicator startAnimating];
    if(imgUrl.length == 0) {
        if (isFromLiveTrack) { //dont change constrant height if it came from back screen
            isFromLiveTrack = NO;
            return;
        }
        _constraintHeightSeondview.constant = _constraintHeightSeondview.constant - _constraintShipmentImageview.constant;
        _constraintShipmentImageview.constant = 170;
        _constraintHeightContentview.constant = _constraintHeightContentview.constant-_constraintShipmentImageview.constant;
        _constraintShipmentImageview.constant = 0;
        _activityindicator.hidden = YES;
        shipmentImageUrl = @"";
    } else {
        
    shipmentImageUrl = [NSString stringWithFormat:@"%@",imgUrl];
    [self.imageviewShipment sd_setImageWithURL:[NSURL URLWithString:shipmentImageUrl]
                                  placeholderImage:nil
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                             _activityindicator.hidden = YES;
                                             if(!error) {
                                                 [_activityindicator stopAnimating];
                                             }
                                         }];
    }
}

// for calculating delivery fee value
-(void)sendRequestToGetCostAndDistance:(NSString *)dropLat and:(NSString *)dropLong and:(NSString *)key shipmentvalue:(NSString *)shipmentValue
{

    //setup parameters
    NSDictionary *params = @{
                             @"ent_wrk_type":[ud objectForKey:@"vehicleTypeid"],
                             @"ent_pick_lat":[ud objectForKey:@"sourceLatitude"],
                             @"ent_pick_long":[ud objectForKey:@"sourceLongitude"],
                             @"ent_drop_lat":dropLat,
                             @"ent_drop_long":dropLong,
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"ShipmentFare"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                   if (success) {
                                       
                                       _labelDeliveryFee.text = [NSString stringWithFormat:@"%@",[PatientGetLocalCurrency getCurrencyLocal:[response[@"ApproxFare"]floatValue]]];
                                       
                                   }
                                   else
                                   {
                                       _labelDeliveryFee.text = [NSString stringWithFormat:@"%@",[PatientGetLocalCurrency getCurrencyLocal:0.00]];
                                       
                                   }
                                   deliveryfee = response[@"ApproxFare"];
                               }];
}

-(void)initialisePubnub
{
    Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
    [Update_SubscribeToPubNubSharedObject subscribeToPassengerChannel];
    Update_SubscribeToPubNubSharedObject.delegate = self;
    
    if (_jobStatus==KConfirm) {
        [self publishPubNubStream];
        
        _timer = [NSTimer
                  scheduledTimerWithTimeInterval:1 target:self
                  selector:@selector(publishPubNubStream)
                  userInfo:nil
                  repeats:YES];
    }
    
}

-(void)publishPubNubStream
{
    if(_timer == nil){
        return;
    }
    double srcLat= [_shipmentDetailsBookingList[@"PickupLatLongs"][@"lat"] doubleValue];
    
    double srcLong = [_shipmentDetailsBookingList[@"PickupLatLongs"][@"log"] doubleValue];
    
    NSString *dt = @"";
    
    NSDictionary *message = @{
                              @"a":@"1",
                              @"pid": _patientEmail,
                              @"chn": _patientPubNubChannel,
                              @"lt": [NSNumber numberWithFloat:srcLat],
                              @"lg": [NSNumber numberWithFloat:srcLong],
                              @"st": @"3",
                              @"tp": [NSNumber numberWithInteger:tp],
                              @"dt": dt
                              };
    
    if (!(srcLat == 0 || srcLong == 0))
    {
        [Update_SubscribeToPubNubSharedObject publishPubNubStream:message];
    }
}

-(void)callReciepient
{
    if(_labelRecepientMobile.text.length == 0){
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"The driver Phone Number is not available.."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else{
        
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", _labelRecepientMobile.text]];
        //check  Call Function available only in iphone
        if([[UIApplication sharedApplication] canOpenURL:telURL])
        {
            [[UIApplication sharedApplication] openURL:telURL];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"This function is only available in iPhone."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }

}


#pragma -mark - pubnub delegate -

-(void)recievedMessageWithDictionary:(NSDictionary *)messageDict OnChannel:(NSString *)channelName {
    
    if (![messageDict isKindOfClass:[NSDictionary class]])
    {
        return;
    }
    if ([channelName isEqualToString:_patientPubNubChannel] && [messageDict[@"a"] intValue] == 2)
    {
        if ([[messageDict objectForKey:@"flag"] intValue] == 0) //No channels
        {
            
            
            for (int i = 0; i< [messageDict[@"es"] count]; i++) {
                
                NSInteger typeId = [messageDict[@"es"][i][@"tid"] integerValue];
                if (typeId == tp)
                {
                    onlinedriver = messageDict[@"es"][i][@"em"];
                }
            }
            
            if (_jobStatus == KConfirm)
            {
                _labelVehicleType.text = [NSString stringWithFormat:@"%@ (%@ :%lu)",_shipmentDetailsBookingList[@"vehicleType"][@"typename"],onlinedrivertext,(unsigned long)onlinedriver.count];
            }
        }
    }
   else if ([messageDict[@"a"] intValue] == kNotificationTypeBookingOnMyWay ||[messageDict[@"a"] intValue] ==  kNotificationTypeBookingReachedLocation ||[messageDict[@"a"] intValue] == kNotificationTypeBookingStarted || [messageDict[@"a"] intValue] == kNotificationTypeBookingComplete ||  [messageDict[@"a"] intValue] ==  kNotificationTypeIndividualBooking ||[messageDict[@"a"] intValue] == kNotificationTypeBookingClosed || [messageDict[@"a"] intValue] == kNotificationTypeBookingCancelled)
    {
        
        [liveBookingStatusUpdateVC updateLiveBookingStatusAndShowVC:messageDict];
        if ([messageDict[@"bid"] integerValue] == _bid)
        {
            _BookingStatus = [messageDict[@"a"] intValue];
            [self setBookingTimeAccordingToStatus:_BookingStatus with:messageDict[@"dt"]];
            [self changeUIAccordingToStatus:[messageDict[@"a"] intValue]];
            [_tableviewOrderStatus reloadData];
        }
    }
}

-(void)didFailedToConnectPubNub:(PNErrorStatus *)error
{
    
}

-(void)setBookingTimeAccordingToStatus:(NSInteger)status with:(NSString *)date
{
    
    switch (status) {
        case kNotificationTypeBookingAccept:
            
            
            break;
        case kNotificationTypeBookingOnMyWay:
            
            driverOnTheWayTime = date;
            break;
        case kNotificationTypeBookingReachedLocation:
            arrivedTime = date;
            break;
        case kNotificationTypeBookingStarted:
        
            pickedupTime = date;
            break;
        case  kNotificationTypeIndividualBooking:
            dropTime = date;
            
            break;
            
        case kNotificationTypeBookingComplete:
            completedTime = date;
            
            break;
        case kNotificationTypeBookingClosed:
            completedTime = date;
            break;
        case kNotificationTypeBookingCancelled:
            
            break;
            
        default:
            break;
    }

}

#pragma mark - BtnAction -

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"signin_back_icon_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"signin_back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,30,30)];
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [navCancelButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 40,0.0f,30,30)];
    }
    
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}


-(void)createNavRightButton:(NSString *)title
{
    navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navNextButton setFrame:CGRectMake(-6,0,54,44)];
    
    [navNextButton addTarget:self action:@selector(liveButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [Helper setButton:navNextButton Text:title WithFont:OpenSans_Regular FSize:15 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navNextButton setTitle:title forState:UIControlStateNormal];
    [navNextButton setTitle:title forState:UIControlStateSelected];
    [navNextButton setTitleColor:UIColorFromRGB(0xd53e30) forState:UIControlStateNormal];
    [navNextButton setTitleColor:UIColorFromRGB(0xd53e30) forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}


-(void)cancelButtonClicked
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void)liveButtonClicked:(id)sender
{
    if (_BookingStatus == kNotificationTypeBookingAccept)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TitleAlert message:NSLocalizedString(@"Are you sure you want to Cancel this booking ?","Are you sure you want to Cancel this booking ?")delegate:self cancelButtonTitle:TitleNo otherButtonTitles:TitleYes,nil];
        alert.tag=40;
        [alert show];
    }
    else
    {
    isFromLiveTrack = YES;
    OnTheWayViewController *onthewayVC = [self.storyboard instantiateViewControllerWithIdentifier:@"onTheWayVC"];
    
    onthewayVC.shipmentDictionary = _shipmentDetailsBookingList;
    
    onthewayVC.bookingId = _shipmentDetailsBookingList[@"bid"];
    
    [self.navigationController pushViewController:onthewayVC animated:YES];
    }
}

- (IBAction)sendToCustomerBtnAction:(id)sender
{
    apointmentTime = [Helper getCurrentDateTime];
    
    if (_jobStatus == KConfirm)
    {
        if(onlinedriver.count>0)
        {
            ShipmentBookingViewController *shipmentBookVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShipmentBookingViewController"];
            shipmentBookVC.arrayOfDriverEmails = [onlinedriver mutableCopy];
            shipmentBookVC.pickupAddress = _shipmentDetailsBookingList[@"PickupAddress"];
            shipmentBookVC.bId = _shipmentDetailsBookingList[@"bookingId"];
            shipmentBookVC.apointmentTime = _shipmentDetailsBookingList[@"appointment_datetime"];
            shipmentBookVC.vehicleId = _shipmentDetailsBookingList[@"vehicleType"][@"id"];
            
            [self.navigationController pushViewController:shipmentBookVC animated:YES];
        }
        else
        {
            [Helper showAlertWithTitle:@"Message" Message:NSLocalizedString(@"No Drivers Available", @"No Drivers Available")];
        }
    }
    else
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TitleAlert message:NSLocalizedString(@"Are you sure you want to Cancel this booking ?","Are you sure you want to Cancel this booking ?")delegate:self cancelButtonTitle:TitleNo otherButtonTitles:TitleYes,nil];
        alert.tag=60;
        [alert show];
    }
}
- (IBAction)callRecipientBtnAction:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TitleAlert message:NSLocalizedString(@"Are you want to call the Recipient","Are you want to call the Recipient")delegate:self cancelButtonTitle:TitleNo otherButtonTitles:TitleYes,nil];
    alert.tag=50;
    [alert show];
}

#pragma mark - Uitableview Datasource Delegte -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section     //present in data souurce protocol
{
    return statusTitle.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"OrderStatusTableViewCell";
    OrderStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
   
    
    cell.labelOrderStatusTitle.text = [statusTitle objectAtIndex:indexPath.row];
    
    
    switch (_jobStatus) {
        case KPending:
            if (indexPath.row == 1) {
                cell.labelOrderStatusTitle.textColor = UIColorFromRGB(0xe1e1e1);
            }
            cell.imageviewOrderStatus.image = [UIImage imageNamed:[trackingImages objectAtIndex:indexPath.row]];
            break;
        case KConfirm:
            if (indexPath.row != 0) {
                cell.labelOrderStatusTitle.textColor = UIColorFromRGB(0xe1e1e1);
                cell.labelOrderStatusMessage.textColor = UIColorFromRGB(0xe1e1e1);
               
            }
            else
            {
                 cell.labelOrderStatusMessage.text = [statusTitleMessage objectAtIndex:indexPath.row];
            }
            
            cell.imageviewOrderStatus.image = [UIImage imageNamed:[tracImage objectAtIndex:indexPath.row]];
            break;
        default:
            if ([StatusTitleColorCode[indexPath.row] isEqualToString:@"0"]) {
                cell.labelOrderStatusTitle.textColor = UIColorFromRGB(0xe1e1e1);
                cell.labelOrderStatusMessage.textColor = UIColorFromRGB(0xe1e1e1);
            }
            else
            {
                cell.labelOrderStatusTitle.textColor = UIColorFromRGB(0x484848);
                cell.labelOrderStatusMessage.textColor = UIColorFromRGB(0x484848);
                cell.labelOrderStatusMessage.text = [statusTitleMessage objectAtIndex:indexPath.row];
            }
             cell.imageviewOrderStatus.image = [UIImage imageNamed:[tracImage objectAtIndex:indexPath.row]];
            cell.labelTime.text = timeStatusArray[indexPath.row];
            break;
    }
    
    return cell;
}

#pragma mark - Alert View Delegate -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 40)
    {
        if (buttonIndex == 1)
        {
            [self showCancelReasons];
        }
    }
    else if (alertView.tag == 60)
    {
        [self cancelTheNewAppointment];
    }
    else
    {
        if (buttonIndex == 1)
        {
            [self callReciepient];
        }
    }
}

- (void)showCancelReasons {
    
    [self.view endEditing:YES];
    UIActionSheet *actionSheet;
    actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Cancel Reason", @"Select Cancel Reason")
                                              delegate:self
                                     cancelButtonTitle:TitleCancel
                                destructiveButtonTitle:nil
                                     otherButtonTitles:cancelReason1,cancelReason2,cancelReason3,cancelReason4,nil];
    
    actionSheet.tag = 100;
    [actionSheet showInView:self.view];
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self sendRequestForCancelAppointment:cancelReason1];
                break;
            }
            case 1:
            {
                [self sendRequestForCancelAppointment:cancelReason2];
                break;
            }
            case 2:
            {
                [self sendRequestForCancelAppointment:cancelReason3];
                break;
            }
            case 3:
            {
                [self sendRequestForCancelAppointment:cancelReason4];
                break;
            }
                
            default:
                break;
        }
    }
}

#pragma mark - WebService Request -

-(void)sendRequestForCancelAppointment:(NSString *)cancelReason{
    
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:TitleCancel];    //setup parameters
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_dri_email":driverEmail,
                             @"ent_appnt_dt":_shipmentDetailsBookingList[@"apntDt"],
                             @"ent_date_time":[Helper getCurrentDateTime],
                             @"ent_cancel_type":cancelReason,
                             };
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMCancelAppointment
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       [self getLiveCancelResponse:response];
                                   }
                                   else{
                                       
                                   }
                               }];
}

-(void)cancelTheNewAppointment
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnWindow:WINDOW withMessge:LS(@"Cancelling Order...")];

    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_dri_email":@"",
                             @"ent_appnt_dt":flStrForObj(_apptDate),
                             @"ent_date_time":[Helper getCurrentDateTime],
                             @"ent_bid":[NSString stringWithFormat:@"%ld",_bid],
                             };
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"cancelbookingRequested"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       [self getNewBookingResponse:response];
                                   }
                                   else{
                                       
                                   }
                               }];

}


//cancelAppointment

-(void)sendRequestToGetAppointmentDetails {
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnWindow:WINDOW withMessge:LS(@"Loading Order Details....")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *currentDate = [Helper getCurrentDateTime];
    
    @try {
        NSDictionary *params = @{
                                 
                                 @"ent_sess_token":flStrForObj(sessionToken),
                                 @"ent_dev_id":flStrForObj(deviceID),
                                 @"ent_email":flStrForObj(_shipmentDetailsBookingList[@"email"]),
                                 @"ent_user_type":flStrForObj(@"2"),
                                 @"ent_appnt_dt":flStrForObj(_apptDate),
                                 @"ent_date_time":flStrForObj(currentDate),
                                 
                                 };
        
        //setup request
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:kSMGetAppointmentDetial
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success) { //handle success response
                                           //    TELogInfo(@"response %@",response);
                                           [self parseAppointmentDetailResponse:response];
                                       }
                                   }];
    }
    @catch (NSException *exception) {
        //  TELogInfo(@"Invoice Exception : %@",exception);
    }
}



-(void)sendRequestToGetBookingDetails
{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnWindow:WINDOW withMessge:LS(@"")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    @try {
        NSDictionary *params = @{
                                 
                                 @"ent_sess_token":flStrForObj(sessionToken),
                                 @"ent_dev_id":flStrForObj(deviceID),
                                 @"ent_booking_id":[NSString stringWithFormat:@"%ld",_bid],
                                 };
        
        //setup request
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:@"bookingStatus"
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success) { //handle success response
                                           //    TELogInfo(@"response %@",response);
                                           [self bookingDetailResponse:response];
                                       }
                                   }];
    }
    @catch (NSException *exception) {
        //  TELogInfo(@"Invoice Exception : %@",exception);
    }
}

#pragma mark - WebService Response -

-(void)getNewBookingResponse:(NSDictionary *)response
{
    if (response != nil &&  [response[@"errFlag"] integerValue] == 0) {
        
        [Helper showAlertWithTitle:LS(@"Message") Message:[response objectForKey:@"errMsg"]];
        [self cancelButtonClicked];
    }
    
}

-(void)getLiveCancelResponse:(NSDictionary *)responseDictionary{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    NSString *titleMsg = TitleMessage;
    NSString *errMsg = @"";
    
    if (!responseDictionary)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleError message:[responseDictionary objectForKey: @"Message"] delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alertView show];
    }
    else if ([responseDictionary objectForKey:@"Error"])
    {
        errMsg = [responseDictionary objectForKey:@"errMsg"];
    }
    else if ([responseDictionary[@"errFlag"] intValue] == 1 && ([responseDictionary[@"errNum"] intValue] == 96 || [responseDictionary[@"errNum"] intValue] == 7)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:LS(@"Message") Message:[responseDictionary objectForKey:@"errMsg"]];
        
    }
    else
    {
        if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0)
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
            [Database deleteAllLiveBookingDetails];
            [Database deleteAllLiveBookingShipmentDetails];
        }
        else if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 1)
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
        else
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
    }
    
    [Helper showAlertWithTitle:titleMsg Message:errMsg];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)parseAppointmentDetailResponse:(NSDictionary*)response{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil) {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:TitleError Message:[response objectForKey:@"Error"]];
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 81 || [response[@"errNum"] intValue] == 78 )) { //session Expired
        
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            NSDictionary *dict = response[@"shipment_details"][0];
            
            customerConfirmedTime = flStrForStr(dict[@"customerconfirmedtime"]);
            [timeStatusArray replaceObjectAtIndex:0 withObject:[Helper togetTimeDifference:customerConfirmedTime]];
            
            acceptedTime = flStrForStr(dict[@"DriverAcceptedTime"]);
            driverOnTheWayTime = flStrForStr(dict[@"DriverOnTheWayTime"]);
            arrivedTime =  flStrForStr(dict[@"DriverArrivedTime"]);
            pickedupTime = flStrForStr(dict[@"DriverLoadedStartedTime"]);
            dropTime = flStrForStr(dict[@"DriverDropedTime"]);
            completedTime = flStrForStr(dict[@"completedTime"]);
            
            _labelDropoffAddress.text = response[@"shipment_details"][0][@"address"];
            _BookingStatus = [dict[@"status"] integerValue];
            [self changeUIAccordingToStatus:_BookingStatus];
            [_tableviewOrderStatus reloadData];
        }
        else
        {
            [Helper showAlertWithTitle:TitleMessage Message:@"errMsg"];
        }
        
    }
}


-(void)bookingDetailResponse:(NSDictionary*)response{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil) {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:TitleError Message:[response objectForKey:@"Error"]];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            if (_BookingStatus != [response[@"bookingData"][@"statCode"] integerValue]) {
            _BookingStatus = [response[@"bookingData"][@"statCode"] integerValue];
            [self changeUIAccordingToStatus:_BookingStatus];
            [_tableviewOrderStatus reloadData];
            }
        }
        else
        {
            [Helper showAlertWithTitle:TitleMessage Message:@"errMsg"];
        }
        
    }
}


@end
