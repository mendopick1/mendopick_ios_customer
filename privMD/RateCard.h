//
//  RateCard.h
//  iServe_Customer
//
//  Created by Rahul Sharma on 23/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RateCard : UIView

@property (weak, nonatomic) IBOutlet UILabel *labelMinFare;
@property (weak, nonatomic) IBOutlet UILabel *labelAfter2mi;
@property (weak, nonatomic) IBOutlet UILabel *labelAfterHour;
@property (weak, nonatomic) IBOutlet UILabel *labelCapacity;
@property (weak, nonatomic) IBOutlet UILabel *labelLBH;

@property NSInteger bookingType;
+(id)sharedInstance;
-(void)showPopUpWithDictionary:(NSDictionary *)shipments;
-(void)removePopUpWithDictionary:(NSDictionary *)shipments;
@end
