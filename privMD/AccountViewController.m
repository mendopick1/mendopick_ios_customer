//
//  AccountViewController.m
//  privMD
//
//  Created by Rahul Sharma on 19/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "AccountViewController.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "CustomNavigationBar.h"
#import "Database.h"
#import "User.h"
#import "AmazonTransfer.h"
#import "LanguageManager.h"

@interface AccountViewController () <CustomNavigationBarDelegate,UserDelegate>
{
    NSString *url;
    BOOL isProfileImageChanged;
    NSArray *languageInformation;

}
@property (assign ,nonatomic) BOOL isKeyboardIsShown;
@property (strong,nonatomic)  UITextField *activeTextField;

@end

@implementation AccountViewController
@synthesize logoutButton;
@synthesize accFirstNameTextField;
@synthesize accEmailTextField;
@synthesize accPhoneNoTextField;
@synthesize accProfilePic;
@synthesize accProfileButton;
@synthesize activityIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - WebService call

-(void)sendServiceForUpdateProfile
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Saving..", @"Saving..")];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceId;
    if (IS_SIMULATOR) {
        
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *fName = accFirstNameTextField.text;
    NSString *eMail = accEmailTextField.text;
    NSString *phone = accPhoneNoTextField.text;
    if(isProfileImageChanged == NO){
        url = [[NSUserDefaults standardUserDefaults] objectForKey:KDAProfilePic];
    }
    
    NSDictionary *params =  @{ @"ent_sess_token":sessionToken,
                               @"ent_dev_id": deviceId,
                               @"ent_first_name":fName,
                               @"ent_email":eMail,
                               @"ent_phone":phone,
                               @"ent_date_time":[Helper getCurrentDateTime],
                               @"ent_profile_pic":url,
                               };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"updateProfile" paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 
                                 [self updateProfileResponse:response];
                                 
                             }
                             else {
                                 
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 
                             }
                         }];

}

-(void)updateProfileResponse:(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleError message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:TitleError Message:[response objectForKey:@"Error"]];
        
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 7)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:TitleMessage Message:[response objectForKey:@"errMsg"]];
        
    }
    else
    {
        
        
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:accFirstNameTextField.text forKey:KDAFirstName];
            //[[NSUserDefaults standardUserDefaults] setObject:accLastNameTextField.text forKey:KDALastName];
            [[NSUserDefaults standardUserDefaults] setObject:accEmailTextField.text forKey:KDAEmail];
           // [[NSUserDefaults standardUserDefaults] setObject:accPhoneNoTextField.text forKey:KDAPhoneNo];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }

        [[NSNotificationCenter defaultCenter] postNotificationName:@"proPicChange" object:nil userInfo:nil];
    }
    
}

-(void)sendServiceForegetProfileData
{
   // [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    CGRect fra = accProfilePic.frame;
    fra.origin.x = accProfilePic.frame.size.width/2-10;
    fra.origin.y = accProfilePic.frame.size.height/2-10;
    fra.size.width = 20;
    fra.size.height = 20;
    
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [activityIndicator startAnimating];

    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params =  @{ @"ent_sess_token":sessionToken,
                               @"ent_dev_id": deviceId,
                               @"ent_date_time":[Helper getCurrentDateTime]
                               };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getProfile"
                              paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 [self getProfileResponse:response];
                             }
                             else{
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 
                             }
                         }];
}

-(void)getProfileResponse:(NSDictionary *)response
{
  //  ProgressIndicator *pi = [ProgressIndicator sharedInstance];
  //  [pi hideProgressIndicator];
    
   // TELogInfo(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleError message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:TitleError Message:[response objectForKey:@"Error"]];
        
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 81 || [response[@"errNum"] intValue] == 78 )) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:TitleMessage Message:[response objectForKey:@"errMsg"]];
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
          
            

            
            NSString *strImageUrl = [NSString stringWithFormat:@"%@",dictResponse[@"pPic"]];

            
            __weak typeof(self) weakSelf = self;
            
            if (strImageUrl.length>0) {
                
            [accProfilePic sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                          placeholderImage:[UIImage imageNamed:@"signup_profile_default_image_circle.png"]
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                     [weakSelf.activityIndicator stopAnimating];
                                 }];
            }
            else
            {
                activityIndicator.hidden=YES;

            }
            
            accFirstNameTextField.text = dictResponse[@"fName"];
            accPhoneNoTextField.text = [Helper appendToString:dictResponse[@"phone"]];
            accEmailTextField.text = dictResponse[@"email"];
            
            [[NSUserDefaults standardUserDefaults] setObject:accFirstNameTextField.text forKey:KDAFirstName];
            [[NSUserDefaults standardUserDefaults] setObject:accEmailTextField.text forKey:KDAEmail];
            [[NSUserDefaults standardUserDefaults] setObject:dictResponse[@"phone"] forKey:KDAPhoneNo];
            [[NSUserDefaults standardUserDefaults]setObject:dictResponse[@"pPic"] forKey:KDAProfilePic];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"proPicChange" object:nil userInfo:nil];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        
        else
        {
            [Helper showAlertWithTitle:TitleMessage Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   
    self.navigationController.navigationBarHidden = YES;
    
    accFirstNameTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:KDAFirstName];
    accFirstNameTextField.textColor = UIColorFromRGB(0x000000);
    accFirstNameTextField.font = [UIFont fontWithName:OpenSans_Regular size:13];
        
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tapGesture];
    
    
    _labelEmail.text = LS(@"Email");
    accEmailTextField.placeholder = LS(@"Email");
    _labelPhoneNumber.text = LS(@"Phone Number");
    accPhoneNoTextField.placeholder = LS(@"Phone Number");
    _btnSelectedLanguage.text = LS(@"Selected Language");
    
    [_btnPickerCancel setTitle:TitleCancel forState:UIControlStateNormal];
    [_btnPickerDone setTitle:LS(@"Done") forState:UIControlStateNormal];
    
    accEmailTextField.userInteractionEnabled = NO;
    accFirstNameTextField.userInteractionEnabled = NO;
    accPhoneNoTextField.userInteractionEnabled = NO;
    accProfileButton.userInteractionEnabled = NO;
    
    accEmailTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:KDAEmail];
    accEmailTextField.textColor = UIColorFromRGB(0x000000);
    accEmailTextField.font = [UIFont fontWithName:OpenSans_Regular size:13];

    
    
    accPhoneNoTextField.text = [Helper appendToString:[[NSUserDefaults standardUserDefaults]objectForKey:KDAPhoneNo]];
    
    accPhoneNoTextField.textColor = UIColorFromRGB(0x000000);
    accPhoneNoTextField.font = [UIFont fontWithName:OpenSans_Regular size:13];
    
    [accProfilePic layoutIfNeeded];
    accProfilePic.layer.cornerRadius = accProfilePic.frame.size.width/2;
    [accProfilePic setClipsToBounds:YES];
   
    [logoutButton setTitle:NSLocalizedString(@"Sign Out", @"Sign Out") forState:UIControlStateNormal];

    _labelPickerTitle.text = LS(@"Language Change");
    
    [self addCustomNavigationBar];
    
    [self sendServiceForegetProfileData];
}

-(void)viewWillAppear:(BOOL)animated
{
    isProfileImageChanged = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    _isKeyboardIsShown = NO;
    
    
    if (!languageInformation) {
        languageInformation = [[NSArray alloc] init];
    }
    languageInformation =  @[LS(@"English"),
                             LS(@"Arabic")];
    
    [_btnLanguageChange setTitle:languageInformation[0] forState:UIControlStateNormal];
    
    if ([[LanguageManager currentLanguageCode]isEqualToString:ArabicCode])
    {
        [_btnLanguageChange setTitle:languageInformation[1] forState:UIControlStateNormal];
        [_pickerChangeLanguage selectRow:1 inComponent:0 animated:YES];
    }


}
-(void)viewDidAppear:(BOOL)animated{
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [accPhoneNoTextField resignFirstResponder];
    [accFirstNameTextField resignFirstResponder];
   // [accLastNameTextField resignFirstResponder];
    [accEmailTextField resignFirstResponder];
    
    
}
#pragma mark ButtonAction Methods -

- (void)menuButtonPressedAccount
{
    [self.view endEditing:YES];
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}
- (void)saveUserDetails:(id)sender
{
    UIButton *mBut = (UIButton *)sender;
    
    mBut.userInteractionEnabled = YES;
    {
        if(mBut.isSelected)
        {
            mBut.selected =NO;
            [mBut setTitle:NSLocalizedString(@"Edit", @"Edit") forState:UIControlStateNormal];
            accEmailTextField.userInteractionEnabled = NO;
            accFirstNameTextField.userInteractionEnabled = NO;
            accPhoneNoTextField.userInteractionEnabled = NO;
            accProfileButton.userInteractionEnabled = NO;
            
            [self sendServiceForUpdateProfile];
        }
        else
        {
            mBut.selected = YES;
            [mBut setTitle:NSLocalizedString(@"Save", @"Save") forState:UIControlStateSelected];
            accEmailTextField.userInteractionEnabled = NO;
            accFirstNameTextField.userInteractionEnabled = YES;
            accPhoneNoTextField.userInteractionEnabled = NO;
            accProfileButton.userInteractionEnabled = YES;
            
        }
    }
}

#pragma mark Custom Methods -

- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Profile", @"Profile")];

    UILabel *bglabel = [[UILabel alloc]initWithFrame:CGRectMake(0, customNavigationBarView.frame.size.height-1, customNavigationBarView.frame.size.width, 1)];
    [customNavigationBarView addSubview:bglabel];
    bglabel.backgroundColor = UIColorFromRGB(0xcccccc);
   
    [customNavigationBarView setRightBarButtonTitle:NSLocalizedString(@"Edit",@"Edit")];
    [self.view addSubview:customNavigationBarView];
    
}

-(void)rightBarButtonClicked:(UIButton *)sender{
    
    [self.view endEditing:YES];
    [self saveUserDetails:sender];
}
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}

- (IBAction)profilePicButtonClicked:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Picture",@"Edit Picture") delegate:self cancelButtonTitle:TitleCancel destructiveButtonTitle:nil otherButtonTitles:TitlePhoto,TitlePhotoLibrary, nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

- (IBAction)passwordButtonClicked:(id)sender
{
//    [Helper showAlertWithTitle:@"Message" Message:@"Please visit our website Roadyo.net to change your password."];

}

- (IBAction)languageChangeBtnAction:(id)sender
{
    [self pickerOpenAnimate];
}

- (IBAction)logoutButtonClicked:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirm",@"Confirm") message:NSLocalizedString(@"Are you sure you want to logout?",@"Are you sure you want to logout?") delegate:self cancelButtonTitle:TitleNo otherButtonTitles:TitleYes, nil];
    [alertView show];

}
- (IBAction)cancelBtnAction:(id)sender
{
    [self pickerDownAnimate];
}

- (IBAction)pickerDoneBtnAction:(id)sender
{
    [self pickerDownAnimate];
    
    NSInteger selectedRow = [self.pickerChangeLanguage selectedRowInComponent:0];
    
    [_btnLanguageChange setTitle:languageInformation[selectedRow] forState:UIControlStateNormal];
    
    if (selectedRow == 0 &&  [[LanguageManager currentLanguageCode]isEqualToString:ArabicCode])
    {
        [LanguageManager saveLanguageByIndex:0];
        [self reloadRootViewController];
    }
    else if(selectedRow == 1 && ![[LanguageManager currentLanguageCode]isEqualToString:ArabicCode])
    {
        [LanguageManager saveLanguageByIndex:3];
        [self reloadRootViewController];
    }
}

- (void)reloadRootViewController
{
    [XDKAirMenuController relese];
    PatientAppDelegate *delegate = (PatientAppDelegate *)[UIApplication sharedApplication].delegate;
    NSString *storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    delegate.window.rootViewController = [storyboard instantiateInitialViewController];
}


#pragma mark - picker Animation -

-(void)pickerOpenAnimate
{
    [UIView animateWithDuration:0.6 animations:^{
        _constraintBottomPickerView.constant = -0;
        [_viewPicker layoutIfNeeded];
        [self.view layoutIfNeeded];
    }];
}

-(void)pickerDownAnimate
{
    [UIView animateWithDuration:0.6 animations:^{
        _constraintBottomPickerView.constant = -197;
        [_viewPicker layoutIfNeeded];
        [self.view layoutIfNeeded];
    }];
}

#pragma mark- UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) { // logout
        
        [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Logging out..",@"Logging out..")];
        
        User *user = [[User alloc] init];
        user.delegate = self;
        
        [user logout];
    }
}

#pragma mark - Actionsheet Delegate -

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            default:
                break;
        }
    }
}


-(void)cameraButtonClicked:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TitleMessage message: NSLocalizedString(@"Camera is not available",@"Camera is not available") delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Uploading Profile Pic...",@"Uploading Profile Pic...")];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    _pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    
    accProfilePic.image = _pickedImage;
    
    _pickedImage = [self imageWithImage:_pickedImage scaledToSize:CGSizeMake(100,100)];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddhhmmssa"];
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[formatter stringFromDate:[NSDate date]]]];
    
    NSData *data = UIImageJPEGRepresentation(_pickedImage,1.0);
    [data writeToFile:getImagePath atomically:YES];
    
    
    NSString *name = [NSString stringWithFormat:@"%@%@.jpg",@"image",[Helper getCurrentTime]];
    NSString *fullImageName = [NSString stringWithFormat:@"%@/ProfilePhotos/",name];
    
    [AmazonTransfer upload:getImagePath
                   fileKey:fullImageName
                  toBucket:Bucket
                  mimeType:@"image/jpeg"
             progressBlock:^(NSInteger progressSize, NSInteger expectedSize) {
                 
             } completionBlock:^(BOOL success, id result, NSError *error) {
                 
                 isProfileImageChanged = YES;
                 ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
                 [progressIndicator hideProgressIndicator];
                url = [NSString stringWithFormat:@"%@%@/%@",imgLinkForAmazon,Bucket,fullImageName];
                 [progressIndicator hideProgressIndicator];
                 [[NSUserDefaults standardUserDefaults] setObject:url forKey:KDAProfilePic];
                 [[NSUserDefaults standardUserDefaults]synchronize];
                                 
             }];
    
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
   
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;   
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    textFieldEditedFlag = 1;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
        if(textField == accFirstNameTextField)
        {
            [accPhoneNoTextField becomeFirstResponder];
        }
        else if(textField == accPhoneNoTextField)
        {
            [accPhoneNoTextField resignFirstResponder];
        }
    

        return YES;
}
/**
 *  KeyBoard Methods
 */

-(void) keyboardWillShow:(NSNotification *)inputViewNotification
{
    if(_isKeyboardIsShown)
    {
        return;
    }
    // Get the keyboard size
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.scrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.scrollView.scrollIndicatorInsets = ei;
    self.scrollView.contentInset = ei;
    _isKeyboardIsShown = YES;
}

-(void) keyboardWillHide:(NSNotification *)inputViewNotification
{
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.scrollView.scrollIndicatorInsets = ei;
        self.scrollView.contentInset = ei;
        self.scrollView.contentOffset = CGPointMake(0, 0);
    }];
    _isKeyboardIsShown = NO;
}

#pragma mark - Picker Delegate -


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return 2;
    
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [NSString stringWithFormat:@"%@",languageInformation[row]];
    
}


#pragma mark - UserDelegate
-(void)userDidLogoutSucessfully:(BOOL)sucess
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
}

-(void)userDidFailedToLogout:(NSError *)error{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
}



@end
