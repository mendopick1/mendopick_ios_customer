//
//  HelpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "HelpViewController.h"
#import "SignInViewController.h"
#import "SignUpViewController.h"
#import <Canvas/CSAnimationView.h>
#import "UILabel+DynamicHeight.h"
#import "UIImage+animatedGIF.h"
#import "UIImage+GIF.h"
#import "UIImage+GIF.h"
#import <ImageIO/ImageIO.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "Helper.h"
#import "PatientAppDelegate.h"
#import "LanguageManager.h"

static NSBundle *bundle = nil;
@interface HelpViewController ()<UIApplicationDelegate>
{
   
}
@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@end

@implementation HelpViewController
@synthesize topView;
@synthesize signInButton;
@synthesize signUpButton;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [Helper setCornerRadius:3.0 bordercolor:CLEAR_COLOR withBorderWidth:0 toview:signInButton];
    [Helper setCornerRadius:3.0 bordercolor:CLEAR_COLOR withBorderWidth:0 toview:signUpButton];

   // [Helper makeShadowForView:signInButton];
  //  [Helper makeShadowForView:signUpButton];
    
    UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    imageview.image = [UIImage imageNamed:@"Default"];
    
    [_btnLanguageChange setTitle:@"English" forState:UIControlStateSelected];
    [_btnLanguageChange setTitle:@"Arabic" forState:UIControlStateNormal];
    
    if (![[LanguageManager currentLanguageString] isEqualToString:ArabicLang])
    {
        _btnLanguageChange.selected = NO;
    }
    else
    {
        _btnLanguageChange.selected = YES;
    }
}

-(void)createMoviePlayer {
    
    NSURL *movieURL = [[NSBundle mainBundle] URLForResource:@"splash" withExtension:@"mov"];
    
    //movieView
    self.movieView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width,[[UIScreen mainScreen] bounds].size.height)];
    [self.view addSubview:self.movieView];
    
    //grediant view
    self.gradientView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width,[[UIScreen mainScreen] bounds].size.height)];
    [self.view addSubview:self.gradientView];
    [self.view sendSubviewToBack:self.movieView];
    [self.view sendSubviewToBack:self.gradientView];
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
    
    
}



- (void)playerItemDidReachEnd:(NSNotification *)notification {
    
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
    [self.avplayer play];
}

- (void)playerStartPlaying
{
    [self.avplayer play];
}

- (float)sd_frameDurationAtIndex:(NSUInteger)index source:(CGImageSourceRef)source {
    
    float frameDuration = 0.1f;
    CFDictionaryRef cfFrameProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil);
    NSDictionary *frameProperties = (__bridge NSDictionary *)cfFrameProperties;
    NSDictionary *gifProperties = frameProperties[(NSString *)kCGImagePropertyGIFDictionary];
    
    NSNumber *delayTimeUnclampedProp = gifProperties[(NSString *)kCGImagePropertyGIFUnclampedDelayTime];
    if (delayTimeUnclampedProp) {
        frameDuration = [delayTimeUnclampedProp floatValue];
    }
    else {
        
        NSNumber *delayTimeProp = gifProperties[(NSString *)kCGImagePropertyGIFDelayTime];
        if (delayTimeProp) {
            frameDuration = [delayTimeProp floatValue];
        }
    }
    
    // Many annoying ads specify a 0 duration to make an image flash as quickly as possible.
    // We follow Firefox's behavior and use a duration of 100 ms for any frames that specify
    // a duration of <= 10 ms. See <rdar://problem/7689300> and <http://webkit.org/b/36082>
    // for more information.
    
    if (frameDuration < 0.011f) {
        frameDuration = 0.100f;
    }
    
    CFRelease(cfFrameProperties);
    return frameDuration;
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [self animateIn];

}
- (void)animateIn
{
  //  topView.backgroundColor = [UIColor whiteColor];
    
    [UIView animateWithDuration:0.6f animations:^
    {
        _bottomspaceConstraint.constant = 12;
        [self.topView layoutIfNeeded];
        
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [Helper setCornerRadius:4.5 bordercolor:[UIColor redColor] withBorderWidth:0.9 toview:_btnLanguageChange];
    self.navigationController.navigationBarHidden = YES;
    [self.view bringSubviewToFront:topView];

    self.navigationItem.hidesBackButton = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)languageChangeBtnAction:(id)sender
{
    
    if (self.btnLanguageChange.isSelected ) {
        [self.btnLanguageChange setSelected:NO];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:SelectedLanguage];
        
        [signInButton setTitle:@"Sign In" forState:UIControlStateNormal];
         [signInButton setTitle:@"Sign In" forState:UIControlStateHighlighted];
        
        [signUpButton setTitle:@"Sign Up" forState:UIControlStateNormal];
        [signUpButton setTitle:@"Sign Up" forState: UIControlStateHighlighted];
        
         [LanguageManager saveLanguageByIndex:0];
        
    } else {
        
        [self.btnLanguageChange setSelected:YES];
        
        [signUpButton setTitle:@"تسجيل الدخول" forState:UIControlStateNormal];
        [signInButton setTitle:@"التسجيل" forState:UIControlStateNormal];
        
        [signUpButton setTitle:@"تسجيل الدخول" forState:UIControlStateHighlighted];
        [signInButton setTitle:@"التسجيل" forState:UIControlStateHighlighted];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:SelectedLanguage];
        
         [LanguageManager saveLanguageByIndex:3];
        
    }
    
    [self reloadRootViewController];
}

- (void)reloadRootViewController
{
    PatientAppDelegate *delegate = (PatientAppDelegate *)[UIApplication sharedApplication].delegate;
    NSString *storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    delegate.window.rootViewController = [storyboard instantiateInitialViewController];
}



- (IBAction)signInButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"SignIn" sender:self];
    
}

- (IBAction)registerButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"SignUp" sender:self];
}


@end
