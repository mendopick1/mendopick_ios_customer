//
//  PreviousAddressTableViewCell.h
//  MenDoPick
//
//  Created by Rahul Sharma on 25/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviousAddressTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelRecipientName;
@property (weak, nonatomic) IBOutlet UILabel *labelRecipientNumber;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewAccessory;

@end
