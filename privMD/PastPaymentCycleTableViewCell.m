//
//  PastPaymentCycleTableViewCell.m
//  MenDoPick
//
//  Created by Rahul Sharma on 17/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PastPaymentCycleTableViewCell.h"
#import "PatientGetLocalCurrency.h"


@implementation PastPaymentCycleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [Helper setCornerRadius:4.0 bordercolor:UIColorFromRGB(0xb6b6b6) withBorderWidth:0.9 toview:_viewContent];
    [Helper makeShadowForView:_viewContent];
    
}

-(void)setUpData:(NSDictionary *)dict
{
    if (dict.count > 0) {
        
        _labelMessage.hidden = YES;
        _labelNetReceivableAmount.text = [PatientGetLocalCurrency getCurrencyLocal:[dict[@"closingBal"] floatValue]];
        _labelPaidOutBalance.text = [PatientGetLocalCurrency getCurrencyLocal:[dict[@"settilementamount"] floatValue]];
        _labelPaidDate.text = dict[@"settilementDate"];
  
        
    }
    
    else
    {
        _labelMessage.hidden = NO;
        [self.contentView bringSubviewToFront:_labelMessage];
    }
    
   }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
