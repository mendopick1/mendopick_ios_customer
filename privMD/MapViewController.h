//
//  MapViewController.h
//  DoctorMapModule
//
//  Created by Rahul Sharma on 03/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "WildcardGestureRecognizer.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "MyAppTimerClass.h"
#import <AXRatingView/AXRatingView.h>

@interface MapViewController : UIViewController<GMSMapViewDelegate,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    BOOL isNurseSelected;
    BOOL isLaterSelected;
    BOOL isNowSelected;
    BOOL isCustomMarkerSelected;
    BOOL isFareButtonClicked;
    BOOL isRequestingButtonClicked;
    PatientAppDelegate *appDelegate;
    NSMutableArray		*arrDBResult;
    NSManagedObjectContext *context;
    UIActionSheet *newSheet;

    MFMailComposeViewController *mailer;
    NSInteger selectedRowForTip;
}

@property(nonatomic,strong) UIPickerView *pkrView;
@property(nonatomic,strong) UIPickerView *pkrViewForAddTip;
@property (strong, nonatomic) NSMutableArray *drivers;
@property (strong, nonatomic) NSArray *nurses ;
@property (strong, nonatomic) NSArray *buttonArray;
@property (strong, nonatomic) NSArray *buttonArray2;
@property (strong, nonatomic) NSDictionary *dictSelectedDoctor;
@property (strong, nonatomic) UITextField *textFeildAddress;
@property MyAppTimerClass *myAppTimerClassObj;
@property (weak, nonatomic) IBOutlet UIButton *currentLocationButton;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *datepickerBottomConstraint;


@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *guesture;

- (IBAction)datePickerDoneButtonAction:(id)sender;
- (IBAction)datePickerCancelButtonAction:(id)sender;
+ (instancetype) getSharedInstance;
- (void)publishPubNubStream;
- (void)hideActivityIndicatorWithMessage;

@end
