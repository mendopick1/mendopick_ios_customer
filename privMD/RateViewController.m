//
//  Rating.m
//  DeliveryPlus
//
//  Created by Rahul Sharma on 15/12/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import "RateViewController.h"
#import "InvoiceViewController.h"
#import "UIImageView+WebCache.h"
#import "XDKAirMenuController.h"
#import "MapViewController.h"
#import "Database.h"
#import "ReceiptViewController.h"
#import "CustomNavigationBar.h"
#import "PatientAppDelegate.h"

#define kDescriptionPlaceholder NSLocalizedString(@"Please add Comment here...",@"Please add Comment here...")

@interface RateViewController ()<UIKeyboardDelegates>
{
    BOOL isKeyBoardShown;
    UIWindow *window;
    UITextField *activeTextField;
    PatientAppDelegate *appDelegate;
    NSDictionary *appointmentDetail;
    
    CustomNavigationBar *customNavigationBarView;
}

@property(nonatomic,strong)ReceiptViewController *receiptView;

@end

@implementation RateViewController


-(void)viewDidLoad
{
   
    [self updateUI];
    [self addCustomNavigationBar];
    [self sendRequestForAppointmentInvoice];
    self.navigationController.navigationBarHidden = NO;
    [[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
}

- (void) addCustomNavigationBar
{
    customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    customNavigationBarView.tag = 78;
    [customNavigationBarView hideLeftMenuButton:YES];
    
    UILabel *bglabel = [[UILabel alloc]initWithFrame:CGRectMake(0, customNavigationBarView.frame.size.height-1, customNavigationBarView.frame.size.width, 1)];
    [customNavigationBarView addSubview:bglabel];
    bglabel.backgroundColor = UIColorFromRGB(0xD2D2D2);
    [self.view addSubview:customNavigationBarView];
    appointmentDetail = [[NSDictionary alloc] init];
}

-(void)updateUI
{
    _ratingStarView.markFont = [UIFont systemFontOfSize:60];
    _ratingStarView.value = 5;
    _ratingStarView.baseColor = UIColorFromRGB(0xd3d3d3);
    _ratingStarView.highlightColor = UIColorFromRGB(0xffd740);
    _ratingStarView.stepInterval = 1.0;
    _ratingStarView.userInteractionEnabled = YES;
    window = [[UIApplication sharedApplication] keyWindow];
    [Helper setCornerRadius:2.0 bordercolor:UIColorFromRGB(0x00aeef) withBorderWidth:0.9 toview:_viewDetails];
}

-(void)viewDidAppear:(BOOL)animated
{
    appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.keyboardDelegate = self;
}

-(void)viewDidDisappear:(BOOL)animated
{
    appDelegate.keyboardDelegate = nil;
}

-(void)sendRequestForAppointmentInvoice {

    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnWindow:window withMessge:NSLocalizedString(@"Please wait..", @"Please wait..")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
        
    }
    else {
        
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
        
    }
    
    
    NSString *currentDate = [Helper getCurrentDateTime];
    
    @try {
        NSDictionary *params = @{
                                 @"ent_sess_token":sessionToken,
                                 @"ent_dev_id":deviceID,
                                 @"ent_email":_customerEmail,
                                 @"ent_user_type":@"2",
                                 @"ent_appnt_dt":_appointmentDate,
                                 @"ent_date_time":currentDate,
                                 
                                 };
        
        //setup request
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:kSMGetAppointmentDetial
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success) { //handle success response
                                          // TELogInfo(@"response %@",response);
                                           [self parseAppointmentDetailResponse:response];
                                       }
                                   }];
    }
    @catch (NSException *exception) {
       // TELogInfo(@"Invoice Exception : %@",exception);
    }
    
}
-(void)parseAppointmentDetailResponse:(NSDictionary*)response{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil)
    {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:TitleError Message:[response objectForKey:@"Error"]];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            
            [_driverImage layoutIfNeeded];
            _driverImage.layer.cornerRadius = _driverImage.frame.size.width/2;
            _driverImage.clipsToBounds = YES;
   
            if([response[@"pPic"] isEqualToString:@"aa_default_profile_pic.gif"] || [response[@"pPic"] isEqualToString:@""]){
                
                _driverImage.image = [UIImage imageNamed:@"signup_profile_default_image_circle"];
            }
            else{
                
                NSString *strImageUrl = [NSString stringWithFormat:@"%@",response[@"pPic"]];
                
                [_driverImage sd_setImageWithURL:[NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strImageUrl]]
                                placeholderImage:[UIImage imageNamed:@"signup_profile_default_image_circle"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                           
                                           if(error == nil){
                                               
                                               _driverImage.image = image;
                                               
                                               _driverImage.layer.borderWidth=1.5;
                                               _driverImage.layer.masksToBounds = YES;
                                               _driverImage.layer.borderColor=[UIColorFromRGB(0xef4836)CGColor];
                                           }
                                           
                                       }];
            }
            
            appointmentDetail = response;

            _labelDriverName.text = [NSString stringWithFormat:@"%@ %@",response[@"fName"],response[@"lName"]];
            
            _labelPickupAddress.text = response[@"addr1"];
            _labelReceiverAddress.text = response[@"shipment_details"][0][@"address"];
            _labelTotalCost.text = [PatientGetLocalCurrency getCurrencyLocal:[response[@"shipment_details"][0][@"Accounting"][@"Totalamount"] floatValue]];
            NSString *myString = [appointmentDetail[@"apptDt"] substringToIndex:10];
            [customNavigationBarView setCustomTitle:[Helper makeAttributedTitlle:myString]];
            
        }
    }
}

-(void)createNavView
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80,-10, 160, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(10,8, 147, 30)];
    // navTitle.text = @"Confirm Number";
    
    NSString *str = LS(@"Your Last Booking");
    
    NSString *myString = [appointmentDetail[@"apptDt"] substringToIndex:10];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"dd MMM yyyy";

    navTitle.text = [NSString stringWithFormat:@"%@: %@",str,[dateFormatter stringFromDate:yourDate]];
    navTitle.textColor = UIColorFromRGB(0x565656);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:Hind_Medium size:17];
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;
    
}


-(void)sendRequestForReviewSubmit {
    
    [[ProgressIndicator sharedInstance] showPIOnView:_mainscrollView withMessage:NSLocalizedString(@"Submiting Review..",@"Submiting Review..")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    if ([_textfieldComment.text isEqualToString:@""]) {
        _textfieldComment.text = @"";
    }
    
    NSDictionary *params = @{
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_dri_email":_customerEmail,
                             @"ent_appnt_dt":_appointmentDate,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             @"ent_rating_num":[NSNumber numberWithFloat:_ratingStarView.value],
                             @"ent_review_msg":_textfieldComment.text,
                             @"ent_shipemnt_id":[NSNumber numberWithInt:1],
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMUpdateSlaveReview
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                     //  TELogInfo(@"response %@",response);
                                       [self parseSubmitReviewResponse:response];
                                   }
                               }];
}

-(void)parseSubmitReviewResponse:(NSDictionary*)response {
    
    [[ProgressIndicator sharedInstance] hideProgressIndicator];

    [Database deleteAllLiveBookingDetails];
    [Database deleteAllLiveBookingShipmentDetails];

    if (response == nil) {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:TitleError Message:[response objectForKey:@"Error"]];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReviewSubmitted object:nil userInfo:nil];


            [self dismissViewControllerAnimated:YES completion:nil];
        
        }
        else {
            
            [Helper showAlertWithTitle:TitleMessage Message:[response objectForKey:@"errMsg"]];
        }
    }
}

- (IBAction)submitRatingButtonAction:(id)sender {
    
    [self sendRequestForReviewSubmit];
}

#pragma mark -
#pragma mark TextView Delegate methods

#pragma - mark - TextfieldDelegate -

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
    
    if ([arrayOfString count]>1) {
        if ([arrayOfString[1] length] > 2)
        {
            return NO;
        }
    }
    return YES;
}


#pragma mark - Textfield Delegate -

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)keyboardWillHide:(__unused NSNotification *)inputViewNotification {
    
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.mainscrollView.scrollIndicatorInsets = ei;
        self.mainscrollView.contentInset = ei;
        self.mainscrollView.contentOffset = CGPointMake(0,0);
    }];
    activeTextField = nil;
    
    
}

-(void)keyboardWillShown:(__unused NSNotification *)inputViewNotification {
    
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.mainscrollView convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.mainscrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height+70, 0.0);
    self.mainscrollView.scrollIndicatorInsets = ei;
    self.mainscrollView.contentInset = ei;
    
    [_mainscrollView setContentOffset:CGPointMake(0,170) animated:YES];
}

-(void)dismissKeyboard
{
    [activeTextField resignFirstResponder];
    [self.view endEditing:YES];
}

- (IBAction)detailsBtnAction:(id)sender
{
    _receiptView = [ReceiptViewController sharedInstance];
    _receiptView.modalPresentationStyle = UIModalPresentationOverFullScreen;
    _receiptView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    if (_receiptView.appointmentDetails == nil)
    {
         _receiptView.appointmentDetails = [[NSDictionary alloc] init];
    }
    _receiptView.appointmentDetails = appointmentDetail;
   // [_receiptView  customizeUI];
    [self presentViewController:_receiptView animated:YES completion:nil];
}


@end
