//
//  LanguageChangeViewController.h
//  MenDoPick
//
//  Created by Rahul Sharma on 02/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageChangeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableviewLangChange;

@end
