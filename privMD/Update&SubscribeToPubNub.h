//
//  Update&SubscribeToPubNub.h
//  GuydeeSlave
//
//  Created by Rahul Sharma on 26/08/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "PatientPubNubWrapper.h"
#import <PubNub/PubNub.h>


@protocol updateAndSubscribeToPubNubDelegate  <NSObject>

@required

-(void)recievedMessageWithDictionary:(NSDictionary *)messageDictionary OnChannel:(NSString *)channel;
-(void)didFailedToConnectPubNub:(PNErrorStatus*)error;
@end

@interface Update_SubscribeToPubNub : NSObject<PNObjectEventListener>


@property (nonatomic) PubNub *client;
@property(assign,nonatomic) id<updateAndSubscribeToPubNubDelegate> delegate;

+(instancetype)sharedInstance;
-(void)subscribeToPassengerChannel;
-(void)subscribeToDriverChannel:(NSString *)channel;

-(void)publishPubNubStream:(NSDictionary *)parameters;
-(void)stopPubNubStream;
-(void)unSubsCribeToPubNubChannel:(NSString *)channel;

@end
