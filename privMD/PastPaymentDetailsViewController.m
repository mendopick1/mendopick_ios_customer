//
//  PastPaymentDetailsViewController.m
//  MenDoPick
//
//  Created by Rahul Sharma on 17/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "PastPaymentDetailsViewController.h"
#import "PatientGetLocalCurrency.h"
#import "LanguageManager.h"


@interface PastPaymentDetailsViewController ()

@end

@implementation PastPaymentDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Helper setCornerRadius:4.0 bordercolor:UIColorFromRGB(0xb6b6b6) withBorderWidth:0.9 toview:_viewContent];
    [Helper makeShadowForView:_viewContent];
    
    self.navigationController.navigationBarHidden = NO;
    [[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0x565656)}];
    self.title = _walletDetail[@"settilementDate"];
    
//    NSString *myString = _walletDetail[@"settilementDate"];
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//    dateFormatter.dateFormat = @"yyyy-MM-dd";
//    NSDate *yourDate = [dateFormatter dateFromString:myString];
//    dateFormatter.dateFormat = @"dd MMM yyyy";
//    self.title = [dateFormatter stringFromDate:yourDate];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    [self createNavLeftButton];
    [self customizeData];
}

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"signin_back_icon_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"signin_back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,30,30)];
    
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [navCancelButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 40,0.0f,30,30)];
    }
    
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}


-(void)customizeData
{
    
    float openingBalance = [_walletDetail[@"details"][@"openingbal"] floatValue];
    
    float cashEarning = [_walletDetail[@"details"][@"cashEarning"] floatValue];
    
    float cardEarning = [_walletDetail[@"details"][@"cardEarning"] floatValue];
    
    float pgComission = [_walletDetail[@"details"][@"pgcommission"] floatValue];
    
    float netReceivable = [_walletDetail[@"details"][@"netrecivable"] floatValue];
    
    float paidOut = [_walletDetail[@"details"][@"paidOut"] floatValue];
    
    float closingBal = [_walletDetail[@"closingBal"] floatValue];
    
    NSString *cashBookingT = LS(@"Total Cash Booking");
    NSString *cardBookingT = LS(@"Total Card Booking");

    NSString *totalBooking = _walletDetail[@"details"][@"bookings"];
    NSString *totalCashBooking = _walletDetail[@"details"][@"cashbooking"];
    NSString *totalCardBooking = _walletDetail[@"details"][@"cardBooking"];
    
    _labelOpeningBalance.text = [PatientGetLocalCurrency getCurrencyLocal:openingBalance];
    
    _labelNetReceivable.text = [PatientGetLocalCurrency getCurrencyLocal:netReceivable];
    
    _labelCashBookingText.text = [NSString stringWithFormat:@"%@ (%@)",cashBookingT,totalCashBooking];
    
    _labelTotalCardBookingText.text = [NSString stringWithFormat:@"%@ (%@)",cardBookingT,totalCardBooking];
    
    _labelTotalCardBooking.text = [PatientGetLocalCurrency getCurrencyLocal:cardEarning];
    
    _labelCashBooking.text = [PatientGetLocalCurrency getCurrencyLocal:cashEarning];
    
    
    _labelTotalBooking.text = totalBooking;
    
    _labelPgCommission.text = [PatientGetLocalCurrency getCurrencyLocal:pgComission];
    
    _labelPaidOut.text = [PatientGetLocalCurrency getCurrencyLocal:paidOut];
    
    _labelClosingBalance.text = [PatientGetLocalCurrency getCurrencyLocal:closingBal];
    
}

-(void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
