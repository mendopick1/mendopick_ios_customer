//
//  ViewController.m
//  arma on 03/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "ViewController.h"
#import "PickUpViewController.h"
#import "AccountViewController.h"
#import "MapViewController.h"
#import "NewOrderViewController.h"
#import "PaymentViewController.h"
#import "SupportViewController.h"
#import "InviteViewController.h"
#import "AboutViewController.h"
#import "AddressManageViewController.h"
#import "BookingListViewController.h"
#import "LanguageChangeViewController.h"

typedef NS_ENUM(NSInteger,iDeliverVCType) {
    
    deliveryPlusProfileVC = 0,
    deliveryPlusBookVC,
    deliveryPlusBookingDetailsVC,
    deliveryPlusPaymentVC,
    deliveryPlusSupportVC,
    deliveryPlusAboutVC,
    deliveryPlusShareVC,
    deliveryPlusSettingVC,
    
};



@interface ViewController ()<XDKAirMenuDelegate>
@property (nonatomic, strong) UITableView *tableView;

//@property(nonatomic, strong) MapViewController *homeVC;
@property(nonatomic, strong) BookingListViewController *homeVC;
@property(nonatomic, strong) NewOrderViewController *OrderVC;
@property(nonatomic, strong) SupportViewController *supportVC;
@property(nonatomic, strong) PaymentViewController *paymentVC;
@property(nonatomic, strong) AboutViewController *aboutVC;
@property(nonatomic, strong) InviteViewController *inviteVC;
@property(nonatomic, strong) AccountViewController *profileVC;
@property(nonatomic, strong) AddressManageViewController *addressVC;
@property(nonatomic, strong) LanguageChangeViewController *langchangeVC;
    
@end

@implementation ViewController


- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:UIColorFromRGB(0xffffff)];
    [_tableView setBackgroundColor:UIColorFromRGB(0xffffff)];
    
    if (IS_IOS7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.airMenuController = [XDKAirMenuController sharedMenu];
    self.airMenuController.airDelegate = self;
    [self.view addSubview:self.airMenuController.view];
    [self addChildViewController:self.airMenuController];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
   
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"TableViewSegue"])
    {
        self.tableView = ((UITableViewController*)segue.destinationViewController).tableView;
    }
}


#pragma mark - XDKAirMenuDelegate

- (UIViewController*)airMenu:(XDKAirMenuController*)airMenu viewControllerAtIndexPath:(NSIndexPath*)indexPath
{
    UIStoryboard *storyboard = self.storyboard;
    UIViewController *vc = nil;
    
    vc.view.autoresizesSubviews = TRUE;
    vc.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    
    
    switch (indexPath.row) {
       
        case deliveryPlusBookVC:{
            
            _homeVC = [storyboard instantiateViewControllerWithIdentifier:@"BookingListViewController"];
            return _homeVC;
            
        }
            break;
        case deliveryPlusBookingDetailsVC: {
            
            
            _OrderVC = [storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
            return _OrderVC;
            
        }
            break;
        
        case deliveryPlusPaymentVC: {
            
            _paymentVC = [storyboard instantiateViewControllerWithIdentifier:@"paymentView"];
            return _paymentVC;
            
        }
        break;
        case deliveryPlusSupportVC: {
            
            if (!_supportVC) {
                
                _supportVC = [storyboard instantiateViewControllerWithIdentifier:@"supportTableVC"];
                return _supportVC;
                
            }
            return _supportVC;
            
        }
            break;
            
        case deliveryPlusAboutVC: {
            
            if (!_aboutVC) {
                
                _aboutVC = [storyboard instantiateViewControllerWithIdentifier:@"aboutVC"];
                return _aboutVC;
                
            }
            return _aboutVC;
        }
            
            break;

        case deliveryPlusShareVC: {
            
            if (!_inviteVC) {
                
                _inviteVC = [storyboard instantiateViewControllerWithIdentifier:@"inviteVC"];
                return _inviteVC;
                
            }
            return _inviteVC;
            
        }
            break;
        case deliveryPlusSettingVC: {
            
            if (!_profileVC) {
                
                _profileVC = [storyboard instantiateViewControllerWithIdentifier:@"profileVC"];
                return _profileVC;
                
            }
            
            return _profileVC;
        }
            break;

        default:

        _profileVC = [storyboard instantiateViewControllerWithIdentifier:@"profileVC"];
        return _profileVC;
        
        break;
    }
    
}



#pragma mark Webservice Handler(Request) -

- (UITableView*)tableViewForAirMenu:(XDKAirMenuController*)airMenu
{
    return self.tableView;
}

@end
