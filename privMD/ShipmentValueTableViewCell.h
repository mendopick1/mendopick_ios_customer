//
//  ShipmentValueTableViewCell.h
//  MenDoPick
//
//  Created by 3Embed on 4/20/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShipmentValueTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *textfieldShipmentValue;
@property (strong, nonatomic) IBOutlet UILabel *textfieldShipmentItemName;

@end
