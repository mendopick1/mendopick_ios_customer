//
//  ShipmentValueTableViewCell.m
//  MenDoPick
//
//  Created by 3Embed on 4/20/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ShipmentValueTableViewCell.h"

@implementation ShipmentValueTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
     self.textfieldShipmentValue.keyboardType = UIKeyboardTypeDecimalPad;
    
    _textfieldShipmentItemName.text = LS(@"Shipment Value");
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
