//
//  RecipientAddressTableViewCell.m
//  MenDoPick
//
//  Created by Rahul Sharma on 25/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "RecipientAddressTableViewCell.h"

@implementation RecipientAddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _textfieldReceiverPhone.keyboardType = UIKeyboardTypePhonePad;
    _textfieldReceiverEmail.keyboardType = UIKeyboardTypeEmailAddress;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
