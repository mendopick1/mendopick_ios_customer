//  main.m
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.

#import <UIKit/UIKit.h>

#import "PatientAppDelegate.h"
#import "UIImage+Additions_568.h"
#import "LanguageManager.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        
        [LanguageManager setupCurrentLanguage];
        [UIImage patchImageNamedToSupport568Resources];
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PatientAppDelegate class]));
    }
}


