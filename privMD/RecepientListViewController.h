//
//  RecepientListViewController.h
//
//  Created by Rahul Sharma on 03/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecepientDetails.h"

@protocol ReceipentListDelegate <NSObject>

-(void)addresschangeClick:(RecepientDetails *)address;

@end

@interface RecepientListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewEmptybox;

@property (weak, nonatomic) IBOutlet UIView *viewHeaderTableview;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic)NSDictionary *shipmentData;
@property (strong,nonatomic) id delegate;
@property BOOL isfromSummaryVC;
- (IBAction)removeRecepientsButtonAction:(id)sender;

@end
