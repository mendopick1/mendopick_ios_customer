//
//  InvoiceViewController.h
//  privMD
//
//  Created by Surender Rathore on 29/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <AXRatingView/AXRatingView.h>
@interface InvoiceViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;


@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *labelDriverName;
@property (weak, nonatomic) IBOutlet UILabel *labelSHipmentValue;
@property (weak, nonatomic) IBOutlet UILabel *labelDeliveryFee;
@property (weak, nonatomic) IBOutlet UILabel *pickupAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelPaymentType;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalValue;
@property (weak, nonatomic) IBOutlet UIView *viewCancelled;
@property (weak, nonatomic) IBOutlet UILabel *labelCancelledText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightSignatureImageView;

@property (weak, nonatomic) IBOutlet UILabel *labelReceiverName;
@property (weak, nonatomic) IBOutlet UILabel *labelReceiverMobile;
@property (weak, nonatomic) IBOutlet UILabel *labelReceiverEmail;
@property (weak, nonatomic) IBOutlet UILabel *labelShipmentNote;
@property (weak, nonatomic) IBOutlet UILabel *labelShipmentQuantity;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintEmailLabelHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeight3rdview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightCancelView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopAdditionalDetailTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelAddtionalDetailTitle;



@property (weak, nonatomic) IBOutlet UILabel *labelRating;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewSignature;

@property(nonatomic,strong)NSDictionary *appointmentDetails;
@property(nonatomic,strong)NSString *appointmentDate;
@property(nonatomic,strong)NSString *customerEmail;
@property(nonatomic,strong)NSString *subid;
@property bool cancelled;
@property NSInteger cancelId;

@end
