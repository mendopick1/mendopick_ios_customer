//
//  RecepientListViewController.m
//
//  Created by Rahul Sharma on 03/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "RecepientListViewController.h"
#import "RecepientsListCellTableViewCell.h"
#import "RecepientDetails.h"
#import "Database.h"
#import "AmazonTransfer.h"
#import "OrderSummaryViewController.h"
#import "LanguageManager.h"

@interface RecepientListViewController ()<UIAlertViewDelegate>
{
    RecepientDetails *recepientDetailsFromDB;
    
    NSMutableArray *recepientListArray;
    NSString *patientEmail;
    NSInteger selectedIndex;
    UIWindow *window;
    NSString *imageAdded;
    NSString *shipmentImageUrl;
    
}

//@property (nonatomic, strong) UILabel *messageLabel;

@end

@implementation RecepientListViewController

- (void)viewDidLoad {
     patientEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey];
    [super viewDidLoad];
    
    [self.viewEmptybox setHidden:YES];
    
    _tableView.estimatedRowHeight = 120.0;
    _tableView.rowHeight = UITableViewAutomaticDimension;

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    
    window = [[UIApplication sharedApplication] keyWindow];
    self.navigationController.navigationBarHidden = NO;
    [[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    [self createNavView];
    self.navigationItem.hidesBackButton = YES;
    recepientListArray = [[NSMutableArray alloc] initWithArray:[Database getParticularRecepientsDetails:patientEmail]];
    [_tableView reloadData];
    [self createNavLeftButton];
    [self createNavRightButton];
}

-(void)createNavView
{
    
     UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(90,-10, 160, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(15,10, 147, 30)];
    navTitle.font = [UIFont fontWithName:Hind_Medium size:17];
    navTitle.text =NSLocalizedString(@"Saved Address", @"Saved Address");
    navTitle.textColor = UIColorFromRGB(0x565656);
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;
}
-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"signin_back_icon_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"signin_back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    
  //  NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [navCancelButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 20,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    }

    
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}
//0xd53e30
-(void)createNavRightButton
{
    UIButton *navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navNextButton setFrame:CGRectMake(-6,0,44,44)];
    
    [navNextButton addTarget:self action:@selector(addButtonClickedAction:) forControlEvents:UIControlEventTouchUpInside];
    [Helper setButton:navNextButton Text:NSLocalizedString(@"Add", @"Add") WithFont:Hind_Regular FSize:17 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navNextButton setTitle:NSLocalizedString(@"Add", @"Add") forState:UIControlStateNormal];
    [navNextButton setTitle:NSLocalizedString(@"Add", @"Add") forState:UIControlStateSelected];
    [navNextButton setTitleColor:UIColorFromRGB(0xd53e30) forState:UIControlStateNormal];
    [navNextButton setTitleColor:UIColorFromRGB(0xd53e30) forState:UIControlStateHighlighted];
    
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)backButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addButtonClickedAction:(id)sender
{

    [self performSegueWithIdentifier:@"toAddRecepientVC" sender:nil];
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

}



#pragma mark -tableview Delegates-

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows;
    if([recepientListArray count] > 0)
    {
        rows = recepientListArray.count;
        [self.viewEmptybox setHidden:YES];
        [_viewHeaderTableview setHidden:NO];
    }
    else
    {
        rows = 0;
        [self.viewEmptybox setHidden:NO];
        [_viewHeaderTableview setHidden:YES];
    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *tableidentifier = @"recepientsListCell";
    
    RecepientsListCellTableViewCell *cell = (RecepientsListCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
   [cell.removeRecepientButton setTag:indexPath.row + 100];
    cell.contentView.layer.cornerRadius = 1.0f;
    cell.contentView.layer.borderColor = UIColorFromRGB(0xD2D2D2).CGColor;
  cell.nameLabel.textAlignment = NSTextAlignmentNatural;
    cell.addressLabel.textAlignment = NSTextAlignmentNatural;
    cell.phoneNumberLabel.textAlignment = NSTextAlignmentNatural;
    cell.emailLabel.textAlignment = NSTextAlignmentNatural;
    
    [Helper setCornerRadius:2.0 bordercolor:UIColorFromRGB(0xD2D2D2) withBorderWidth:0.9 toview:cell.viewContent];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    recepientDetailsFromDB =  recepientListArray[indexPath.row];
    
    cell.nameLabel.text = recepientDetailsFromDB.name;
    cell.addressLabel.text = recepientDetailsFromDB.address;
    cell.phoneNumberLabel.text = [NSString stringWithFormat:@"Ph-No:%@",recepientDetailsFromDB.phoneNumber];
        
    if(recepientDetailsFromDB.recepientEmail.length >0){
        
        cell.emailLabel.text = [NSString stringWithFormat:@"E-mail:%@",recepientDetailsFromDB.recepientEmail];
    }
    else
    {
        cell.emailLabel.text= @"";
    }
    

     return cell;

    
   
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSMutableArray *array;
    array = [[NSMutableArray alloc] initWithArray:[Database getParticularRecepientsDetails:patientEmail]];
    
    recepientDetailsFromDB  = [array objectAtIndex:indexPath.row];
    
    if (_isfromSummaryVC)
    {
         if (self.delegate && [self.delegate respondsToSelector:@selector(addresschangeClick:)]) {
             [self.delegate addresschangeClick:recepientDetailsFromDB];
         }
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else
    {
        ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
        [progressIndicator showPIOnWindow:window withMessge:NSLocalizedString(@"Saving Shipment Details...", @"Saving Shipment Details...")];

        imageAdded = [[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
    
        if([imageAdded isEqualToString:@"YES"]){

            NSString *name = [NSString stringWithFormat:@"%@%@.jpg",@"image",[Helper getCurrentTime]];
            NSString *fullImageName = [NSString stringWithFormat:@"%@/ShipmentImage/%@",@"MenDoImages",name];
            NSString *getImagePath = [[NSUserDefaults standardUserDefaults] objectForKey:@"imagePath"];
            NSData *data = UIImageJPEGRepresentation(_shipmentData[@"shipmentImage"],0.6);
            [data writeToFile:getImagePath atomically:YES];

            [AmazonTransfer upload:getImagePath
                           fileKey:fullImageName
                          toBucket:Bucket
                          mimeType:@"image/jpeg"
                     progressBlock:^(NSInteger progressSize, NSInteger expectedSize) {

                     } completionBlock:^(BOOL success, id result, NSError *error) {

                         shipmentImageUrl = [NSString stringWithFormat:@"%@%@/%@",imgLinkForAmazon,Bucket,fullImageName];
                         [self addShipmentToDataBase:recepientDetailsFromDB];

                     }];
        }

        else {
            
            shipmentImageUrl = @"";
            [self addShipmentToDataBase:recepientDetailsFromDB];
        }
}
}

-(void)addShipmentToDataBase:(RecepientDetails *)recepientDetails{
    
    NSInteger randomNumber = arc4random();
    
    NSDictionary *dict = @{
                           @"image":flStrForObj(shipmentImageUrl),
                           @"shipmentDetails":_shipmentData[@"shipmentDetails"],
                           @"weight":_shipmentData[@"shipmentWeight"],
                           @"quantity":_shipmentData[@"shipmentQuantity"],
                           @"name":flStrForObj(recepientDetails.name),
                           @"phoneNumber":flStrForObj(recepientDetails.phoneNumber),
                           @"address":flStrForObj(recepientDetails.address),
                           @"email":flStrForObj(recepientDetails.recepientEmail),
                           @"randomNumber":[NSNumber numberWithInteger:randomNumber],
                           @"landMark":flStrForObj(recepientDetails.landMark),
                           @"city":flStrForObj(recepientDetails.city),
                           @"flatNumber":flStrForObj(recepientDetails.flatNumber),
                           @"zipCode":flStrForObj(recepientDetails.zipCode),
                           @"dropLatitude":flStrForObj(recepientDetails.dropLatitude),
                           @"dropLongitude":flStrForObj(recepientDetails.dropLongitude),
                           @"shipmentvalue":flStrForObj(_shipmentData[@"shipmentvalue"])
                        };
    
    
    Database *db = [[Database alloc]init];
    [db makeDataBaseEntryForShipments:dict];
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    [self performSegueWithIdentifier:@"goToShipmentSummary" sender:nil];
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)removeRecepientsButtonAction:(id)sender {
    
    if (_isfromSummaryVC && recepientListArray.count == 1) {
       
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Add one More Recipient Details To Delete",@"Add one More Recipient Details To Delete")];
        return;
    }
    
    UIButton *mBtn = (UIButton *)sender;
    selectedIndex = mBtn.tag - 100;
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Remove Recipient?",
                                                                             @"Remove Recipient?") message:NSLocalizedString(@"Are you sure you want to remove this Recipient Detail ?","Are you sure you want to remove this Recipient Detail ?") delegate:self cancelButtonTitle:TitleNo otherButtonTitles:TitleYes, nil];
    alert.tag=40;
    [alert show];

}
#pragma mark -
#pragma mark -alert view delegates-

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 40)
    {
        if (buttonIndex == 0)
        {
            
        }
        else
        {
            recepientDetailsFromDB  = recepientListArray[selectedIndex];
            [Database DeleteRecepientDetails: recepientDetailsFromDB.randomNumber];
            recepientListArray = [[NSMutableArray alloc] initWithArray:[Database getParticularRecepientsDetails:patientEmail ]];
            [_tableView reloadData];
        }
    }
}

@end
