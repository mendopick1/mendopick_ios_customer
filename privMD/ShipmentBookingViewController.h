//
//  AcceptShipmentViewController.h
//  DeliveryPlus
//
//  Created by Rahul Sharma on 30/09/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShipmentBookingViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *acceptedLaabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;

@property (strong ,nonatomic) NSString *apointmentTime;
@property (strong ,nonatomic) NSString *additionalNotes;
@property (strong, nonatomic) NSMutableArray *arrayOfDriverEmails;
@property (strong, nonatomic) NSString *pickupAddress;
@property (strong, nonatomic) NSString *bId;
@property (strong, nonatomic) NSString *vehicleId;


- (IBAction)cancelButtonAction:(id)sender;

@end
