//
//  OrderStatusTableViewCell.h
//  MenDoPick
//
//  Created by Rahul Sharma on 27/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderStatusTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelOrderStatusTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelOrderStatusMessage;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewOrderStatus;
@property (strong, nonatomic) IBOutlet UILabel *labelTime;

@end
