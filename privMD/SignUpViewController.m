//
//  SignUpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SignUpViewController.h"
#import "TermsnConditionViewController.h"
#import "SignInViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "ViewController.h"
#import "CountryPicker.h"
#import "ConfirmNumberViewController.h"
#import "AmazonTransfer.h"
#import "CountryNameTableViewController.h"
#import "LanguageManager.h"


#define REGEX_PASSWORD_ONE_UPPERCASE @"^(?=.*[A-Z]).*$"  //Should contains one or more uppercase letters
#define REGEX_PASSWORD_ONE_LOWERCASE @"^(?=.*[a-z]).*$"  //Should contains one or more lowercase letters
#define REGEX_PASSWORD_ONE_NUMBER @"^(?=.*[0-9]).*$"  //Should contains one or more number
#define REGEX_PASSWORD_ONE_SYMBOL @"^(?=.*[!@#$%&_]).*$"  //Should contains one or more symbol


@interface SignUpViewController ()
{
    BOOL isEmailChecked;
    BOOL isEmailValid;
    CGRect imageFrame;
    BOOL isMobileNumberValid;
    BOOL isFromGuesture;
    NSString *message;
    NSString *url;
    UIWindow *window;
}
@property (assign ,nonatomic) BOOL isImageNeedsToUpload;
@property (assign ,nonatomic) BOOL isKeyboardIsShown;
@property (assign ,nonatomic) id amazonURL;

typedef enum {
    PasswordStrengthTypeWeak,
    PasswordStrengthTypeModerate,
    PasswordStrengthTypeStrong
}PasswordStrengthType;

-(int)checkPasswordStrength:(NSString *)password;

@end

@implementation SignUpViewController

@synthesize mainView, mainScrollView;
@synthesize firstNameTextField;
@synthesize emailTextField;
@synthesize passwordTextField;
@synthesize phoneNoTextField;
@synthesize helperCountry;
@synthesize helperCity;
@synthesize navNextButton;
@synthesize saveSignUpDetails;
@synthesize profileButton;
@synthesize profileImageView;
@synthesize tncCheckButton;
@synthesize isKeyboardIsShown;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    window = [[UIApplication sharedApplication] keyWindow];
    isFromGuesture = NO;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    isEmailChecked = NO;
    isEmailValid = NO;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    [self createNavLeftButton];
    
    NSDictionary *attrs = @{ NSForegroundColorAttributeName :UIColorFromRGB(0x333333),
                             };
    NSAttributedString *attrStr1 = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"I accept the", @"I accept the") attributes:attrs];
    NSDictionary *attrs2 = @{ NSForegroundColorAttributeName : UIColorFromRGB(0x4399ca),
                              };
    
    NSMutableAttributedString *title1 = [[NSMutableAttributedString alloc]initWithString:NSLocalizedString(@"Terms & Conditions.",@"Terms & Conditions.") attributes:attrs2];
    
    NSMutableAttributedString *ti = [attrStr1 mutableCopy];
    
    [ti appendAttributedString:title1];
    
    [_tncButton setAttributedTitle:ti  forState:UIControlStateNormal];
    [profileImageView layoutIfNeeded];
    profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2;
    [profileImageView setClipsToBounds:YES];
    _countryCodeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    
    NSDictionary *dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
    
    NSString *stringCountryCode = [dictionaryWithCode objectForKey:countryCode];
    
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", countryCode];
    [_countryCodeButton setTitle:[NSString stringWithFormat:@"+%@",stringCountryCode] forState:UIControlStateNormal];
    _countryCodeImageView.image = [UIImage imageNamed:imagePath];
    _labelConfirmPassword.text = LS(@"Confirm Password");
    _referalTextfield.placeholder = LS(@"Confirm Password");
    
    firstNameTextField.placeholder = LS(@"Merchant Name");
    emailTextField.placeholder = LS(@"Email");
    phoneNoTextField.placeholder = LS(@"Phone Number");
    passwordTextField.placeholder = LS(@"Password");
    
    mainScrollView.scrollEnabled = YES;
    isTnCButtonSelected = NO;
    
     [Helper setCornerRadius:3.0 bordercolor:CLEAR_COLOR withBorderWidth:0 toview:_signUpButton];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.view endEditing:YES];
    [self createNavView];
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    mainScrollView.frame = CGRectMake(0, 0, screenSize.size.width, screenSize.size.height);
    
    
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    isKeyboardIsShown = NO;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if(_isFromCountryCode == YES){
        
        [phoneNoTextField becomeFirstResponder];
    }
    
    else if(firstNameTextField.text.length == 0){
        
        [firstNameTextField becomeFirstResponder];
    }
        
    else if(emailTextField.text.length == 0){
        
        [emailTextField becomeFirstResponder];
    }
    else if(phoneNoTextField.text.length == 0){
        
        [phoneNoTextField becomeFirstResponder];
    }
    else if(passwordTextField.text.length == 0){
        
        [passwordTextField becomeFirstResponder];
    }

}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    _isFromCountryCode = NO;
    _isFromTandC = NO;
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.navigationItem.titleView  = nil;
}

-(void)createNavView
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80,-10, 160, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(10,8, 147, 30)];
    navTitle.text = NSLocalizedString(@"Profile  Details", @"Profile  Details");
    navTitle.textColor = UIColorFromRGB(0x565656);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:Hind_Medium size:17];
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;
    
}

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"signin_back_icon_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"signin_back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    
  //  NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [navCancelButton setFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x+20,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    }
    
    
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}



-(void)dismissKeyboard
{
    isFromGuesture = YES;
    [self.view endEditing:YES];
//    [firstNameTextField resignFirstResponder];
//    [emailTextField resignFirstResponder];
//    [passwordTextField resignFirstResponder];
//    [phoneNoTextField resignFirstResponder];
    
    
}

-(void)signUp
{
    
}


- (void) validatemobilePhoneNumber
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnWindow:window withMessge:NSLocalizedString(@"Validating Mobile Number...",@"Validating Mobile Number...")];
    isMobileNumberValid = NO;
    
    NSString *mobNo = [_countryCodeButton.titleLabel.text stringByAppendingString:phoneNoTextField.text];
    
    NSDictionary *params = @{
                             
                             @"ent_mobile":mobNo,
                             @"ent_user_type":@"2",
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"checkMobile"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                   
                                   if (success) { //handle success response
                                       
                                       [self validatingMobileNumberResponse:response];
                                       
                                   }
                               }];
}

-(void) validatingMobileNumberResponse:(NSDictionary *) response{
    
    if (!response)
    {
        
        [Helper showAlertWithTitle:TitleMessage Message:[response objectForKey:@"Message"]];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        
        [Helper showAlertWithTitle:TitleMessage Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            isMobileNumberValid = YES;
            [passwordTextField becomeFirstResponder];
        }
        else
        {
            phoneNoTextField.text = @"";
            [phoneNoTextField becomeFirstResponder];
            [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"This number is already registered , so try logging in or try a new number",@"This number is already registered , so try logging in or try a new number")];
            
        }
    }
    //    [self signUp];
    
}


- (void) validateEmailAndPostalCode
{
    [[ProgressIndicator sharedInstance]showPIOnWindow:window withMessge:NSLocalizedString(@"Validating Email..",@"Validating Email..")];
    
    
    NSDictionary *params = @{
                             
                             @"ent_email":emailTextField.text,
                             @"zip_code":@"560024",
                             @"ent_user_type":@"2",
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"validateEmailZip"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                    //   TELogInfo(@"response %@",response);
                                       [self validateEmailResponse:response];
                                   }
                               }];
    
    
}

-(void)validateEmailResponse :(NSDictionary *)response
{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
  //  TELogInfo(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleError message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        
        [Helper showAlertWithTitle:TitleError Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            isEmailChecked = YES;
            isEmailValid = YES;
            [phoneNoTextField becomeFirstResponder];
        }
        else
        {
            emailTextField.text = @"";
            [emailTextField becomeFirstResponder];
            [Helper showAlertWithTitle:TitleMessage Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
}


//SignupServiceCall

-(void) checkSignUp:(NSString *)accessToken
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnWindow:window withMessge:NSLocalizedString(@"Signing Up...",@"Signing Up...")];
    
    NSString *pToken = @"";
    if([[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken])
    {
        pToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken];
    }
    else
    {
        pToken = @"";
    }
    
    NSString *country = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCountry])
        country = @"";
    else
        country = [[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCountry];
    
    
    NSString *city = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity])
        city = @"";
    else
        city = [[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity];
    
    
    NSString *lat = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat])
        lat = @"";
    else
        lat = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat];
    
    NSString *lon = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong])
        lon = @"";
    else
        lon = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong];
    NSString *deviceId;
    if (IS_SIMULATOR) {
        
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    if(accessToken.length == 0)
    {
        accessToken = @"";
        
    }
    
    NSString *mobNo = [_countryCodeButton.titleLabel.text stringByAppendingString:phoneNoTextField.text];
    saveSignUpDetails = [[NSArray alloc]initWithObjects:firstNameTextField.text,emailTextField.text,mobNo,passwordTextField.text,_referalTextfield.text, nil];
    

    
    NSDictionary *params = @{
                             KDASignUpFirstName:[saveSignUpDetails objectAtIndex:0],
                             KDASignUpEmail:[saveSignUpDetails objectAtIndex:1],
                             KDASignUpMobile:[saveSignUpDetails objectAtIndex:2],
                             KDASignUpPassword:[saveSignUpDetails objectAtIndex:3],
                             KDASignUpDeviceId:deviceId,
                             KDASignUpAccessToken:accessToken,
                             KDASignUpCity:city,
                             KDASignUpCountry:country,
                             KDASignUpPushToken:pToken,
                             KDASignUpLatitude:lat,
                             KDASignUpLongitude:lon,
                             KDASignUpTandC:@"1",
                             KDASignUpPricing:@"1",
                             KDASignUpDeviceType:@"1",
                             KDASignUpReferralCode:@"",
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             };
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:params];
    [progressIndicator hideProgressIndicator];
    [self performSegueWithIdentifier:@"toConfirmNumber" sender:dict];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - TextFields

#pragma mark - TextFields
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if([textField isEqual:emailTextField]){
        isEmailChecked = NO;
    }
    else if([textField isEqual:phoneNoTextField]){
        isMobileNumberValid = NO;
    }
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    if ([textField isEqual:emailTextField]) {
        
        if ([textField.text length] > 0) {
            
            if ([Helper emailValidationCheck:emailTextField.text]) {
                
                [self validateEmailAndPostalCode];
                
            }
            else{
                [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Invalid Email Id",@"Invalid Email Id")];
                emailTextField.text = @"";
                [emailTextField becomeFirstResponder];
            }
        }
    }
    else if ([textField isEqual:phoneNoTextField]) {
        
        if ([textField.text length] > 0) {
            
            [self validatemobilePhoneNumber];
            
        }
        
    }
    else if ([textField isEqual:passwordTextField]) {
        
        if(textField.text.length >0){
            
            if([self checkPasswordStrength:passwordTextField.text] == 0){
                
                passwordTextField.text = @"";
                [_referalTextfield becomeFirstResponder];
                
            }
        }
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    if(textField == firstNameTextField)
    {
        [emailTextField becomeFirstResponder];
    }
    else if (textField == emailTextField)
    {
        [phoneNoTextField becomeFirstResponder];
    }

    else if (textField == phoneNoTextField)
    {
        [passwordTextField becomeFirstResponder];
    }
    else if (textField == passwordTextField)
    {
        [_referalTextfield becomeFirstResponder];
    }
    else
    {
        [_referalTextfield resignFirstResponder];
        
    }
    
    return YES;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"toConfirmNumber"])
    {
        ConfirmNumberViewController *cnVC = (ConfirmNumberViewController*)[segue destinationViewController];
        cnVC.pickedImage = _pickedImage;
        cnVC.signUpDetails = sender;
        
    }
}

- (void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)checkButtonClicked:(id)sender
{
    UIButton *mBut = (UIButton *)sender;
    mBut.userInteractionEnabled = YES;
    
    if(mBut.isSelected)
    {
        isTnCButtonSelected = NO;
        mBut.selected=NO;
        [tncCheckButton setImage:[UIImage imageNamed:@"signup_check_mark_icon_off"] forState:UIControlStateNormal];
        
    }
    else
    {
        isTnCButtonSelected = YES;
        mBut.selected=YES;
        [tncCheckButton setImage:[UIImage imageNamed:@"signup_check_mark_icon_on"] forState:UIControlStateSelected];
        
    }
}

- (IBAction)signUpButtonClickedAction:(id)sender {
    
    [self dismissKeyboard];
    
    NSString *signupFirstName = firstNameTextField.text;
    NSString *signupEmail = emailTextField.text;
    NSString *signupPassword = passwordTextField.text;
    NSString *confirmPassword = _referalTextfield.text;
    NSString *signupPhoneno = phoneNoTextField.text;
    
    
    if ((unsigned long)signupFirstName.length == 0)
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Enter Name", @"Enter Name")];
        [firstNameTextField becomeFirstResponder];
    }
    else if ((unsigned long)signupEmail.length == 0)
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Enter Email ID",@"Enter Email ID")];
        [emailTextField becomeFirstResponder];
    }
    else if(isEmailChecked == NO){
        
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Email is Not Validated",@"Email is Not Validated")];
        [emailTextField becomeFirstResponder];
    }
    else if ((unsigned long)signupPhoneno.length == 0)
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Enter PhoneNo",@"Enter PhoneNo")];
        [phoneNoTextField becomeFirstResponder];
    }
    else if(isMobileNumberValid == NO){
        
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Phone Number is Not Validated",@"Phone Number is Not Validated")];
        [emailTextField becomeFirstResponder];
    }
    
    else if ((unsigned long)signupPassword.length == 0)
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Enter Password",@"Enter Password")];
        [passwordTextField becomeFirstResponder];
    }
    else if ((unsigned long)confirmPassword.length == 0)
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Enter Confirm Password",@"Enter Confirm Password")];
        [passwordTextField becomeFirstResponder];
    }
    else if (![signupPassword isEqualToString:confirmPassword])
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"It seems password are not same",@"It seems password are not same")];
        [passwordTextField becomeFirstResponder];
    }
    else if (!isTnCButtonSelected)
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Please select our Terms and Condition",@"Please select our Terms and Condition")];
    }
    else{
        
        [self checkSignUp:@""];
    }
    
}




- (IBAction)profileButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    UIActionSheet *actionSheet;
    if (_pickedImage != nil) {
        
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture")
                                                  delegate:self
                                         cancelButtonTitle:TitleCancel
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:TitlePhoto,
                       TitlePhotoLibrary,
                       TitleRemovePhoto,nil];
        actionSheet.tag = 200;
    }
    else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture")
                                                  delegate:self
                                         cancelButtonTitle:TitleCancel
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:TitlePhoto,
                       TitlePhotoLibrary,nil];
        
        actionSheet.tag = 100;
    }
    
    
    [actionSheet showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked];
                break;
            }
            default:
                break;
        }
    }
    else  if (actionSheet.tag == 200) {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked];
                break;
            }
            case 2:
            {
                [self deSelectProfileImage];
                break;
            }
                
            default:
                break;
        }
        
        
    }
    
}

-(void)deSelectProfileImage {
    
    profileImageView.image =  [UIImage imageNamed:@"signup_profile_default_image_circle"];;
    _pickedImage = nil;
    _outerImageview.hidden = NO;
    profileImageView.layer.borderWidth=0.0;
    profileImageView.layer.masksToBounds = YES;
    
}

-(void)cameraButtonClicked
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TitleMessage message: NSLocalizedString(@"Camera is not available", @"Camera is not available") delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)libraryButtonClicked
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    _pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    _pickedImage = [self imageWithImage:_pickedImage scaledToSize:CGSizeMake(90, 90)];
    profileImageView.image = _pickedImage;
    
   // profileImageView.frame = CGRectMake(121, 7, 78, 78);
    
    _outerImageview.hidden = YES;
    profileImageView.layer.borderWidth=2.5;
    profileImageView.layer.masksToBounds = YES;
    profileImageView.layer.borderColor=[UIColorFromRGB(0xef4836)CGColor];
    
    
    
    
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark - Password Checking

-(int)checkPasswordStrength:(NSString *)password
{
    unsigned long int  len = password.length;
    //will contains password strength
    int strength = 0;
    
    if (len == 0) {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Please enter password first", @"Please enter password first")];
        
        return 0;//PasswordStrengthTypeWeak;
    } else if (len <= 5) {
        strength++;
    } else if (len <= 10) {
        strength += 2;
    } else{
        strength += 3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Password should contain atleast one uppercase & one lowercase alphabet & one number!", @"Password should contain atleast one uppercase & one lowercase alphabet & one number!")];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Password should contain atleast one uppercase & one lowercase alphabet & one number!", @"Password should contain atleast one uppercase & one lowercase alphabet & one number!")];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Password should contain atleast one uppercase & one lowercase alphabet & one number!", @"Password should contain atleast one uppercase & one lowercase alphabet & one number!")];
        // [passwordTextField becomeFirstResponder];
        return 0;
    }
    return 1;
}

// Validate the input string with the given pattern and
// return the result as a boolean
- (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    //NSLog(@"test range %ld",textRange);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = 0;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = 1;
    
    return didValidate;
}

///**
// *  KeyBoard Methods
// */

- (void)keyboardWillHide:(__unused NSNotification *)inputViewNotification {
    
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.mainScrollView.scrollIndicatorInsets = ei;
        self.mainScrollView.contentInset = ei;
        self.mainScrollView.contentOffset = CGPointMake(0, 0);
    }];
    
}

- (void)keyboardWillShow:(__unused NSNotification *)inputViewNotification {
    
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.mainScrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.mainScrollView.scrollIndicatorInsets = ei;
    self.mainScrollView.contentInset = ei;
}

- (IBAction)TermsNconButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"gotoTerms" sender:self];
}

- (IBAction)countryCodeButtonClickedAction:(id)sender {
    
    CountryNameTableViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"countryPicker"];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    pickController.oncomplete = ^(NSString * code, UIImage *flagimg, NSString *countryName)
    {
        self.countryCodeImageView.image = flagimg;
        NSString *countryCode = [NSString stringWithFormat:@"+%@", code];
        [Helper setButton:self.countryCodeButton Text:countryCode WithFont:OpenSans_SemiBold FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        _isFromCountryCode = YES;
    };
    [self presentViewController:navBar animated:YES completion:nil];
    
}

@end


