//
//  BookingListViewController.m
//  MenDoPick
//
//  Created by Rahul Sharma on 18/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "BookingListViewController.h"
#import "BookingListTableViewCell.h"
#import "CustomNavigationBar.h"
#import "XDKAirMenuController.h"
#import "ShipmentAddViewController.h"
#import "PatientViewController.h"
#import "OrderSummaryViewController.h"
#import "Database.h"
#import "LanguageManager.h"
#import "AnimationsWrapperClass.h"
#import "PaymentViewController.h"
#import "RateViewController.h"
#import "Update&SubscribeToPubNub.h"
#import "LiveBookingStatusUpdatingClass.h"

@interface BookingListViewController ()<UITableViewDelegate,UITableViewDataSource,CustomNavigationBarDelegate,updateAndSubscribeToPubNubDelegate>
{
    NSArray *shipmentDetails;
    NSString * bookingId;
    NSString *netReceivebleAmount;
    UILabel *actualMoney;
    
    Update_SubscribeToPubNub *Update_SubscribeToPubNubSharedObject;
    LiveBookingStatusUpdatingClass *liveBookingStatusUpdateVC;

}
@property(nonatomic,strong) CustomNavigationBar *customNavigationBarView;

@end

@implementation BookingListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addCustomNavigationBar];
    shipmentDetails = [[NSArray alloc] init];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orderSummary)
                                                 name:BOOKINGCONFIRMED object:nil];
    
    [self.navigationController setNavigationBarHidden:YES];
    liveBookingStatusUpdateVC = [LiveBookingStatusUpdatingClass sharedInstance];
    [self orderSummary];
    [self sendRequestForReviewStatus];
    [self subscribePubNub];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self unSubscribePubNub];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BOOKINGCONFIRMED object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
    
-(void)subscribePubNub {
        
  Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
  [Update_SubscribeToPubNubSharedObject subscribeToPassengerChannel];
   Update_SubscribeToPubNubSharedObject.delegate = self;
}
    
-(void)unSubscribePubNub
{
   // Update_SubscribeToPubNubSharedObject.delegate = nil;
}

#pragma mark - CustomNavigationBar -

- (void) addCustomNavigationBar {
    
    if(!_customNavigationBarView)
    {
        _customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
        _customNavigationBarView.tag = 78;
        _customNavigationBarView.delegate = self;
        [_customNavigationBarView addTitleButton];
        UILabel *bglabel = [[UILabel alloc]initWithFrame:CGRectMake(0, _customNavigationBarView.frame.size.height-1, _customNavigationBarView.frame.size.width, 1)];
        
        // right text label
        
        UILabel *typePayment;
        
        UIButton *PaymentBtn;
        
        
        typePayment = [[UILabel alloc]initWithFrame:CGRectMake(_customNavigationBarView.frame.size.width-120, 22, 100, 23)];
        [Helper setToLabel:typePayment Text:LS(@"Wallet") WithFont:Hind_Regular FSize:16 Color:UIColorFromRGB(0x646464)];
        
        PaymentBtn = [[UIButton alloc] initWithFrame:CGRectMake(_customNavigationBarView.frame.size.width-70, 0, 50, 55)];
        
        [PaymentBtn addTarget:self action:@selector(walletBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        actualMoney = [[UILabel alloc]initWithFrame:CGRectMake(_customNavigationBarView.frame.size.width-120, _customNavigationBarView.frame.size.height-25, 100, 23)];
        
        [Helper setToLabel:actualMoney Text:[PatientGetLocalCurrency getCurrencyLocal:0.0] WithFont:Hind_Regular FSize:16 Color:UIColorFromRGB(0xd53e30)];
        
        
        typePayment.textAlignment = NSTextAlignmentRight;
        actualMoney.textAlignment = NSTextAlignmentRight;
        
        if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
            
            typePayment = [[UILabel alloc]initWithFrame:CGRectMake(_customNavigationBarView.frame.origin.x+20, 22, 40, 23)];
            [Helper setToLabel:typePayment Text:LS(@"Wallet") WithFont:Hind_Regular FSize:16 Color:UIColorFromRGB(0x646464)];
            
            actualMoney = [[UILabel alloc]initWithFrame:CGRectMake(_customNavigationBarView.frame.origin.x+20, _customNavigationBarView.frame.size.height-25, 100, 23)];
            
             PaymentBtn.frame = CGRectMake(_customNavigationBarView.frame.origin.x, 0, 55, 55);
           //PaymentBtn.backgroundColor = [UIColor redColor];
            
            typePayment.textAlignment = NSTextAlignmentLeft;
            actualMoney.textAlignment = NSTextAlignmentLeft;

        }
        
        [_customNavigationBarView addSubview:PaymentBtn];
        [_customNavigationBarView addSubview:bglabel];
        [_customNavigationBarView addSubview:typePayment];
        [_customNavigationBarView addSubview:actualMoney];
        bglabel.backgroundColor = UIColorFromRGB(0xD2D2D2);
        
        [_customNavigationBarView.titleImageButton setCenter:CGPointMake(_customNavigationBarView.frame.size.width/2, _customNavigationBarView.frame.size.height/2+10)];
        
        [_customNavigationBarView removeRightBarButton];
        [self.view addSubview:_customNavigationBarView];
    }
    
    else
    {
        [_customNavigationBarView setTitle:@""];
        [_customNavigationBarView setRightBarButtonTitle:@""];
        [_customNavigationBarView setLeftBarButtonTitle:@""];
        [_customNavigationBarView hideTitleButton:NO];
        [_customNavigationBarView hideLeftMenuButton:NO];
        [_customNavigationBarView hideRightBarButton:NO];
        [_customNavigationBarView removeRightBarButton];
    }
}

-(void)walletBtnAction:(id)sender
{
    PaymentViewController *walletVC = [self.storyboard instantiateViewControllerWithIdentifier:@"paymentView"];
    
    walletVC.isComingBookingList = YES;
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController pushViewController:walletVC animated:NO];
}

#pragma mark - Network Calls -

-(void) orderSummary
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading Order Details....", @"Loading Order Details....")];
    _tableviewBookinglist.hidden = YES;
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceId;
    
    if (IS_SIMULATOR)
    {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM"];
    NSString *months = [df stringFromDate:[NSDate date]];
    NSDictionary *params =  @{
                              @"ent_sess_token":flStrForObj(sessionToken),
                              @"ent_dev_id": flStrForObj(deviceId),
                              @"ent_date_time":flStrForObj([Helper getCurrentDateTime]),
                              @"ent_appnt_dt":flStrForObj(months),
                              @"ent_page_index":@"0"
                              };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getSlaveAppointmentsCurrent" paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 //handle success response
                                 [self orderSummaryResponse:response];
                             }
                             else if(response == nil)
                             {
                             }
                         }];
}

-(void)orderSummaryResponse:(NSDictionary *)dictionary
{
    shipmentDetails = dictionary[@"appointments"];
    netReceivebleAmount = dictionary[@"netreceivable"];
    if (netReceivebleAmount == nil || [netReceivebleAmount isEqualToString:@""]) {
        
        netReceivebleAmount = @"0";
    }
    
    actualMoney.text = [PatientGetLocalCurrency getCurrencyLocal:[netReceivebleAmount floatValue]];
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if(!dictionary)
    {
        return;
    }
    else if ([dictionary[@"errFlag"] intValue] == 1 && ([dictionary[@"errNum"] intValue] == 96 || [dictionary[@"errNum"] intValue] == 7)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:@"Message" Message:[dictionary objectForKey:@"errMsg"]];
    }
    
    else if ([dictionary[@"errFlag"] intValue] == 1 && [dictionary[@"errNum"] intValue] == 65)
    {
        
    }
    else if (shipmentDetails.count == 0)
    {
        _tableviewBookinglist.hidden = YES;
        _viewEmptyBox.hidden = NO;
    }
    else if ([[dictionary objectForKey:@"errFlag"]integerValue] == 0 && shipmentDetails.count > 0)
    {
        _tableviewBookinglist.hidden = NO;
        _viewEmptyBox.hidden = YES;
        [_tableviewBookinglist reloadData];
    }
}

#pragma mark - tableview Delegates and Datasource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return shipmentDetails.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *tableidentifier = @"BookingListTableViewCell";
    BookingListTableViewCell *cell = (BookingListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell loadCellData:shipmentDetails[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderSummaryViewController *sdVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderSummaryViewController"];
    sdVC.isFromBookingList = YES;
    sdVC.shipmentDetailsBookingList = shipmentDetails[indexPath.row];
    sdVC.bid = [ shipmentDetails[indexPath.row][@"bookingId"] integerValue];
    sdVC.apptDate = shipmentDetails[indexPath.row][@"appointment_datetime"];
    sdVC.jobStatus = [shipmentDetails[indexPath.row][@"status"] integerValue];
    [self.navigationController pushViewController:sdVC animated:YES];
}

#pragma mark - uibutton Action -

- (IBAction)newOrderBtnAction:(id)sender
{
    [self performSegueWithIdentifier:@"goToMap" sender:nil];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    [self menuButtonPressed:sender];
}

- (IBAction)menuButtonPressed:(id)sender
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

#pragma mark - Rating Popop - 


-(void) sendRequestForReviewStatus {
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"appointmnet_Reviwed_Ack"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       //  TELogInfo(@"response %@",response);
                                       [self parseToGetReviewNotDoneDetails:response];
                                   }
                               }];
}




-(void)parseToGetReviewNotDoneDetails:(NSDictionary *)response{
    
    if(!response){
        return;
    }
    else  if ([[response objectForKey:@"errFlag"] integerValue] == 1) {
        
        
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 7)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:@"Message" Message:[response objectForKey:@"errMsg"]];
        
    }
    else  if ([response[@"errFlag"] integerValue] == 0 && [response[@"errNum"] integerValue] == 227 ) {
        
        RateViewController *invoiceVC = [self.storyboard instantiateViewControllerWithIdentifier:@"rateVC"];
        invoiceVC.customerEmail = response[@"data"][@"email"];
        invoiceVC.appointmentDate = response[@"data"][@"appointment_dt"];
        self.definesPresentationContext = YES;
        invoiceVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
        invoiceVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        // invoiceVC.view.superview.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height-30); //it's important to do this after presentModalViewController
        invoiceVC.view.superview.center = self.view.center;
        
        [self presentViewController:invoiceVC animated:YES completion:nil];
    }
}

#pragma - mark Pubnub Delegate -


-(void)recievedMessageWithDictionary:(NSDictionary *)messageDict OnChannel:(NSString *)channelName
    {
        if (![messageDict isKindOfClass:[NSDictionary class]]) {
            
            return;
        }
        if ([messageDict[@"a"] intValue] == 6 ||[messageDict[@"a"] intValue] == 7||[messageDict[@"a"] intValue] == 8 || [messageDict[@"a"] intValue] == 9 ||  [messageDict[@"a"] intValue] == 21 ||[messageDict[@"a"] intValue] == 22 || [messageDict[@"a"] intValue] == 5 || [messageDict[@"a"] intValue] == 11 )
        {
            
            [liveBookingStatusUpdateVC updateLiveBookingStatusAndShowVC:messageDict];
            
        }
    }

@end
