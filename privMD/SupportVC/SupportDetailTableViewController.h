//
//  SupportDetailTableViewController.h
//  DallasPoshPassenger
//
//  Created by Rahul Sharma on 12/05/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportDetailTableViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property(strong,nonatomic) NSMutableArray *detailsArray;
@property(strong,nonatomic) NSString *titleNav;
@property (weak, nonatomic) IBOutlet UITableView *supportDetailsTable;


@end
