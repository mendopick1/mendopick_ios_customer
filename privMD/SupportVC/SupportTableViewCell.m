//
//  SupportTableViewCell.m
//  DallasPoshPassenger
//
//  Created by Rahul Sharma on 14/05/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "SupportTableViewCell.h"

@implementation SupportTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    // Configure the view for the selected state
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        //Section 1
//        if (!_cellBgImage) {
//            _cellBgImage =  [[UIImageView alloc] initWithFrame: CGRectMake(0,0,[[UIScreen mainScreen] bounds].size.width-40,49)];
//            [self.contentView addSubview:_cellBgImage];
//            
//        }
        if(!_titleLabel)
        {
            _titleLabel =  [[UILabel alloc] initWithFrame: CGRectMake(0,0,[[UIScreen mainScreen] bounds].size.width-45,51)];
            [Helper setToLabel:_titleLabel Text:@"" WithFont:Hind_Medium FSize:13 Color:UIColorFromRGB(0x333333)];
            [self.contentView addSubview:_titleLabel];
        }

//        if (!_accessoryImage)
//        {
//            _accessoryImage =  [[UIImageView alloc] initWithFrame: CGRectMake(270,21,10,12)];
//            [self.contentView addSubview:_accessoryImage];
//        }
        
    }
    return self;
}

@end
