//
//  AboutViewController.m
//  UBER
//
//  Created by Rahul Sharma on 21/05/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.


#import "AboutViewController.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "WebViewController.h"
#import "LanguageManager.h"

@interface AboutViewController ()<CustomNavigationBarDelegate>

@end

@implementation AboutViewController
@synthesize topView;
@synthesize topViewevery1Label;
@synthesize topviewroadyoLabel;
@synthesize likeButton;
@synthesize legalButton;
@synthesize rateButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    [self addCustomNavigationBar];
    [Helper setToLabel:topViewevery1Label Text:NSLocalizedString(@"'On-demand delivery and courier services'", @"'On-demand delivery and courier services'") WithFont:OpenSans_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
    [Helper setButton:topviewroadyoLabel Text:websiteLabel WithFont:OpenSans_Regular FSize:11 TitleColor:UIColorFromRGB(0x3399ff) ShadowColor:nil];

    [Helper setButton:rateButton Text:NSLocalizedString(@"RATE US IN THE APP STORE", @"RATE US IN THE APP STORE") WithFont:OpenSans_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    rateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    rateButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [rateButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    [Helper setButton:likeButton Text:NSLocalizedString(@"LIKE US ON FACEBOOK", @"LIKE US ON FACEBOOK") WithFont:OpenSans_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    likeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    likeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [likeButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    [Helper setButton:legalButton Text:NSLocalizedString(@"LEGAL", @"LEGAL") WithFont:OpenSans_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    legalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    if ([[LanguageManager currentLanguageString]isEqualToString:ArabicLang])
    {
        legalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        likeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        rateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    
    legalButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [legalButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Custom Methods -

- (void) addCustomNavigationBar{

    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    customNavigationBarView.tag = 78;
    UILabel *bglabel = [[UILabel alloc]initWithFrame:CGRectMake(0, customNavigationBarView.frame.size.height-1, customNavigationBarView.frame.size.width, 1)];
    [customNavigationBarView addSubview:bglabel];
    bglabel.backgroundColor = UIColorFromRGB(0xcccccc);
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"About", @"About")];
    [self.view addSubview:customNavigationBarView];
    
}
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

- (void)menuButtonPressedAccount
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

- (IBAction)rateButtonClicked:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:itunesURL]];
}

- (IBAction)likeonFBButtonClicked:(id)sender {
    
    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:@"webView"];
    webView.title = NSLocalizedString(@"LIKE", @"LIKE");
    webView.weburl = facebookURL;
    [self.navigationController pushViewController:webView animated:YES];
}

- (IBAction)legalButtonClicked:(id)sender {
    
    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:@"webView"];
    webView.title = NSLocalizedString(@"LEGAL", @"LEGAL");
    webView.weburl = privacyPolicy;
    [self.navigationController pushViewController:webView animated:YES];
}

- (IBAction) webButtonClicked:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:websiteURL]];
}

@end
