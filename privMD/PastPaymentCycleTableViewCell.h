//
//  PastPaymentCycleTableViewCell.h
//  MenDoPick
//
//  Created by Rahul Sharma on 17/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PastPaymentCycleTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UILabel *labelNetReceivableAmount;
@property (weak, nonatomic) IBOutlet UILabel *labelPaidOutBalance;
@property (weak, nonatomic) IBOutlet UILabel *labelPaidDate;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;

-(void)setUpData:(NSDictionary *)dict;

@end
