//
//  PatientAppDelegate.m
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PatientAppDelegate.h"
#import "HelpViewController.h"
#import "PatientViewController.h"
#import "MapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "AppointedDoctor.h"
#import "InvoiceViewController.h"
#import "PMDReachabilityWrapper.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <AudioToolbox/AudioToolbox.h>
#import "XDKAirMenuController.h"
#import "Flurry.h"
#import "RateViewController.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "NetworkStatusShowingView.h"
#import "AmazonTransfer.h"
#import "Update&SubscribeToPubNub.h"
#import "OnTheWayViewController.h"
#import "NewOrderViewController.h"
#import "LiveBookingStatusUpdatingClass.h"
#import "Database.h"
#import "LiveBookingTable.h"
#import "CurrentOrderViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface PatientAppDelegate()<UIAlertViewDelegate>{
    
    NSDictionary *pushDictionary;
    LiveBookingStatusUpdatingClass *liveBookingStatusUpdate;
    LiveBookingTable *liveBookingFromDB;
    NSArray *liveBookingArray;
    NSString *shipmentId;
    UIStoryboard *mainstoryboard;
    UINavigationController *naviVC;
    NSArray *controllerArray;
    NSTimer *timer;
    
}

@property (nonatomic,strong) HelpViewController *helpViewController;
@property (nonatomic,strong) PatientViewController *splashViewController;

@end

@implementation PatientAppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

void uncaughtExceptionHandler(NSException *exception) {
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [GMSServices provideAPIKey:kPMDGoogleMapsAPIKey];
    [self setupAppearance];
    
        if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken])
        {
            Update_SubscribeToPubNub *object = [Update_SubscribeToPubNub sharedInstance];
            [object subscribeToPassengerChannel];
        }
    
    
    [AmazonTransfer setConfigurationWithRegion:AWSRegionEUCentral1
                                     accessKey:AmazonAccessKey
                                     secretKey:AmazonSecretKey];
    //save device id
    NSString *uuidSting =  [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [[NSUserDefaults standardUserDefaults] setObject:uuidSting forKey:kPMDDeviceIdKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    //  Replace YOUR_API_KEY with the api key in the downloaded package
    [Flurry startSession:kPMDFlurryId];
    
    [self handlePushNotificationWithLaunchOption:launchOptions];
    
    
    [Fabric with:@[[Crashlytics class]]];
    //Network Check
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.reachabilityManager startMonitoring];
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         switch (status)
         {
             case AFNetworkReachabilityStatusReachableViaWWAN:
             case AFNetworkReachabilityStatusReachableViaWiFi:
             {
                 [NetworkStatusShowingView removeViewShowingNetworkStatus];
             }
                 break;
             case AFNetworkReachabilityStatusNotReachable:
             default:
             {
                 [NetworkStatusShowingView sharedInstance];
             }
                 break;
         }
     }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self handleNotificationForUserInfo:userInfo];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];

//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:dt delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alertView show];
    
    
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"])
    {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:dt forKey:KDAgetPushToken];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:dt forKey:KDAgetPushToken];
        //for testing
    }
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:KDAgetPushToken];
    }
}

-(void)applicationWillEnterForeground:(UIApplication *)application
{
}

-(void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLocationServicesChangedNameKey object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationToSubscribe" object:nil userInfo:nil];
    // Use Reachability to monitor connectivity notificationToSubscribe
    PMDReachabilityWrapper *reachablity = [PMDReachabilityWrapper sharedInstance];
    [reachablity monitorReachability];
    
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"STATUS - Application will Resign Active");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationToUnSubscribe" object:nil userInfo:nil];
    
}



- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  //  TELogInfo(@"applicationWillTerminate");
    
    NSUserDefaults *ud =  [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:@"isFromHomeVC"];
    [ud removeObjectForKey:@"isFromShipmentDetailVC"];
    [ud removeObjectForKey:@"isFromMyOrder"];
    [ud removeObjectForKey:@"isFromOntheWayVC"];
    [ud removeObjectForKey:kNSUPassengerBookingStatusKey];
    [ud synchronize];
    
    [self saveContext];
}

#pragma mark - HandlePushNotification
/**
 *  handle push if app is opened by clicking push
 *
 *  @param launchOptions pushpayload dictionary
 */
-(void)handlePushNotificationWithLaunchOption:(NSDictionary*)launchOptions {
    
  //  TELogInfo(@"handle push");
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotificationPayload) {
        
        //check if user is logged in
        if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken])
        {
            [self handleNotificationForUserInfo:remoteNotificationPayload];
            
        }
    }
}

-(void)playNotificationSound{
    
    //play sound
    SystemSoundID	pewPewSound;
    NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"sms-received" ofType:@"wav"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pewPewURL, & pewPewSound);
    AudioServicesPlaySystemSound(pewPewSound);
}



-(void)handleNotificationForUserInfo:(NSDictionary*)userInfo{
    
  //  TELogInfo(@"Push Message %@ " ,userInfo);
    [self playNotificationSound];
    
        int type = [userInfo[@"aps"][@"nt"] intValue];
    
//        NSString * message = [NSString stringWithFormat:@"%@",userInfo];
    
//        UIAlertView *alertViews = [[UIAlertView alloc] initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alertViews show];

    
        if(type == 6 || type == 7|| type == 8 ||type == 21||type == 22 ||type == 10 || type == 51){
        
        liveBookingStatusUpdate = [LiveBookingStatusUpdatingClass sharedInstance];
        
        pushDictionary = userInfo;
        shipmentId =  userInfo[@"aps"][@"sub_id"];
        
        if(shipmentId == nil){
            
            shipmentId = @"1";
            
        }
        [liveBookingStatusUpdate updateLiveBookingStatusAndShowVC:userInfo[@"aps"]];
    }
    else if(type == 25 )
    {
      NSString * message = [NSString stringWithFormat:@"%@",userInfo[@"aps"][@"alert"]];
        
        UIAlertView *alertViews = [[UIAlertView alloc] initWithTitle:TitleMessage message:message delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil, nil];
        [alertViews show];
        [[NSNotificationCenter defaultCenter] postNotificationName:BOOKINGCONFIRMED object:nil userInfo:userInfo];
    }
    else if(type == 32)
    {
        
        NSString * message = [NSString stringWithFormat:@"%@",userInfo[@"aps"][@"alert"]];
        
        UIAlertView *alertViews = [[UIAlertView alloc] initWithTitle:TitleMessage message:message delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil, nil];
        [alertViews show];
        [[NSNotificationCenter defaultCenter] postNotificationName:DROPADDRESSCHANGED object:nil userInfo:userInfo];
        
    }

}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
           // TELogInfo(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark -
#pragma mark Core Data stack



// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)setupAppearance
{
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:OpenSans_SemiBold size:16], NSFontAttributeName,UIColorFromRGB(0xFFFFFF),NSForegroundColorAttributeName, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
}

#pragma mark - UIKeyboard Methods -
/**
 *   This method will called when the user wants the keyboard's
 */
- (void)keyboardWillBeHidden:(__unused NSNotification *)inputViewNotification {
    
    if (self.keyboardDelegate && [self.keyboardDelegate respondsToSelector:@selector(keyboardWillHide:)]) {
        [self.keyboardDelegate keyboardWillHide:inputViewNotification];
    }
}

- (void)keyboardWillBeShown:(__unused NSNotification *)inputViewNotification {
    
    if (self.keyboardDelegate && [self.keyboardDelegate respondsToSelector:@selector(keyboardWillShown:)]) {
        [self.keyboardDelegate keyboardWillShown:inputViewNotification];
    }
}








@end
