//
//  OntheWayViewController.m
//  Delivery_Plus
//
//  Created by Rahul Sharma on 23/10/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import "AcceptShipmentViewController.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import <MessageUI/MessageUI.h>
#import "OnTheWayViewController.h"
#import "CurrentOrderViewController.h"
#import "Database.h"

#define cancelReason1 NSLocalizedString(@"I Booked by mistake",@"I Booked by mistake")
#define cancelReason2 NSLocalizedString(@"I Changed my mind",@"I Changed my mind")
#define cancelReason3 NSLocalizedString(@"Its Taking too long",@"Its Taking too long")
#define cancelReason4 NSLocalizedString(@"Other",@"Other")

@interface AcceptShipmentViewController ()<MFMessageComposeViewControllerDelegate,UIAlertViewDelegate,UIActionSheetDelegate>
{
    UIWindow *window;
}
@end

@implementation AcceptShipmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    _labelCall.text = LS(@"CALL");
    _labelMessage.text = LS(@"MESSAGE");
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) viewWillAppear:(BOOL)animated{
    
//    [self getDriverDetail];
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
    _addressLabel.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"pickupAddress"]];
    window = [[UIApplication sharedApplication] keyWindow];

   
}

-(IBAction)callButtonClickedAction:(id)sender{
    
    if(_driverMobileNumber.length == 0){
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:TitleMessage message:NSLocalizedString(@"The driver Phone Number is not available..", @"The driver Phone Number is not available..")  delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alert show];
    }
    else{
        
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", _driverMobileNumber]];
        //check  Call Function available only in iphone
        if([[UIApplication sharedApplication] canOpenURL:telURL])
        {
            [[UIApplication sharedApplication] openURL:telURL];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:TitleAlert message:NSLocalizedString(@"This function is only available in iPhone.", @"This function is only available in iPhone.")  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }

}

-(IBAction)messageButtonClickedAction:(id)sender{
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:TitleAlert message:NSLocalizedString(@"Your device doesn't support SMS!", @"Your device doesn't support SMS!") delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    messageController.recipients = [NSArray arrayWithObject:_driverMobileNumber];
    
    if([messageController.navigationBar respondsToSelector:@selector(barTintColor)]) {
        // iOS7
        // Set Navigation Bar Title Color
        [messageController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:OpenSans_SemiBold size:14], NSFontAttributeName, [UIColor blackColor], NSForegroundColorAttributeName, nil]];
        
        // Set back button color
        
    }

    [self presentViewController:messageController animated:YES completion:nil];
    
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:TitleError message:NSLocalizedString(@"Failed to send !", @"Failed to send !") delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction)cancelButtonAction:(id)sender{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TitleAlert message:NSLocalizedString(@"Are you sure you want to Cancel this booking ?","Are you sure you want to Cancel this booking ?")delegate:self cancelButtonTitle:TitleNo otherButtonTitles:TitleYes,nil];
    alert.tag=40;
    [alert show];

    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 40)
    {
        if (buttonIndex == 0)
        {
            
        }
        else
        {
            
            [self showCancelReasons];
            
        }
    }
}

- (void)showCancelReasons {
    
    [self.view endEditing:YES];
    UIActionSheet *actionSheet;
    actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Cancel Reason", @"Select Cancel Reason")
                                              delegate:self
                                     cancelButtonTitle:TitleCancel
                                destructiveButtonTitle:nil
                                     otherButtonTitles:cancelReason1,cancelReason2,cancelReason3,cancelReason4,nil];
    
    actionSheet.tag = 100;
    [actionSheet showInView:self.view];
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self sendRequestForCancelAppointment:cancelReason1];
                break;
            }
            case 1:
            {
                [self sendRequestForCancelAppointment:cancelReason2];
                break;
            }
            case 2:
            {
                [self sendRequestForCancelAppointment:cancelReason3];
                break;
            }
            case 3:
            {
                [self sendRequestForCancelAppointment:cancelReason4];
                break;
            }
                
            default:
                break;
        }
    }
}



- (IBAction)backButtonAction:(id)sender {
       
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)sendRequestForCancelAppointment:(NSString *)cancelReason{
    
    [[ProgressIndicator sharedInstance] showPIOnWindow:window withMessge:TitleCancel];    //setup parameters
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_dri_email":_driverEmail,
                             @"ent_appnt_dt":[[NSUserDefaults standardUserDefaults]  objectForKey:@"apptDate"],
                             @"ent_date_time":[Helper getCurrentDateTime],
                             @"ent_cancel_type":cancelReason,
                             
                            };
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMCancelAppointment
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       [self getLiveCancelResponse:response];
                                   }
                                   else{
                                       
                                   }
                               }];
}

-(void)getLiveCancelResponse:(NSDictionary *)responseDictionary{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    NSString *titleMsg = TitleMessage;
    NSString *errMsg = @"";
    
    if (!responseDictionary)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleError message:[responseDictionary objectForKey: @"Message"] delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alertView show];
    }
    else if ([responseDictionary objectForKey:@"Error"])
    {
        errMsg = [responseDictionary objectForKey:@"errMsg"];
    }
    else if ([responseDictionary[@"errFlag"] intValue] == 1 && ([responseDictionary[@"errNum"] intValue] == 96 || [responseDictionary[@"errNum"] intValue] == 7)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:@"Message" Message:[responseDictionary objectForKey:@"errMsg"]];
        
    }
    else
    {
        if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0)
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
            [Database deleteAllLiveBookingDetails];
            [Database deleteAllLiveBookingShipmentDetails];
        }
        else if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 1)
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
        else
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
    }
    
    [Helper showAlertWithTitle:titleMsg Message:errMsg];

    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
