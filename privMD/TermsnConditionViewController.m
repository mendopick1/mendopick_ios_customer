//
//  TermsnConditionViewController.m
//  privMD
//
//  Created by Rahul Sharma on 27/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "TermsnConditionViewController.h"
#import "WebViewController.h"
#import "LanguageManager.h"

@interface TermsnConditionViewController ()

@end

@implementation TermsnConditionViewController
@synthesize link1Button;
@synthesize link2Button;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self createNavLeftButton];
    [self createNavView];
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.png"]];
    
    link1Button.tag = 100;
    link2Button.tag = 200;
    
    link1Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    link1Button.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [link1Button setTitle:NSLocalizedString(@"Terms & Condition", @"Terms & Condition") forState:UIControlStateNormal];
    [link1Button setTitle:NSLocalizedString(@"Terms & Condition", @"Terms & Condition") forState:UIControlStateSelected];
    [link1Button setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
    [link1Button setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateHighlighted];
    link1Button.titleLabel.font = [UIFont fontWithName:OpenSans_Regular size:15];
    
    link2Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    link2Button.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [link2Button setTitle:NSLocalizedString(@"Privacy Policy", @"Privacy Policy") forState:UIControlStateNormal];
    [link2Button setTitle:NSLocalizedString(@"Privacy Policy", @"Privacy Policy") forState:UIControlStateSelected];
    [link2Button setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
    [link2Button setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateHighlighted];
    link2Button.titleLabel.font = [UIFont fontWithName:OpenSans_Regular size:15];
    
    
    [link1Button addTarget:self action:@selector(linkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [link2Button addTarget:self action:@selector(linkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
   
}

-(void)viewWillAppear:(BOOL)animated {
    
    [self.navigationController.navigationItem setHidesBackButton:YES animated:YES];
    self.navigationController.navigationBarHidden = NO;
}

-(void)createNavView
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80,-10, 160, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(10,8, 147, 30)];
    navTitle.text = NSLocalizedString(@"Terms & Conditions", @"Terms & Conditions");
    navTitle.textColor = UIColorFromRGB(0x565656);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:Hind_Medium size:17];
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;
    
}

-(void)linkButtonClicked :(id)sender
{
    [self performSegueWithIdentifier:@"gotoWebView" sender:sender];

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"gotoWebView"])
    {
        UIButton *mBtn = (UIButton *)sender;
       
        if (mBtn.tag == 100) {
            
            WebViewController *webView = (WebViewController*)[segue destinationViewController];
            webView.termscondition = 1;
            webView.weburl = termsAndCondition;
        }
        else if (mBtn.tag == 200) {
            
            WebViewController *webView = (WebViewController*)[segue destinationViewController];
            webView.termscondition = 0;
            webView.weburl = privacyPolicy;
        }
    }
}

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"signin_back_icon_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"signin_back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    
  //  NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [navCancelButton setFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x+20,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    }
    
    
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
