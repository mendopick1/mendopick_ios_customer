//
//  PastOrderViewController.m
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 12/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "PastOrderViewController.h"
#import "InvoiceViewController.h"
#import "NewOrderViewController.h"
#import "PatientAppDelegate.h"
#import "Helper.h"
#import "BookingHistoryTableViewCell.h"

@interface PastOrderViewController ()<UITableViewDataSource, UITableViewDelegate>
{
   
    NSMutableArray *allshipmentDetails;
    NSArray *fullShipmentDetails;
    NSArray *shipmentDetailsArray;
    NSMutableArray *bookingCount;
    int apptStatus;
    NewOrderViewController *newOrder;

}
@property (nonatomic, strong) UILabel *messageLabel;

@end
static PastOrderViewController *pastVC = nil;
@implementation PastOrderViewController

+ (instancetype) getSharedInstance
{
    return pastVC;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    pastVC = self;
    [_pastOrderTableView registerNib:[UINib nibWithNibName:@"BookingHistoryTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BookingHistoryTableViewCell"];
    
    _pastOrderTableView.estimatedRowHeight = 100;
    _pastOrderTableView.rowHeight = UITableViewAutomaticDimension;

    self.pastOrderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if (_allshipmentDetails.count == 0) {
        self.pastOrderTableView.backgroundView = self.labelMessage;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)getDataFromOrderVC:(NSArray *)item
{
    fullShipmentDetails = [NSArray arrayWithArray: item];
    allshipmentDetails = [[NSMutableArray alloc]init];
    bookingCount = [[NSMutableArray alloc]init];
    NSString *count;
    
    if(fullShipmentDetails.count >0 )
    {
        for(int i = 0; i<fullShipmentDetails.count;i++){
            
            shipmentDetailsArray = [NSArray arrayWithArray:fullShipmentDetails[i][@"shipment_details"]];
            
            if(i > 0)
                count = [NSString stringWithFormat:@"%lu",(unsigned long)([shipmentDetailsArray count]+[count integerValue])];
            else
                count = [NSString stringWithFormat:@"%lu",(unsigned long)[shipmentDetailsArray count]];
            [bookingCount addObject:count];
            for(int j =0;j<shipmentDetailsArray.count;j++){
                [allshipmentDetails addObject:shipmentDetailsArray[j]];
            }
        }
    }
    
    [self.pastOrderTableView reloadData];
}

#pragma mark - Table View Data source -
/*--------------------------------------------------------------------------------------------*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section     //present in data souurce protocol
{
    return _allshipmentDetails.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = @"BookingHistoryTableViewCell";    //must be indentical
    BookingHistoryTableViewCell *cell = (BookingHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    [cell loadCellData:_allshipmentDetails[indexPath.row]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

#pragma mark - TableView delegate
/*--------------------------------------------------------------------------------------------*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    apptStatus = [_allshipmentDetails[indexPath.row][@"statCode"] intValue];
    
    if(apptStatus== kNotificationTypeBookingComplete || apptStatus== kNotificationTypeBookingCancel || apptStatus== kNotificationTypeBookingCancelled )
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(goToCompletedVc:and:)])
        {
            [_delegate goToCompletedVc:indexPath.row and:_allshipmentDetails[indexPath.row]];
        }
    }
    else
    {
        [Helper showAlertWithTitle:TitleMessage Message:LS(@"This shipment is Cancelled")];
    }
}
-(void)userSelectedRow
{
    
    
}
@end
