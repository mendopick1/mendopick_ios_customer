//
//  BookingListViewController.h
//  MenDoPick
//
//  Created by Rahul Sharma on 18/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableviewBookinglist;
@property (weak, nonatomic) IBOutlet UIView *viewEmptyBox;

@end
