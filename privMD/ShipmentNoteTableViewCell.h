//
//  ShipmentNoteTableViewCell.h
//  MenDoPick
//
//  Created by Rahul Sharma on 19/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShipmentNoteTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *txtviewShipmentDetail;

@end
