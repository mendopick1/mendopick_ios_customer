//
//  OrderSummaryViewController.h
//  MenDoPick
//
//  Created by Rahul Sharma on 21/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderSummaryViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelPickupAddress;
@property (weak, nonatomic) IBOutlet UIView *viewFirst;
@property (weak, nonatomic) IBOutlet UIView *viewSecond;
@property (weak, nonatomic) IBOutlet UILabel *labelDropoffAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelVehicleType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintVehicleDetailViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopfirstView;
@property (weak, nonatomic) IBOutlet UIView *viewVehicleDetails;


@property (weak, nonatomic) IBOutlet UILabel *labelShipmentItemName;
@property (weak, nonatomic) IBOutlet UILabel *labelWeghtDetails;
@property (weak, nonatomic) IBOutlet UILabel *labelShipmentValue;
@property (weak, nonatomic) IBOutlet UILabel *labelDeliveryFee;
@property (weak, nonatomic) IBOutlet UIButton *btnSendToCustomer;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewShipment;
@property (weak, nonatomic) IBOutlet UILabel *labelRecepientname;
@property (weak, nonatomic) IBOutlet UILabel *labelRecepientMobile;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintShipmentImageview;
@property (weak, nonatomic) IBOutlet UILabel *labelRecepientMail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightSeondview;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollviewOrderSummary;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintShipmentImageviewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTableviewHeight;
@property (weak, nonatomic) IBOutlet UITableView *tableviewOrderStatus;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintEmailHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightContentview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintScrollViewBottom;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityindicator;
@property (weak, nonatomic) IBOutlet UILabel *labelAditionalDetailTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintAdditiionalDetails;

@property BOOL isFromBookingList;
@property BOOL isFromShipmentAdd;
@property (nonatomic,strong) NSDictionary *shipmentDetailsBookingList;
@property (nonatomic) NSInteger jobStatus;
@property (nonatomic) NSInteger BookingStatus;
@property (nonatomic) NSInteger bid;

@property (nonatomic,strong) NSString *apptDate;

-(void)changeUIAccordingToStatus:(NSInteger)status;

+ (instancetype) getSharedInstance;

@end
