//
//  Update&SubscribeToPubNub.m
//  GuydeeSlave
//
//  Created by Rahul Sharma on 26/08/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "Update&SubscribeToPubNub.h"


static Update_SubscribeToPubNub *updateAndSubscribeToPubnub;

@interface Update_SubscribeToPubNub ()


//@property (strong, nonatomic) PatientPubNubWrapper *pubNub;
@property (strong, nonatomic) NSString *serverPubNubChannel;
@end

@implementation Update_SubscribeToPubNub
//@synthesize pubNub;

+(instancetype)sharedInstance
{
    if (!updateAndSubscribeToPubnub) {
        
            updateAndSubscribeToPubnub = [[self alloc] init];
            
    }
    
    return updateAndSubscribeToPubnub;
}

-(instancetype)init {
    
    if (self == [super init]) {
        
        _serverPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDPublishStreamChannel];
        
        PNConfiguration *configuration = [PNConfiguration configurationWithPublishKey:kPMDPubNubPublisherKey
                                                                         subscribeKey:kPMDPubNubSubcriptionKey];
        
        configuration.presenceHeartbeatInterval = 120;
        configuration.presenceHeartbeatValue = 30;
        configuration.uuid = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey];
        
        
        self.client = [PubNub clientWithConfiguration:configuration];


        
}
    
    return self;
}
#pragma mark - PubNub Methods

-(void)subsCribeToPubNubChannels:(NSArray *)channels{
    
    [self.client addListener:self];
    [self.client subscribeToChannels:channels withPresence:YES];

    
}

-(void)unSubsCribeToPubNubChannels:(NSArray *)channels{
    
    [self.client unsubscribeFromChannels:@[channels]
                            withPresence:NO];
    
}


-(void)subscribeToDriverChannel:(NSString *)channel
{
    [self.client subscribeToChannels:@[channel] withPresence:YES];
}


-(void)unSubsCribeToPubNubChannel:(NSString *)channel{
    
    
   
    NSString *presencechannel = flStrForObj([[NSUserDefaults standardUserDefaults]objectForKey:@"presencechannel"]);
    
    [self.client unsubscribeFromChannels:@[channel,presencechannel]
                            withPresence:NO];
    
}

/*
 
 SUBSCRIBE TO YOUR OWN CHANNEL SO THAT YOU CAN LISTEN TO SERVER DATA AND DRIVER DATA FROM PUBNUB
 
 */
-(void)subscribeToPassengerChannel {
    
    //pubnub initilization and subscribe to patient channel
    
    NSString *patientPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientPubNubChannelkey];
    NSString *presencechannel = flStrForObj([[NSUserDefaults standardUserDefaults]objectForKey:@"presencechannel"]);
    [self.client addListener:self];
    
    [self.client subscribeToChannels:@[patientPubNubChannel,presencechannel] withPresence:YES];

}

/**
 *  Stop the receiving data from pubnub server
 */
-(void)stopPubNubStream {
    

}


-(void)publishPubNubStream:(NSDictionary *)parameters {
    
    NSDictionary *message = [parameters mutableCopy];
    //[pubNub sendMessageAsDictionary:message toChannel:_serverPubNubChannel];
    
    NSError * error;
    
    [self.client publish: message
               toChannel: _serverPubNubChannel
          withCompletion:^(PNPublishStatus *status) {
              if (!status.isError) {  // Check whether request successfully completed or not.
                  NSLog(@"success publish");
              } else {
                  
                  [error localizedDescription];
                  NSLog(@"error");
                  
                  // Handle message publish error. Check 'category' property to find out possible issue
                  // because of which request did fail.
                  // Request can be resent using: [status retry];
              }
          }];

    
    
    
        
}


-(void)didFailedToConnectPubNub:(PNErrorStatus *)error {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didFailedToConnectPubNub:)]) {
        [self.delegate didFailedToConnectPubNub:error];
    }
    
}



#pragma mark - PubNub Delegate


- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message {
    
    // Handle new message stored in message.data.message
    if (message.data.actualChannel) {
        
        // Message has been received on channel group stored in
        // message.data.subscribedChannel
    }
    else {
        
        // Message has been received on channel stored in
        // message.data.subscribedChannel
    }
    NSLog(@"Received message: %@ on channel %@ at %@", message.data.message,
          message.data.subscribedChannel, message.data.timetoken);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(recievedMessageWithDictionary:OnChannel:)]) {
        
        [self.delegate recievedMessageWithDictionary:message.data.message OnChannel:message.data.subscribedChannel];
    }
}

/**
 *  subscription status
 *
 *  @param client <#client description#>
 *  @param status <#status description#>
 */

- (void)client:(PubNub *)client didReceiveStatus:(PNSubscribeStatus *)status {
    
    if (status.category == PNUnexpectedDisconnectCategory) {
        // This event happens when radio / connectivity is lost
    }
    
    else if (status.category == PNConnectedCategory) {
        
    }
    else if (status.category == PNReconnectedCategory) {
        
        // Happens as part of our regular operation. This event happens when
        // radio / connectivity is lost, then regained.
    }
    else if (status.category == PNDecryptionErrorCategory) {
        
        // Handle messsage decryption error. Probably client configured to
        // encrypt messages and on live data feed it received plain text.
    }
}



@end
