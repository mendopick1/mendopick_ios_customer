//
//  ConfirmNumberViewController.h
//  DeliveryPlus
//
//  Created by Rahul Sharma on 06/10/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmNumberViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *topMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *enterMessageLabel;
@property (weak, nonatomic) IBOutlet UITextField *numberTextfield;
@property (weak, nonatomic) IBOutlet UILabel *didnotReceiveLabel;
@property (weak, nonatomic) IBOutlet UIButton *resendSmsButton;
@property (strong ,nonatomic) NSMutableDictionary *signUpDetails;
@property (strong, nonatomic) UIImage *pickedImage;


- (IBAction)resendSmsButtonAction:(id)sender;

@end
