//
//  PaymentCycleTableViewCell.m
//  MenDoPick
//
//  Created by Rahul Sharma on 15/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "PaymentCycleTableViewCell.h"
#import "Helper.h"
#import "PatientGetLocalCurrency.h"

@implementation PaymentCycleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    [Helper setCornerRadius:4.0 bordercolor:UIColorFromRGB(0xb6b6b6) withBorderWidth:0.9 toview:_viewContent];
    [Helper makeShadowForView:_viewContent];

    _labelNetReceivableText.text = LS(@"NET RECEIVABLE");
}

-(void)setUpData:(NSDictionary *)dict
{
    
    float openingBal = [dict[@"openingbal"] floatValue];
    
    float cashEarning = [dict[@"cashEarning"] floatValue];
    
    float cardEarning = [dict[@"cardEarning"] floatValue];
    
    float pgComission = [dict[@"pgcommission"] floatValue];
    
    float netReceivable = [dict[@"netrecivable"] floatValue];
    
    NSString *cashBookingT = LS(@"Total Cash Booking");
    NSString *cardBookingT = LS(@"Total Card Booking");
    
    NSString *totalBooking = dict[@"bookings"];
    NSString *totalCashBooking = flStrForObj(dict[@"cashbooking"]);
    NSString *totalCardBooking = flStrForObj(dict[@"cardBooking"]);
    
    if (totalCashBooking==nil || [totalCashBooking isEqualToString:@""]) {
        totalCashBooking = @"0";
    }
    if (totalCardBooking==nil || [totalCardBooking isEqualToString:@""]) {
        totalCardBooking = @"0";

    }

    _labelOpmeningBalance.text = [PatientGetLocalCurrency getCurrencyLocal:openingBal];
    
    _labelNetReceivableAmount.text = [PatientGetLocalCurrency getCurrencyLocal:netReceivable];
    
    _labelTotalNetReceivableAmount.text =  [PatientGetLocalCurrency getCurrencyLocal:netReceivable];
    
    _labelCashBookingText.text = [NSString stringWithFormat:@"%@ (%@)",cashBookingT,totalCashBooking];
    
    _labelTotalCardBookingText.text = [NSString stringWithFormat:@"%@ (%@)",cardBookingT,totalCardBooking];
    
    _labelTotalCardBooking.text = [PatientGetLocalCurrency getCurrencyLocal:cardEarning];
    
    _labelCashBooking.text = [PatientGetLocalCurrency getCurrencyLocal:cashEarning];
    
    
    _labelTotalBooking.text = totalBooking;
    
    _labelPgComission.text = [PatientGetLocalCurrency getCurrencyLocal:pgComission];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
