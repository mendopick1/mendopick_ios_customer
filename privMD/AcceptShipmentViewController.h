//
//  OntheWayViewController.h
//  Delivery_Plus
//
//  Created by Rahul Sharma on 23/10/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
//
//@protocol AcceptShipmentViewDelegate <NSObject>
//
//@required
//-(void)hasUserCancelledBooking:(BOOL)status;
//
//@end

@interface AcceptShipmentViewController : UIViewController
//@property(weak,nonatomic) id<AcceptShipmentViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *messageButton;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *acceptedLaabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *labelCall;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;

@property (strong ,nonatomic) NSDictionary *shipmentDetailDictionary;
@property NSInteger shipmentNumber;
@property NSString *driverEmail;
@property NSString *driverMobileNumber;

- (IBAction)callButtonClickedAction:(id)sender;
- (IBAction)messageButtonClickedAction:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;
- (IBAction)backButtonAction:(id)sender;

@end
