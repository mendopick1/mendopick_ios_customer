//
//  InvoiceViewController.m
//  privMD
//
//  Created by Surender Rathore on 29/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "InvoiceViewController.h"
#import "NetworkHandler.h"
#import "UIImageView+WebCache.h"
#import "Entity.h"
#import <AXRatingView/AXRatingView.h>
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "CustomNavigationBar.h"
#import "LanguageManager.h"
#import "Helper.h"


#define keyPadHeight 216

@interface InvoiceViewController () <CustomNavigationBarDelegate>{
    
}

@end

@implementation InvoiceViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    [self customizeUIAndData];
}

-(void)viewWillAppear:(BOOL)animated {
    [self addCustomNavigationBar];
}

-(void)customizeUIAndData
{
    
    if ([_appointmentDetails[@"pPic"] length] > 0) {
        
        [self downloadImage:_profileImageView and:_appointmentDetails[@"pPic"] placeholder:[UIImage imageNamed:@"profile_default_image.png"]];
    
    }
    
    _pickupAddressLabel.text = _appointmentDetails[@"shipment_details"][0][@"Merchantaddress"];
    _dropAddressLabel.text = _appointmentDetails[@"shipment_details"][0][@"address"];
    
    _labelReceiverName.text = _appointmentDetails[@"shipment_details"][0][@"reciverName"];
    
    _labelReceiverMobile.text = [Helper appendToString:_appointmentDetails[@"shipment_details"][0][@"reciverMobile"]];
    
    _labelDriverName.text = _appointmentDetails[@"fname"];
    _labelReceiverEmail.text = _appointmentDetails[@"shipment_details"][0][@"email"];
    
    if([_labelReceiverEmail.text length] == 0)
    {
        _constraintEmailLabelHeight.constant = 0;
        _constraintHeight3rdview.constant = _constraintHeight3rdview.constant - 20;
        _constraintHeightContentView.constant = _constraintHeightContentView.constant - 20;
    }
    
    _labelShipmentNote.text = _appointmentDetails[@"shipment_details"][0][@"productname"];
    _labelAddtionalDetailTitle.text = LS(@"Additional Details");
    
    if ([_labelShipmentNote.text isEqualToString:@""]) {
        _labelShipmentNote.text = @"";
        _labelAddtionalDetailTitle.text = @"";
        _constraintTopAdditionalDetailTitle.constant = 0;
    }
    
    _labelShipmentQuantity.text = [NSString stringWithFormat:@"%@: %@  %@: %@",weight1,_appointmentDetails[@"shipment_details"][0][@"weight"],quantity1,_appointmentDetails[@"shipment_details"][0][@"quantity"]];
    
    _labelDeliveryFee.text = [PatientGetLocalCurrency getCurrencyLocal:[_appointmentDetails[@"shipment_details"][0][@"ApproxFare"] floatValue]];
    _labelSHipmentValue.text =  [PatientGetLocalCurrency getCurrencyLocal:[_appointmentDetails[@"shipment_details"][0][@"shipementValue"] floatValue]];
   
    _labelTotalValue.text = [PatientGetLocalCurrency getCurrencyLocal:[_appointmentDetails[@"shipment_details"][0][@"shipementValue"] floatValue] + [_appointmentDetails[@"shipment_details"][0][@"ApproxFare"] floatValue]];
    
    _labelRating.text = _appointmentDetails[@"shipment_details"][0][@"rating"];
    
    if ([_appointmentDetails[@"shipment_details"][0][@"signatureImg"] length] > 0)
    {
        [self downloadImage:_imageviewSignature and:_appointmentDetails[@"shipment_details"][0][@"signatureImg"]placeholder:nil];
    }
    
    if ([_appointmentDetails[@"shipment_details"][0][@"PaymentType"] isEqualToString:@"2"]) {
        
        _labelPaymentType.text = LS(@"Cash");
    }
    else
    {
        _labelPaymentType.text = LS(@"Card");
    }
    
    if (!_cancelled) {
        _constraintHeightCancelView.constant = 0;
        _viewCancelled.hidden = YES;
    }
    else
    {
        _constraintHeightContentView.constant = _constraintHeightContentView.constant + 25;
        
        _constraintHeight3rdview.constant = _constraintHeight3rdview.constant - _constraintHeightSignatureImageView.constant;
        _constraintHeightContentView.constant = _constraintHeightContentView.constant - _constraintHeightSignatureImageView.constant;
        
        _constraintHeightSignatureImageView.constant = 0;
        
        _imageviewSignature.hidden = YES;
        
        NSDictionary *attrs1 = @{ NSForegroundColorAttributeName : UIColorFromRGB(0xd53e30),
                                  };
        NSDictionary *attrs2 = @{ NSForegroundColorAttributeName : UIColorFromRGB(0x646464),
                                  };
        
        NSAttributedString *ca = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" (%@)",LS(@"Cancelled")] attributes:attrs1];
        
         NSMutableAttributedString *title1 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",[PatientGetLocalCurrency getCurrencyLocal:0.00]] attributes:attrs2];
        
        [title1 appendAttributedString:ca];
        
        _labelTotalValue.attributedText = title1;
        
        _labelReceiverName.text = _appointmentDetails[@"shipment_details"][0][@"name"];
        
        _labelReceiverMobile.text = [Helper appendToString:_appointmentDetails[@"shipment_details"][0][@"mobile"]];
        
        
        if (_cancelId == kNotificationTypeBookingCancel){
            _labelCancelledText.text = LS(@"Booking cancelled by merchant");
        }
        else
        {
            _labelCancelledText.text = LS(@"Booking cancelled by driver");
        }
        
    }
    
    [Helper makeCircular:_profileImageView];
}

-(void)downloadImage:(UIImageView *)image and:(NSString *)url placeholder:(UIImage *)imge
{
    [image sd_setImageWithURL:[NSURL URLWithString:[Helper removeWhiteSpaceFromURL:url]]
             placeholderImage:imge
                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                        
                        if(error == nil){
                        }
                        
                    }];
}

- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    
    [customNavigationBarView setCustomTitle:[Helper makeAttributedTitlle:_appointmentDetails[@"apntDate"]]];
    
    [customNavigationBarView hideLeftMenuButton:YES];
    
    UIImage *buttonImage = [UIImage imageNamed:@"signin_back_icon_off"];
    UIImage *buttonImage1 = [UIImage imageNamed:@"signin_back_icon_on"];
    [customNavigationBarView setleftBarButtonImage:buttonImage1 :buttonImage];
    
    UILabel *bglabel = [[UILabel alloc]initWithFrame:CGRectMake(0, customNavigationBarView.frame.size.height-1, customNavigationBarView.frame.size.width, 1)];
    [customNavigationBarView addSubview:bglabel];
    bglabel.backgroundColor = UIColorFromRGB(0xD2D2D2);

    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton*)sender {

    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)viewWillDisappear:(BOOL)animated {
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)sendRequestForAppointmentInvoice {
    
  //  ProgressIndicator *pi = [ProgressIndicator sharedInstance];
   // [pi showPIOnWindow:window withMessge:NSLocalizedString(@"Please wait..", @"Please wait..")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    

    NSString *currentDate = [Helper getCurrentDateTime];
    
    @try {
        NSDictionary *params = @{
                                 
                                 @"ent_sess_token":flStrForObj(sessionToken),
                                 @"ent_dev_id":flStrForObj(deviceID),
                                 @"ent_email":flStrForObj(_customerEmail),
                                 @"ent_user_type":flStrForObj(@"2"),
                                 @"ent_appnt_dt":flStrForObj(_appointmentDate),
                                 @"ent_date_time":flStrForObj(currentDate),
                                 
                                 };
        
        //setup request
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:kSMGetAppointmentDetial
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success) { //handle success response
                                           [self parseAppointmentDetailResponse:response];
                                       }
                                   }];
    }
    @catch (NSException *exception) {
      //  TELogInfo(@"Invoice Exception : %@",exception);
    }
    
    
}

#pragma mark - WebService Response

-(void)parseAppointmentDetailResponse:(NSDictionary*)response{
    
//    [[ProgressIndicator sharedInstance] hideProgressIndicator];
//    
//    if (response == nil) {
//        return;
//    }
//    else if ([response objectForKey:@"Error"])
//    {
//        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
//    }
//    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 81 || [response[@"errNum"] intValue] == 96 )) { //session Expired
//        
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        
//        [XDKAirMenuController relese];
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
//        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
//        [Helper showAlertWithTitle:@"Message" Message:[response objectForKey:@"errMsg"]];
//        
//    }
//    else
//    {
//        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
//        {
//            [_driverImage layoutIfNeeded];
//            _driverImage.layer.cornerRadius = _driverImage.frame.size.width/2;
//            _driverImage.clipsToBounds = YES;
//            
//            
//            NSString *curreny =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(response[@"shipment_details"][_subid.integerValue-1][@"Fare"]) floatValue]];
//            _priceLabel.text = curreny;
//            
//            if([response[@"pPic"] isEqualToString:@"aa_default_profile_pic.gif"] || [response[@"pPic"] isEqualToString:@""]){
//                _profileImageView.image = [UIImage imageNamed:@"signup_profile_default_image_circle"];
//                _driverImage.image = [UIImage imageNamed:@"signup_profile_default_image_circle"];
//            }
//            else{
//                
//                NSString *strImageUrl = [NSString stringWithFormat:@"%@",response[@"pPic"]];
//                
//                [_profileImageView sd_setImageWithURL:[NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strImageUrl]]
//                                     placeholderImage:[UIImage imageNamed:@"signup_profile_default_image_circle"]
//                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
//                                                
//                                                if(error == nil){
////                                                    _profileImageView.frame = CGRectMake(119, 54, 81, 83);
////                                                    _driverImage.frame = CGRectMake(106, 24, 79, 83);
//                                                    
//                                                    _outerImageview.hidden = YES;
//                                                    _profileImageView.layer.borderWidth=2.5;
//                                                    _profileImageView.layer.masksToBounds = YES;
//                                                    _profileImageView.layer.borderColor=[UIColorFromRGB(0xef4836)CGColor];
//                                                    _driverImage.image = image;
//                                                }
//                                                
//                }];
//                
//            }
//            
//            //Download Signature Image
//            
//            NSString *signatureImageUrl = [NSString stringWithFormat:@"%@",response[@"shipment_details"][_subid.integerValue-1][@"signatureImg"]];
//            
//            [_signatureImageView sd_setImageWithURL:[NSURL URLWithString:[Helper removeWhiteSpaceFromURL:signatureImageUrl]]
//                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
//                                         
//                                            if(image == nil){
//                                                
//                                                [Helper showAlertWithTitle:@"Message" Message:[error localizedDescription]];
//                                                
//                                            }
//
//                                            
//            }];
//
//            _profileNameLabel.text = [NSString stringWithFormat:@"%@ %@",response[@"fName"],response[@"lName"]];
//            
//            
//            _pickupAddressLabel.text = response[@"addr1"];
//            _dropAddressLabel.text = flStrForObj(response[@"shipment_details"][_subid.integerValue-1][@"address"]);
//            
//            NSArray *shipments = [[NSArray alloc]initWithArray:response[@"shipment_details"]];
//            NSInteger shipmentCounts = shipments.count;
//            
//            NSString *str = LS(@"Delivery");
//            NSString *str1 = LS(@"Deliveries");
//            
//            if( shipmentCounts == 1){
//                
//                _timeLabel.text =[NSString stringWithFormat:@"%ld %@",(long)shipmentCounts,str];
//            }
//            else{
//                _timeLabel.text =[NSString stringWithFormat:@"%ld %@",(long)shipmentCounts,str1];
//            }
//        
//            [[NSUserDefaults standardUserDefaults] setObject:response[@"apptDt"] forKey:@"acceptedTime"];
//            [[NSUserDefaults standardUserDefaults] setObject:response[@"bid"] forKey:@"bid"];
//            [[NSUserDefaults standardUserDefaults] setObject:response[@"additional_info"] forKey:@"additional_notes"];
//            [[NSUserDefaults standardUserDefaults] setObject:response[@"email"] forKey:@"driverEmail"];
//            
//            [[NSUserDefaults standardUserDefaults]synchronize];
//            
//            shipmentDetailDict = response[@"shipment_details"][_subid.integerValue-1];
//            
//            if([response[@"review"] integerValue] == 0){
//            
//                RateViewController *invoiceVC = [self.storyboard instantiateViewControllerWithIdentifier:@"rateVC"];
//                invoiceVC.customerEmail = _customerEmail;
//                invoiceVC.appointmentDate = _appointmentDate;
//                self.definesPresentationContext = YES;
//                invoiceVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
//                invoiceVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//                invoiceVC.view.superview.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height-30); //it's important to do this after presentModalViewController
//                invoiceVC.view.superview.center = self.view.center;
//                
//                [self presentViewController:invoiceVC animated:YES completion:nil];
//
//                
//            }
//            
//            
//        }
//        else
//        {
//            [Helper showAlertWithTitle:TitleMessage Message:@"errMsg"];
//        }
//        
//    }
}

-(NSString *)removeWhiteSpacetoPercentageFromURL:(NSString *)url
{
    NSMutableString *string = [[NSMutableString alloc] initWithString:url] ;
    return string;
}

-(NSString *)togetTimeDifference:(NSString *)time1 and:(NSString *)time2{
    
    NSDateFormatter *df=[[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date1 = [df dateFromString:time1];
    NSDate *date2 = [df dateFromString:time2];
    
    NSTimeInterval timeDifference = [date2 timeIntervalSinceDate:date1];
    
    
    long hours = timeDifference / 3600;
    long minutes = timeDifference/60;
    NSString *result;
    if(hours > 0){
        result = [NSString stringWithFormat:@"%ldhr %ldmin",hours,minutes];
    }
    else{
        result = [NSString stringWithFormat:@"%ld min",minutes];
    }
    return result;
}

@end
