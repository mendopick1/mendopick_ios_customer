//
//  OnTheWayViewController.m
//  DeliveryPlus
//
//  Created by Rahul Sharma on 07/11/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import "OnTheWayViewController.h"
#import "CustomNavigationBar.h"
#import "UIImageView+WebCache.h"
#import <MessageUI/MessageUI.h>
#import "DirectionService.h"
#import "InvoiceViewController.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "Database.h"
#import "LiveBookingTable.h"
#import "LiveBookingStatusUpdatingClass.h"
#import "Update&SubscribeToPubNub.h"
#import "LiveBookingShipmentDetails.h"
#import "PatientAppDelegate.h"
#import "LanguageManager.h"

@interface OnTheWayViewController ()<GMSMapViewDelegate,CustomNavigationBarDelegate,MFMessageComposeViewControllerDelegate,UIAlertViewDelegate,updateAndSubscribeToPubNubDelegate,UINavigationControllerDelegate,CLLocationManagerDelegate,UIActionSheetDelegate>
{
    
    BOOL  isPathPlotted;
  
    GMSMarker *appointmentLocationMarker;
    GMSMarker *DropoffLocationMarker;
    CLLocationCoordinate2D previouCoord;
    double pickupLatitude;
    double pickupLongitude;
    double dropLatitude;
    double dropLongitude;
    CGRect frame;
    LiveBookingTable *liveBookingDB;
    LiveBookingStatusUpdatingClass *liveBookingStatus;
    Update_SubscribeToPubNub *Update_SubscribeToPubNubSharedObject;
    LiveBookingShipmentDetails *liveBookingShipmentDB;
    NSInteger responseBid;
    NSInteger responseShipmentId;
    PatientAppDelegate *appDelegate;
    GMSMarker *marker;
    NSInteger count;
    double driverLatitude;
    double driverLongitude;
    NSUserDefaults *ud;
    CLLocationManager *clmanager;
    double srcLat;
    double srcLong;
    BOOL isplotted;
    UIImage * markerimage;
    NSString *driverChannel;
    NSString *mapIconImage;
    
    NSInteger bookingStatus;
    
}

@property(nonatomic,strong) NSArray *shipmentDetailsFromDB;
@property(nonatomic,strong) NSArray *bookingDetailFromDB;
@property (nonatomic,strong)  NSTimer *timer;
@property(nonatomic,strong) NSString *patientPubNubChannel;
@property(nonatomic,strong) NSString *serverPubNubChannel;
@property(nonatomic,strong) NSString *patientEmail;


@end

@implementation OnTheWayViewController



- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    ud = [NSUserDefaults standardUserDefaults];
    _patientEmail = [ud objectForKey:kNSUPatientEmailAddressKey];
    _patientPubNubChannel = [ud objectForKey:kNSUPatientPubNubChannelkey];
    
    liveBookingStatus = [LiveBookingStatusUpdatingClass sharedInstance];
    
    [self addCustomNavigationBar];
   ;
    [self changeViews:[_shipmentDictionary[@"statCode"] integerValue]];
    [self sendRequestToGetBookingDetails];
    bookingStatus = [_shipmentDictionary[@"statCode"] integerValue];
    
    [self getDriverDetail];
    
    _bookingDetailFromDB = [[NSArray alloc]initWithArray:[Database getLiveBookingDetails:_bookingId]];
    liveBookingDB = _bookingDetailFromDB[0];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subscribePubNub:) name:@"notificationToSubscribe" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unSubscribePubNub:) name:@"notificationToUnSubscribe" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReviewSubmitted) name:kNotificationReviewSubmitted object:nil];

}

-(void)subscribePubNub:(NSNotification *)notice
{
    [self sendRequestToGetBookingDetails];
    if(driverChannel.length>0)
    {
        NSLog(@"subscribed on the way");
        Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
        [Update_SubscribeToPubNubSharedObject subscribeToDriverChannel:driverChannel];
        Update_SubscribeToPubNubSharedObject.delegate = self;
    }
}

-(void)unSubscribePubNub:(NSNotification *)notice
{
    if(driverChannel.length>0)
    {
        Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
        [Update_SubscribeToPubNubSharedObject unSubsCribeToPubNubChannel:driverChannel];
    }
}

-(void)didReviewSubmitted
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
     [self updateUI];
    Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
    [Update_SubscribeToPubNubSharedObject subscribeToPassengerChannel];
    Update_SubscribeToPubNubSharedObject.delegate = self;
    
    if(self.navigationController.navigationBarHidden == NO)
    {
        self.navigationController.navigationBarHidden = YES;
        self.navigationItem.hidesBackButton = YES;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
   
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    CLLocation *location = [locations lastObject];
    NSString *lat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString * log = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:lat forKey:KNUCurrentLat];
    [[NSUserDefaults standardUserDefaults]setObject:log forKey:KNUCurrentLong];
    srcLat = lat.doubleValue;
    srcLong = log.doubleValue;
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [clmanager stopUpdatingLocation];
    
}

/*
 To print error msg of location manager
 @param error msg.
 @return nil.
 */

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied) {
        // The user denied your app access to location information.
        
    }
    else  if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorNetwork) {
        
    }
    else  if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied) {
        
    }
    
    //   TELogInfo(@"locationManager failed to update location : %@",[error localizedDescription]);
    
}

-(void)publishPubNubStream
{
    
    if(_timer == nil){
        
        return;
    }
    
    
    NSString *dt = @"";
    NSInteger tp = 1;
    
    NSDictionary *message = @{
                              
                              @"a": [NSNumber numberWithInt:kPubNubStartStreamAction],
                              @"pid": _patientEmail,
                              @"chn": _patientPubNubChannel,
                              @"lt": [NSNumber numberWithFloat:srcLat],
                              @"lg":[NSNumber numberWithFloat:srcLong],
                              @"st": [NSNumber numberWithInt:kAppointmentTypeNow],
                              @"tp": [NSNumber numberWithInteger:tp],
                              @"dt": dt
                              
                              };
    
    Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
    
    
    if (!(srcLat == 0 || srcLong == 0))
    {
        [Update_SubscribeToPubNubSharedObject publishPubNubStream:message];
        
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFromOntheWayVC"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self.timer invalidate];
    self.timer = nil;
    _isFromInvoice = NO;
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"driverchannel"];
    if (driverChannel.length>0)
    {
        Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
        [Update_SubscribeToPubNubSharedObject unSubsCribeToPubNubChannel:driverChannel];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationReviewSubmitted object:nil];
}


-(void)statusUpdatedResponse:(NSDictionary *)dict and:(NSString *)shipmentId{
    
    NSString *statusFromResponse;
    statusFromResponse = dict[@"nt"];
    if(statusFromResponse == nil){
        statusFromResponse = dict[@"a"];
    }
    
    
    
//    shipmentID = shipmentId;
//    
//    if (!shipmentId) {
//        shipmentID = @"1";
//    }
    
    _status = statusFromResponse.integerValue;
    
//    _shipmentDetailsFromDB = [[NSArray alloc]initWithArray:[Database getLiveBookingEachShipmentDetails:dict[@"bid"] shipmentId:shipmentID]];
    
    NSLog(@"Shipment Details:%@",_shipmentDetailsFromDB);
    liveBookingShipmentDB = _shipmentDetailsFromDB[0];
    
    
    _status = liveBookingShipmentDB.shipmentStatus.integerValue;
    
    [self changeViews:statusFromResponse.integerValue];
    
}

-(void)sendRequestToGetAppointmentDetails {
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *emailId;
    NSString *appointmentDate;
    
    if(_isFromInvoice == YES){
        
        appointmentDate = [[NSUserDefaults standardUserDefaults]  objectForKey:@"acceptedTime"];
        emailId = [[NSUserDefaults standardUserDefaults]  objectForKey:@"driverEmail"];
        
    }
    else{
        
        emailId = liveBookingDB.passengerEmail;
        appointmentDate = liveBookingDB.appointmentDate;
    }
    
    NSString *currentDate = [Helper getCurrentDateTime];
    
    @try {
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForObj(sessionToken),
                                 @"ent_dev_id":flStrForObj(deviceID),
                                 @"ent_email":flStrForObj(emailId),
                                 @"ent_user_type":flStrForObj(@"2"),
                                 @"ent_appnt_dt":flStrForObj(appointmentDate),
                                 @"ent_date_time":flStrForObj(currentDate),
                                 };
        
        //setup request
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:kSMGetAppointmentDetial
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success) { //handle success response
                                           //    TELogInfo(@"response %@",response);
                                       }
                                   }];
    }
    @catch (NSException *exception) {
    }
}

-(void)updateUI
{
    driverLatitude = 0;
    driverLongitude = 0;
    
    isPathPlotted = NO;
    
    _labelPickupAddress.text = _shipmentDictionary[@"shipment_details"][0][@"PickupAddress"];
    _labelDropoffAddress.text = _shipmentDictionary[@"shipment_details"][0][@"address"];
    _labelDriverName.text = _shipmentDictionary[@"fname"];
    
    if (_shipmentDictionary[@"fname"] == nil || [_shipmentDictionary[@"fname"] isEqualToString:@""]) {
        
        _labelDriverName.text = _shipmentDictionary[@"fName"];
    }
    
    [Helper makeCircular:_driverImageView];
    
    pickupLatitude =  [_shipmentDictionary[@"shipment_details"][0][@"PickupLatLongs"][@"lat"] doubleValue];
    pickupLongitude = [_shipmentDictionary[@"shipment_details"][0][@"PickupLatLongs"][@"log"] doubleValue];
    
    dropLatitude =  [_shipmentDictionary[@"shipment_details"][0][@"location"][@"lat"] doubleValue];
    dropLongitude = [_shipmentDictionary[@"shipment_details"][0][@"location"][@"log"] doubleValue];
    
    _onTheWayMapView.delegate = self;
    [_onTheWayMapView clear];
    [self showAppointmentLocationMarker];
    [self showDropoffLocationMarker];

}

-(void)changeViews:(NSInteger)status
{
    
    _status = status;
    switch (status) {
        case kNotificationTypeBookingAccept:
            break;
        case kNotificationTypeBookingOnMyWay:
            _onTheWayMessageLabel.text = LS(@"On the way to pickup");
            
            break;
        case kNotificationTypeBookingReachedLocation:
            _onTheWayMessageLabel.text = LS(@"Arrived at pickup location");
            
            break;
        case kNotificationTypeBookingStarted:
            _onTheWayMessageLabel.text = LS(@"On the way to deliver");
            
            break;
        case  kNotificationTypeIndividualBooking:
             _onTheWayMessageLabel.text = LS(@"Arrived at drop location");
            
            break;
            
        case kNotificationTypeBookingClosed:
            _onTheWayMessageLabel.text = LS(@"Order Completed");
            [self didReviewSubmitted];
                    
        break;
        case kNotificationTypeBookingCancelled:
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;

        default:
             [self didReviewSubmitted];
            break;
    }

}

/**
 *  Show Appointment Location Marker On Map
 */
-(void)showAppointmentLocationMarker
{
    if(!appointmentLocationMarker)
    {
        appointmentLocationMarker = [[GMSMarker alloc]init];
        appointmentLocationMarker.flat = YES;
        appointmentLocationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        appointmentLocationMarker.icon = [UIImage imageNamed:@"default_marker_p"];
        appointmentLocationMarker.map = _onTheWayMapView;
        appointmentLocationMarker.position = CLLocationCoordinate2DMake(pickupLatitude, pickupLongitude);
        GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:appointmentLocationMarker.position zoom:16];
        [_onTheWayMapView animateWithCameraUpdate:updatedCamera];

    }
}

-(void)showDropoffLocationMarker
{
    if(!DropoffLocationMarker)
    {
        DropoffLocationMarker = [[GMSMarker alloc]init];
        DropoffLocationMarker.flat = YES;
        DropoffLocationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        DropoffLocationMarker.icon = [UIImage imageNamed:@"default_marker_d"];
        DropoffLocationMarker.map = _onTheWayMapView;
        DropoffLocationMarker.position = CLLocationCoordinate2DMake(dropLatitude, dropLongitude);
        [self fitsCameraIntoMapView];
    }
}

-(void)fitsCameraIntoMapView
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:appointmentLocationMarker.position    coordinate:DropoffLocationMarker.position];
    
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:170];
    
    [_onTheWayMapView animateWithCameraUpdate:update];
    
   // [_onTheWayMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withEdgeInsets:UIEdgeInsetsMake(200, 25, 200, 25)]];
}

-(void)didFailedToConnectPubNub:(PNErrorStatus *)error
{
    
}

-(void)recievedMessageWithDictionary:(NSDictionary *)messageDictionary OnChannel:(NSString *)channelName
{
    
    NSLog(@"messageDict %@",messageDictionary);
    if ([messageDictionary[@"a"] integerValue] == 4)
    {
        [self toShowDriverLiveTracking:messageDictionary];
    }
    else if ([messageDictionary[@"a"] intValue] == 6 ||[messageDictionary[@"a"] intValue] == 7||[messageDictionary[@"a"] intValue] == 8 || [messageDictionary[@"a"] intValue] == 9 ||  [messageDictionary[@"a"] intValue] == 21 ||[messageDictionary[@"a"] intValue] == 22 ||[messageDictionary[@"a"] integerValue] == 5)
    {
        
        responseBid = [messageDictionary[@"bid"] intValue];
        
        if([messageDictionary[@"a"] intValue] == 21 || [messageDictionary[@"a"] intValue] == 22){
            responseShipmentId = [messageDictionary[@"sub_id"] intValue];
        }
        else
        {
            responseShipmentId = liveBookingShipmentDB.shipmentId.intValue;
        }
        [liveBookingStatus updateLiveBookingStatusAndShowVC:messageDictionary];
        
        if ([messageDictionary[@"bid"] isEqualToString:_bookingId]) {
            [self changeViews:[messageDictionary[@"a"] integerValue]];
        }
    }
}

-(void)toShowDriverLiveTracking:(NSDictionary *)driverDetails {
    
    if (isplotted)
    {
        CLLocationCoordinate2D position1 = CLLocationCoordinate2DMake(driverLatitude, driverLongitude);
        marker.map = _onTheWayMapView;
        driverLatitude = [driverDetails[@"lt"] doubleValue];
        driverLongitude = [driverDetails[@"lg"] doubleValue];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(driverLatitude, driverLongitude);
        CLLocationDirection heading = GMSGeometryHeading(position1, position);
        
        [self showDriver:marker];
        
        if ([_shipmentDictionary[@"statCode"] integerValue] == kNotificationTypeBookingStarted) {
            
            [self showDistance:driverLatitude and:driverLongitude with:dropLatitude :dropLongitude];
            
        }
        else
        {
             [self showDistance:driverLatitude and:driverLongitude with:pickupLatitude :pickupLongitude];
        }
        
        [CATransaction begin];
        if (heading > 60 && heading < 300)
        {
            [CATransaction setAnimationDuration:1.5];
        }
        else if (heading<=0 || heading==360)
        {
            heading = 0;
            [CATransaction setAnimationDuration:5.0];
        }
        else
        {
            [CATransaction setAnimationDuration:2.5];
        }
        
        
        
        NSLog(@"Heading value =%f", heading);
        marker.position = position;
        marker.rotation = heading;
        
        [CATransaction commit];
        count = count + 1;
        marker.map = _onTheWayMapView;
      
        marker.icon = markerimage;
    }
    else
    {
        NSString *selectedVehicleImageURL = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,mapIconImage];
        UIImageView *mapImageView = [[UIImageView alloc]init];
        [mapImageView sd_setImageWithURL:[NSURL URLWithString:selectedVehicleImageURL]
                        placeholderImage:nil
                               completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                   
                                   if(!error)
                                   {
                                       
                                       if(driverLatitude != [driverDetails[@"lt"] doubleValue] ||
                                          driverLongitude != [driverDetails[@"lg"] doubleValue]){
                                           
                                           CLLocationCoordinate2D position1 = CLLocationCoordinate2DMake(driverLatitude, driverLongitude);
                                           
                                           marker.map = _onTheWayMapView;
                                           marker.map = nil;
                                           
                                           driverLatitude = [driverDetails[@"lt"] doubleValue];
                                           driverLongitude = [driverDetails[@"lg"] doubleValue];
                                            [self showDistance:driverLatitude and:driverLongitude with:pickupLatitude :pickupLongitude];
                                           CLLocationCoordinate2D position = CLLocationCoordinate2DMake(driverLatitude, driverLongitude);
                                           CLLocationDirection heading = GMSGeometryHeading(position1, position);
                                           [CATransaction begin];
                                           [CATransaction setAnimationDuration:5.0];
                                           marker = [GMSMarker markerWithPosition:position];
                                           GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:position zoom:16];
                                           [_onTheWayMapView animateWithCameraUpdate:updatedCamera];
                                           
                                           [CATransaction commit];
                                           if (heading>0)
                                           {
                                               marker.rotation = heading;
                                               
                                           }
                                           count = count + 1;
                                           marker.map = _onTheWayMapView;
                                         
                                       }
                                       image = [self imageWithImage:image scaledToSize:[self makeSize:image.size fitInSize:CGSizeMake(35, 35)]];
                                       marker.icon = image;
                                       markerimage = [[UIImage alloc]init];
                                       markerimage  = image;
                                       isplotted = YES;
                                   }
                                   
                               }];
    }
}

- (void)showDriver:(GMSMarker *)markerr
{
    GMSVisibleRegion region = _onTheWayMapView.projection.visibleRegion;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:region];
    if (![bounds containsCoordinate:markerr.position])
    {
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:markerr.position.latitude
                                                                longitude:markerr.position.longitude
                                                                     zoom:_onTheWayMapView.camera.zoom];
        [_onTheWayMapView animateToCameraPosition: camera];
    }
}

- (CGSize)makeSize:(CGSize)originalSize fitInSize:(CGSize)boxSize
{
    float widthScale = 0;
    float heightScale = 0;
    widthScale = boxSize.width/originalSize.width;
    heightScale = boxSize.height/originalSize.height;
    float scale = MIN(widthScale, heightScale);
    CGSize newSize = CGSizeMake(originalSize.width * scale, originalSize.height * scale);
    return newSize;
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {

    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



- (void)addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView hideLeftMenuButton:YES];
    UILabel *bglabel = [[UILabel alloc]initWithFrame:CGRectMake(0, customNavigationBarView.frame.size.height-1, customNavigationBarView.frame.size.width, 1)];
    [customNavigationBarView addSubview:bglabel];
    bglabel.backgroundColor = UIColorFromRGB(0xcccccc);

    
    UIImage *buttonImage = [UIImage imageNamed:@"signin_back_icon_off"];
    UIImage *buttonImage1 = [UIImage imageNamed:@"signin_back_icon_on"];
    [customNavigationBarView setleftBarButtonImage:buttonImage1 :buttonImage];
    
    NSString *str = LS(@"Job Id");
    NSString *BID = [NSString stringWithFormat:@"%@ \n",str];

    BID = [BID stringByAppendingString:[NSString stringWithFormat:@"%@",_bookingId]];
    
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc]initWithString:BID];
    [title addAttribute: NSForegroundColorAttributeName value:UIColorFromRGB(0x565656) range:NSMakeRange(0,BID.length)];
    
    [customNavigationBarView setCustomTitle:title];
    
    [self.view addSubview:customNavigationBarView];
}



#pragma mark - alertview Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 0)
    {
        
        
    }
    else {
        
        [self showCancelReasons];
        
    }
    
}

- (void)showCancelReasons {
    
    [self.view endEditing:YES];
    UIActionSheet *actionSheet;
    actionSheet = [[UIActionSheet alloc] initWithTitle:LS(@"Select Cancel Reason")
                                              delegate:self
                                     cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                destructiveButtonTitle:nil
                                     otherButtonTitles:LS(@"I Booked by mistake"),LS(@"I Changed my mind"),LS(@"Its Taking too long"),LS(@"Other"),nil];
    
    actionSheet.tag = 100;
    [actionSheet showInView:self.view];
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self sendRequestForCancelAppointment:LS(@"I Booked by mistake")];
                break;
            }
            case 1:
            {
                [self sendRequestForCancelAppointment:LS(@"I Changed my mind")];
                break;
            }
            case 2:
            {
                [self sendRequestForCancelAppointment:LS(@"Its Taking too long")];
                break;
            }
            case 3:
            {
                [self sendRequestForCancelAppointment:LS(@"Other")];
                break;
            }
                
            default:
                break;
        }
    }
}



#pragma mark - Web Servicecalls -


-(void)getDriverDetail {
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading Driver Details....", @"Loading Driver Details....")];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    
    NSDictionary *params =  @{
                              @"ent_sess_token":flStrForObj(sessionToken),
                              @"ent_dev_id": flStrForObj(deviceId),
                              @"ent_date_time":flStrForObj([Helper getCurrentDateTime]),
                              @"ent_dri_email":flStrForObj(_shipmentDictionary[@"email"]),
                              
                              };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getMasterDetails" paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 //handle success response
                                 [self driverDetailResponse:response];
                             }
                             else
                             {
                                 
                             }
                         }];
    
}

-(void)driverDetailResponse:(NSDictionary *)response{
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if(!response)
    {
        return;
    }
    
    if ([[response objectForKey:@"Error"] length] != 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleError message:[response objectForKey:@"Error"] delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
        driverChannel = flStrForStr(response[@"mas_chan"]);
        mapIconImage = flStrForStr(response[@"MapIcon"]);
        if (driverChannel.length>0)
        {
            [ud setValue:driverChannel forKey:@"driverchannel"];
            Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
            [Update_SubscribeToPubNubSharedObject subscribeToDriverChannel:driverChannel];
            Update_SubscribeToPubNubSharedObject.delegate = self;
        }
        else if([[NSUserDefaults standardUserDefaults]objectForKey:@"driverchannel"])
        {
            driverChannel = flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:@"driverchannel"]);
            Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
            [Update_SubscribeToPubNubSharedObject subscribeToDriverChannel:driverChannel];
            Update_SubscribeToPubNubSharedObject.delegate = self;
        }
        
        _driverMobileNumber = response[@"mobile"];
        
        if([response[@"pPic"] isEqualToString:@"aa_default_profile_pic.gif"] || [response[@"pPic"] isEqualToString:@""]){
            
            _driverImageView.image = [UIImage imageNamed:@"signup_profile_default_image_circle"];
            
            
        }
        else{
            
            NSString *strImageUrl = [NSString stringWithFormat:@"%@",response[@"pPic"]];
            
            [_driverImageView sd_setImageWithURL:[NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strImageUrl]]
                                placeholderImage:[UIImage imageNamed:@"signup_profile_default_image_circle"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                           
                                           if(error == nil){
                                               
                                           }
                                           
                                       }];
        }
    }
}



-(void)sendRequestForCancelAppointment:(NSString *)cancelReason
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:@"Cancel.."];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    
    NSDictionary *params = @{
                             @"ent_sess_token":flStrForObj(sessionToken),
                             @"ent_dev_id":flStrForObj(deviceID),
                             @"ent_dri_email":flStrForObj(liveBookingDB.passengerEmail),
                             @"ent_appnt_dt":flStrForObj(liveBookingDB.appointmentDate),
                             @"ent_date_time":flStrForObj([Helper getCurrentDateTime]),
                             @"ent_cancel_type":cancelReason,
                             };
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMCancelAppointment
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) {
                                       //handle success response
                                       [self parseCancelAppointmentResponse:response];
                                       
                                   }
                               }];
}

-(void)parseCancelAppointmentResponse:(NSDictionary*)response {
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil) {
        return;
    }
    [Database deleteAllLiveBookingDetails];
    [Database deleteAllLiveBookingShipmentDetails];
    [self.navigationController popViewControllerAnimated:YES];
    [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
}


- (IBAction)currentLocationButtonClickedAction:(id)sender
{
    if (driverLatitude != 0) {
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(driverLatitude, driverLongitude);
        GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:position zoom:16];
        [_onTheWayMapView animateWithCameraUpdate:updatedCamera];
        
    }

}



- (void)addDirections:(NSDictionary *)json
{
    
    if ([json[@"routes"] count]>0)
    {
        NSDictionary *routes = [json objectForKey:@"routes"][0];
        
        NSDictionary *route = [routes objectForKey:@"overview_polyline"];
        NSString *overview_route = [route objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        [polyline setStrokeWidth:5.0];
        [polyline setStrokeColor:UIColorFromRGB(0x4AB5FE)];
        polyline.map = _onTheWayMapView;
    }
}

#pragma mark Prepare Segue
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NSDictionary *)sender
{

}

#pragma mark - UIbutton Action -

- (IBAction)callButtonAction:(id)sender {
    
    if(_driverMobileNumber.length == 0){
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Message" message:@"The driver Phone Number is not available.."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else{
        
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", _driverMobileNumber]];
        //check  Call Function available only in iphone
        if([[UIApplication sharedApplication] canOpenURL:telURL])
        {
            [[UIApplication sharedApplication] openURL:telURL];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"This function is only available in iPhone."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    
}

- (IBAction)messageButtonAction:(id)sender {
    
    if(![MFMessageComposeViewController canSendText]) {
        
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT", @"ALERT") message:NSLocalizedString(@"Your device doesn't support SMS!", @"Your device doesn't support SMS!") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [warningAlert show];
        return;
        
    }
    
    
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    
    messageController.recipients = [NSArray arrayWithObject:_driverMobileNumber];
    
    if([messageController.navigationBar respondsToSelector:@selector(barTintColor)]) {
        // iOS7
        // Set Navigation Bar Title Color
        [messageController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:OpenSans_SemiBold size:14], NSFontAttributeName, [UIColor blackColor], NSForegroundColorAttributeName, nil]];
        
        // Set back button color
        
    }
    
    [self presentViewController:messageController animated:YES completion:nil];
    
}
- (IBAction)cancelBtnAction:(id)sender
{
    
    if (_status == kNotificationTypeBookingStarted
         || _status == kNotificationTypeIndividualBooking  || _status == kNotificationTypeBookingClosed  )
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:TitleAlert message:LS(@"You Can't Cancel this booking") delegate:self cancelButtonTitle:TitleOk otherButtonTitles:nil,nil];
        [warningAlert show];

    }
    else
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:TitleAlert message:LS(@"Are you sure you want to Cancel this booking ?") delegate:self cancelButtonTitle:TitleNo otherButtonTitles:@"YES",nil];
        warningAlert.tag = 3333;
        [warningAlert show];
    }
}

- (IBAction)detailsBtnAction:(id)sender
{
    [self leftBarButtonClicked:nil];
}

- (IBAction)driverDetailBtnAction:(id)sender
{
    if (_constraintBottomDriverDetail.constant == -176) {
        [UIView animateWithDuration:0.6 animations:^{
            _constraintBottomDriverDetail.constant = 0;
            [_viewDriverInformation layoutIfNeeded];
            [self.view layoutIfNeeded];
        }];
    }
    else
    {
        [UIView animateWithDuration:0.6 animations:^{
            _constraintBottomDriverDetail.constant = -176;
            [_viewDriverInformation layoutIfNeeded];
            [self.view layoutIfNeeded];
        }];
    }
}

-(void)leftBarButtonClicked:(UIButton*)sender
{
   [self.navigationController popViewControllerAnimated:YES];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:NSLocalizedString(@"Failed to send !", @"Failed to send !") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)showDistance:(float)providerLat and:(float)providerLong with:(float)currentLat :(float)currentLong
{
NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f",currentLat,currentLong,providerLat,providerLong];

NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

NSData *jsonData = [NSData dataWithContentsOfURL:url];

if(jsonData != nil)
{
    NSError *error = nil;
    
    id result = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    [self distanceResponse:result];
}
else
{
    NSLog(@"N.A.");
}
}
-(void)distanceResponse:(NSDictionary *)response
{
    if (!response) {
        
    }else if ([[response objectForKey:@"rows"] count] == 0){ // Limit Exceed Response
        
        NSDictionary * dict = @{@"Time":@"0",
                                @"Distance":@"Distance: 0 Km"
                                };
        
    }
    else
    {
        
        int distance = [response[@"rows"][0][@"elements"][0][@"distance"][@"value"] intValue];
        
        NSString *dist = [NSString stringWithFormat:@"Distance: %.2f Km Away",distance/kPMDDistanceMetric];
        
        _labelEta.text = dist;
        int timeInSec =  [response[@"rows"][0][@"elements"][0][@"duration"][@"value"] intValue];
        NSInteger timeInMin = [self convertTimeInMin:timeInSec];
        
        NSString *time = [NSString stringWithFormat:@"%ld",(long)timeInMin];
        
        
        NSDictionary * dict = @{@"Time":time,
                                @"Distance":dist
                                };
        
    }
}

-(NSInteger)convertTimeInMin:(NSInteger)timeFromServer
{
    NSInteger min;
    if (timeFromServer < 60 && timeFromServer <= 0)
    {
        min = 1;
    }
    else if (timeFromServer > 60)
    {
        min = timeFromServer/60;
        
        NSInteger remainingTime = timeFromServer%60;
        
        if (remainingTime < 60 && remainingTime >= 30)
        {
            min++;
        }
    }
    else
    {
        min = 1;
    }
    return min;
}



-(void)sendRequestToGetBookingDetails
{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnWindow:WINDOW withMessge:LS(@"")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    @try {
        NSDictionary *params = @{
                                 
                                 @"ent_sess_token":flStrForObj(sessionToken),
                                 @"ent_dev_id":flStrForObj(deviceID),
                                 @"ent_booking_id":flStrForObj(_bookingId),
                                 };
        
        //setup request
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:@"bookingStatus"
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success) { //handle success response
                                           //    TELogInfo(@"response %@",response);
                                           [self parseAppointmentDetailResponse:response];
                                       }
                                   }];
    }
    @catch (NSException *exception) {
        //  TELogInfo(@"Invoice Exception : %@",exception);
    }
}

-(void)parseAppointmentDetailResponse:(NSDictionary*)response{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil) {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:TitleError Message:[response objectForKey:@"Error"]];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            bookingStatus = [response[@"bookingData"][@"statCode"] integerValue];
            [self changeViews:bookingStatus];
        }
        else
        {
            [Helper showAlertWithTitle:TitleMessage Message:@"errMsg"];
        }
        
    }
}


@end
