//
//  BookingHistoryTableViewCell.h
//  MenDoPick
//
//  Created by Rahul Sharma on 28/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelStatus;
@property (weak, nonatomic) IBOutlet UILabel *labelReceiverName;
@property (weak, nonatomic) IBOutlet UILabel *labelShipmentValue;
@property (weak, nonatomic) IBOutlet UILabel *labelDeliveryFee;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UIView *viewBox;
@property (weak, nonatomic) IBOutlet UILabel *labelBid;
-(void)loadCellData:(NSDictionary *)shipmentValue;
@end
