//
//  PromoCodeViewController.h
//  CabRyderPassenger
//
//  Created by Rahul Sharma on 23/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromoCodeViewController : UIViewController<UITextFieldDelegate>

@property (nonatomic, copy)   void (^onCompletion)(NSString *promoCode);

@property(assign,nonatomic) double userLatitude;
@property(assign,nonatomic) double userLongitude;

@end
