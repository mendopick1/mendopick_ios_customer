//
//  PickUpAddressTableViewCell.h
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 11/05/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol pickUpAddressCellDelegate <NSObject>
- (void) deleteButtonDelegate: (int) deleteBtnTag;
- (void) addButtonDelegate: (int) addBtnTag;
@end

@interface PickUpAddressTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *addressLine1;
@property (strong, nonatomic) IBOutlet UILabel *addressLine2;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) IBOutlet UIButton *addButton;
- (IBAction)deleteButton:(id)sender;
- (IBAction)addButton:(id)sender;

@property (nonatomic, assign) id<pickUpAddressCellDelegate> delegate;
@end
