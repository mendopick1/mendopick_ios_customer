//
//  DeliveryDetailTableViewCell.h
//  MenDoPick
//
//  Created by Rahul Sharma on 25/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeliveryDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnPickupAddressChange;
@property (weak, nonatomic) IBOutlet UIButton *btnDropAddressChange;
@property (weak, nonatomic) IBOutlet UILabel *labelDropAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelPickupAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelDeliveryFee;

@end
