//
//  Map1ViewController.h
//
//  Created by Rahul Sharma on 10/09/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//



#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

/**
 *  sending Address to MapVC Delegate Method
 */
@protocol pickupAddressFromMap <NSObject>

@optional

-(void)pickupAddressFromMapbySearch:(NSDictionary *)addressDetails;

@end


@interface PickUpAddressFromMapController : UIViewController <GMSMapViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,CLLocationManagerDelegate>

@property(assign,nonatomic) id <pickupAddressFromMap> pickupAddressDelegate;

@property (assign, nonatomic) NSInteger locationType;

@property NSMutableArray *pastSearchWords;
@property NSMutableArray *pastSearchResults;
@property (strong, nonatomic)  NSString *searchString;
@property (strong, nonatomic) NSMutableArray *arrayOfAddress;

@property (weak, nonatomic) IBOutlet UIButton *currentLocationbutton;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UITextField *flatNumberTextFiewld;
@property (weak, nonatomic) IBOutlet UILabel *addressLine1Label;

@property (strong, nonatomic) IBOutlet GMSMapView *mapView_;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel1;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirmLocation;


@property(nonatomic, assign) BOOL isFromHomeVC;
@property(nonatomic, assign) BOOL isFromRecepientVC;
@property (nonatomic,assign) BOOL isFromMapVC;
@property (nonatomic,assign) double latitude;
@property (nonatomic,assign) double longitude;



@end


