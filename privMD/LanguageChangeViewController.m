//
//  LanguageChangeViewController.m
//  MenDoPick
//
//  Created by Rahul Sharma on 02/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "LanguageChangeViewController.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "LanguageManager.h"
#import "XDKAirMenuController.h"


@interface LanguageChangeViewController ()<CustomNavigationBarDelegate>
{
    UITableViewCell *cell;
    NSInteger cindexPath;
}
@end

@implementation LanguageChangeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addCustomNavigationBar];
}

-(void)viewWillAppear:(BOOL)animated
{
    if ( [LanguageManager isCurrentLanguageRTL]) {
        
        cindexPath = 1;
        
    }
    else
    {
        cindexPath = 0;
    }
    
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Table View Data source -
/*--------------------------------------------------------------------------------------------*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section     //present in data souurce protocol
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"pastOrderCell";    //must be indentical
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if (indexPath.row == 0) {
        
        cell.textLabel.text = LS(@"English");
        
    }
    else
    {
        cell.textLabel.text = LS(@"Arabic");
    }
    
    if (indexPath.row == cindexPath) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
    
}

#pragma mark - TableView delegate
/*--------------------------------------------------------------------------------------------*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (cindexPath != indexPath.row) {
        
        cindexPath = indexPath.row;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Change Language?",
                                                                                 @"Change Language?") message:NSLocalizedString(@"Are you sure you want to Change The Language?","Are you sure you want to Change The Language?") delegate:self cancelButtonTitle:TitleNo otherButtonTitles:TitleYes, nil];
        [alert show];
        
    }
}



#pragma mark -
#pragma mark -alert view delegates-

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
        if (buttonIndex == 0)
        {
            
        }
        else
        {
            if (cindexPath == 0)
            {
                [LanguageManager saveLanguageByIndex:0];
                [self reloadRootViewController];
                
            }
            else
            {
                [LanguageManager saveLanguageByIndex:3];
                [self reloadRootViewController];
                //[XDKAirMenuController sharedMenu]re;
            }
            [_tableviewLangChange reloadData];
        }
}

- (void)reloadRootViewController
{
    [XDKAirMenuController relese];
    PatientAppDelegate *delegate = (PatientAppDelegate *)[UIApplication sharedApplication].delegate;
    NSString *storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    delegate.window.rootViewController = [storyboard instantiateInitialViewController];
}


- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Language Change", @"Language Change")];
    
    UILabel *bglabel = [[UILabel alloc]initWithFrame:CGRectMake(0, customNavigationBarView.frame.size.height-1, customNavigationBarView.frame.size.width, 1)];
    [customNavigationBarView addSubview:bglabel];
    bglabel.backgroundColor = UIColorFromRGB(0xcccccc);
    
    [self.view addSubview:customNavigationBarView];
    
}

-(void)rightBarButtonClicked:(UIButton *)sender{
    
    [self.view endEditing:YES];
}
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

- (void)menuButtonPressedAccount
{
    [self.view endEditing:YES];
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


@end
