//
//  PaymentCycleTableViewCell.h
//  MenDoPick
//
//  Created by Rahul Sharma on 15/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentCycleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UILabel *labelNetReceivableAmount;
@property (weak, nonatomic) IBOutlet UILabel *labelOpmeningBalance;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalBooking;
@property (weak, nonatomic) IBOutlet UILabel *labelPgComission;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalNetReceivableAmount;
@property (strong, nonatomic) IBOutlet UILabel *labelCashBookingText;
@property (strong, nonatomic) IBOutlet UILabel *labelCashBooking;
@property (strong, nonatomic) IBOutlet UILabel *labelTotalCardBookingText;
@property (strong, nonatomic) IBOutlet UILabel *labelTotalCardBooking;

@property (weak, nonatomic) IBOutlet UILabel *labelNetReceivableText;
-(void)setUpData:(NSDictionary *)dict;

@end
