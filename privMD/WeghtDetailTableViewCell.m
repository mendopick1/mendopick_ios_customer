//
//  WeghtDetailTableViewCell.m
//  MenDoPick
//
//  Created by Rahul Sharma on 19/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "WeghtDetailTableViewCell.h"

@implementation WeghtDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)loadCellData:(NSArray *)itemArray :(NSInteger)row
{
    
    [self.btnPaidByCustomer setImage: [UIImage imageNamed:@"add_shipment_check_mark_icon_off"] forState:UIControlStateNormal];
    [self.btnPaidByCustomer setImage: [UIImage imageNamed:@"add_shipment_check_mark_icon_on"] forState:UIControlStateSelected];
       switch (row) {
        case 0:
        {
            self.labelItemname.text = itemArray[row];
            self.txtfieldItemValue.placeholder = @"0 kg";
            self.btnPaidByCustomer.hidden = NO;
            self.labelPaidByCustomer.hidden = NO;
            self.textfieldQuantity.placeholder = @"0";
            self.labelQuantityText.text = LS(@"Quantity");
            self.txtfieldItemValue.returnKeyType = UIReturnKeyNext;
        }
            break;
        case 1:
        {
            self.labelItemname.text = itemArray[row];
            self.txtfieldItemValue.placeholder = @"0";
            self.btnPaidByCustomer.hidden = YES;
            self.labelPaidByCustomer.hidden = YES;
            self.txtfieldItemValue.keyboardType = UIKeyboardTypeDecimalPad;
        }
            break;
//        case 2:
//        {
//            self.labelItemname.text = itemArray[row];
//            self.txtfieldItemValue.placeholder = @"0";
//            self.btnPaidByCustomer.hidden = YES;
//            self.labelPaidByCustomer.hidden = YES;
//            self.txtfieldItemValue.keyboardType = UIKeyboardTypeDecimalPad;
//        }
            
        default:
           break;
    }

}

@end
