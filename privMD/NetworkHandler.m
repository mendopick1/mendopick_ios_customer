//
//  NetworkHandler.m
//  privMD
//
//  Created by Surender Rathore on 29/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "NetworkHandler.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import "NetworkStatusShowingView.h"

@interface NetworkHandler ()
@property(nonatomic,strong)AFHTTPSessionManager *manager;
@property(nonatomic,strong) NSString *lastMethod;
@end

@implementation NetworkHandler
static NetworkHandler *networkHandler;

+ (id)sharedInstance {
    if (!networkHandler) {
        networkHandler  = [[self alloc] init];
    }
    return networkHandler;
}

-(void)composeRequestWithMethod:(NSString*)method paramas:(NSDictionary*)paramas onComplition:(void (^)(BOOL succeeded, NSDictionary  *response))completionBlock {
    
    _manager = [AFHTTPSessionManager manager];
    
    __block  NSString *postUrl = [self getBaseString:method];
    __weak AFHTTPSessionManager *nm = _manager;
    
    nm.requestSerializer = [AFJSONRequestSerializer serializer];
    nm.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"application/json", nil];
    [nm.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
        [nm POST:postUrl parameters:paramas success:^(NSURLSessionTask *operation, id responseObject) {
            
            //send success response with data
            completionBlock(YES,responseObject);
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {

            [[ProgressIndicator sharedInstance]hideProgressIndicator];
            
            completionBlock(NO,nil);
            
        }];
}

-(void)cancelRequestOperation {
    
    for (NSOperation *operation in _manager.operationQueue.operations) {
        // here you can check if this is an operation you want to cancel
        [operation cancel];
    }
}

-(NSString*)getBaseString:(NSString*)method
{
    return [NSString stringWithFormat:@"%@%@", BASE_URL, method];
}

@end
