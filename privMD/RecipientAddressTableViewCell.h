//
//  RecipientAddressTableViewCell.h
//  MenDoPick
//
//  Created by Rahul Sharma on 25/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipientAddressTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *textfileldReceiverName;
@property (weak, nonatomic) IBOutlet UITextField *textfieldReceiverPhone;
@property (weak, nonatomic) IBOutlet UITextField *textfieldReceiverEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnContryCode;
@property (weak, nonatomic) IBOutlet UILabel *labelCountryCode;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewCountry;
@property (weak, nonatomic) IBOutlet UIButton *btnContact;

@end
