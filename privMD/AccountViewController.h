//
//  AccountViewController.h
//  privMD
//
//  Created by Rahul Sharma on 19/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadFiles.h"
#import "RoundedImageView.h"


@interface AccountViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    BOOL textFieldEditedFlag;
}

@property (strong, nonatomic) IBOutlet UIImageView *accProfilePic;
@property (strong, nonatomic) IBOutlet UIButton *accProfileButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UITextField *accFirstNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *accEmailTextField;
@property (strong, nonatomic) IBOutlet UITextField *accPhoneNoTextField;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UILabel *btnSelectedLanguage;
@property (weak, nonatomic) IBOutlet UILabel *labelPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *labelEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnPickerDone;

@property (weak, nonatomic) IBOutlet UIView *viewPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerChangeLanguage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomPickerView;
@property (weak, nonatomic) IBOutlet UILabel *labelPickerTitle;

@property (assign ,nonatomic) BOOL isImageNeedsToUpload;
@property (strong, nonatomic) UIImage *pickedImage;
@property (weak, nonatomic) IBOutlet UIButton *btnLanguageChange;

- (IBAction)logoutButtonClicked:(id)sender;
- (IBAction)profilePicButtonClicked:(id)sender;

@end
