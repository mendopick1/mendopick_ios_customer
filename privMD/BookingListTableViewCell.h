//
//  BookingListTableViewCell.h
//  MenDoPick
//
//  Created by Rahul Sharma on 18/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelType;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelShipmentValue;
@property (weak, nonatomic) IBOutlet UILabel *labelFareValue;
@property (weak, nonatomic) IBOutlet UIView *viewBox;
@property (weak, nonatomic) IBOutlet UILabel *labelBid;

-(void)loadCellData:(NSDictionary *)shipmentValue;
@end
