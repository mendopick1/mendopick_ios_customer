//
//  SignInViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SignInViewController.h"
#import "ViewController.h"
#import "Database.h"
#import <Crashlytics/Crashlytics.h>
#import "LanguageManager.h"
@interface SignInViewController (){
    
    UIWindow *window;
    
}
@property (nonatomic,strong)  NSMutableArray *arrayContainingCardInfo;

@end

@implementation SignInViewController

@synthesize emailTextField;
@synthesize passwordTextField;
@synthesize navDoneButton;
@synthesize emailImageView;
@synthesize signinButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [Helper setCornerRadius:3.0 bordercolor:CLEAR_COLOR withBorderWidth:0 toview:signinButton];
    window = [[UIApplication sharedApplication] keyWindow];
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80,-10, 160, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(10,10, 147, 30)];
    navTitle.text = NSLocalizedString(@"Sign In", @"Sign In");
    navTitle.textColor = UIColorFromRGB(0x565656);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:Hind_Medium size:17];
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;

    [[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.title = NSLocalizedString(@"Sign In", @"Sign In");
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    
    [self createNavLeftButton];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
}

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"signin_back_icon_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"signin_back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
   if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [navCancelButton setFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x+20,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    }
    
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)viewDidDisappear:(BOOL)animated
{
   // self.navigationController.navigationBarHidden = YES;
    
}

-(void)viewDidAppear:(BOOL)animated
{
  [emailTextField becomeFirstResponder];
    
}

-(void)dismissKeyboard
{
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    
}

-(void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
    //[self callPop];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)signInUser
{
    NSString *email = emailTextField.text;
    
    NSString *password = passwordTextField.text;
    
    if((unsigned long)email.length == 0)
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Enter Email ID", @"Enter Email ID")];
        [emailTextField becomeFirstResponder];
    }
    else if([self emailValidationCheck:email] == 0)
    {
        //email is not valid
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Invalid Email ID", @"Invalid Email ID")];
        emailTextField.text = @"";
        [emailTextField becomeFirstResponder];
        
    }
    else if((unsigned long)password.length == 0)
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Enter Password", @"Enter Password")];
        [passwordTextField becomeFirstResponder];
    }
    
    else
    {
        checkLoginCredentials = YES;
        [self sendServiceForLogin];
        
    }
    
}



-(void)DoneButtonClicked
{
    
    [self signInUser];
}

-(void)sendServiceForLogin
{
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *lat = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat])
        lat = @"";
    else
        lat = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat];
    
    NSString *lon = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong])
        lon = @"";
    else
        lon = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong];
    NSString *pToken = @"";
    if([[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken])
    {
        pToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken];
    }
    else
    {
        pToken = @"garbagevalue";
    }
    NSString *city = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity])
        city = @"";
    else
        city = [[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity];
    
    NSDictionary *params = @{
                             KDALoginEmail:emailTextField.text,
                             KDALoginPassword:passwordTextField.text,
                             KDALoginDevideId:deviceId,
                             KDALoginPushToken:pToken,
                             KDASignUpLatitude:lat,
                             KDASignUpLongitude:lon,
                             KDASignUpCity:city,
                             KDALoginDeviceType:@"1",
                             KDALoginUpDateTime:[Helper getCurrentDateTime],
                            };
    
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodPatientLogin
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       NSLog(@"Test Sign In Response %@",response);
                                       [self loginResponse:response];
                                   }
                               }];
    
    // Activating progress Indicator.
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnWindow:window withMessge:NSLocalizedString(@"Signing In...", @"Signing In...")];
}

-(void)loginResponse:(NSDictionary *)dictionary
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    itemList = [dictionary mutableCopy];
    
    if(!dictionary)
    {
        return;
    }
    
    
    if ([[dictionary objectForKey:@"error"] length] != 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleError message:[dictionary objectForKey:@"error"] delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
        
        if ([[itemList objectForKey:@"errFlag"]integerValue] == 0 || ([[itemList objectForKey:@"errFlag"]integerValue] == 1 && [[itemList objectForKey:@"errNum"]integerValue] == 115))
        {
          //  TELogInfo(@" Item List contains %@ ",itemList);
            //save information
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setObject:[itemList objectForKey:@"token"] forKey:KDAcheckUserSessionToken];
            [ud setObject:itemList[@"chn"] forKey:kNSUPatientPubNubChannelkey];
            [ud setObject:itemList[@"email"] forKey:kNSUPatientEmailAddressKey];
            [ud setObject:itemList[@"apiKey"] forKey:kNSUMongoDataBaseAPIKey];
            [ud setObject:itemList[@"serverChn"] forKey:kPMDPublishStreamChannel];
            [ud setObject:itemList[@"coupon"] forKey:kNSUPatientCouponkey];
            [ud setObject:itemList[@"presenseChn"] forKey:@"presencechannel"];
            [ud setObject:passwordTextField.text forKey:KDAPassword];
                      
            [ud synchronize];
            
            NSMutableArray *carTypes = [[NSMutableArray alloc]initWithArray:itemList[@"types"]];
            
            if (!carTypes || !carTypes.count){
             //   TELogInfo(@"do nothing");
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:carTypes forKey:KUBERCarArrayKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            [Database DeleteAllCard];
            NSArray *cardDetails = [[NSArray alloc]initWithArray:itemList[@"cards"]];
            Database *db = [[Database alloc]init];
            for(int i = 0; i<cardDetails.count;i++){
                
                [db makeDataBaseEntry:cardDetails[i]];
            }
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            
            if ([[itemList objectForKey:@"errFlag"]integerValue] == 1 && [[itemList objectForKey:@"errNum"]integerValue] == 115) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", @"Message") message:[itemList objectForKey: @"errMsg"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
                [alertView show];
            }
            
        }
        else if ([[itemList objectForKey:@"errFlag"]integerValue] == 1)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleMessage message:[itemList objectForKey:@"errMsg"] delegate:self cancelButtonTitle:TitleOk otherButtonTitles:nil];
            [alertView show];
            checkLoginCredentials = NO;
            passwordTextField.text = @"";
            [passwordTextField becomeFirstResponder];
        }
        
    }
    
}

- (void)shakeView:(UIView *)viewToShake
{
    CGFloat t = 15.0;
    CGAffineTransform translateRight  = CGAffineTransformTranslate(CGAffineTransformIdentity, t, 0.0);
    CGAffineTransform translateLeft = CGAffineTransformTranslate(CGAffineTransformIdentity, -t, 0.0);
    
    viewToShake.transform = translateLeft;
    
    [UIView animateWithDuration:0.07 delay:0.0 options:UIViewAnimationOptionAutoreverse|UIViewAnimationOptionRepeat animations:^{
        [UIView setAnimationRepeatCount:2.0];
        viewToShake.transform = translateRight;
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView animateWithDuration:0.05 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                viewToShake.transform = CGAffineTransformIdentity;
            } completion:NULL];
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   
           
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:emailTextField])
    {
        if (textField.text.length > 0)
        {
            BOOL isValid = [Helper emailValidationCheck:emailTextField.text];
            if (!isValid)
            {
                [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Your Email-Id is not valid.Please enter a valid email.", @"Your Email-Id is not valid.Please enter a valid email.")];
                emailTextField.text = @"";
                [emailTextField becomeFirstResponder];
            }
        }
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if(textField == emailTextField)
    {
        if (textField.text.length > 0)
        {
            BOOL isValid = [Helper emailValidationCheck:emailTextField.text];
            if (!isValid)
            {
                [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Your Email-Id is not valid.Please enter a valid email.", @"Your Email-Id is not valid.Please enter a valid email.")];
                emailTextField.text = @"";
                [emailTextField becomeFirstResponder];
            }
            else
            {
                [passwordTextField becomeFirstResponder];
            }
        }
    }
    else
    {
        [passwordTextField resignFirstResponder];
        [self DoneButtonClicked];
    }
    return YES;
}



- (IBAction)forgotPasswordButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    UIAlertView *forgotPasswordAlert = [[UIAlertView alloc]
                                        initWithTitle:NSLocalizedString(@"Forgot Password?", @"Forgot Password?")
                                        message:NSLocalizedString(@"Enter your email address below and we will send you password reset instruction.)", @"Enter your email address below and we will send you password reset instruction.")
                                        delegate:self
                                        cancelButtonTitle:TitleCancel
                                        otherButtonTitles:NSLocalizedString(@"Submit", @"Submit"), nil];
    forgotPasswordAlert.tag = 1;
    forgotPasswordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *txtfld = [forgotPasswordAlert textFieldAtIndex:0];
    txtfld.keyboardType = UIKeyboardTypeEmailAddress;
    [forgotPasswordAlert show];
    //forgotView.hidden=NO;
   // TELogInfo(@"password recovery to be done here");
}

- (IBAction)signInButtonClicked:(id)sender {
    [self.view endEditing:YES];
     [self signInUser];
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        UITextField *forgotEmailtext = [alertView textFieldAtIndex:0];
        NSLog(@"Email Name: %@", forgotEmailtext.text);
        
        if (((unsigned long)forgotEmailtext.text.length ==0))
        {
            [self forgotPasswordButtonClicked:self];
        }
        else if ([Helper emailValidationCheck:forgotEmailtext.text] == 0)
        {
            [self forgotPaswordAlertviewTextField:NSLocalizedString(@"Invalid Email ID. Reenter your email ID", @"Invalid Email ID. Reenter your email ID")];
        }
        else
        {
            [self retrievePassword:forgotEmailtext.text];
        }
    }
    else
    {
        NSLog(@"cancel");
        self.passwordTextField.text = @"";
    }
}

#pragma mark - Custom Methods -

- (void) forgotPaswordAlertviewTextField:(NSString *)message
{
    UIAlertView *forgotPasswordAlert = [[UIAlertView alloc]
                                        initWithTitle:TitleMessage
                                        message:message
                                        delegate:self
                                        cancelButtonTitle:TitleCancel
                                        otherButtonTitles:NSLocalizedString(@"Submit", @"Submit"), nil];
    forgotPasswordAlert.tag = 1;
    forgotPasswordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *txtfld = [forgotPasswordAlert textFieldAtIndex:0];
    txtfld.keyboardType = UIKeyboardTypeEmailAddress;
    [forgotPasswordAlert show];
}

# pragma mark - WebServices -
- (void)retrievePassword:(NSString *)text
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    NSDictionary *params = @{
                             @"ent_email":text,
                             @"ent_user_type":@"2",
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"forgotPassword"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       //handle success response
                                       [self retrievePasswordResponse:response];
                                   }
                               }];
}


-(void)retrievePasswordResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleError message:[response objectForKey: @"Message"] delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alertView show];
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:TitleError Message:[response objectForKey:@"Error"]];
    }
    else
    {
        NSDictionary *dictResponse = [response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [Helper showAlertWithTitle:TitleMessage Message:[dictResponse objectForKey:@"errMsg"]];
        }
        else
        {
            [Helper showAlertWithTitle:TitleMessage Message:[dictResponse objectForKey:@"errMsg"]];
        }
    }
}

- (BOOL) emailValidationCheck: (NSString *) emailToValidate
{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}

-(void)addInDataBase
{
    Database *db = [[Database alloc] init];
       {
        for (int i =0; i<_arrayContainingCardInfo.count; i++)
        {
            [db makeDataBaseEntry:_arrayContainingCardInfo[i]];
        }
    }
    
}


@end
