//
//  PaymentViewController.m
//  privMD
//
//  Created by Rahul Sharma on 02/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PaymentViewController.h"
#import "XDKAirMenuController.h"
#import "PaymentCycleTableViewCell.h"
#import "CustomNavigationBar.h"
#import "PastPaymentCycleTableViewCell.h"
#import "PatientViewController.h"
#import "AnimationsWrapperClass.h"
#import "LanguageManager.h"
#import "PastPaymentDetailsViewController.h"

@interface PaymentViewController ()<CustomNavigationBarDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary *currentCycle;
    NSArray *pastCycle;
    NSInteger index;
}


@end

@implementation PaymentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addCustomNavigationBar];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ( [reachability isNetworkAvailable]) {
        
    }
    else {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self walletSummary];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)cancelButtonPressedAccount
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Payment Cycle", @"Payment Cycle")];
    [customNavigationBarView hideLeftMenuButton:YES];
    
    
    UIImage *buttonImage ;
    UIImage *buttonImage1 ;
    
    if (_isComingBookingList) {
        
        buttonImage = [UIImage imageNamed:@"signin_back_icon_off"];
        buttonImage1 = [UIImage imageNamed:@"signin_back_icon_on"];
        
    }
    else
    {
        buttonImage = [UIImage imageNamed:@"home_menu_icon_off"];
        buttonImage1 = [UIImage imageNamed:@"home_menu_icon_on"];
    }
    
    [customNavigationBarView setleftBarButtonImage:buttonImage1 :buttonImage];
    
    UILabel *bglabel = [[UILabel alloc]initWithFrame:CGRectMake(0, customNavigationBarView.frame.size.height-1, customNavigationBarView.frame.size.width, 1)];
    [customNavigationBarView addSubview:bglabel];
    bglabel.backgroundColor = UIColorFromRGB(0xD2D2D2);
    
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton*)sender {
    
    if (_isComingBookingList) {
        
        [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                                  subType:kCATransitionFromBottom
                                                  forView:self.navigationController.view
                                             timeDuration:0.3];
        
        [self.navigationController popViewControllerAnimated:NO];

    }
    else
    {
        [self.view endEditing:YES];
        XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
        
        if (menu.isMenuOpened)
        [menu closeMenuAnimated];
        else
        [menu openMenuAnimated];
        
    }
    
    
}

#pragma mark - WebService call


-(void)walletSummary
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@""];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceId;
    
    if (IS_SIMULATOR)
    {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM"];
    NSString *months = [df stringFromDate:[NSDate date]];
    NSDictionary *params =  @{
                              @"ent_sess_token":flStrForObj(sessionToken),
                              @"ent_dev_id": flStrForObj(deviceId),
                              @"ent_date_time":flStrForObj([Helper getCurrentDateTime]),
                              @"ent_appnt_dt":flStrForObj(months),
                              @"ent_page_index":@"4"
                              };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"ShyprFinancials" paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 //handle success response
                                 [self orderSummaryResponse:response];
                             }
                             else if(response == nil)
                             {
                             }
                         }];
}

-(void)orderSummaryResponse:(NSDictionary *)dictionary
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if(!dictionary)
    {
        return;
    }
    else if ([dictionary[@"errFlag"] intValue] == 1 && ([dictionary[@"errNum"] intValue] == 96 || [dictionary[@"errNum"] intValue] == 7)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:@"Message" Message:[dictionary objectForKey:@"errMsg"]];
    }
    else if ([dictionary[@"errFlag"] intValue] == 1 && [dictionary[@"errNum"] intValue] == 65)
    {
    }
    else
    {
        
        if (!currentCycle) {
            
            currentCycle = [[NSDictionary alloc] init];
        }
        if (!pastCycle) {
            
            pastCycle = [[NSArray alloc] init];

        }
        currentCycle = dictionary[@"currentWeek"];
        pastCycle = dictionary[@"lastWeek"];
        
    }
    [_tableviewPaymentCycle reloadData];
    
}




#pragma mark - Uitableview Datasource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    return [self addHeaderView:section];
   
}

-(UIView *)addHeaderView:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _tableviewPaymentCycle.frame.size.width, 30)];
    
    headerView.backgroundColor = UIColorFromRGB(0xf7f9fa);
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(12, 0, headerView.frame.size.width, headerView.frame.size.height)];
    titleLabel.backgroundColor = CLEAR_COLOR;
    [Helper setToLabel:titleLabel Text:LS(@"CURRENT CYCLE") WithFont:Hind_Medium FSize:14 Color:UIColorFromRGB(0xb6b6b6)];
    
    if (section == 1)
    {
        if (pastCycle.count ==0) {

            CGRect newFrame = headerView.frame;
            newFrame.size = CGSizeMake(headerView.frame.size.width, 130.0);
            headerView.frame = newFrame;
            
            UILabel *bgLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, headerView.frame.size.width, 1)];
            bgLabel.backgroundColor = UIColorFromRGB(0xDEE7E8);
            [headerView addSubview:bgLabel];
            
            headerView.frame = CGRectMake(0, 0, _tableviewPaymentCycle.frame.size.width, 40);
            titleLabel.frame = CGRectMake(12, 5, headerView.frame.size.width, headerView.frame.size.height);
            
            titleLabel.text = LS(@"PAST CYCLE");
            
            UILabel *BgLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, bgLabel.frame.origin.y+5, headerView.frame.size.width, 120)];
 
             [Helper setToLabel:BgLabel1 Text:LS(@"No Past Cycle Available") WithFont:Hind_Medium FSize:14 Color:UIColorFromRGB(0x333333)];
            
            BgLabel1.textAlignment = NSTextAlignmentCenter;
            
            [headerView addSubview:BgLabel1];
        }
        else
        {
        UILabel *bgLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, headerView.frame.size.width, 1)];
        bgLabel.backgroundColor = UIColorFromRGB(0xDEE7E8);
        [headerView addSubview:bgLabel];
        
        headerView.frame = CGRectMake(0, 0, _tableviewPaymentCycle.frame.size.width, 40);
        titleLabel.frame = CGRectMake(12, 5, headerView.frame.size.width, headerView.frame.size.height);
        
        titleLabel.text = LS(@"PAST CYCLE");
    }
    }
    
    if ([[LanguageManager currentLanguageCode ]isEqualToString:ArabicCode]) {
     
        titleLabel.frame = CGRectMake(0, 0, headerView.frame.size.width-10, 40);
//        titleLabel.backgroundColor = [UIColor redColor];
        titleLabel.textAlignment = NSTextAlignmentRight;
    }
    
    [headerView addSubview:titleLabel];
    
     return headerView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1)
    {
        if (pastCycle.count ==0) {
            
            return 130;
        }
    }
    
    return 30.0;    
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return CGFLOAT_MIN;;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 1;
    }
    else
    {
        return pastCycle.count;
    }
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
         static NSString *tableidentifier = @"Cell";
        PaymentCycleTableViewCell *cell = (PaymentCycleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setUpData:currentCycle];
        return cell;
    }
    else
    {
        static NSString *tableidentifier = @"PastPaymentCycleTableViewCell";
        PastPaymentCycleTableViewCell *cell = (PastPaymentCycleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setUpData:pastCycle[indexPath.row]];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return 320;
    }
    return 130;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        index = indexPath.row;
        [self performSegueWithIdentifier:@"goToPaymentDetail" sender:nil];
    }
}

#pragma mark navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"goToPaymentDetail"]) {
        
        PastPaymentDetailsViewController *pickup = [segue destinationViewController];
       
        pickup.walletDetail = pastCycle[index];
    }
}


@end
