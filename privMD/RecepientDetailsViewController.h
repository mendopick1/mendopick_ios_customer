//
//  RecepientDetailsViewController.h
//  DeliveryPlus
//
//  Created by Rahul Sharma on 05/10/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface RecepientDetailsViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate,CLLocationManagerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *selectedAddressLabel;
@property (weak, nonatomic) IBOutlet UITextField *recepientNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *countryCodeLabel;

@property (strong, nonatomic)NSDictionary *shipmentData;
@property (weak, nonatomic) IBOutlet UIView *divider1;
@property (weak, nonatomic) IBOutlet UIView *divider2;
@property (weak, nonatomic) IBOutlet UIView *divider3;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)nextButtonAction:(id)sender;

@end
