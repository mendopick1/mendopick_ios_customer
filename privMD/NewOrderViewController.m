//
//  NewOrderViewController.m
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 11/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "NewOrderViewController.h"
#import "CustomNavigationBar.h"
#import "XDKAirMenuController.h"
#import "InvoiceViewController.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "Database.h"
#import "CAPSPageMenu.h"
#import "CurrentOrderViewController.h"
#import "PastOrderViewController.h"
#import "OrderSummaryViewController.h"
#import "LiveBookingTable.h"
#import "LiveBookingShipmentDetails.h"
#import "Update&SubscribeToPubNub.h"
#import "LiveBookingTable.h"
#import "LiveBookingStatusUpdatingClass.h"
#import "LiveBookingShipmentDetails.h"


@interface NewOrderViewController () <CustomNavigationBarDelegate, UIScrollViewDelegate,updateAndSubscribeToPubNubDelegate,CurrentOrderViewControllerDelegate,PastOrderViewControllerDelegate>
{
    NSDictionary *itemList;
    NSMutableArray *items;
    NSArray *shipmentDetailsArray;
    NSMutableArray *bookingCount;
    BOOL isFromPastBooking;
    CurrentOrderViewController *currentOrder;
    PastOrderViewController *pastOrder;
    Update_SubscribeToPubNub *Update_SubscribeToPubNubSharedObject;
    
    
    LiveBookingTable *liveBookingDetailsDB;
    LiveBookingStatusUpdatingClass *liveBookingStatusUpdateVC;
    LiveBookingShipmentDetails *liveBookingShipmentDB;
}

@property (nonatomic) CAPSPageMenu *pageMenu;

@end

static NewOrderViewController *newVC = nil;

@implementation NewOrderViewController

+(instancetype)getSharedInstance
{
    return newVC;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addCustomNavigationBar];
    isFromPastBooking = NO;
    newVC = self;
    _labelMessage.text = LS(@"Currently No Active Bookings......");
    
}

-(void)didReviewSubmitted
{
    [self statusUpdatedReloadViews];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReviewSubmitted)
                                                 name:kNotificationReviewSubmitted
                                               object:nil];
    
    liveBookingStatusUpdateVC = [LiveBookingStatusUpdatingClass sharedInstance];
    
    Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
    [Update_SubscribeToPubNubSharedObject subscribeToPassengerChannel];
    Update_SubscribeToPubNubSharedObject.delegate = self;
    
    [self statusUpdatedReloadViews];
}



-(void)viewWillDisappear:(BOOL)animated
{
    [Update_SubscribeToPubNubSharedObject stopPubNubStream];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationReviewSubmitted object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)customizeUi:(NSDictionary *)response
{
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CurrentOrderViewController *allOnGoingBookingvc = [mainstoryboard instantiateViewControllerWithIdentifier:@"appointmentViewController"];
    PastOrderViewController *AllPstBookingsvc = [mainstoryboard instantiateViewControllerWithIdentifier:@"PastOrderViewController"];
    
    allOnGoingBookingvc.title = LS(@"Live");
    allOnGoingBookingvc.delegate = self;
    allOnGoingBookingvc.allshipmentDetails = response[@"current"];
    
    AllPstBookingsvc.title = LS(@"Completed");
    AllPstBookingsvc.delegate = self;
    AllPstBookingsvc.allshipmentDetails = response[@"Past_order"];
    
    NSArray *controllerArray = @[allOnGoingBookingvc, AllPstBookingsvc];
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor whiteColor],
                                 CAPSPageMenuOptionSelectionIndicatorColor: UIColorFromRGB(0xd53e30),
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:OpenSans_SemiBold size:13.0],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(self.view.frame.size.width/controllerArray.count),
                                 CAPSPageMenuOptionCenterMenuItems: @(NO)
                                 };
    NSInteger pageindex;
    if (isFromPastBooking) {  // if came from past booking to open past tab
        isFromPastBooking = NO;
        currentOrder = [CurrentOrderViewController getSharedInstance];
        currentOrder.allshipmentDetails = response[@"current"];
        [currentOrder.currentOrderTableView reloadData];
        
        AllPstBookingsvc.allshipmentDetails = response[@"Past_order"];
        [AllPstBookingsvc.pastOrderTableView reloadData];
    }
    else
    {
        pageindex = 0;
        _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 64, self.view.frame.size.width, self.view.frame.size.height) options:parameters openindex:pageindex];
        
        [self.view addSubview:_pageMenu.view];
}
}

#pragma mark- Custom Navigation Methods

- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView hideRightBarButton:YES];
    [customNavigationBarView setTitle:NSLocalizedString(@"My Bookings", @"My Bookings")];
    UILabel *bglabel = [[UILabel alloc]initWithFrame:CGRectMake(0, customNavigationBarView.frame.size.height-1, customNavigationBarView.frame.size.width, 1)];
    [customNavigationBarView addSubview:bglabel];
    bglabel.backgroundColor = UIColorFromRGB(0xD2D2D2);
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    [self menuButtonclicked];
}

- (void)menuButtonclicked
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
    {
        [menu closeMenuAnimated];
    }
    else
    {
        [self.view endEditing:YES];
        [menu openMenuAnimated];
    }
}

#pragma mark -Web Service-
-(void) orderSummary
{
     [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:LS(@"Loading Order Details....")];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceId;
    
    if (IS_SIMULATOR) {
        
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM"];
    NSString *months = [df stringFromDate:[NSDate date]];
    
    NSDictionary *params =  @{
                              @"ent_sess_token":flStrForObj(sessionToken),
                              @"ent_dev_id": flStrForObj(deviceId),
                              @"ent_date_time":flStrForObj([Helper getCurrentDateTime]),
                              @"ent_appnt_dt":months,
                              @"ent_page_index":@"0",
                              };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getSlaveAppointments" paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success) {
                                 //handle success response
                                 [self orderSummaryResponse:response];
                             }
                             else if(response == nil)
                             {
                                 
                             }
                         }];
}

-(void)orderSummaryResponse:(NSDictionary *)dictionary
{
    itemList = [dictionary mutableCopy];
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];

    if(!dictionary)
    {
        return;
    }
    else if ([dictionary[@"errFlag"] intValue] == 1 && ([dictionary[@"errNum"] intValue] == 96 || [dictionary[@"errNum"] intValue] == 7)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:@"Message" Message:[dictionary objectForKey:@"errMsg"]];
        
    }
    else if ([[itemList objectForKey:@"errFlag"]integerValue] == 0) {
        
        if (itemList) {
            
            [self customizeUi:itemList];
            [self storeDataInLocal:itemList];
        }
        else {
            
        }

    }
    
}

// store data in Local after acceptin order

-(void)storeDataInLocal:(NSDictionary *)order
{
    NSArray *fullShipmentDetailsCurrent = [order objectForKey:@"current"];
    
  //  fullShipmentDetailsCurrent = [NSArray arrayWithArray: itemcurrent];
    Database *db = [[Database alloc]init];
    
   // bookingCount = [[NSMutableArray alloc]init];
    NSString *count;
    
    if(fullShipmentDetailsCurrent.count >0 )
    {
        
        for(int i = 0; i<fullShipmentDetailsCurrent.count;i++) {
            
            NSDictionary *bookingDetailDictionary = @{
                                                      
                                                      @"bid":flStrForObj(fullShipmentDetailsCurrent[i][@"bid"]),
                                                      @"passengerEmail":flStrForObj(fullShipmentDetailsCurrent[i][@"email"]),
                                                      @"passengerPhotoURL":flStrForObj(fullShipmentDetailsCurrent[i][@"pPic"]),
                                                      @"pickupAddress":flStrForObj(fullShipmentDetailsCurrent[i][@"addrLine1"]),
                                                      @"pickupLat":flStrForObj(fullShipmentDetailsCurrent[i][@"apptLat"]),
                                                      @"pickupLong":flStrForObj(fullShipmentDetailsCurrent[i][@"apptLong"]),
                                                      @"status":flStrForObj(fullShipmentDetailsCurrent[i][@"statCode"]),
                                                      @"passengerMobile":flStrForObj(fullShipmentDetailsCurrent[i][@"phone"]),
                                                      @"appntDate":flStrForObj(fullShipmentDetailsCurrent[i][@"apntDt"]),
                                                      @"driverName":flStrForObj(fullShipmentDetailsCurrent[i][@"fname"]),
                                                      @"laterbooking":flStrForObj(fullShipmentDetailsCurrent[i][@"apptType"])
                                                      };
            
            
            shipmentDetailsArray = [NSArray arrayWithArray:fullShipmentDetailsCurrent[i][@"shipment_details"]];
            
            if(i > 0)
                count = [NSString stringWithFormat:@"%lu",(unsigned long)([shipmentDetailsArray count]+[count integerValue])];
            else
                count = [NSString stringWithFormat:@"%lu",(unsigned long)[shipmentDetailsArray count]];
            
            [bookingCount addObject:count];
            
            for(int j =0;j<shipmentDetailsArray.count;j++) {
                
                NSString *status = @"";
                
                if([shipmentDetailsArray[j][@"status"] integerValue] == kNotificationTypeIndividualBooking || [shipmentDetailsArray[j][@"status"] integerValue] == kNotificationTypeBookingClosed){
                    
                    status = shipmentDetailsArray[j][@"status"];
                    
                }
                else
                {
                    status = fullShipmentDetailsCurrent[i][@"statCode"];
                }
                
                if ([status integerValue] == kNotificationTypeBookingAccept) {
                 
                    status = @"2";
                }
                
                NSDictionary *shipmentDictionary = @{
                                                     
                                                     @"bid":flStrForObj(fullShipmentDetailsCurrent[i][@"bid"]),
                                                     @"shipmentAddress":flStrForObj(shipmentDetailsArray[j][@"address"]),
                                                     @"shipmentImage":flStrForObj(shipmentDetailsArray[j][@"photo"]),
                                                     @"shipmentId":flStrForObj(shipmentDetailsArray[j][@"subid"]),
                                                     @"shipmentPhoneNumber":flStrForObj(shipmentDetailsArray[j][@"mobile"]),
                                                     @"shipmentStatus":flStrForObj(status),
                                                     @"dropLatitude":flStrForObj(shipmentDetailsArray[j][@"location"][@"lat"]),
                                                     @"dropLongitude":flStrForObj(shipmentDetailsArray[j][@"location"][@"log"]),
                                                     @"name":flStrForObj(shipmentDetailsArray[j][@"name"])
                                                     
                                                     };
                
                NSString *onGoingBookidId = flStrForObj(fullShipmentDetailsCurrent[i][@"bid"]);
                
                NSArray *itemsArray = [Database getLiveBookingDetails:onGoingBookidId];
                
                
                if(itemsArray.count > 0)
                {
                    LiveBookingTable *objectOfLive = [itemsArray objectAtIndex:0];
                    
                    objectOfLive.status = flStrForObj(fullShipmentDetailsCurrent[i][@"statCode"]);
                    
                    if ([flStrForObj(fullShipmentDetailsCurrent[i][@"statCode"]) integerValue] == kNotificationTypeBookingAccept) {
                        
                        objectOfLive.status = @"2";
                    }
                    
                    NSLog(@"LiveBookingTable bid = %@  status = %@ \n",objectOfLive.bid,objectOfLive.status);
                }
                else
                {
                    
                    [db makeDataBaseEntryForLiveBooking:bookingDetailDictionary];
                    
                }
                
                NSArray *itemsArray2 = [Database getLiveBookingEachShipmentDetails:onGoingBookidId shipmentId:shipmentDetailsArray[j][@"subid"]];
                LiveBookingShipmentDetails *obj;
                if(itemsArray2.count > 0) {
                    
                    obj  = [itemsArray2 objectAtIndex:0];
                    NSLog(@"LiveBookingShipmentDetails bid = %@  ShipmentStatus = %@ \n",obj.bid,obj.shipmentStatus);
                    [obj setShipmentStatus:shipmentDetailsArray[j][@"status"]];
                    
                }
                else {
                    
                    [db makeDataBaseEntryForLiveBookingEachShipment:shipmentDictionary];
                }
                
            }
            
        } // Each shipment loop
        
        
    } // Outer shipment loop
    
    else
    {
        [Database deleteAllLiveBookingDetails];
        [Database deleteAllLiveBookingShipmentDetails];
    }
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];

}


-(void)statusUpdatedReloadViews
{
    PMDReachabilityWrapper *reachable = [PMDReachabilityWrapper sharedInstance];
    if ([reachable isNetworkAvailable] == YES)
    {
        [self orderSummary];
    }
    else
    {
        [[ProgressIndicator sharedInstance]showMessage:@"No Internet Connection" On:self.view];
    }
}


#pragma mark Custom Delegate
-(void)goToOngoingVc:(NSInteger)index and:(NSDictionary *)response
{
    isFromPastBooking = YES;
    OrderSummaryViewController *orderSummaryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderSummaryViewController"];
    orderSummaryVC.shipmentDetailsBookingList = response;
    orderSummaryVC.apptDate = response[@"apntDt"];
    orderSummaryVC.BookingStatus = [response[@"statCode"] integerValue];
    orderSummaryVC.bid = [response[@"bid"] integerValue];
    
    [self.navigationController pushViewController:orderSummaryVC animated:YES];
}

-(void)goToCompletedVc:(NSInteger)index and:(NSDictionary *)response
{
     isFromPastBooking = YES;
    InvoiceViewController *invoiceVC = [self.storyboard instantiateViewControllerWithIdentifier:@"invoiceVC"];
    
  NSInteger  apptStatus = [response[@"statCode"] intValue];
    
    if(apptStatus== kNotificationTypeBookingCancel || apptStatus== kNotificationTypeBookingCancelled )
    {
        invoiceVC.cancelled = YES;
        invoiceVC.cancelId = apptStatus;
    }

    invoiceVC.appointmentDetails = response;
    invoiceVC.appointmentDate = response[@"apntDt"];
    invoiceVC.customerEmail = response[@"email"];
    [self.navigationController pushViewController:invoiceVC animated:YES];
}

#pragma mark - Pubnub Delegate -

-(void)recievedMessageWithDictionary:(NSDictionary *)messageDict OnChannel:(NSString *)channelName {
    
    if (![messageDict isKindOfClass:[NSDictionary class]])
    {
        return;
    }
    if ([messageDict[@"a"] intValue] == kNotificationTypeBookingOnMyWay ||[messageDict[@"a"] intValue] ==  kNotificationTypeBookingReachedLocation ||[messageDict[@"a"] intValue] == kNotificationTypeBookingStarted || [messageDict[@"a"] intValue] == kNotificationTypeBookingComplete ||  [messageDict[@"a"] intValue] ==  kNotificationTypeIndividualBooking ||[messageDict[@"a"] intValue] == kNotificationTypeBookingClosed || [messageDict[@"a"] intValue] == kNotificationTypeBookingCancelled || [messageDict[@"a"] intValue] == 11 )
    {
        
        [liveBookingStatusUpdateVC updateLiveBookingStatusAndShowVC:messageDict];
        
    }
    
    
}


@end
