//
//  RateCard.m
//  iServe_Customer
//
//  Created by Rahul Sharma on 23/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "RateCard.h"
#import "PatientGetLocalCurrency.h"
#import "LanguageManager.h"


@implementation RateCard

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

static  RateCard *rateCard;

+ (id)sharedInstance
{
    if (!rateCard ) {
        rateCard=[[self alloc]init];
    }
    return rateCard;
}
- (instancetype)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"RateCard"
                                          owner:self
                                        options:nil] firstObject];
    
    return self;
}

-(void)showPopUpWithDictionary:(NSDictionary *)shipments
{
    _labelMinFare.text = [PatientGetLocalCurrency getCurrencyLocal:flDoubleForObj(shipments[@"min_fare"])];
    
    _labelAfter2mi.text = [PatientGetLocalCurrency getCurrencyLocal:flDoubleForObj(shipments[@"mileage_after_x_km_miles"])];
    
    _labelCapacity.text = flStrForObj(shipments[@"vehicle_capacity"]);
    
    _labelAfterHour.text = [PatientGetLocalCurrency getCurrencyLocal:flDoubleForObj(shipments[@"price_after_x_minutes"])];
    
    _labelLBH.text = [NSString stringWithFormat:@"%@x%@x%@",flStrForObj(shipments[@"length"]),flStrForObj(shipments[@"breadth"]),flStrForObj(shipments[@"height"])];
    
    self.layer.cornerRadius = 5.0;
    [WINDOW addSubview:self];
}

-(void)removePopUpWithDictionary:(NSDictionary *)shipments
{
    
    [self removeFromSuperview];
}

@end
