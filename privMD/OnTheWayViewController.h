//
//  OnTheWayViewController.h
//  DeliveryPlus
//
//  Created by Rahul Sharma on 07/11/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface OnTheWayViewController : UIViewController<GMSMapViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *currentLocationButton;

@property (weak, nonatomic) IBOutlet GMSMapView *onTheWayMapView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImageView;
@property (weak, nonatomic) IBOutlet UILabel *onTheWayMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelPickupAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelDropoffAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelDriverName;
@property (weak, nonatomic) IBOutlet UILabel *labelEta;
@property (weak, nonatomic) IBOutlet UIView *viewDriverInformation;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomDriverDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@property (strong ,nonatomic) NSDictionary *shipmentDictionary;
@property (strong ,nonatomic) NSDictionary *selectedShipment;
@property  NSString *shipmentNumber;
@property  NSString *bookingId;
@property  NSInteger status;
@property  NSString *driverMobileNumber;
@property BOOL isFromInvoice;
@property NSString *driverChannelName;

- (IBAction)currentLocationButtonClickedAction:(id)sender;

- (IBAction)callButtonAction:(id)sender;
- (IBAction)messageButtonAction:(id)sender;
- (void)changeViews:(NSInteger)status;
- (void)statusUpdatedResponse:(NSDictionary *)dict and:(NSString *)shipmentId;

@end
