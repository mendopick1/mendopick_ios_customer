//
//  ShipmentAddViewController.h
//  MenDoPick
//
//  Created by Rahul Sharma on 18/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShipmentAddViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableviewShipment;
@property (weak, nonatomic) IBOutlet UIView *tableviewHeaderView;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewShipment;
@property (weak, nonatomic) IBOutlet UIButton *btnAddphoto;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UILabel *pickerTitle;
@property (weak, nonatomic) IBOutlet UIView *viewPickerBox;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintPickerViewTop;
@property (strong, nonatomic) IBOutlet UILabel *labelDeliveryFeeText;
@property (strong, nonatomic) IBOutlet UILabel *labelDeliveryFee;
@property (strong, nonatomic) IBOutlet UILabel *labelSendToCustomerText;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHorizontallyCenterSendToCustomerLabel;
@property (strong, nonatomic) IBOutlet UILabel *labelAddPhotos;



@end
