//
//  AddressManageViewController.m
//  Uberx
//
//  Created by Rahul Sharma on 19/09/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//



#import "AddressManageViewController.h"
#import "CustomNavigationBar.h"
#import "XDKAirMenuController.h"
#import "Database.h"
#import "Addresscell.h"
#import "UserAddress.h"
#import "PickUpAddressFromMapController.h"
#import "LanguageManager.h"

@interface AddressManageViewController ()<CustomNavigationBarDelegate>
{
    UserAddress *userAddressFromDB;
    NSInteger value;
    BOOL addNewAddressClicked;
    NSInteger selectedIndex;
    NSMutableArray *addressArray;
    NSString *patientEmail;
    NSInteger identity;

}

@end

@implementation AddressManageViewController

@synthesize getAddress,manageAddressTableView;

#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];

//    self.manageAddressTableView.frame = CGRectMake(0, 126, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-126);
    self.view.backgroundColor = [UIColor whiteColor];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    patientEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
   
    addNewAddressClicked = NO;
    if(_isFromRecepientVC == YES)
        identity = 1;
    else
        identity = 0;

    addressArray = [[NSMutableArray alloc] initWithArray:[Database getParticularUserAddressDetails:patientEmail and:[NSNumber numberWithInteger:identity]]];
    
    [manageAddressTableView reloadData];
    
     [self addCustomNavigationBar];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
    if(addNewAddressClicked == NO){
        
        _isFromHomeVC = NO;
        _isFromRecepientVC = NO;
    }
    
}

#pragma mark -tableview Delegates-

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return addressArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *tableidentifier = @"adressManageCell";
    
    Addresscell *cell = (Addresscell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
    [cell.removeAddressButton setTag:indexPath.row + 100];
    
    [cell.removeAddressButton addTarget:self action:@selector(removeAddressButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.addressLabel1.textAlignment = NSTextAlignmentNatural;
    cell.addressLabel2.textAlignment = NSTextAlignmentNatural;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    userAddressFromDB =  addressArray[indexPath.row];
    if(userAddressFromDB.flatNumber.length == 0){
        
        [cell.addressLabel1 setText:[NSString stringWithFormat:@"%@",userAddressFromDB.addressLine1]];
    }
    else
        [cell.addressLabel1 setText:[NSString stringWithFormat:@"House No:%@, %@",userAddressFromDB.flatNumber,userAddressFromDB.addressLine1]];
    [cell.addressLabel2 setText:[NSString stringWithFormat:@"%@",userAddressFromDB.tagAddress]];
   
//    float height1 = [self measureHeightLabel:cell.addressLabel1];
//    float height2 = [self measureHeightLabel:cell.addressLabel2];
//    
//    CGRect frameLabel1 = cell.addressLabel1.frame;
//    frameLabel1.size.height = height1;
//    cell.addressLabel1.frame = frameLabel1;
//    
//    CGRect frameLabel2 = cell.addressLabel2.frame;
//    frameLabel2.origin.y = height1+5;    
//    frameLabel2.size.height = height2;
//    cell.addressLabel2.frame = frameLabel2;
//    
//    CGRect frameCrossButton = cell.removeAddressButton.frame;
//    frameCrossButton.origin.y = (height1+5+height2+5)/2 - 30/2;
//    cell.removeAddressButton.frame = frameCrossButton;
//    
//    CGRect framePin = cell.addressPin.frame;
//    framePin.origin.y = (height1+height2-10)/2;
//    cell.addressPin.frame = framePin;
//    
//    CGRect frameRemove = cell.removeAddressButton.frame;
//    frameRemove.origin.y = (height1+height2-20)/2;
//    cell.removeAddressButton.frame = frameRemove;
//    
//    CGRect frameBackView = cell.backgroundView.frame;
//    frameBackView.size.height = height1+height2+11;
//    cell.backgroundView.frame = frameBackView;
//    [cell bringSubviewToFront:cell.removeAddressButton];
//    
//    CGRect frameDivider = cell.cellDivider.frame;
//    frameDivider.origin.y = height1+height2+10;
//    cell.cellDivider.frame = frameDivider;

    return cell;
}

- (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(label.frame.size.width  , 9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    if (requiredHeight.size.width > label.frame.size.width)
    {
        requiredHeight = CGRectMake(0,0, label.frame.size.width, requiredHeight.size.height);
    }
    CGRect newFrame = label.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSMutableArray *array;
    
        
    array = [[NSMutableArray alloc] initWithArray:[Database getParticularUserAddressDetails:patientEmail and:[NSNumber numberWithInteger:identity]]];
    
    userAddressFromDB  = [array objectAtIndex:indexPath.row];
    
    NSDictionary *dict = @{
                           @"address1":userAddressFromDB.addressLine1,
                           @"tagAddress":userAddressFromDB.tagAddress,
                           @"flatNumber":userAddressFromDB.flatNumber,
                           @"latitude":userAddressFromDB.latitude,
                           @"longitude":userAddressFromDB.longitude,
                           @"city":userAddressFromDB.city,
                         };
    
    if (_isComingFromVC == 1) {
        
        [_addressManageDelegate getAddressFromDatabase:dict];
        [self leftBarButtonClicked:nil];
        
    }
}


#pragma mark -
#pragma mark -alert view delegates-

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 40)
    {
        if (buttonIndex == 0)
        {
            
        }
        else
        {
            userAddressFromDB  = addressArray[selectedIndex];
            
            [Database DeleteAddress: userAddressFromDB.randomNumber];
            
            addressArray = [[NSMutableArray alloc] initWithArray:[Database getParticularUserAddressDetails:patientEmail and:[NSNumber numberWithInteger:identity]]];
            
            [manageAddressTableView reloadData];
        }
    }
}
/*-------------------------------- Navigation Bar ----------------------------------*/
#pragma mark -
#pragma mark -Navigation Bar Methods-

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"back_btn_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"back_btn_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    
   // NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [navCancelButton setFrame:CGRectMake(ScreenWidth - 40,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    }

    
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backButtonAction {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    if(_isFromRecepientVC == YES){
        
        [customNavigationBarView setTitle:(addressArray.count>1 ? @"Recipent Address":@"RECIPIENT ADDRESS")];
        
    }
    else{
        
        [customNavigationBarView setTitle:(addressArray.count>1 ? @"FAVOURITES ADDRESS":@"FAVOURITE ADDRESS")];
        
    }
    
    [customNavigationBarView hideRightBarButton:YES];
    
    if (_isComingFromVC == 1) {
        UIImage *buttonImageOn = [UIImage imageNamed:@"signin_back_icon_on"];
        UIImage *buttonImageOff = [UIImage imageNamed:@"signin_back_icon_off"];
        [customNavigationBarView hideLeftMenuButton:YES];
        [customNavigationBarView setLeftBarButtonTitle:@""];
        [customNavigationBarView setleftBarButtonImage:buttonImageOn :buttonImageOff];
    }
    
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    if (_isComingFromVC == 1) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else {
        
        [self menuButtonclicked];
        
    }
    
}

- (void)menuButtonclicked
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

#pragma mark-
-(void)removeAddressButtonAction:(id)sender
{
    
    
    UIButton *mBtn = (UIButton *)sender;
    selectedIndex = mBtn.tag - 100;
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Remove Address?", @"Remove Address?") message:@"Are you sure you want to remove this address ?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alert.tag=40;
    [alert show];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"toPickUpAddress"])
    {
        PickUpAddressFromMapController *obj1 = (PickUpAddressFromMapController *)[segue destinationViewController];
        obj1.isFromHomeVC = _isFromHomeVC;
        obj1.isFromRecepientVC = _isFromRecepientVC;
        
    }
}

- (IBAction)addNewAddressButtonAction:(id)sender
{
    addNewAddressClicked = YES;
    [self performSegueWithIdentifier:@"toPickUpAddress" sender:sender];
}
@end

