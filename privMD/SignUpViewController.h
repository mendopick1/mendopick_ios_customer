//
//  SignUpViewController.h
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadFiles.h"

@interface SignUpViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,CLLocationManagerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIScrollViewDelegate>
{
    NSMutableArray *array;
    BOOL checkSignupCredentials;
    BOOL isTnCButtonSelected;
    
}


@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *phoneNoTextField;
@property (strong, nonatomic) IBOutlet UIButton *profileButton;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UIButton *tncCheckButton;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet UIButton *tncButton;
@property (weak, nonatomic) IBOutlet UIButton *countryCodeButton;
@property (weak, nonatomic) IBOutlet UIButton *countryCodeImageButton;
@property (weak, nonatomic) IBOutlet UIImageView *countryCodeImageView;
@property (weak, nonatomic) IBOutlet UITextField *referalTextfield;
@property (weak, nonatomic) IBOutlet UILabel *labelConfirmPassword;

@property (weak, nonatomic) IBOutlet UIImageView *outerImageview;

@property (nonatomic, strong) NSMutableArray *helperCountry;
@property (nonatomic, strong) NSMutableArray *helperCity;
@property (strong, nonatomic) UIButton *navNextButton;
@property (strong, nonatomic) UIImage *pickedImage;
@property (strong, nonatomic) NSArray *saveSignUpDetails;
@property (strong,nonatomic)  UITextField *activeTextField;

@property  BOOL isFromTandC;
@property  BOOL isFromCountryCode;




- (IBAction)countryCodeButtonClickedAction:(id)sender;

- (IBAction)profileButtonClicked:(id)sender;
- (IBAction)checkButtonClicked:(id)sender;
- (IBAction)signUpButtonClickedAction:(id)sender;
- (IBAction)TermsNconButtonClicked:(id)sender;

@end
