//
//  BookingHistoryTableViewCell.m
//  MenDoPick
//
//  Created by Rahul Sharma on 28/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "BookingHistoryTableViewCell.h"

@implementation BookingHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [Helper setCornerRadius:4.0 bordercolor:UIColorFromRGB(0xD2D2D2) withBorderWidth:1.0 toview:_viewContent];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)loadCellData:(NSDictionary *)shipmentValue
{
     [Helper setCornerRadius:0.0 bordercolor:UIColorFromRGB(0xD43E30) withBorderWidth:0.7 toview:_viewBox];
    self.labelShipmentValue.text = [PatientGetLocalCurrency getCurrencyLocal:[shipmentValue[@"shipment_details"][0][@"shipementValue"] floatValue]];
    
    self.labelReceiverName.text = shipmentValue[@"shipment_details"][0][@"name"];
    self.labelDeliveryFee.text = [PatientGetLocalCurrency getCurrencyLocal:[shipmentValue[@"shipment_details"][0][@"ApproxFare"] floatValue]];
    
    NSString *str = LS(@"Job Id");
    self.labelBid.text = [NSString stringWithFormat:@"%@: %@",str,shipmentValue[@"bid"]];
    
    NSString *myString = shipmentValue[@"apntDate"];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"dd MMM yyyy";
    
    self.labelDate.text = [dateFormatter stringFromDate:yourDate];
    
    
    if ([shipmentValue[@"statCode"] integerValue] == kNotificationTypeBookingComplete)
    {
        [Helper setCornerRadius:0.0 bordercolor:UIColorFromRGB(0xffd740) withBorderWidth:0.7 toview:_viewBox];

        _labelStatus.textColor = UIColorFromRGB(0xffd740);
        _labelStatus.text = LS(@"Completed");
    }
    else if ([shipmentValue[@"statCode"] integerValue] == kNotificationTypeBookingCancelled || [shipmentValue[@"statCode"] integerValue] == kNotificationTypeBookingReject  || [shipmentValue[@"statCode"] integerValue] == kNotificationTypeBookingCancel)
    {
        [Helper setCornerRadius:0.0 bordercolor:UIColorFromRGB(0xd53e30) withBorderWidth:0.7 toview:_viewBox];
        
        _labelStatus.textColor = UIColorFromRGB(0xd53e30);

        _labelStatus.text = LS(@"Cancelled");
        
    }
    else
    {
        _labelStatus.text = LS(@"On going");
    }
    
   
    
}



@end
