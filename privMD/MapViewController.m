
//  MapViewController.m
//  DoctorMapModule
//
//  Created by Rahul Sharma on 03/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "MapViewController.h"
#import "XDKAirMenuController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "PatientAppDelegate.h"
#import "CustomNavigationBar.h"
#import "NetworkHandler.h"
#import <AFNetworking.h>
#import "LocationServicesMessageVC.h"
#import "DirectionService.h"
#import "PatientViewController.h"
#import "User.h"
#import "AddressManageViewController.h"
#import "PickUpAddressFromMapController.h"
#import "Update&SubscribeToPubNub.h"
#import "PickUpViewController.h"
#import "MyAppTimerClass.h"
#import "RateViewController.h"
#import "LiveBookingStatusUpdatingClass.h"
#import "ShipmentAddViewController.h"
#import "LanguageManager.h"
#import "AnimationsWrapperClass.h"
#import "RateCard.h"

//Define Constants tag for my view
#define myProgressTag  5001
#define mapZoomLevel 16
#define pickupAddressTag 2500
#define addProgressViewTag 6000
#define msgLabelTag 7004

#define myCustomMarkerTag 5000
#define curLocImgTag 5001
#define driverArrivedViewTag 3000
#define driverMessageViewTag 3001

#define bottomViewWithVehicleTag 106
#define topViewTag 90

#define bottomViewTag 4
#define bookNowButtonTag 5
#define bookLaterButtonTag 6
#define detailsViewTag 300
#define statusTopViewTag 70
#define rightArrowTag 7
#define leftArrowTag 8

//Localization strings
#define timeLabelForLocalization NSLocalizedString(@"Time :", @"Time :")
#define distanceLabelForLocalization NSLocalizedString(@"Distance :", @"Distance :")
#define NoDriver NSLocalizedString(@"No Drivers", @"No Drivers")
#define SetPickupLocation NSLocalizedString(@"Set Pickup Location",@"Set Pickup Location")
#define Location NSLocalizedString(@"Location",@"Location")
#define ConfirmPickupLocation NSLocalizedString(@"Confirm Pickup Location",@"Confirm Pickup Location")
#define kDescriptionPlaceholder @"Please add Comment here..."


static MapViewController *sharedInstance = nil;

typedef enum
{
    isChanging = 0,
    isFixed = 1
}locationchange;

@interface CoordsList : NSObject
@property(nonatomic, readonly, copy) GMSPath *path;
@property(nonatomic, readonly) NSUInteger target;

- (id)initWithPath:(GMSPath *)path;

- (CLLocationCoordinate2D)next;

@end

@implementation CoordsList

- (id)initWithPath:(GMSPath *)path {
    if ((self = [super init])) {
        _path = [path copy];
        _target = 0;
    }
    return self;
}

- (CLLocationCoordinate2D)next {
    ++_target;
    if (_target == [_path count]) {
        _target = 0;
    }
    return [_path coordinateAtIndex:_target];
}

@end


@interface MapViewController ()<CLLocationManagerDelegate,CustomNavigationBarDelegate,UIScrollViewDelegate,UserDelegate,AddressManageDelegate,pickupAddressFromMap,updateAndSubscribeToPubNubDelegate,UITextViewDelegate,UIGestureRecognizerDelegate>
{
    MapViewController *mapVC;
    GMSMapView *mapView_;
    GMSGeocoder *geocoder_;
    LiveBookingStatusUpdatingClass *liveBookingStatusUpdateVC;
    float desLat;
    float desLong;
    NSString *desAddr;
    NSString *desAddrline2;
    float srcLat;
    float srcLong;
    NSString *srcAddr;
    NSString *srcAddrline2;
    
    //Path Plotting Variables
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    float newLat;
    float newLong;
    double distanceOfClosetCar;
    NSInteger carTypesArrayCountValue;
    NSMutableArray *arrayOfMapIcons;
    NSMutableArray *arrayOfMapImages;
    NSMutableArray *allHorizontalData;
    UIScrollView *scroller;
    NSMutableArray *arrayOfButtons;
    NSInteger vehicleTypeTag;
    NSMutableArray *arrayOfImages;
    NSMutableArray *arrayOfVehicleIcons;
    Update_SubscribeToPubNub *Update_SubscribeToPubNubSharedObject;
    CGRect screenSize;
    UIButton *leftArrow;
    UIButton *rightArrow;
    BOOL laterBooking;
    NSString *laterBookingDate;
    PatientAppDelegate *appdelegate;
    NSArray *notRatedBookings;
    NSInteger ratingCounts;
    UIWindow *window;
    UITextView *activeTextView;
    NSString *ratingEmail;
    NSString *ratingAppointmentDate;
    BOOL isRatingShowing;
    NSString *time;
    UIView *navigationView;
    RateCard *rateviewPopUp;

}


@property(nonatomic,strong) NSString *laterSelectedDateServer;
@property(nonatomic,strong) CustomNavigationBar *customNavigationBarView;
@property(nonatomic,strong) NSString *patientPubNubChannel;
@property(nonatomic,strong) NSString *serverPubNubChannel;
@property(nonatomic,strong) NSString *patientEmail;
@property(nonatomic,strong) NSMutableDictionary *allMarkers;
@property(nonatomic,strong) WildcardGestureRecognizer * tapInterceptor;
@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;
@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,assign) BOOL isUpdatedLocation;
@property(nonatomic,strong) UIView *PikUpViewForConfirmScreen;
@property(nonatomic,strong) UIView *DropOffViewForConfirmScreen;
@property(nonatomic,strong) NSString *subscribedChannel;

//Path Plotting Variables
@property(nonatomic,assign)BOOL isPathPlotted;
@property(nonatomic,assign)CLLocationCoordinate2D previouCoord;
@property(nonatomic,strong)GMSMarker *destinationMarker;
@property(nonatomic,strong)NSString *startLocation;
@property(nonatomic,assign) double driverCurLat;
@property(nonatomic,assign) double driverCurLong;
@property(nonatomic,strong) NSMutableArray *arrayOFDriverChannels;
@property(nonatomic,strong) NSMutableArray *arrayOfDriverEmail;
@property(nonatomic,strong) NSMutableArray *arrayOfVehicleTypes;
@property(nonatomic,strong) NSMutableArray *arrayOfCarColor;
@property(nonatomic,strong) NSMutableArray *arrayOfDriversAround;
@property(nonatomic,strong) NSMutableArray *arrayOfpickerData;
@property(nonatomic,strong) UIView *spinnerView;
@property (nonatomic,strong)  NSTimer *timer;


@property(nonatomic,assign)BOOL isAddressManuallyPicked;

@end

static int isLocationChanged;

@implementation MapViewController

@synthesize drivers;
@synthesize dictSelectedDoctor;
@synthesize PikUpViewForConfirmScreen;
@synthesize DropOffViewForConfirmScreen;
@synthesize customNavigationBarView;
@synthesize subscribedChannel;
@synthesize datePicker;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
    }
    return self;
}


+ (instancetype) getSharedInstance
{
    return sharedInstance;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    screenSize = [[UIScreen mainScreen] bounds];
    CGRect frame =   _currentLocationButton.frame;
    frame.origin.y = screenSize.size.height - 170;
    _currentLocationButton.frame = frame;
    
    arrayOfMapIcons = [[NSMutableArray alloc]init];
    arrayOfMapImages = [[NSMutableArray alloc]init];
    
    liveBookingStatusUpdateVC = [LiveBookingStatusUpdatingClass sharedInstance];
    
    newLat = [[[NSUserDefaults standardUserDefaults]objectForKey:KNUCurrentLat]floatValue];
    newLong = [[[NSUserDefaults standardUserDefaults]objectForKey:KNUCurrentLong]floatValue];
    
    isNowSelected = YES;
    isLaterSelected = NO;
    
    NSUserDefaults *ud =   [NSUserDefaults standardUserDefaults];
    srcLat = [[ud objectForKey:KNUCurrentLat] doubleValue];
    srcLong = [[ud objectForKey:KNUCurrentLong] doubleValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:srcLat longitude:srcLong zoom:mapZoomLevel];
    
    UIView *customMapView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenSize.size.width, screenSize.size.height)];
    CGRect rectMap = customMapView.frame;
    
    mapView_ = [GMSMapView mapWithFrame:rectMap camera:camera];
    mapView_.settings.compassButton = YES;
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    [mapView_ clear];
    customMapView = mapView_;
    geocoder_ = [[GMSGeocoder alloc] init];
    [self.view addSubview:customMapView];
    
    [self addCustomNavigationBar];
    
    [self changeContentOfPresentController:nil];
    
    
    window =  [(PatientAppDelegate *)[[UIApplication sharedApplication] delegate]window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout:) name:@"LOGOUT" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesChanged:) name:kNotificationLocationServicesChangedNameKey object:nil];
    [self regiesterForNotification];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isFromMyOrder"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(void)regiesterForNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subscribePubNub:) name:@"notificationToSubscribe" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unSubscribePubNub:) name:@"notificationToUnSubscribe" object:nil];
}

-(void)deRegiesterForNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"notificationToSubscribe" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"notificationToUnSubscribe" object:nil];
}


-(void)subscribePubNub:(NSNotification *)notice
{
    Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
    [Update_SubscribeToPubNubSharedObject subscribeToPassengerChannel];
    Update_SubscribeToPubNubSharedObject.delegate = self;
}

-(void)unSubscribePubNub:(NSNotification *)notice
{
    [_timer invalidate];
    Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
    [Update_SubscribeToPubNubSharedObject unSubsCribeToPubNubChannel:[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientPubNubChannelkey]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"promocodeValid"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"enteredPromocode"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"notesEntered"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"additionalNotes"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"paymentSelected"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"selectedPayment"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Ask for My Location data after the map has already been added to the UI.
    self.view.backgroundColor = [UIColor clearColor];
    sharedInstance = self;
    _patientEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey];
    _patientPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientPubNubChannelkey];
    _serverPubNubChannel = kPMDPublishStreamChannel;
    laterBooking = NO;
   // withDispatch = NO;
    
   // [[TELogger getInstance] initiateFileLogging];
    
    Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
    [Update_SubscribeToPubNubSharedObject subscribeToPassengerChannel];
    Update_SubscribeToPubNubSharedObject.delegate = self;
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.mapViewContoller = self;
    
    
    _timer = [NSTimer
              scheduledTimerWithTimeInterval:5 target:self
              selector:@selector(publishPubNubStream)
              userInfo:nil
              repeats:YES];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
    });
    
    self.navigationController.navigationBarHidden = YES;
    [self.view sendSubviewToBack:_datePickerView];
     _datePickerView.hidden = YES;
    [self sendRequestForReviewStatus];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self deRegiesterForNotification];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFromHomeVC"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [_allMarkers removeAllObjects];
    [mapView_ clear];
    if (_arrayOFDriverChannels)
    {
        [_arrayOFDriverChannels removeAllObjects] ;
        _arrayOFDriverChannels = nil;
    }
    [self.timer invalidate];
    self.timer = nil;
}

-(void) sendRequestForReviewStatus {
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"appointmnet_Reviwed_Ack"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                     //  TELogInfo(@"response %@",response);
                                       [self parseToGetReviewNotDoneDetails:response];
                                   }
                               }];
}




-(void)parseToGetReviewNotDoneDetails:(NSDictionary *)response{
    
    if(!response){
        return;
    }
    else  if ([[response objectForKey:@"errFlag"] integerValue] == 1) {
         
    
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 7)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:@"Message" Message:[response objectForKey:@"errMsg"]];
        
    }
    else  if ([response[@"errFlag"] integerValue] == 0 && [response[@"errNum"] integerValue] == 227 ) {
        
        RateViewController *invoiceVC = [self.storyboard instantiateViewControllerWithIdentifier:@"rateVC"];
        invoiceVC.customerEmail = response[@"data"][@"email"];
        invoiceVC.appointmentDate = response[@"data"][@"appointment_dt"];
        self.definesPresentationContext = YES;
        invoiceVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
        invoiceVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
       // invoiceVC.view.superview.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-40, [UIScreen mainScreen].bounds.size.height-30); //it's important to do this after presentModalViewController
        invoiceVC.view.superview.center = self.view.center;

        [self presentViewController:invoiceVC animated:YES completion:nil];
    }
}

/**
 *  Stop the receiving data from pubnub server
 */
- (void) viewDidAppear:(BOOL)animated{
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFromHomeVC"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self addgestureToMap];
    
    if (IS_SIMULATOR)
    {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord zoom:14];
        
    }
}

#pragma mark - UserDelegate

-(void)logout:(NSNotification *)notification
{
    User *user = [User alloc];
    user.delegate = self;
    [user logout];
}

-(void)userDidLogoutSucessfully:(BOOL)sucess{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
}

-(void)userDidFailedToLogout:(NSError *)error{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
}



-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    XDKAirMenuController *controller = [XDKAirMenuController sharedMenu];
    if (controller.isMenuOpened) {
        [controller closeMenuAnimated];
    }
    return YES;
}

-(void)changeContentOfPresentController:(NSNotification *)notification {
    
    [mapView_ animateToViewingAngle:0];
    [self addCustomLocationView];
    [self createCenterView];
    [self getCurrentLocationFromGPS];
    NSMutableArray *carTypes = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:KUBERCarArrayKey]];
    
    _arrayOfVehicleTypes = [carTypes mutableCopy];
    
    if (_arrayOfVehicleTypes.count > 0)
    {
        [self addBottomView];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)checkDifferenceBetweenTwoLocationAndUpdateCache {
    
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:newLat longitude:newLong];
    
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:srcLat longitude:srcLong];
    
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    
    if (distance > 50000) {
        
        isComingTocheckCarTypes = NO;
        [mapView_ clear];
        newLat = srcLat;
        newLong = srcLong;
    } //what if he changes his city
    
    
}


-(void)publishPubNubStream
{
    if(_timer == nil)
    {
        return;
    }
    NSString *dt = @"";
    NSInteger tp = vehicleTypeTag;
    
    NSDictionary *message = @{
                              
                              @"a": @"1",
                              @"pid": _patientEmail,
                              @"chn": _patientPubNubChannel,
                              @"lt": [NSNumber numberWithFloat:srcLat],
                              @"lg":[NSNumber numberWithFloat:srcLong],
                              @"st": @"3",
                              @"tp": [NSNumber numberWithInteger:tp],
                              @"dt": dt
                              
                              };
    
    Update_SubscribeToPubNubSharedObject = [Update_SubscribeToPubNub sharedInstance];
    
    
    if (!(srcLat == 0 || srcLong == 0))
    {
        [Update_SubscribeToPubNubSharedObject publishPubNubStream:message];
        
    }
}




-(void)messageA2ComeshereWithFlag1:(NSDictionary *)messageDict {
    
    [mapView_ clear];
    // [self unSubsCribeToPubNubChannels:_arrayOFDriverChannels];
    [_arrayOFDriverChannels removeAllObjects] ;
    _arrayOFDriverChannels = nil;
    [_arrayOfDriverEmail removeAllObjects] ;
    _arrayOfDriverEmail = nil;
    [_allMarkers removeAllObjects] ;
    _allMarkers = nil;
    [self hideAcitvityIndicator];
    //    [self hideActivityIndicatorWithMessage];
    _myAppTimerClassObj = [MyAppTimerClass sharedInstance];
    [_myAppTimerClassObj stopSpinTimer];
    
}


-(void)messageA2ForTesting:(NSDictionary *)messageDict
{
    
    if (!_arrayOFDriverChannels){ //|| ![_arrayOfDriversAround isEqualToArray: messageDict[@"masArr"]]) {
        
        _arrayOFDriverChannels = [[NSMutableArray alloc] init];
        _arrayOfDriversAround = messageDict[@"masArr"];
        for (int masArrIndex = 0;masArrIndex < _arrayOfDriversAround.count; masArrIndex++) {
            
            NSArray *channelsData = _arrayOfDriversAround[masArrIndex][@"mas"];
            for(NSDictionary *dict in channelsData){
                [_arrayOFDriverChannels addObject:dict[@"chn"]];
                
            }
        }
        
        
        for(int i = 0; i< _arrayOfDriversAround.count;i++){
            
            drivers = [[NSMutableArray alloc] init];
            [drivers addObjectsFromArray:_arrayOfDriversAround[i][@"mas"]];
            
            UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
            buttonCategory = [arrayOfButtons objectAtIndex:i];
            
           
            
            if (drivers.count > 0) {
                
                id dis = _arrayOfDriversAround[i][@"mas"][0][@"d"];
                distanceOfClosetCar = [dis floatValue]/kPMDDistanceMetric;
                
                NSString *distance = [NSString stringWithFormat:@"%@\n\n\n\n%@",[NSString stringWithFormat:@"%.2f %@",distanceOfClosetCar, kPMDDistanceParameter],
                                      
                                      [allHorizontalData objectAtIndex:i]];
                
                if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
                    [buttonCategory setImageEdgeInsets:UIEdgeInsetsMake(0, -65, 0, 20)];
                }
                
                [Helper setButton:buttonCategory Text:distance WithFont:OpenSans_SemiBold FSize:10 TitleColor:UIColorFromRGB(0x616161) ShadowColor:nil];
                [buttonCategory setTitle:distance forState:UIControlStateSelected];
            }
            else {
                
                [Helper setButton:buttonCategory Text:[NSString stringWithFormat:@"%@\n\n\n\n%@",NoDriver,[allHorizontalData objectAtIndex:i]] WithFont:OpenSans_SemiBold FSize:10 TitleColor:UIColorFromRGB(0x616161) ShadowColor:nil];
                
                if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
                    [buttonCategory setImageEdgeInsets:UIEdgeInsetsMake(0, -85, 0, 20)];
                }

                
                [buttonCategory setTitle:[NSString stringWithFormat:@"%@\n\n\n\n%@",NoDriver,[allHorizontalData objectAtIndex:i]] forState:UIControlStateSelected];
                
            }
            
        }
        
        
        drivers = [[NSMutableArray alloc] init];
        [drivers addObjectsFromArray:_arrayOfDriversAround[vehicleTypeTag-1][@"mas"]];
        
        
        if (drivers.count > 0) {
            
            id dis = _arrayOfDriversAround[vehicleTypeTag-1][@"mas"][0][@"d"];
            [self updateTheNearestDriverinSpinitCircle:dis];
            [self addCustomMarkerFor];
        }
        else {
            [self hideActivityIndicatorWithMessage];
        }
        
        
        
        //[self subsCribeToPubNubChannels:_arrayOFDriverChannels];
    }
    else {
        
        NSArray *arrayOfNewDriversAround = messageDict[@"masArr"];
        NSMutableArray *arrayNewChannels = [[NSMutableArray alloc] init];
        for (int masArrIndex = 0;masArrIndex < arrayOfNewDriversAround.count; masArrIndex++) {
            
            NSArray *channelsData = arrayOfNewDriversAround[masArrIndex][@"mas"];
            for(NSDictionary *dict in channelsData){
                [arrayNewChannels addObject:dict[@"chn"]];
                
            }
        }
        
        for(int i = 0; i< arrayOfNewDriversAround.count;i++){
            
            drivers = [[NSMutableArray alloc] init];
            [drivers addObjectsFromArray:arrayOfNewDriversAround[i][@"mas"]];
            
            UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
            buttonCategory = [arrayOfButtons objectAtIndex:i];
            
            if (drivers.count > 0) {
                
                id dis = arrayOfNewDriversAround[i][@"mas"][0][@"d"];
                distanceOfClosetCar = [dis floatValue]/kPMDDistanceMetric;
                
                NSString *distance = [NSString stringWithFormat:@"%@\n\n\n\n%@",[NSString stringWithFormat:@"%.2f %@",distanceOfClosetCar, kPMDDistanceParameter],
                                      [allHorizontalData objectAtIndex:i]];
                
                [Helper setButton:buttonCategory Text:distance WithFont:OpenSans_SemiBold FSize:10 TitleColor:UIColorFromRGB(0x616161) ShadowColor:nil];
                [buttonCategory setTitle:distance forState:UIControlStateSelected];

                
            }
            else {
                
                [Helper setButton:buttonCategory Text:[NSString stringWithFormat:@"%@\n\n\n\n%@",NoDriver,[allHorizontalData objectAtIndex:i]] WithFont:OpenSans_SemiBold FSize:10 TitleColor:UIColorFromRGB(0x616161) ShadowColor:nil];
                [buttonCategory setTitle:[NSString stringWithFormat:@"%@\n\n\n\n%@",NoDriver,[allHorizontalData objectAtIndex:i]] forState:UIControlStateSelected];
            }
        }
        NSArray *previousDriverDetails = _arrayOfDriversAround;
        _arrayOfDriversAround = [arrayOfNewDriversAround mutableCopy]; //update the array
        NSArray *arr = [[NSArray alloc]initWithArray:_arrayOfDriversAround[vehicleTypeTag-1][@"mas"]];
        if (arr.count > 0) {
            id dis = _arrayOfDriversAround[vehicleTypeTag-1][@"mas"][0][@"d"];
            
                            float dlat = [_arrayOfDriversAround[vehicleTypeTag-1][@"mas"][0][@"lt"]floatValue ];
                            float dlong = [_arrayOfDriversAround[vehicleTypeTag-1][@"mas"][0][@"lg"]floatValue ];
               [self servicesForTime:dlat and:dlong];
            
            [self updateTheNearestDriverinSpinitCircle:dis];
        }
        else {
            [self hideActivityIndicatorWithMessage];
        }
        
        if (_arrayOFDriverChannels.count > arrayNewChannels.count) {
            
            NSMutableArray *channelToRemove = [[NSMutableArray alloc] init];
            //need to remove one channel
            for(NSString *channel in _arrayOFDriverChannels){
                if ([arrayNewChannels containsObject:channel]) {
                    //do nothing
                }
                else {
                    
                    //remove channel
                    [channelToRemove addObject:channel];
                    GMSMarker *marker =  _allMarkers[channel];
                    [self removeMarker:marker];
                    [_allMarkers removeObjectForKey:channel];
                }
            }
            
            //unsubscribe removed channels
            [_arrayOFDriverChannels removeObjectsInArray:channelToRemove];
            // [self unSubsCribeToPubNubChannels:channelToRemove];
        }
        else if (_arrayOFDriverChannels.count < arrayNewChannels.count) {
            //need to add new channel
            
            NSMutableArray *channelsToAdd = [[NSMutableArray alloc] init];
            NSMutableArray *markerToAdd = [[NSMutableArray alloc] init];
            
            for(NSString *channel in arrayNewChannels){
                
                if ([_arrayOFDriverChannels containsObject:channel]) {
                    //do nothing
                }
                else {
                    //add channel and subscribe here
                    [channelsToAdd addObject:channel];
                    [markerToAdd addObjectsFromArray:arrayNewChannels];
                }
            }
            //            if (!drivers) {
            drivers = [[NSMutableArray alloc]init];
            //            }
            [drivers addObjectsFromArray:_arrayOfDriversAround[vehicleTypeTag-1][@"mas"]];
            
            [self addCustomMarkerFor];
            [_arrayOFDriverChannels addObjectsFromArray:channelsToAdd];
            // [self subsCribeToPubNubChannels:channelsToAdd];
        }
        
        if(![previousDriverDetails isEqualToArray: messageDict[@"masArr"]]){
            
            _arrayOFDriverChannels = [[NSMutableArray alloc] init];
            _arrayOfDriversAround = messageDict[@"masArr"];
            for (int masArrIndex = 0;masArrIndex < _arrayOfDriversAround.count; masArrIndex++) {
                
                NSArray *channelsData = _arrayOfDriversAround[masArrIndex][@"mas"];
                for(NSDictionary *dict in channelsData){
                    [_arrayOFDriverChannels addObject:dict[@"chn"]];
                    
                }
            }
            
            
            for(int i = 0; i< _arrayOfDriversAround.count;i++){
                
                drivers = [[NSMutableArray alloc] init];
                [drivers addObjectsFromArray:_arrayOfDriversAround[i][@"mas"]];
                
                UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
                buttonCategory = [arrayOfButtons objectAtIndex:i];
                
                if (drivers.count > 0) {
                    
                    id dis = _arrayOfDriversAround[i][@"mas"][0][@"d"];
                    distanceOfClosetCar = [dis floatValue]/kPMDDistanceMetric;
                    
                    NSString *distance = [NSString stringWithFormat:@"%@\n\n\n\n%@",[NSString stringWithFormat:@"%.2f %@",distanceOfClosetCar, kPMDDistanceParameter],
                                          [allHorizontalData objectAtIndex:i]];
                    
                    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
                        [buttonCategory setImageEdgeInsets:UIEdgeInsetsMake(0, -65, 0, 20)];
                    }

                    
                    [Helper setButton:buttonCategory Text:distance WithFont:OpenSans_SemiBold FSize:10 TitleColor:UIColorFromRGB(0x616161) ShadowColor:nil];
                    [buttonCategory setTitle:distance forState:UIControlStateSelected];
                    
                }
                else {
                    
                    [Helper setButton:buttonCategory Text:[NSString stringWithFormat:@"%@\n\n\n\n%@",NoDriver,[allHorizontalData objectAtIndex:i]] WithFont:OpenSans_SemiBold FSize:10 TitleColor:UIColorFromRGB(0x616161) ShadowColor:nil];
                    
                    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
                        [buttonCategory setImageEdgeInsets:UIEdgeInsetsMake(0, -85, 0, 20)];
                    }

                    
                    [buttonCategory setTitle:[NSString stringWithFormat:@"%@\n\n\n\n%@",NoDriver,[allHorizontalData objectAtIndex:i]] forState:UIControlStateSelected];
                    
                }
                
            }
            drivers = [[NSMutableArray alloc] init];
            [drivers addObjectsFromArray:_arrayOfDriversAround[vehicleTypeTag-1][@"mas"]];
            
            
            if (drivers.count > 0)
            {
                id dis = _arrayOfDriversAround[vehicleTypeTag-1][@"mas"][0][@"d"];
                [self updateTheNearestDriverinSpinitCircle:dis];
                [self addCustomMarkerFor];
            }
            else {
                [self hideActivityIndicatorWithMessage];
            }
            
        }
    }
    
    
}


-(void)servicesForTime:(float)lat and:(float)longi
{
    
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f",lat,longi,_currentLatitude,_currentLongitude];
    NSURL *url = [NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strUrl]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(addressResponse:)];
    
}

- (BOOL)containsKey: (NSString *)key dictionary:(NSDictionary *) dict
{
    BOOL retVal = 0;
    NSArray *allKeys = [dict allKeys];
    retVal = [allKeys containsObject:key];
    if (retVal)
    {
        if ([dict[@"status"] isEqualToString:@"OK"])
        {
            retVal = YES;
        }
        else
        {
            retVal = NO;
        }
    }
    
    return retVal;
}





- (void)addressResponse:(NSDictionary *)response
{
    
    if (!response) {
        
    }else if ([response objectForKey:@"Error"]){
        
    }
    else
    {
        BOOL checkStatus = [self containsKey:@"status" dictionary:response[@"ItemsList"]];
        if (!checkStatus)
        {
            NSString *str = @"Limit Exceed";
             [self showTime:str];
        }
        else
        {
            NSString *distance = response[@"ItemsList"][@"rows"][0][@"elements"][0][@"distance"][@"value"];
            distance = [NSString stringWithFormat:@"%.2f %@",[distance doubleValue]/kPMDDistanceMetric, kPMDDistanceParameter];
            int timeInSec =  [response[@"ItemsList"][@"rows"][0][@"elements"][0][@"duration"][@"value"] intValue];
            int timeInMin = [self convertTimeInMin:timeInSec];
            
             NSString *str = NSLocalizedString(@"Min", @"Min");
            time = [NSString stringWithFormat:@"%d",timeInMin];
            time =[NSString stringWithFormat:@"%d %@",timeInMin,str];
            [self showTime:time];
            
        }
    }

    
}


-(int)convertTimeInMin:(int)timeInSec
{
    
    int min;
    if (timeInSec < 60 && timeInSec <= 0)
    {
        min = 1;
    }
    else if (timeInSec > 60)
    {
        min = timeInSec/60;
        int remainingTime = timeInSec%60;
        
        if (remainingTime < 60 && remainingTime >= 30)
        {
            min++;
        }
    }
    else
    {
        min = 1;
    }
    return min;
    
    
}





-(void)messageA2ComeshereWithDriverEmails:(NSDictionary *)messageDict {
    
    //This will come inside only when the requesting is not happening when the user will request this will stop coming inside so that the array of driver email list will be static
    
    NSArray *emailData = messageDict[@"masArr"][vehicleTypeTag-1][@"mas"];
    
    if(!_arrayOfDriverEmail) { //For the first time when the array in not initialised copy all the data from the array
        _arrayOfDriverEmail = [emailData mutableCopy];
    }
    if (emailData.count != _arrayOfDriverEmail.count) { //when array count from server changes
        
        if(_arrayOfDriverEmail) {
            [_arrayOfDriverEmail removeAllObjects];
            _arrayOfDriverEmail = nil;
            _arrayOfDriverEmail = [emailData mutableCopy];
        }
        else {
            _arrayOfDriverEmail = [emailData mutableCopy];
        }
        
    }
    else if (emailData.count == _arrayOfDriverEmail.count){ //when array count from server is same
        
        if ([emailData isEqualToArray:_arrayOFDriverChannels]) {
            
        }
        else { //if the cotent is not same just copy the new array from the server
            
            [_arrayOfDriverEmail removeAllObjects];
            _arrayOfDriverEmail = nil;
            _arrayOfDriverEmail = [emailData mutableCopy];
            
        }
    }
}

-(void)messageA2ComeshereWithCarTypes:(NSDictionary *)messageDict {
    
    NSArray *carTypesArray = messageDict[@"types"];
    
    if (carTypesArray.count > 0) {
        
        isComingTocheckCarTypes = NO;
        if(!_arrayOfVehicleTypes) { //For the first time when the array in not initialised copy all the data from the array
            _arrayOfVehicleTypes = [carTypesArray mutableCopy];
            
            [[NSUserDefaults standardUserDefaults] setObject:_arrayOfVehicleTypes forKey:KUBERCarArrayKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            for (int count = 0; count<_arrayOfVehicleTypes.count;count++) {
                
                NSString *mapIcon = _arrayOfVehicleTypes[count][@"MapIcon"];
                [arrayOfMapIcons addObject:mapIcon];
                
            }
            
            
            [self handleTap:nil];
            [self addBottomView];
            
        }
        if (carTypesArray.count != _arrayOfVehicleTypes.count) { //when array count from server changes
            
            [_arrayOfVehicleTypes removeAllObjects];
            _arrayOfVehicleTypes = nil;
            
            [arrayOfMapIcons removeAllObjects];
            arrayOfMapIcons = [[NSMutableArray alloc]init];
            
            _arrayOfVehicleTypes = [carTypesArray mutableCopy];
            
            for (int count = 0; count<_arrayOfVehicleTypes.count;count++) {
                
                NSString *mapIcon = _arrayOfVehicleTypes[count][@"MapIcon"];
                [arrayOfMapIcons addObject:mapIcon];
            }
            
            [scroller removeFromSuperview];
            [leftArrow removeFromSuperview];
            [rightArrow removeFromSuperview];
            
            scroller = nil;
            
            [self handleTap:nil];
            [self addBottomView];
            [[NSUserDefaults standardUserDefaults] setObject:_arrayOfVehicleTypes forKey:KUBERCarArrayKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        else if (carTypesArray.count == _arrayOfVehicleTypes.count){ //when array count from server is same
            
            if ([carTypesArray isEqualToArray:_arrayOfVehicleTypes]) {
                
            }
            else { //if the cotent is not same just copy the new array from the server
                
                [_arrayOfVehicleTypes removeAllObjects];
                _arrayOfVehicleTypes = nil;
                
                [arrayOfMapIcons removeAllObjects];
                arrayOfMapIcons = [[NSMutableArray alloc]init];
                
                _arrayOfVehicleTypes = [carTypesArray mutableCopy];
                
                [scroller removeFromSuperview];
                [leftArrow removeFromSuperview];
                [rightArrow removeFromSuperview];
                
                scroller = nil;
                
                for (int count = 0; count<_arrayOfVehicleTypes.count;count++) {
                    NSString *mapIcon = _arrayOfVehicleTypes[count][@"MapIcon"];
                    [arrayOfMapIcons addObject:mapIcon];
                }
                
                [self handleTap:nil];
                [self addBottomView];
                [[NSUserDefaults standardUserDefaults] setObject:_arrayOfVehicleTypes forKey:KUBERCarArrayKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        
    }
    else {
        
        
        if (!isComingTocheckCarTypes) {
            
            isComingTocheckCarTypes = YES;
            [_arrayOfVehicleTypes removeAllObjects];
            _arrayOfVehicleTypes = nil;
            
            [arrayOfMapIcons removeAllObjects];
            arrayOfMapIcons = [[NSMutableArray alloc]init];
            [scroller removeFromSuperview];
            [leftArrow removeFromSuperview];
            [rightArrow removeFromSuperview];
            
            UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
            [msgLabel setText:[NSString stringWithFormat:@"No\nVehicles"]];
            
            scroller = nil;
            
            [self handleTap:nil];
            [self addBottomView];
            
        }
        
    }
}

bool isComingTocheckCarTypes;

-(void)recievedMessageWithDictionary:(NSDictionary *)messageDict OnChannel:(NSString *)channelName
{    
    if (![messageDict isKindOfClass:[NSDictionary class]]) {
        
        return;
    }
    if ([messageDict[@"a"] intValue] == 6 ||[messageDict[@"a"] intValue] == 7||[messageDict[@"a"] intValue] == 8 || [messageDict[@"a"] intValue] == 9 ||  [messageDict[@"a"] intValue] == 21 ||[messageDict[@"a"] intValue] == 22 || [messageDict[@"a"] intValue] == 5 || [messageDict[@"a"] intValue] == 11 )
    {
        [liveBookingStatusUpdateVC updateLiveBookingStatusAndShowVC:messageDict];
    }

   else if ([channelName isEqualToString:_patientPubNubChannel] && [messageDict[@"a"] intValue] == 2)  {
        
        if ([[messageDict objectForKey:@"flag"] intValue] == 1) //No channels
        {
            [self messageA2ComeshereWithFlag1:messageDict];
            
            //CarTypes Will come always irresective of the flag Value
            [self messageA2ComeshereWithCarTypes:messageDict];
            
        }
        else if ([[messageDict objectForKey:@"flag"] intValue] == 0) //success
        {
            //CarTypes Will come always irresective of the flag Value
            [self messageA2ComeshereWithCarTypes:messageDict];
            
            [self messageA2ComeshereWithDriverEmails:messageDict];
            
            [self messageA2ForTesting:messageDict];
            
        }
    }
    
    else if ([messageDict[@"a"] integerValue] == 4){ //getting drivers from pubnub
        
        
        if ([messageDict[@"tp"] integerValue] == vehicleTypeTag) {
            drivers = [NSMutableArray arrayWithObject:messageDict];
            [self addCustomMarkerFor];
        }
        
    }
    else{
        
    }
}

#pragma Mark- Path Plotting

/**
 *  Update Destination of an incoming Doctor
 *  Returns void and accept two arguments
 *  @param latitude  Doctor Latitude
 *  @param longitude incoming Doctor longitude
 */

-(void)updateDestinationLocationWithLatitude:(float)latitude Longitude:(float)longitude {
    
    if (!_isPathPlotted) {
        
       // TELogInfo(@"pathplotted");
        _isPathPlotted = YES;
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        
        _previouCoord = position;
        
        _destinationMarker = [GMSMarker markerWithPosition:position];
        _destinationMarker.map = mapView_;
        _destinationMarker.flat = YES;
        _destinationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        NSInteger selectedCarColor = [[NSUserDefaults standardUserDefaults]integerForKey:@"selectedindexforcarcolor"];
        _destinationMarker.icon = _arrayOfCarColor[selectedCarColor];
        
        [waypoints_ addObject:_destinationMarker];
        NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",latitude,longitude];
        
        [waypointStrings_ addObject:positionString];
        
        if([waypoints_ count] > 1){
            NSString *sensor = @"false";
            NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                                   nil];
            NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
            NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                              forKeys:keys];
            DirectionService *mds=[[DirectionService alloc] init];
            SEL selector = @selector(addDirections:);
            [mds setDirectionsQuery:query
                       withSelector:selector
                       withDelegate:self];
        }
        
    }
    else {
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        _previouCoord = position;
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        _destinationMarker.position = position;
        [CATransaction commit];
        if (_destinationMarker.flat) {
            _destinationMarker.rotation = heading;
        }
        
    }
}

-(void)setStartLocationCoordinates:(float)latitude Longitude:(float)longitude :(int)type {
    
    //change map camera postion to current location
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:mapZoomLevel];
    [mapView_ setCamera:camera];
    
    
    //add marker at current location
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    marker.map = mapView_;
    [waypoints_ addObject:marker];
    if (type == 2) {
        marker.icon = [UIImage imageNamed:@"default_marker_p"];
    }
    else {
        marker.icon = [UIImage imageNamed:@"default_marker_d"];
    }
    
    //save current location to plot direciton on map
    _startLocation = [NSString stringWithFormat:@"%.6f,%.6f",latitude, longitude];
    [waypointStrings_ insertObject:_startLocation atIndex:0];
    
}
-(void)subscribeOnlyBookedDriver{
    
    /*    here array count is checking app goes background or not
     because at that point of time it content will be zero so get the value from user default
     
     */
    
    if (_arrayOFDriverChannels) {
        
        if (_arrayOFDriverChannels.count > 0) {
            
            NSMutableArray *channelToRemove = [[NSMutableArray alloc] init];
            
            for(NSString *channel in _arrayOFDriverChannels){
                if  ([subscribedChannel isEqualToString:channel]) {
                    //do nothing
                    
                }
                else {
                    [channelToRemove addObject:channel];
                    GMSMarker *marker =  _allMarkers[channel];
                    [self removeMarker:marker];
                    [_allMarkers removeObjectForKey:channel];
                }
            }
            [_arrayOFDriverChannels removeObjectsInArray:channelToRemove];
            // [self unSubsCribeToPubNubChannels:channelToRemove];
            
        }
    }
    else {
        subscribedChannel = [[NSUserDefaults standardUserDefaults] objectForKey:@"subscribedChannel"];
        if (subscribedChannel.length != 0) {
            //[self subsCribeToPubNubChannel:subscribedChannel];
        }
        else {
            // [self clearUserDefaultsAfterBookingCompletion];
        }
        
    }
}
- (void)addDirections:(NSDictionary *)json {
    
    if ([json[@"routes"] count]>0) {
        NSDictionary *routes = [json objectForKey:@"routes"][0];
        
        NSDictionary *route = [routes objectForKey:@"overview_polyline"];
        NSString *overview_route = [route objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        [polyline setStrokeWidth:3.0];
        [polyline setStrokeColor:[UIColor blackColor]];
        polyline.map = mapView_;
    }
    
    
}

- (void)animateToNextCoord:(GMSMarker *)marker {
    
    CoordsList *coords = marker.userData;
    CLLocationCoordinate2D coord = [coords next];
    CLLocationCoordinate2D previous = marker.position;
    
    CLLocationDirection heading = GMSGeometryHeading(previous, coord);
    CLLocationDistance distance = GMSGeometryDistance(previous, coord);
    
    // Use CATransaction to set a custom duration for this animation. By default, changes to the
    // position are already animated, but with a very short default duration. When the animation is
    // complete, trigger another animation step.
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:(distance / (1 * 1000))];  // custom duration, 50km/sec
    
    __weak MapViewController *weakSelf = self;
    [CATransaction setCompletionBlock:^{
        [weakSelf animateToNextCoord:marker];
    }];
    
    marker.position = coord;
    
    [CATransaction commit];
    
    // If this marker is flat, implicitly trigger a change in rotation, which will finish quickly.
    if (marker.flat) {
        marker.rotation = heading;
    }
}

-(void)didFailedToConnectPubNub:(NSError *)error {
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showMessage:@"Subscription failed..." On:self.view];
    [self hideAcitvityIndicator];
}

-(void)addgestureToMap
{
    _tapInterceptor = [[WildcardGestureRecognizer alloc] init];
    __weak typeof(self) weakSelf = self;
    _tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event , int touchtype)
    {
        {
            if (touchtype == 1)
            {
                [weakSelf startAnimation];
            }
            else {
                
                [weakSelf endAnimation];
            }
        }
    };
    
    [mapView_  addGestureRecognizer:_tapInterceptor];
    
}


#pragma mark GMSMapviewDelegate -

- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture{
    
    if(PikUpViewForConfirmScreen && DropOffViewForConfirmScreen)
    {
        isLocationChanged = isFixed;
    }
    else
    {
        if(isFareButtonClicked == NO)
        {
            isLocationChanged = isChanging;
        }
        else
        {
            isFareButtonClicked = NO;
        }
    }
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position{
    
}

- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
    
}
- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    isLocationChanged = isChanging;
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
    if ((bookingStatus == kNotificationTypeBookingOnMyWay) || (bookingStatus == kNotificationTypeBookingReachedLocation)) {
        return;
    }
    else
    {
        CGPoint point1 = mapView_.center;
        CLLocationCoordinate2D coor = [mapView_.projection coordinateForPoint:point1];
        _currentLatitude = coor.latitude;
        _currentLongitude = coor.longitude;
        
        if (_isAddressManuallyPicked)
        {
            _isAddressManuallyPicked = NO;
            return;
        }
        else
        {
            [self getAddress:coor];
            [self startSpinitAgain];
        }
    }
}

#pragma mark Custom Methods -

- (void) addCustomNavigationBar
{
    navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenSize.size.width, 64)];
    navigationView.tag = 78;
    UIImage *buttonImage = [UIImage imageNamed:@"home_mendo_pick_logo"];
    UIImageView * imageb = [[UIImageView alloc] initWithFrame:CGRectMake(screenSize.size.width/2-20,20,37,40)];
    imageb.image = buttonImage;
    [navigationView addSubview:imageb];
    navigationView.backgroundColor = [UIColor whiteColor];
    UIButton*   leftbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftbarButton.frame = CGRectMake(10, 25, 30, 30);
    
//    NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        leftbarButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-40, 25, 30, 30);
    }
    
    [leftbarButton setBackgroundImage:[UIImage imageNamed:@"signin_back_icon_off"] forState:UIControlStateNormal];
    [leftbarButton setBackgroundImage:[UIImage imageNamed:@"signin_back_icon_on"] forState:UIControlStateHighlighted];
    [leftbarButton addTarget:self action:@selector(leftBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navigationView addSubview:leftbarButton];
    [self.view addSubview:navigationView];
    
}

-(void)addCustomMapChangeView
{
    UIView *vb = [self.view viewWithTag:topViewTag];
    NSArray *arr = self.view.subviews;
    
    if (![arr containsObject:vb]) {
        
        UIView *customView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenSize.size.width, 44)];
        UIButton *buttonMapTypeChange = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonMapTypeChange.frame = CGRectMake(270,self.view.frame.size.height-150,45,45);
        [buttonMapTypeChange setBackgroundImage:[UIImage imageNamed:@"map_googlehybrid"] forState:UIControlStateNormal];
        [buttonMapTypeChange setBackgroundImage:[UIImage imageNamed:@"map_googlenormal_on"] forState:UIControlStateSelected];
        [buttonMapTypeChange addTarget:self action:@selector(changeMapType:) forControlEvents:UIControlEventTouchUpInside];
        buttonMapTypeChange.selected = NO;
        [customView addSubview:buttonMapTypeChange];
        [self.view addSubview:buttonMapTypeChange];
    }
}

-(void)changeMapType:(UIButton *)sender{
    
    if (sender.isSelected == NO) {
        sender.selected = YES;
        mapView_.mapType = kGMSTypeSatellite;
    }
    else {
        sender.selected = NO;
        mapView_.mapType = kGMSTypeNormal;
    }
    
}

-(void)addCustomLocationView
{
    UIView *vb = [self.view viewWithTag:topViewTag];
    NSArray *arr = self.view.subviews;
    
    if (![arr containsObject:vb]) {
        
        UIView *customView = [[UIView alloc]initWithFrame:CGRectMake(0,64, screenSize.size.width,50)];
        customView.backgroundColor = [UIColor whiteColor];
        customView.tag = topViewTag;
        
        [customView setHidden:NO];
        customView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_last_white_background"]];
        
        UIImageView *imgStr;
        
        imgStr = [[UIImageView alloc]initWithFrame:CGRectMake(12,14,13, 24)];
        
        if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang])
        {
             imgStr = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 22,14,13, 24)];
        }
        
        imgStr.image =  [UIImage imageNamed:@"home_map_pin_icon"];
        [customView addSubview:imgStr];
        
        [self.view bringSubviewToFront:_currentLocationButton];
        
        [_currentLocationButton addTarget:self action:@selector(getCurrentLocation:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *buttonAddressTextField = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonAddressTextField.frame = CGRectMake(40,0,screenSize.size.width-40,50);
        
        [customView addSubview:buttonAddressTextField];
        buttonAddressTextField.tag = 91;
        
        UILabel *labelPickUp = [[UILabel alloc]initWithFrame:CGRectMake(0,5,buttonAddressTextField.frame.size.width-40,20)];
        
        [Helper setToLabel:labelPickUp Text:SetPickupLocation WithFont:OpenSans_Regular FSize:12 Color:UIColorFromRGB(0x4c4c4c)];
        labelPickUp.textAlignment = NSTextAlignmentCenter;
        [buttonAddressTextField addSubview:labelPickUp];
        
        
        _textFeildAddress = [[UITextField alloc]initWithFrame:CGRectMake(0,27,buttonAddressTextField.frame.size.width-40,22)];
        [_textFeildAddress setTextColor:UIColorFromRGB(0x1c1c1c)];
        _textFeildAddress.placeholder = Location;
        _textFeildAddress.textAlignment = NSTextAlignmentCenter;
        
        
        if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang])
        {
            _textFeildAddress.textAlignment = NSTextAlignmentRight;
            CGRect tFrame  = _textFeildAddress.frame;
            tFrame.size.width = buttonAddressTextField.frame.size.width-60;
            
            _textFeildAddress.frame = tFrame;
        }

        _textFeildAddress.tag = 101;
        _textFeildAddress.enabled = NO;
        _textFeildAddress.delegate = self;
        
        [buttonAddressTextField addTarget:self action:@selector(pickupLocationAction:) forControlEvents:UIControlEventTouchUpInside];
        _textFeildAddress.font = [UIFont fontWithName:OpenSans_Regular size:15];
        [buttonAddressTextField addSubview:_textFeildAddress];
        
        _textFeildAddress.adjustsFontSizeToFitWidth = YES;
        //  _textFeildAddress.minimumFontSize = 12;
        
        [self.view addSubview:customView];
        
        
        UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0,screenSize.size.height-50, screenSize.size.width,50)];
        bottomView.tag = bottomViewTag;
        
        
        UIButton *PickupNow = [UIButton buttonWithType:UIButtonTypeCustom];
        PickupNow.frame = CGRectMake(0,6,screenSize.size.width,45);
        PickupNow.tag = 2050;
        [PickupNow setBackgroundColor:UIColorFromRGB(0xd53e30)];
        [PickupNow setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateSelected];
        [Helper setButton:PickupNow  Text:ConfirmPickupLocation WithFont: Hind_Bold  FSize:16 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:[UIColor whiteColor]];
        PickupNow.titleLabel.textAlignment = NSTextAlignmentCenter;
        [PickupNow addTarget:self action:@selector(gotoShipmentDetails:) forControlEvents:UIControlEventTouchUpInside];

        [bottomView addSubview:PickupNow];
        
        [self.view addSubview:bottomView];
    }
    
}
-(void)pickUpLaterButtonClicked:(id)sender
{
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self pickupLater];
              //  withDispatch = YES;
                
                
                break;
            }
            case 1:
            {
                  [self pickupLater];
              //  withDispatch = NO;
               
                break;
            }
            default:
                break;
        }
    }
}


-(void)pickupLater
{
    datePicker.minimumDate = [[NSDate date] dateByAddingTimeInterval:60*60];
    [self pickerOpenAnimationMethod];
}


- (IBAction)datePickerDoneButtonAction:(id)sender {
    
    laterBooking = YES;
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"dd"];
    
    NSString *date = [outputFormatter stringFromDate:datePicker.date];
    
    [outputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *selectedDate = [outputFormatter stringFromDate:datePicker.date];
    
    [[NSUserDefaults standardUserDefaults] setObject:selectedDate forKey:@"selectedDate"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSString *suffix;
    int ones = date.integerValue % 10;
    int tens = (date.integerValue/10) % 10;
    
    if (tens ==1) {
        suffix = @"th";
    } else if (ones ==1){
        suffix = @"st";
    } else if (ones ==2){
        suffix = @"nd";
    } else if (ones ==3){
        suffix = @"rd";
    } else {
        suffix = @"th";
    }
    
    
    
    [outputFormatter setDateFormat:@"MMM yyyy"];
    
    NSString *Year = [outputFormatter stringFromDate:datePicker.date];
    
    
    
    [outputFormatter setDateFormat:@"h:mm a"];
    outputFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    NSString *time = [outputFormatter stringFromDate:datePicker.date];
    
    laterBookingDate = [NSString stringWithFormat:@"%@%@ %@ %@",date,suffix,Year,time];
    
    [self pickerCloseAnimationMethod];
    [self gotoShipmentDetails:nil];
    
    
}

- (IBAction)datePickerCancelButtonAction:(id)sender
{
    laterBooking = NO;
  //  withDispatch = NO;
    [self pickerCloseAnimationMethod];
}


-(void)pickerOpenAnimationMethod{
    
//    CGRect frame = self.datePickerView.frame;
//    frame.origin.y = screenSize.size.height - frame.size.height;
//    [UIView animateWithDuration:0.5f animations:^{
//        
//        [self.view bringSubviewToFront:_datePickerView];
//        self.datePickerView.frame = frame;
//        
//    }];
    
    
    [self.view bringSubviewToFront:_datePickerView];
       
    
    [UIView animateWithDuration:0.6f animations:^{
        
        _datepickerBottomConstraint.constant = 0;
        
        [self.datePickerView layoutIfNeeded];
        
    }];
    
    
}

-(void)pickerCloseAnimationMethod{
    
//    CGRect frame = self.datePickerView.frame;
//    frame.origin.y = screenSize.size.height;
//    [UIView animateWithDuration:0.5f animations:^{
//        
//        [self.view bringSubviewToFront:_datePickerView];
//        self.datePickerView.frame = frame;
//        
//    }];
    
    [UIView animateWithDuration:0.6f animations:^{
        
        _datepickerBottomConstraint.constant = -self.view.frame.size.height*0.3;
        
        [self.datePickerView layoutIfNeeded];
        
    }];

    
    
}

-(void)addressButtonClicked:(id)sender{
    
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PickUpAddressFromMapController *toPickupAddressVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"Map1ViewController"];
    toPickupAddressVC.isFromMapVC = YES;
    toPickupAddressVC.pickupAddressDelegate = self;
    toPickupAddressVC.searchString = srcAddr;
    toPickupAddressVC.latitude = srcLat;
    toPickupAddressVC.longitude = srcLong;
    [self.navigationController pushViewController:toPickupAddressVC animated:YES];
    
    
}

-(IBAction)heartButtonClicked:(id)sender{
    
//    PatientAppDelegate *appDelegates = [UIApplication sharedApplication].delegate;
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddressManageViewController *addressVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"AddressManageViewController"];
    
    addressVC.isComingFromVC = 1;
    addressVC.addressManageDelegate = self;
    
  //  UINavigationController *naviVC =(UINavigationController*) appDelegates.window.rootViewController;
    [self.navigationController pushViewController:addressVC animated:YES];
    
}


/**
 *  after the completion of request submission the view in again bring into picture as it was hidden during the time of creating pikupviewcontroller
 */
-(void)requestSubmittedAddthelocationView
{
    UIView *LV = [self.view viewWithTag:topViewTag];
    [LV setHidden:NO];
    CGRect RTEC = LV.frame;
    RTEC.origin.y = 64;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         LV.frame = RTEC;
                     }
                     completion:^(BOOL finished){
                     }];
    [self startAnimation];
    
}

/**
 *  add another view that will open after clicking setpickup location on map
 */


/**
 *  Add thid view for showing current position and a Button to choose pcikup location
 *
 */
-(void)createCenterView
{

    UIView *alreadyContains = [self.view viewWithTag:curLocImgTag];
    NSArray *arr = self.view.subviews;
    
    if (![arr containsObject:alreadyContains]) {
        
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(screenSize.size.width/2 ,(mapView_.frame.size.height/2), 45, 50)];
        img.center = CGPointMake(self.view.frame.size.width  / 2,
                                 self.view.frame.size.height / 2-20);
        img.image = [UIImage imageNamed:@"select_vehicle_map_pin_icon"];
        img.tag = curLocImgTag;
        [img setHidden:NO];
        [self.view addSubview:img];
        img.tag = myCustomMarkerTag;
        _spinnerView = [[UIView alloc] initWithFrame:CGRectMake(5,5, 30, 30)];
        _spinnerView.backgroundColor = [UIColor clearColor];
        _spinnerView.tag = msgLabelTag;
        [img addSubview:_spinnerView];
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(7,5, 20, 20)];
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
        [activityIndicator startAnimating];
        activityIndicator.tag = 200;
        [_spinnerView addSubview:activityIndicator];
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(1,0,35, 30)];
        messageLabel.tag = 100;
        messageLabel.numberOfLines = 2;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.hidden = YES;
        [Helper setToLabel:messageLabel Text:@"" WithFont:Hind_Medium FSize:9 Color:[UIColor whiteColor]];
        [_spinnerView addSubview:messageLabel];
        [self.view addSubview:img];
    }
    else{
        
    }
}

-(void)setPickupButtonClicked:(UIButton *)sender {
    
    if (![sender isSelected]) {
        
        _isAddressManuallyPicked = YES;
        [sender setSelected:YES];
        
    }
    else {
        
        _isAddressManuallyPicked = NO;
        [sender setSelected:NO];
        
    }
}

-(IBAction)gotoShipmentDetails:(id)sender
{
    if(srcAddr.length != 0 && _arrayOfVehicleTypes.count > 0) {
                
        [self addDetailToAppointmentClass];
        
        PickUpViewController *pickup = [self.storyboard instantiateViewControllerWithIdentifier:@"pick"];
        pickup.isComingFromMapVC = YES;
        pickup.latitude =  [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.latitude];
        pickup.longitude = [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.longitude];
        [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                                  subType:kCATransitionFromTop
                                                  forView:self.navigationController.view
                                             timeDuration:0.3];
        
        [self.navigationController pushViewController:pickup animated:NO];
    }
    else {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please select Pick up Address!",@"Please select Pick up Address!")];
    }
}

-(void)addDetailToAppointmentClass
{
    AppointmentLocation *apLocaion = [AppointmentLocation sharedInstance];
    apLocaion.currentLatitude = [NSNumber numberWithDouble:_currentLatitude];
    apLocaion.currentLongitude =  [NSNumber numberWithDouble:_currentLongitude];
    apLocaion.pickupLatitude = [NSNumber numberWithDouble:srcLat];
    apLocaion.pickupLongitude = [NSNumber numberWithDouble:srcLong];
    apLocaion.dropOffLatitude = [NSNumber numberWithDouble:desLat];
    apLocaion.dropOffLongitude = [NSNumber numberWithDouble:desLong];
    apLocaion.srcAddressLine1 = flStrForObj(srcAddr);
    apLocaion.srcAddressLine2 = flStrForObj(srcAddrline2);
    apLocaion.desAddressLine1 = flStrForObj(desAddr);
    apLocaion.desAddressLine2 = flStrForObj(desAddrline2);
    apLocaion.typeID = [_arrayOfVehicleTypes[vehicleTypeTag-1][@"type_id"] integerValue];
}

-(void)startSpinitAgain
{
    _myAppTimerClassObj = [MyAppTimerClass sharedInstance];
    [_myAppTimerClassObj startSpinTimer];
    [self shoActivityIndicator];
    
}
-(void)updateTheNearestDriverinSpinitCircle:(id)disTance {
    
    id dis = disTance;
//    distanceOfClosetCar = [dis floatValue];
//    UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
//    [msgLabel setText:[NSString stringWithFormat:@"%.2f %@",[dis floatValue]/kPMDDistanceMetric, kPMDDistanceParameter]];
    
    NSString *str = [NSString stringWithFormat:@"%.2f %@",[dis floatValue]/kPMDDistanceMetric, kPMDDistanceParameter];
    
    UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonCategory = [arrayOfButtons objectAtIndex:vehicleTypeTag-1];
    [Helper setButton:buttonCategory Text:[NSString stringWithFormat:@"%@\n\n\n\n%@",str,[allHorizontalData objectAtIndex:vehicleTypeTag-1]] WithFont:OpenSans_SemiBold FSize:10 TitleColor:UIColorFromRGB(0x616161) ShadowColor:nil];
    
    [buttonCategory setTitle:[NSString stringWithFormat:@"%@\n\n\n\n%@",str,[allHorizontalData objectAtIndex:vehicleTypeTag-1]] forState:UIControlStateSelected];
    
}

-(void)hideAcitvityIndicator {
    
    UIView *markerView = (UIView *)[self.view viewWithTag:myCustomMarkerTag];
    [markerView setUserInteractionEnabled:YES];
    UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
    UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:200];
    msgLabel.hidden = NO;
    indicatorView.hidden = YES;
}

-(void)shoActivityIndicator {
    
    UIView *markerView = (UIView *)[self.view viewWithTag:myCustomMarkerTag];
    [markerView setUserInteractionEnabled:NO];
    UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
    UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:200];
    msgLabel.hidden = YES;
    indicatorView.hidden = NO;
}

-(void)hideActivityIndicatorWithMessage{
    
    NSArray *arr = _arrayOfDriversAround[vehicleTypeTag-1][@"mas"];
    UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
    
    if (arr.count != 0) {
        
        id dis = _arrayOfDriversAround[vehicleTypeTag-1][@"mas"][0][@"d"];
        [self updateTheNearestDriverinSpinitCircle:dis];
    }
    else {
        
        msgLabel.hidden = NO;
        [msgLabel setText:[NSString stringWithFormat:NoDriver]];
        
        //[mapView_ clear];
        UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonCategory = [arrayOfButtons objectAtIndex:vehicleTypeTag-1];
        [Helper setButton:buttonCategory Text:[NSString stringWithFormat:@"%@\n\n\n\n%@",msgLabel.text,[allHorizontalData objectAtIndex:vehicleTypeTag-1]] WithFont:OpenSans_SemiBold FSize:10 TitleColor:UIColorFromRGB(0x616161) ShadowColor:nil];
        [buttonCategory setTitle:[NSString stringWithFormat:@"%@\n\n\n\n%@",msgLabel.text,[allHorizontalData objectAtIndex:vehicleTypeTag-1]] forState:UIControlStateSelected];
        
        [self hideAcitvityIndicator];
    }
    if (isLaterSelected) {
        [msgLabel setText:[NSString stringWithFormat:@""]];
    }
}

- (void)animateInBottomView
{
    UIView *bottomView1 = [self.view viewWithTag:bottomViewWithVehicleTag];
    CGRect frameTopview1 = bottomView1.frame;
    frameTopview1.origin.y = screenSize.size.height-115;
    
    
    [UIView animateWithDuration:0.6f animations:^{
        bottomView1.frame = frameTopview1;
    }];
    
    UIView *bottomView2 = [self.view viewWithTag:bottomViewTag];
    CGRect frameTopview2 = bottomView2.frame;
    frameTopview2.origin.y = screenSize.size.height-50;
    
    [UIView animateWithDuration:0.6f animations:^{
        bottomView2.frame = frameTopview2;
    }];
    
    UIView *bottomView3 = [self.view viewWithTag:leftArrowTag];
    CGRect frameTopview3 = bottomView3.frame;
    frameTopview3.origin.y = screenSize.size.height-95;
    
    [UIView animateWithDuration:0.6f animations:^{
        bottomView3.frame = frameTopview3;
    }];
    
    UIView *bottomView4 = [self.view viewWithTag:rightArrowTag];
    CGRect frameTopview4 = bottomView4.frame;
    frameTopview4.origin.y = screenSize.size.height-95;
    
    [UIView animateWithDuration:0.6f animations:^{
        bottomView4.frame = frameTopview4;
    }];
    
    
}

- (void)animateOutBottomView
{
    UIView *bottomView1 = [self.view viewWithTag:bottomViewWithVehicleTag];
    CGRect frameTopview1 = bottomView1.frame;
    frameTopview1.origin.y = screenSize.size.height-50;
    
    
    [UIView animateWithDuration:0.6f animations:^{
        bottomView1.frame = frameTopview1;
    }];
    
    UIView *bottomView2 = [self.view viewWithTag:bottomViewTag];
    CGRect frameTopview2 = bottomView2.frame;
    frameTopview2.origin.y = screenSize.size.height+20;
    
    [UIView animateWithDuration:0.6f animations:^{
        bottomView2.frame = frameTopview2;
    }];
    
    UIView *bottomView3 = [self.view viewWithTag:leftArrowTag];
    CGRect frameTopview3 = bottomView3.frame;
    frameTopview3.origin.y = screenSize.size.height-25;
    
    [UIView animateWithDuration:0.6f animations:^{
        bottomView3.frame = frameTopview3;
    }];
    
    UIView *bottomView4 = [self.view viewWithTag:rightArrowTag];
    CGRect frameTopview4 = bottomView4.frame;
    frameTopview4.origin.y = screenSize.size.height-25;
    
    [UIView animateWithDuration:0.6f animations:^{
        bottomView4.frame = frameTopview4;
    }];
    
}





/**
 *  add bottom view on map for the selection of Medical specialist
 */
- (void) addBottomView
{
    
    carTypesArrayCountValue = _arrayOfVehicleTypes.count;
    if (carTypesArrayCountValue == 0) {
        
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:NSLocalizedString(@"There is no car types available in your area.", @"There is no car types available in your area.")];
    }
    else {
        
        NSArray *arr = self.view.subviews;
        
        if (![arr containsObject:scroller]) {
            
            NSDictionary *type_name = [[NSDictionary alloc]init];
            allHorizontalData = [[NSMutableArray alloc]init];
            arrayOfImages = [[NSMutableArray alloc]init];
            arrayOfVehicleIcons = [[NSMutableArray alloc]init];
            
            for(type_name in _arrayOfVehicleTypes)
            {
                UIImageView *images = [[UIImageView alloc]init];
                [allHorizontalData addObject:flStrForObj([type_name objectForKey:@"type_name"])];
                [arrayOfMapIcons addObject:flStrForObj([type_name objectForKey:@"MapIcon"])];
                [arrayOfImages addObject:images];
                UIImageView *images1 = [[UIImageView alloc]init];
                [arrayOfVehicleIcons addObject:images1];
                
            }
            
            if(!scroller){
                
                scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0,screenSize.size.height-115, screenSize.size.width,70)];
                
                if ([[LanguageManager currentLanguageString]isEqualToString:ArabicLang]) {
                   
                    scroller.transform = CGAffineTransformMakeScale(-1, 1);
                }
                
                scroller.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_last_white_background"]];
                scroller.tag = bottomViewWithVehicleTag;
                scroller.delegate = self;
                [scroller setScrollEnabled:YES];
                scroller.hidden = NO;
                scroller.showsHorizontalScrollIndicator = NO;
                [scroller setContentSize:CGSizeMake(allHorizontalData.count*107,70)];
                
                
                leftArrow = [UIButton buttonWithType:UIButtonTypeCustom];
                rightArrow = [UIButton buttonWithType:UIButtonTypeCustom];
                
                leftArrow = [[UIButton alloc]initWithFrame:CGRectMake(6,screenSize.size.height-95,20, 20)];
                rightArrow = [[UIButton alloc]initWithFrame:CGRectMake(screenSize.size.width-26, screenSize.size.height-95, 20, 20)];
                
                if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang])
                {
                    
                    rightArrow = [[UIButton alloc]initWithFrame:CGRectMake(6,screenSize.size.height-95,20, 20)];
                    leftArrow = [[UIButton alloc]initWithFrame:CGRectMake(screenSize.size.width-26, screenSize.size.height-95, 20, 20)];
                    
                }
                
                [self.view addSubview:leftArrow];
                [self.view addSubview:rightArrow];
                
                
                [leftArrow setImage:[UIImage imageNamed:@"home_left_side_back_icon_off"] forState:UIControlStateNormal];
                [leftArrow setImage:[UIImage imageNamed:@"home_left_side_back_icon_on"] forState:UIControlStateHighlighted];
                
                [rightArrow setImage:[UIImage imageNamed:@"home_right_side_next_icon_off"] forState:UIControlStateNormal];
                [rightArrow setImage:[UIImage imageNamed:@"home_right_side_next_icon_on"] forState:UIControlStateHighlighted];
                
                [leftArrow addTarget:self action:@selector(leftArrowClicked) forControlEvents:UIControlEventTouchUpInside];
                
                [rightArrow addTarget:self action:@selector(rightArrowClicked) forControlEvents:UIControlEventTouchUpInside];
                
                rightArrow.tag = rightArrowTag;
                leftArrow.tag = leftArrowTag;
                
                [self.view addSubview:scroller];
                
                
                arrayOfButtons = [[NSMutableArray alloc]init];
                arrayOfMapImages = [[NSMutableArray alloc]init];
                
                for (int i=0; i < _arrayOfVehicleTypes.count; i++)
                {
                    
                    UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
                    buttonCategory.frame = CGRectMake(107*i,0,107,70);
                    
                    buttonCategory.tag=201+i+1;
                    [arrayOfButtons addObject:buttonCategory];
                    
                    if ([[LanguageManager currentLanguageString]isEqualToString:ArabicLang]) {
                        
                        buttonCategory.transform = CGAffineTransformMakeScale(-1, 1);
                    }

                    
                    if (i == 0)
                    {
                        [buttonCategory setSelected:YES];
                        vehicleTypeTag = 1;
                    }
                    
                    buttonCategory.titleLabel.textAlignment = NSTextAlignmentCenter;
                    
                    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang])
                    {
                        buttonCategory.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
                    }
                    else
                    {
                    buttonCategory.titleEdgeInsets = UIEdgeInsetsMake(-3, -55, 0, 0);
                    }
                        buttonCategory.titleLabel.numberOfLines = 10;
                    [Helper setButton:buttonCategory Text:[NSString stringWithFormat:@"%@\n\n\n\n   %@",NoDriver,[NSString stringWithFormat:@"%@",[allHorizontalData objectAtIndex:i]]] WithFont:OpenSans_SemiBold FSize:10 TitleColor:UIColorFromRGB(0x616161) ShadowColor:nil];
                    [buttonCategory setTitleColor:UIColorFromRGB(0xef4836) forState:UIControlStateSelected];
                    
                    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
                        [buttonCategory setImageEdgeInsets:UIEdgeInsetsMake(0, -85, 0, 20)];
                    }
                    else
                    {
                        [buttonCategory setImageEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
                    }
                    
                    [buttonCategory addTarget:self action:@selector(vehicleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                    [scroller addSubview:buttonCategory];
                    [scroller bringSubviewToFront:buttonCategory];
                    
                }
                
                [self downloadUnSelectedVehicleTypeImages];
                [self downloadSelectedVehicleTypeImages];
                [self downloadMapImages];
                [self.view bringSubviewToFront:leftArrow];
                [self.view bringSubviewToFront:rightArrow];
                [leftArrow setHidden:YES];
                if(_arrayOfVehicleTypes.count > 3){
                    [rightArrow setHidden:NO];
                }
                else{
                    [rightArrow setHidden:YES];
                }
                
            }
            
        }
        
    }
    
}


-(void)setDriverTypeButtonContentInsets:(UIButton *)buttonCategory
{
    // the space between the image and text
    //   CGFloat spacing = 6.0;
    
    // lower the text and push it left so it appears centered
    //  below the image
    CGSize imageSize = CGSizeMake(38, 38);//buttonCategory.imageView.image.size;
    buttonCategory.titleEdgeInsets = UIEdgeInsetsMake(0.0, - imageSize.width, 0.0, 0.0); //Bottom: -(imageSize.height + spacing)
    
    // raise the image and push it right so it appears centered
    //  above the text
    CGSize titleSize = [buttonCategory.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: buttonCategory.titleLabel.font}];
    buttonCategory.imageEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, - titleSize.width);// Top: (titleSize.height + spacing)
    
    // increase the content height to avoid clipping
    CGFloat edgeOffset = fabs(titleSize.height - imageSize.height) / 2.0;
    buttonCategory.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0);
}


-(void)leftArrowClicked{
    
    //    NSInteger x = scroller.contentOffset.x;
    //    scroller.contentOffset = CGPointMake(x-108,70);
    
    
}
-(void)rightArrowClicked{
    
    //    NSInteger x = scroller.contentOffset.x;
    //    scroller.contentOffset = CGPointMake(x+108,70);
    
    
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    NSInteger count = (_arrayOfVehicleTypes.count - 3) * 108;
    if(scrollView.contentOffset.x == count){
        
        [rightArrow setHidden:YES];
        [leftArrow setHidden:NO];
        
    }
    else if(scrollView.contentOffset.x == 0){
        
        [rightArrow setHidden:NO];
        [leftArrow setHidden:YES];
    }
    
}

-(void)downloadMapImages{
    
    arrayOfMapImages = [[NSMutableArray alloc]init];
    
    for (int i=0; i < _arrayOfVehicleTypes.count; i++)
    {
        
        NSString *mapImageURL = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,arrayOfMapIcons[i]];
        
        
        
        [arrayOfImages[i] sd_setImageWithURL:[NSURL URLWithString:mapImageURL]
                            placeholderImage:nil
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                       
                                       if(!error)
                                       {
                                           
                                           //                                           image = [self imageWithImage:image scaledToSize:CGSizeMake(15, 34)];
                                           image = [self imageWithImage:image scaledToSize:[self makeSize:image.size fitInSize:CGSizeMake(35, 35)]];
                                           [arrayOfMapImages addObject:image];
                                           
                                       }
                                   }];
        
    }
    
}

-(void)downloadSelectedVehicleTypeImages{
    
    for (int i=0; i < _arrayOfVehicleTypes.count; i++)
    {
        UIButton *buttonCategory = arrayOfButtons[i];
        NSString *selectedVehicleImageURL = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,_arrayOfVehicleTypes[i][@"vehicle_img"]];
        
        
        [arrayOfVehicleIcons[i] sd_setImageWithURL:[NSURL URLWithString:selectedVehicleImageURL]
                                  placeholderImage:nil
                                         completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                             
                                             if(!error)
                                             {
                                                 
                                                 
                                                 image = [self imageWithImage:image scaledToSize:[self makeSize:image.size fitInSize:CGSizeMake(40, 40)]];
                                                 
                                                 [buttonCategory setImage:image forState:UIControlStateSelected];
                                                 
                                             }
                                             
                                         }];
        
    }
    
}
-(void)downloadUnSelectedVehicleTypeImages{
    
    for (int i=0; i < _arrayOfVehicleTypes.count; i++)
    {
        UIButton *buttonCategory = arrayOfButtons[i];
        NSString *unSelectedVehicleImageURL = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,_arrayOfVehicleTypes[i][@"vehicle_img_off"]];
        
        [buttonCategory.imageView sd_setImageWithURL:[NSURL URLWithString:unSelectedVehicleImageURL]
                                    placeholderImage:nil
                                           completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                               
                                               if(!error)
                                               {
                                                   image = [self imageWithImage:image scaledToSize:[self makeSize:image.size fitInSize:CGSizeMake(40, 40)]];
                                                   [buttonCategory setImage:image forState:UIControlStateNormal];
                                                   
                                               }
                                               
                                           }];
        
    }
    
}

- (CGSize)makeSize:(CGSize)originalSize fitInSize:(CGSize)boxSize
{
    float widthScale = 0;
    float heightScale = 0;
    
    widthScale = boxSize.width/originalSize.width;
    heightScale = boxSize.height/originalSize.height;
    
    float scale = MIN(widthScale, heightScale);
    
    CGSize newSize = CGSizeMake(originalSize.width * scale, originalSize.height * scale);
    
    return newSize;
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)vehicleButtonAction:(UIButton *)sender
{
    
    UIButton *mBtn = (UIButton *)sender;
    
    if(mBtn.selected == YES){
        [self handleSingleTap];
    }
    for (UIView *button in mBtn.superview.subviews) {
        if ([button isKindOfClass:[UIButton class]]) {
            [(UIButton *)button setSelected:NO];
        }
    }
    mBtn.selected = YES;
    vehicleTypeTag = mBtn.tag - 201;
    
    [self changeMapMarker:vehicleTypeTag];
}


/**
 *  Scale Label Text
 *
 *  @param  It will scale the text of the Label
 *
 *  @return returns void and accept integer value
 */

-(void)scaleLabelText:(int)labeltag {
    
    UILabel *lbl = (UILabel *)[[self.view viewWithTag:bottomViewWithVehicleTag]viewWithTag:labeltag];
    lbl.font = [UIFont fontWithName:Roboto_Bold size:10];
    lbl.transform = CGAffineTransformScale(lbl.transform, 0.25, 0.25);
    
    [UIView animateWithDuration:1.0 animations:^{
        lbl.transform = CGAffineTransformScale(lbl.transform, 4, 4);
    }];
}

/**
 *  Touch event on car types
 *
 *  @param typeCar <#typeCar description#>
 */

-(void)clearTheMapBeforeChagingTheCarTypes {
    
    //[self unSubsCribeToPubNubChannels:_arrayOFDriverChannels];
    [_arrayOFDriverChannels removeAllObjects];
    [_allMarkers removeAllObjects];
    [_arrayOfDriverEmail removeAllObjects];
    _arrayOfDriverEmail = nil;
    _arrayOFDriverChannels = nil;
    [mapView_ clear];
    
}

-(void)changeMapMarker:(NSInteger)type {
    
    [_allMarkers removeAllObjects];
    [mapView_ clear];
    drivers = [[NSMutableArray alloc] init];
    [drivers addObjectsFromArray:_arrayOfDriversAround[type-1][@"mas"]];
    
    if (drivers.count > 0) {
        
        if (drivers.count > 3) {
            GMSCameraUpdate *zoomCamera = [GMSCameraUpdate zoomTo:mapZoomLevel+1];
            [mapView_ animateWithCameraUpdate:zoomCamera];
            
        }
        else {
            GMSCameraUpdate *zoomCamera = [GMSCameraUpdate zoomTo:mapZoomLevel];
            [mapView_ animateWithCameraUpdate:zoomCamera];
            
        }
        
        id dis = _arrayOfDriversAround[type-1][@"mas"][0][@"d"];
        [self updateTheNearestDriverinSpinitCircle:dis];
        
        [self servicesForTime:[_arrayOfDriversAround[type-1][@"mas"][0][@"lt"] floatValue] and:[_arrayOfDriversAround[type-1][@"mas"][0][@"lg"] floatValue]];
        
        //[self servicesForTime:[_arrayOfDriversAround[type-1][@"mas"][0][@"lt"] floatValue] :[_arrayOfDriversAround[type-1][@"mas"][0][@"lg"] floatValue]];
        [self addCustomMarkerFor];
    }
    else {
        [self hideActivityIndicatorWithMessage];
    }
    
}

-(void)showTime:(NSString *)time1
{
       if (time1) {
        UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
        [msgLabel setText:time1];

    }
    [self hideAcitvityIndicator];
}


- (void)handleSingleTap
{
    NSInteger type_id = [_arrayOfVehicleTypes[vehicleTypeTag-1][@"type_id"]integerValue];
    [self addCustomCarButtonPressedView:type_id];
}

-(void)addCustomCarButtonPressedView:(NSUInteger)typeCar
{
    UIView* coverView = [[UIView alloc] initWithFrame:window.frame];
    coverView.tag = 12345;
    coverView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    [window addSubview:coverView];
    
    UITapGestureRecognizer *removeView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeCoverView)];
    
    [coverView addGestureRecognizer:removeView];

    rateviewPopUp = [RateCard sharedInstance];

    rateviewPopUp.frame = CGRectMake(0, 0, window.frame.size.width - 15, 104);
    rateviewPopUp.center = CGPointMake(window.frame.size.width/2, window.frame.size.height - 180); ;

    NSMutableArray *carTypes = [_arrayOfVehicleTypes mutableCopy];
    NSDictionary *dict = [[NSDictionary alloc]init];
    for(dict in carTypes)
    {
        if(typeCar == [dict[@"type_id"]integerValue])
        {
            break;
        }
    }
    [rateviewPopUp showPopUpWithDictionary:dict];
}

-(void)removeCoverView
{
    UIView *cView = [window viewWithTag:12345];
    if (cView)
    {
        [cView removeFromSuperview];
        [rateviewPopUp removePopUpWithDictionary:nil];
    }
}


/**
 *  This methods get called after calling Handletap method,it brings the main view to its original position
 */
- (void)handleTap:(UITapGestureRecognizer*)recognizer
{
    if(isCustomMarkerSelected == YES)
    {
        isCustomMarkerSelected = NO;
       // [self addCustomNavigationBar];
    }
    
    UIView *v = [self.view viewWithTag:myCustomMarkerTag];
    v.hidden = NO;
    
    UIView *coverView = [self.view viewWithTag:300];
    [coverView removeFromSuperview];
    //    UIView *backView = [self.view.superview viewWithTag:303];
    //    [backView removeFromSuperview];
    
    
    
    UIView *basketBottom = [self.view viewWithTag:499];
    UIView *pickUp = [self.view viewWithTag:150];
    UIView *dropOff = [self.view viewWithTag:160];
    
    
    
    CGRect basketTopFrame = basketBottom.frame;
    basketTopFrame.origin.y = screenSize.size.height;//-basketTopFrame.size.height;
    
    CGRect pickUpTop = pickUp.frame;
    pickUpTop.origin.y = 0;
    
    CGRect dropOffTop = dropOff.frame;
    dropOffTop.origin.y = 0;
    
    UIView *LV = [self.view viewWithTag:topViewTag];
    CGRect RTEC = LV.frame;
    RTEC.origin.y = 64;
    
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         basketBottom.frame = basketTopFrame;
                         pickUp.frame = pickUpTop;
                         dropOff.frame = dropOffTop;
                         LV.frame = RTEC;
                     }
                     completion:^(BOOL finished){
                     }];
    
    [PikUpViewForConfirmScreen removeFromSuperview];
    PikUpViewForConfirmScreen = nil;
    [self minusButtonClicked:nil];
    
    [basketBottom removeFromSuperview];
    // Do Your thing.
    [self closeAnimation];
}

- (void)closeAnimation
{
    [UIView animateWithDuration:0.5f animations:^{
        self.view.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
        UIView *backView = [self.view.superview viewWithTag:303];
        [backView removeFromSuperview];
    }];
}

- (void)closeTransparentView{
    
    UIView *viewToClose = [self.view viewWithTag:50];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    [viewToClose removeFromSuperview];
    
    [UIView commitAnimations];
    
}

- (void) changeCurrentLocation:(NSDictionary *)dictAddress{
    
    NSString *latitude = [dictAddress objectForKey:@"lat"];
    NSString *longitude = [dictAddress objectForKey:@"lng"];
  //  TELogInfo(@"selected lat and longitude :%@ %@",longitude,latitude);
    
    CLLocation *location = [[CLLocation alloc]initWithLatitude:latitude.doubleValue longitude:longitude.doubleValue];
    mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:mapZoomLevel];
    
}

- (void)custimizeMyLocationButton{
    UIButton *myLocationButton = (UIButton*)[[mapView_ subviews] lastObject];
    myLocationButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin;
    CGRect frame = myLocationButton.frame;
    frame.origin.y = -310;
    frame.origin.x = -10;
    myLocationButton.frame = frame;
}

#pragma mark - Animations
- (void)startAnimation{
    
    [self.view endEditing:YES];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    
   // UIView *customNaviagtionBar = [self.view viewWithTag:78];
    CGRect rect2 = navigationView.frame;
    rect2.origin.y = -44;
    navigationView.frame = rect2;
    
   // navigationView.backgroundColor = [UIColor redColor];
    
    if(isCustomMarkerSelected == YES)
    {
        if(PikUpViewForConfirmScreen && DropOffViewForConfirmScreen){
            CGRect rectPick = PikUpViewForConfirmScreen.frame;
            rectPick.origin.y = 20;
            PikUpViewForConfirmScreen.frame = rectPick;
            
            CGRect rectDrop = DropOffViewForConfirmScreen.frame;
            rectDrop.origin.y = 64;
            DropOffViewForConfirmScreen.frame = rectDrop;
        }
        else{
            CGRect rect = PikUpViewForConfirmScreen.frame;
            rect.origin.y = 20;
            PikUpViewForConfirmScreen.frame = rect;
        }
        
        
    }
    else{
        UIView *customLocationView = [self.view viewWithTag:topViewTag];
        CGRect rect = customLocationView.frame;
        rect.origin.y = 20;
        customLocationView.frame = rect;
        
    }
    [UIView commitAnimations];
    
    [self animateOutBottomView];
    
}

- (void)endAnimation{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    
    UIView *customNaviagtionBar = [self.view viewWithTag:78];
    CGRect rect2 = navigationView.frame;
    
    rect2.origin.y = 0;
    navigationView.frame = rect2;
    
    if(isCustomMarkerSelected == YES)
    {
        if(PikUpViewForConfirmScreen && DropOffViewForConfirmScreen){
            CGRect rectPick = PikUpViewForConfirmScreen.frame;
            rectPick.origin.y = 64;
            PikUpViewForConfirmScreen.frame = rectPick;
            
            CGRect rectDrop = DropOffViewForConfirmScreen.frame;
            rectDrop.origin.y = 108;
            DropOffViewForConfirmScreen.frame = rectDrop;
        }
        else{
            CGRect rect = PikUpViewForConfirmScreen.frame;
            rect.origin.y = 64;
            PikUpViewForConfirmScreen.frame = rect;
        }
        
        
    }
    else {
        UIView *customLocationView = [self.view viewWithTag:topViewTag];
        CGRect rect = customLocationView.frame;
        rect.origin.y = 64;
        customLocationView.frame = rect;
    }
    
    [UIView commitAnimations];
    [self animateInBottomView];
}


#pragma mark -UIButton Action

-(void)minusButtonClicked:(id)sender
{
    UIView *dropOff = [self.view viewWithTag:160];
    CGRect dropOffTop = dropOff.frame;
    dropOffTop.origin.y = 0;
    [UIView animateWithDuration:0.6f animations:^{
        dropOff.frame = dropOffTop;
    }];
    [DropOffViewForConfirmScreen removeFromSuperview];
    DropOffViewForConfirmScreen = nil;
    desLong = desLat = 0;
    desAddr = desAddrline2 = @"";
}


-(void)rightBarButtonClicked:(UIButton *)sender{
    
    //[self showPickerViewForAddingTipForDriver];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)menuButtonPressed:(id)sender
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

- (void)getCurrentLocation:(UIButton *)sender{
    
    CLLocation *location = mapView_.myLocation;
    _currentLatitude = location.coordinate.latitude;
    _currentLongitude = location.coordinate.longitude;
    
    if (location)
    {
        
        GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude) zoom:mapZoomLevel];
        [mapView_ animateWithCameraUpdate:zoomCamera];
    }
}

- (void)getAddress:(CLLocationCoordinate2D)coordinate
{
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response, NSError *error) {
        if (response && response.firstResult)
        {
            GMSAddress *address = response.firstResult;
            NSString *addressText = [NSString stringWithFormat:@"%@, %@, %@, %@, %@.", flStrForStr(address.thoroughfare) ,flStrForStr(address.subLocality), flStrForStr(address.locality), flStrForStr(address.postalCode), flStrForStr(address.administrativeArea)];
            if([addressText hasPrefix:@", "])
            {
                addressText = [addressText substringFromIndex:2];
            }
            if([addressText hasSuffix:@", "])
            {
                addressText = [addressText substringToIndex:addressText.length-2];
            }
            addressText = [addressText stringByReplacingOccurrencesOfString:@", ," withString:@", "];
            UIView *customView = [self.view viewWithTag:topViewTag];
            UITextField *textAddress = (UITextField *)[customView viewWithTag:101];
            textAddress.text =  addressText;
            
            UITextField *pickUpAddress= (UITextField *)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
            if(!(isLocationChanged == isFixed))
            {
                pickUpAddress.text = addressText;
            }
            
            pickUpAddress.text = addressText;
            srcLat = response.firstResult.coordinate.latitude;
            srcLong = response.firstResult.coordinate.longitude;
            srcAddr = addressText;
            srcAddrline2 = flStrForObj(@"");
            
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
        }
    };
    [geocoder_ reverseGeocodeCoordinate:coordinate completionHandler:handler];
}

#pragma mark UITextField Delegate -
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)getCurrentLocationFromGPS {
    
    [[ProgressIndicator sharedInstance] showPIOnWindow:WINDOW withMessge:@""];
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            if  ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
            {
                [self.locationManager requestAlwaysAuthorization];
            }
        }
        [_locationManager startUpdatingLocation];
        
        
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Service", @"Location Service")
                                                            message:NSLocalizedString(@"Unable to find your location,Please enable location services.", @"Unable to find your location,Please enable location services.")
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok")
                                                  otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
        if (!_isUpdatedLocation)
        {
        _isUpdatedLocation = YES;
        [_locationManager stopUpdatingLocation];
        //change map camera postion to current location
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                                longitude:newLocation.coordinate.longitude
                                                                     zoom:mapZoomLevel];
        [mapView_ setCamera:camera];
        
        //add marker at current location
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        //save current location to plot direciton on map
        _currentLatitude = newLocation.coordinate.latitude;
        _currentLongitude =  newLocation.coordinate.longitude;
        
        //get address for current location
        [self getAddress:position];
        
        MyAppTimerClass *obj = [MyAppTimerClass sharedInstance];
        [obj startPublishTimer];
        
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied) {
        // The user denied your app access to location information.
        [self gotoLocationServicesMessageViewController];
    }
}

-(void)locationServicesChanged:(NSNotification*)notification {
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        [self gotoLocationServicesMessageViewController];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        
        BOOL presented = [[self.navigationController viewControllers] count] == 1;
        
        
        if (presented) {
            
            [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
            
        }
    }
}

-(void)gotoLocationServicesMessageViewController {
    
    LocationServicesMessageVC *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
}


/**
 *  Plots Marker on Map
 *
 *  @param medicalSpecialist doctor or nurse
 */
- (void)addCustomMarkerFor
{
    
    if (!_allMarkers) {
        
        _allMarkers = [[NSMutableDictionary alloc] init];
    }
    
    for (int i = 0; i < drivers.count; i++)
    {
        
        NSDictionary *dict = drivers[i];
        float latitude = [dict[@"lt"] floatValue];
        float longitude = [dict[@"lg"] floatValue];
        
        if (!_allMarkers[dict[@"chn"]]) {
            
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(latitude, longitude);
            marker.tappable = YES;
            marker.flat = YES;
            marker.groundAnchor = CGPointMake(0.5f, 0.5f);
            marker.userData =  dict;
            if (arrayOfMapImages.count > 0) {
                
                marker.icon = arrayOfMapImages[vehicleTypeTag-1];
            }
            marker.map = mapView_;
            
            [_allMarkers setObject:marker forKey:dict[@"chn"]];
            
        }
        else {
            
            
            GMSMarker *marker = _allMarkers[dict[@"chn"]];
            CLLocationCoordinate2D lastPosition =  marker.position;
            CLLocationCoordinate2D newPosition = CLLocationCoordinate2DMake(latitude, longitude);
            CLLocationDirection heading = GMSGeometryHeading(lastPosition, newPosition);
            
            [CATransaction begin];
            
            [CATransaction begin];
            if (heading > 60 && heading < 300)
            {
                [CATransaction setAnimationDuration:1.5];
            }
            else if (heading<=0 || heading==360)
            {
                heading = 0;
                [CATransaction setAnimationDuration:5.0];
            }
            else
            {
                [CATransaction setAnimationDuration:3.5];
            }
            marker.position = newPosition;
            marker.rotation = heading;
            [CATransaction commit];
        }
    }
    [drivers removeAllObjects];
}

-(void)removeMarker:(GMSMarker*)marker{
    marker.map = mapView_;
    marker.map = nil;
}


-(void)dealloc
{

}

- (void)pickupLocationAction:(UIButton *)sender
{

    PickUpViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"pick"];
    if( sender.tag == 91){
        pickController.locationType = kSourceAddress;
        pickController.isComingFromMapVC = YES;
    }
    pickController.typeID = vehicleTypeTag;
    pickController.latitude =  [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.latitude];
    pickController.longitude = [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.longitude];
    pickController.onCompletion = ^(NSDictionary *dict,NSInteger locationType)
    {
        _isAddressManuallyPicked = YES;
        NSString *addressText = flStrForStr(dict[@"address1"]);
        if ([dict[@"address2"] rangeOfString:addressText].location == NSNotFound)
        {
            addressText = [addressText stringByAppendingString:@","];
            addressText = [addressText stringByAppendingString:flStrForStr(dict[@"address2"])];
        }
        else
        {
            addressText = flStrForStr(dict[@"address2"]);
        }
        if(locationType == 1)
        {
            UITextField *textAddress = (UITextField *)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
            textAddress.text = addressText;
            srcAddr = flStrForStr(addressText);
            srcAddrline2 = flStrForStr(dict[@"address2"]);
            srcLat = [dict[@"lat"] floatValue];
            srcLong = [dict[@"lng"] floatValue];
            _textFeildAddress.text = addressText;
            _textFeildAddress.frame = CGRectMake(0,27,screenSize.size.width-35,22);
            
            if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang])
            {
                _textFeildAddress.textAlignment = NSTextAlignmentRight;
                CGRect tFrame  = _textFeildAddress.frame;
                tFrame.size.width = screenSize.size.width-60;
                _textFeildAddress.frame = tFrame;
            }
            [self performSelectorOnMainThread:@selector(changeCurrentLocation:) withObject:dict waitUntilDone:YES];
        }
    };
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController pushViewController:pickController animated:NO];
}

#pragma - mark Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"addressPickup" ])
    {
        PickUpViewController *pickup = [segue destinationViewController];
        pickup.isComingFromMapVC = YES;
        pickup.latitude =  [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.latitude];
        pickup.longitude = [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.longitude];
        [self addDetailToAppointmentClass];
    }
}



@end
