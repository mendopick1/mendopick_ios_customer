//
//  ReceiverDetailViewController.m
//  MenDoPick
//
//  Created by Rahul Sharma on 24/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ReceiverDetailViewController.h"
#import "RecipientAddressTableViewCell.h"
#import "DeliveryDetailTableViewCell.h"
#import "PreviousAddressTableViewCell.h"
#import "LanguageManager.h"
#import "AppointmentLocation.h"
#import "CountryPicker.h"
#import "CountryNameTableViewController.h"
#import "ContactTableViewController.h"
#import "MapViewController.h"
#import "PatientGetLocalCurrency.h"
#import "PatientAppDelegate.h"
#import "ShipmentAddViewController.h"
#import "PickUpViewController.h"
#import "AnimationsWrapperClass.h"
#import "RecepientDetails.h"
#import "Database.h"
#import <Contacts/Contacts.h>
#import<ContactsUI/ContactsUI.h>



typedef enum : NSUInteger {
    CellAddressShipment=0,
    CellReceiverDetailShipment=1,
    CellPreviousDetail = 2,
} shipmentBookingCellNo;


@interface ReceiverDetailViewController ()<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UIKeyboardDelegates,CNContactPickerDelegate>
{
    
    AppointmentLocation *ap;
    PatientAppDelegate *appDelegate;
    RecipientAddressTableViewCell *receiverDetailCell;
    PreviousAddressTableViewCell *PreviousAddressCell;
    DeliveryDetailTableViewCell *addressCell;
    
    RecepientDetails *recepientDetailsFromDB;
    NSMutableArray *recepientListArray;
    NSString *patientEmail;

    
    
    double pickupLat;
    double pickupLong;
    NSString *pickupAddress;
    NSString *dropoffAddress;
    NSString *receiverName;
    NSString *receiverPhone;
    NSString *receiverEmail;
    double dropLattitude;
    double dropLongitude;
    NSString *stringCountryCode;
    UIImage *countryCodeImage;
    NSString *currencySymbol;
    NSString *deliveryFee;
    NSInteger selectedRow;
    
    UITextField *activeTextField;
    NSDictionary *dictionaryWithCode;
}
@end

@implementation ReceiverDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpDataAndUI];
}

-(void)setUpDataAndUI
{
    ap = [AppointmentLocation sharedInstance];
    
    _tableviewShipment.estimatedRowHeight = 100;
    _tableviewShipment.rowHeight = UITableViewAutomaticDimension;
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
    stringCountryCode = [dictionaryWithCode objectForKey:countryCode];
    countryCodeImage= [UIImage imageNamed:[NSString stringWithFormat:@"CountryPicker.bundle/%@", countryCode]];
    
    UITapGestureRecognizer *hideKeyboardGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    hideKeyboardGesture.delegate = self;
    [self.tableviewShipment addGestureRecognizer:hideKeyboardGesture];

    
    receiverName = @"";
    receiverEmail = @"";
    receiverPhone = @"";
    deliveryFee = @"";

    patientEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey];
    recepientListArray = [[NSMutableArray alloc] initWithArray:[Database getParticularRecepientsDetails:patientEmail]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(afterGotAddress:) name:kNotificationChangeAddress object:nil];

}

-(void)dismissKeyboard
{
    activeTextField = nil;
    [self.view endEditing:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    
    appDelegate = (PatientAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.keyboardDelegate = self;
    
    self.navigationController.navigationBarHidden = NO;
    [[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    self.title = LS(@"Recipient Details");
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0x565656)}];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    [self createNavLeftButton];
    [self sendRequestToGetCostAndDistance];
}

-(void)viewWillDisappear:(BOOL)animated
{
    appDelegate.keyboardDelegate = nil;
}

#pragma mark UIGestureRecognizerDelegate methods -

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (touch.view.tag ==1 ||  touch.view.tag ==2 || touch.view.tag ==3) {
        
        return NO;
    }
    
    if (activeTextField) {
        [self dismissKeyboard];
    }
    
    if ([touch.view isDescendantOfView:_tableviewShipment]) {
        
        return NO;
    }
    return YES;
}


-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"signin_back_icon_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"signin_back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,30,buttonImageOff.size.height)];
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [navCancelButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 40,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    }
    
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)cancelButtonClicked
{
    for (UIViewController* viewController in self.navigationController.viewControllers) {
        if ([viewController isKindOfClass:[MapViewController class]] )
        {
            [self.navigationController popToViewController:viewController animated:NO];
            return;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



#pragma mark - UITableView Methods -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == CellPreviousDetail)
    {
        if (recepientListArray.count >4) {
            
            return 4;
        }
        
        return recepientListArray.count;
    }
    else
    {
        return 1;
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (recepientListArray.count > 0 && section == CellPreviousDetail)
    {

        float width = tableView.bounds.size.width;
        float Height = 24;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, Height)];
        view.backgroundColor = [UIColor whiteColor];
        
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,0,(WINDOW.frame.size.width-20)/2,22)];
        label.font = [UIFont fontWithName:Hind_Medium size:13];
        label.text = LS(@"Previous Address Details");
        
        [view addSubview:label];
        
        return view;

        
    }
    else
    {
        return nil;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (recepientListArray.count > 0 && section == CellPreviousDetail) {
        return 30;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return CGFLOAT_MIN;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [self loadCellForShipment:tableView :indexPath];
}

-(UITableViewCell *)loadCellForShipment :(UITableView *)tableView :(NSIndexPath *)indexPath
{
    NSString *cellIdentifier;
    
    switch (indexPath.section)
    {
        case CellAddressShipment:
        {
            cellIdentifier = @"DeliveryDetailTableViewCell";
            addressCell = (DeliveryDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

            addressCell.labelPickupAddress.text = ap.srcAddressLine1;
            addressCell.labelDropAddress.text = ap.desAddressLine1;
            
            return addressCell;
        }
            break;
            
        case CellReceiverDetailShipment:
        {
            cellIdentifier = @"RecipientAddressTableViewCell";
            receiverDetailCell = (RecipientAddressTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            receiverDetailCell.labelCountryCode.text = [NSString stringWithFormat:@"+%@",stringCountryCode];
            
            receiverDetailCell.imageviewCountry.image = countryCodeImage;
            
            [receiverDetailCell.btnContact addTarget:self action:@selector(contactButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [receiverDetailCell.btnContryCode addTarget:self action:@selector(countryCodeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            receiverDetailCell.textfileldReceiverName.text = receiverName;
            receiverDetailCell.textfieldReceiverPhone.text = receiverPhone;
            receiverDetailCell.textfieldReceiverEmail.text = receiverEmail;
            
            return receiverDetailCell;
        }
            break;
            
        case CellPreviousDetail:
        {
            cellIdentifier = @"PreviousAddressTableViewCell";
            
            PreviousAddressCell = (PreviousAddressTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            recepientDetailsFromDB =  recepientListArray[indexPath.row];
            
            PreviousAddressCell.labelRecipientName.text = recepientDetailsFromDB.name;
            PreviousAddressCell.labelRecipientNumber.text = recepientDetailsFromDB.phoneNumber;
    
            return PreviousAddressCell;
        }
              break;
    
            default:
            return  nil;
            break;
        }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == CellPreviousDetail && recepientListArray.count >0) {
        
        recepientDetailsFromDB =  recepientListArray[indexPath.row];
        
        PreviousAddressCell.labelRecipientName.text = recepientDetailsFromDB.name;
        PreviousAddressCell.labelRecipientNumber.text = recepientDetailsFromDB.phoneNumber;

        receiverName = recepientDetailsFromDB.name;
        receiverEmail = recepientDetailsFromDB.recepientEmail;
        receiverPhone = recepientDetailsFromDB.phoneNumber;
        
        if ([recepientDetailsFromDB.recepientEmail length] > 0) {
            
             PreviousAddressCell.labelRecipientNumber.text = recepientDetailsFromDB.recepientEmail;
            receiverEmail = recepientDetailsFromDB.recepientEmail;
        }
        [self reloadParticularSectionInTableView:CellReceiverDetailShipment];
        
    }
    
}



#pragma mark - UIbutton Action -

- (IBAction)changePickupAddressButtonAction:(id)sender
{
    [self changeAddress:@"1"];
}

- (IBAction)changeDropoffAddressButtonAction:(id)sender
{
    [self changeAddress:@"2"];
}


- (IBAction)contactButtonAction:(id)sender
{
    
    
    CNContactPickerViewController *contactPicker = [CNContactPickerViewController new];
    contactPicker.delegate = self;
    [self presentViewController:contactPicker animated:YES completion:nil];
    
//    ContactTableViewController *contactController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactTableViewController"];
//    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:contactController];
//    contactController.oncomplete = ^(NSDictionary * contact)
//    {
//        receiverName = contact[@"fullName"];
//        
//        receiverPhone = contact[@"PhoneNumbers"];
//        
//        if ([[contact[@"PhoneNumbers"] substringToIndex:1] isEqualToString:@"+"]) {
//            
//            NSArray *fullName = [contact[@"PhoneNumbers"] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//            if (fullName.count>1) {
//                NSString *tempString = @"";
//                for (int i=1; i<fullName.count; i++) {
//                 tempString = [tempString stringByAppendingString: fullName[i]];
//                }
//                receiverPhone = tempString;
//                
//            }
//        }
//        
//        stringCountryCode = [dictionaryWithCode objectForKey:contact[@"CountryCode"]];
//        countryCodeImage= [UIImage imageNamed:[NSString stringWithFormat:@"CountryPicker.bundle/%@", contact[@"CountryCode"]]];
//
//        receiverEmail = contact[@"userEmailId"];
//        
//        receiverDetailCell.textfileldReceiverName.text = receiverName;
//        receiverDetailCell.textfieldReceiverPhone.text = receiverPhone;
//        
//        receiverDetailCell.labelCountryCode.text = [NSString stringWithFormat:@"+%@",stringCountryCode];
//        
//        receiverDetailCell.imageviewCountry.image = countryCodeImage;
//        
////        if (receiverEmail.length > 0)
////        {
//             receiverDetailCell.textfieldReceiverEmail.text = receiverEmail;
////        }
//    };
//    [self presentViewController:navBar animated:YES completion:nil];
}


- (IBAction)countryCodeButtonAction:(id)sender
{
    CountryNameTableViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"countryPicker"];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    pickController.oncomplete = ^(NSString * code, UIImage *flagimg, NSString *countryName)
    {
        stringCountryCode = code;
        countryCodeImage = flagimg;
        
        receiverDetailCell.labelCountryCode.text = [NSString stringWithFormat:@"+%@",stringCountryCode];
        
        receiverDetailCell.imageviewCountry.image = countryCodeImage;
    };
    [self presentViewController:navBar animated:YES completion:nil];
}

- (IBAction)nextBtnAction:(id)sender
{
    if (receiverName.length == 0)
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Please Enter Recipient Name",@"Please Enter Recipient Name")];
        
        
    }
    else if(receiverPhone.length == 0)
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Please Enter Recipient Phone Number",@"Please Enter Recipient Phone Number")];
    }
    else
    {
        ShipmentAddViewController *sdVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShipmentAddViewController"];
        
        ap.receiverName = receiverName;
        NSMutableString *countryCodeWithPhone = [NSMutableString stringWithFormat:@"+%@",stringCountryCode];
        
        ap.receiverPhoneCountryCode = countryCodeWithPhone;
        ap.receiverPhone = receiverPhone;
        ap.receiverEmail = receiverEmail;
        ap.deliveryFee = deliveryFee;
        
        [self.navigationController pushViewController:sdVC animated:YES];
    }
 }


#pragma mark - UITextField Delegates -

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.tag == 1)
    {
        receiverName = textField.text;
    }
    else if (textField.tag == 2 )
    {
        receiverPhone = textField.text;
    }
    else if (textField.tag == 3)
    {
        if ([Helper emailValidationCheck:textField.text] == 0)
        {
            [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Invalid Email Id", @"Invalid Email Id")];
            textField.text = @"";
            [textField becomeFirstResponder];
            return;
        }
        else
        {
            receiverEmail = textField.text;
        }
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField.tag == 1 )
    {
        [receiverDetailCell.textfieldReceiverPhone becomeFirstResponder];
    }
    else if(textField.tag == 2)
    {
        [receiverDetailCell.textfieldReceiverEmail becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}


#pragma mark - KeyBoard Methods -

- (void)keyboardWillHide:(NSNotification *)inputViewNotification
{
    UIEdgeInsets ei = UIEdgeInsetsMake(64.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.tableviewShipment.scrollIndicatorInsets = ei;
        self.tableviewShipment.contentInset = ei;
    }];
    [self.tableviewShipment scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
    
}

- (void)keyboardWillShown:(NSNotification *)inputViewNotification
{
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.tableviewShipment.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.tableviewShipment.scrollIndicatorInsets = ei;
    self.tableviewShipment.contentInset = ei;
    
    if(activeTextField)
    {
            [self.tableviewShipment scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:CellReceiverDetailShipment] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
            return;
    }
}

-(void)reloadParticularSectionInTableView:(NSInteger)sectionValue{
    NSRange range = NSMakeRange(sectionValue, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableviewShipment reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Network Call -

// for calculating delivery fee value
-(void)sendRequestToGetCostAndDistance
{
    //setup parameters
    NSDictionary *params = @{
                             @"ent_wrk_type":[NSNumber numberWithInteger:ap.typeID],
                             @"ent_pick_lat":ap.pickupLatitude,
                             @"ent_pick_long":ap.pickupLongitude,
                             @"ent_drop_lat":ap.dropOffLatitude,
                             @"ent_drop_long":ap.dropOffLongitude,
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"ShipmentFare"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                   if (success)
                                   {
                                       addressCell.labelDeliveryFee.text = [NSString stringWithFormat:@"%@",[PatientGetLocalCurrency getCurrencyLocal:[response[@"ApproxFare"] floatValue]]];
                                       deliveryFee = response[@"ApproxFare"];
                                   }
                                   else
                                   {
                                       addressCell.labelDeliveryFee.text = [NSString stringWithFormat:@"%@",[PatientGetLocalCurrency getCurrencyLocal:0.0]];
                                   }
                               }];
}


#pragma mark - Custom Method -

-(void)changeAddress:(NSString *)type
{
    PickUpViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"pick"];
    __block  NSString *addressText;
    pickController.locationType = kSourceAddress;
    pickController.isComingFromMapVC = NO;
    
    if ([type isEqualToString:@"1"])
    {
         pickController.locationType = kSourceAddress;
    }
    else
    {
        pickController.locationType = kDestinationAddress;
    }
    pickController.isComingFromReceiverVC = YES;
    pickController.isComingFromMapVC = NO;
    pickController.latitude =  [NSString stringWithFormat:@"%@",ap.pickupLatitude];
    pickController.longitude = [NSString stringWithFormat:@"%@",ap.pickupLongitude];
    pickController.onCompletion = ^(NSDictionary *dict,NSInteger locationType)
    {
        addressText   = flStrForStr(dict[@"address1"]);
        if ([dict[@"address2"] rangeOfString:addressText].location == NSNotFound)
        {
            addressText = [addressText stringByAppendingString:@","];
            addressText = [addressText stringByAppendingString:flStrForStr(dict[@"address2"])];
        }
        else
        {
            addressText = flStrForStr(dict[@"address2"]);
        }
        if ([type isEqualToString:@"1"])
        {
            addressCell.labelPickupAddress.text = addressText;
            ap.pickupLatitude = [NSNumber numberWithFloat:[dict[@"lat"] floatValue]];
            ap.pickupLongitude = [NSNumber numberWithFloat:[dict[@"lng"] floatValue]];
            ap.srcAddressLine1 = addressText;
        }
        else
        {
            addressCell.labelDropAddress.text = addressText;
            ap.dropOffLatitude = [NSNumber numberWithFloat:[dict[@"lat"] floatValue]];
            ap.dropOffLongitude = [NSNumber numberWithFloat:[dict[@"lng"] floatValue]];
            ap.desAddressLine1 = addressText;
        }
    };
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    [self.navigationController pushViewController:pickController animated:NO];
}

-(void)afterGotAddress:(NSNotification *)notifiy
{
    NSDictionary *detail = [notifiy userInfo];
    
    NSInteger lType = [detail[@"locationtype"] integerValue];
    
    if (lType == kSourceAddress)
    {
        addressCell.labelPickupAddress.text = detail[@"address"];
        ap.pickupLatitude = [NSNumber numberWithFloat:[detail[@"lat"] floatValue]];
        ap.pickupLongitude = [NSNumber numberWithFloat:[detail[@"lng"] floatValue]];
        ap.srcAddressLine1 = detail[@"address"];
    }
    else
    {
        addressCell.labelDropAddress.text = detail[@"address"];
        ap.dropOffLatitude = [NSNumber numberWithFloat:[detail[@"lat"] floatValue]];
        ap.dropOffLongitude = [NSNumber numberWithFloat:[detail[@"lng"] floatValue]];
        ap.desAddressLine1 = detail[@"address"];
    }

    
    
}


#pragma - mark - ContactPicker Delegate -

- (void) contactPicker:(CNContactPickerViewController *)picker
      didSelectContact:(CNContact *)contact {
    
    receiverEmail = @"";
    receiverPhone = @"";
    receiverName = @"";
    
    NSString *    firstName = contact.givenName;
    NSString *    lastName = contact.familyName;
    if (lastName == nil) {
        receiverName=[NSString stringWithFormat:@"%@",firstName];
    }else if (firstName == nil){
        receiverName=[NSString stringWithFormat:@"%@",lastName];
    }
    else{
        receiverName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
    }
    if ([[receiverName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
        receiverName = @"";
    }
    
    //        UIImage *image = [UIImage imageWithData:contact.imageData];
    //        if (image != nil) {
    //            profileImage = image;
    //        }else{
    //            profileImage = [UIImage imageNamed:@"placeholder.png"];
    //        }
    
    NSString * countryCode;
    for (CNLabeledValue *label in contact.phoneNumbers) {
        
        NSLog(@"%@", [label.value valueForKey:@"countryCode"]);
        
        
        countryCode   = [NSString stringWithFormat:@"%@", [label.value valueForKey:@"countryCode"]];
        
        stringCountryCode = [dictionaryWithCode objectForKey:[countryCode uppercaseString]];
        receiverPhone = [label.value stringValue];
        
    }
    //Get all E-Mail addresses from contacts
    for (CNLabeledValue *label in contact.emailAddresses) {
        receiverEmail = label.value;
        if ([receiverEmail length] == 0) {
            receiverEmail = @"";
        }
    }
    
    if (receiverPhone.length >1) {
        
        if ([[receiverPhone substringToIndex:1] isEqualToString:@"+"]) {
            
            NSString *tempStr = [[NSString stringWithFormat:@"+"] stringByAppendingString:stringCountryCode];
            NSString *fullStringName = [receiverPhone stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if ([[fullStringName substringToIndex:tempStr.length] isEqualToString:tempStr]) {
                receiverPhone = [fullStringName substringFromIndex:tempStr.length];
            }
        }
    }
    else
    {
        [Helper showAlertWithTitle:TitleMessage Message:LS(@"It seems no phone number is for selected contact")];
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        countryCodeImage= [UIImage imageNamed:[NSString stringWithFormat:@"CountryPicker.bundle/%@",[countryCode uppercaseString] ]];
        
        receiverDetailCell.textfileldReceiverName.text = receiverName;
        receiverDetailCell.textfieldReceiverPhone.text = receiverPhone;
        
        receiverDetailCell.labelCountryCode.text = [NSString stringWithFormat:@"+%@",stringCountryCode];
        
        receiverDetailCell.imageviewCountry.image = countryCodeImage;
        
        receiverDetailCell.textfieldReceiverEmail.text = receiverEmail;
    });
    
}

@end

