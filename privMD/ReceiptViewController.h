//
//  ReceiptViewController.h
//  MenDoPick
//
//  Created by Rahul Sharma on 31/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiptViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelShipmentValue;
@property (weak, nonatomic) IBOutlet UILabel *labelDeliveryFee;
@property (weak, nonatomic) IBOutlet UILabel *labelTotal;
@property (weak, nonatomic) IBOutlet UILabel *labelPaymentType;
@property (weak, nonatomic) IBOutlet UILabel *labelReceiverName;
@property (weak, nonatomic) IBOutlet UILabel *labelReceiverMobile;
@property (weak, nonatomic) IBOutlet UILabel *labelReceiverEmail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewSignature;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityindicator;

@property(nonatomic,strong)NSDictionary *appointmentDetails;
+ (id)sharedInstance;
-(void)customizeUI;
@end
