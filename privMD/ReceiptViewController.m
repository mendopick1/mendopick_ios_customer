//
//  ReceiptViewController.m
//  MenDoPick
//
//  Created by Rahul Sharma on 31/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ReceiptViewController.h"
#import "PatientGetLocalCurrency.h"
#import "UIImageView+WebCache.h"
#import "Entity.h"


@interface ReceiptViewController ()

@end

static ReceiptViewController *receipt = nil;

@implementation ReceiptViewController

+ (id)sharedInstance
{
    if (!receipt)
    {
        receipt = [[self alloc] init];
      //  receipt.view.frame = [[UIScreen mainScreen]bounds];
    }
    return receipt;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self customizeUI];
}

-(void)customizeUI
{
     [self downloadSignatureImage:_appointmentDetails[@"shipment_details"][0][@"signatureImg"]];
    _labelDeliveryFee.text = [PatientGetLocalCurrency getCurrencyLocal:[_appointmentDetails[@"shipment_details"][0][@"ApproxFare"] floatValue]];
    
    _labelShipmentValue.text =  [PatientGetLocalCurrency getCurrencyLocal:[_appointmentDetails[@"shipment_details"][0][@"shipementValue"] floatValue]];
    
     _labelTotal.text = [PatientGetLocalCurrency getCurrencyLocal:[_appointmentDetails[@"shipment_details"][0][@"Accounting"][@"Totalamount"] floatValue]];
    
    if ([_appointmentDetails[@"payType"] isEqualToString:@"1"])
    {
        _labelPaymentType.text = LS(@"Card");
    }
    else
    {
        _labelPaymentType.text = LS(@"Cash");
    }
    
    _labelReceiverName.text = _appointmentDetails[@"shipment_details"][0][@"reciverName"];
    _labelReceiverMobile.text = [Helper appendToString:_appointmentDetails[@"shipment_details"][0][@"reciverMobile"]];
    if ([_appointmentDetails[@"shipment_details"][0][@"email"] length] == 0) {
        
        _constraintHeightEmail.constant = 0;
    }
    else
    {
        _labelReceiverEmail.text = _appointmentDetails[@"shipment_details"][0][@"email"];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)downloadSignatureImage:(NSString *)imgUrl
{
    _activityindicator.hidden = NO;
    [_activityindicator startAnimating];
    if(imgUrl.length == 0) {
    } else {
        
        [self.imageviewSignature sd_setImageWithURL:[NSURL URLWithString:imgUrl]
                                  placeholderImage:nil
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                             _activityindicator.hidden = YES;
                                             if(!error) {
                                                 [_activityindicator stopAnimating];
                                             }
                                         }];
    }
}

- (IBAction)closeBtnAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
