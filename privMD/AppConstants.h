//
//  AppConstants.h
//  privMD
//
//  Created by Surender Rathore on 22/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//


#define LS(str) NSLocalizedString(str, nil)
#define TitleMessage NSLocalizedString(@"Message",@"Message")
#define TitleError NSLocalizedString(@"Error",@"Error")
#define TitleYes NSLocalizedString(@"Yes",@"Yes")
#define TitleNo NSLocalizedString(@"No",@"No")
#define TitleAlert NSLocalizedString(@"Alert",@"Alert")
#define TitleLoading NSLocalizedString(@"Loading...",@"Loading...")

#define TitleOk NSLocalizedString(@"Ok",@"Ok")
#define TitleCancel NSLocalizedString(@"Cancel",@"Cancel")
#define TitlePhoto NSLocalizedString(@"Take Photo",@"Take Photo")
#define TitlePhotoLibrary NSLocalizedString(@"Choose From Library",@"Choose From Library")
#define TitleRemovePhoto NSLocalizedString(@"Remove Photo",@"Remove Photo")

#define weight1 NSLocalizedString(@"Weight", @"Weight")
#define quantity1 NSLocalizedString(@"Quantity", @"Quantity")

#define ScreenWidth [UIScreen mainScreen].bounds.size.width
#define ArabicLang @"Arabic"
#define EnglishLang @"en"
#define ArabicCode @"ar"

#define WINDOW [(PatientAppDelegate *)[[UIApplication sharedApplication] delegate]window]

#pragma mark - enums

typedef enum {
    kNotificationTypeBookingWithDispatch = 1,
    kNotificationTypeBookingAccept = 11,
    kNotificationTypeBookingAccepted = 2,
    kNotificationTypeBookingCancel = 4,
    kNotificationTypeBookingCancelled=5,
    kNotificationTypeBookingOnMyWay = 6,
    kNotificationTypeBookingReachedLocation = 7,
    kNotificationTypeBookingStarted = 8,
    kNotificationTypeBookingComplete = 9,
    kNotificationTypeBookingReject = 10,
    kNotificationTypePassengerRejected = 12,
    kNotificationTypeIndividualBooking = 21,
    kNotificationTypeBookingClosed = 22,
}BookingNotificationType;


typedef enum CancelBookingReasons :NSUInteger{
    
    CBRVehicleBreakDown = 1,
    CBRNoOneAtPickUpPoint = 2,
    CBROtherReasons = 3,
    
}CancelBookingReasons;

typedef enum {
    kPubNubStartStreamAction = 1,
    kPubNubStopStreamAction = 2,
    kPubNubUpdatePatientAppointmentLocationAction = 3,
    kPubNubStartDriverLocationStreamAction = 5,
    kPubNubStopDriverLocationStreamAction  = 6
}PubNubStreamAction;


typedef enum {
    kAppointmentTypeNow = 3,
    kAppointmentTypeLater = 4
}AppointmentType;

typedef enum {
    kSourceAddress = 1,
    kDestinationAddress = 2,
    kUnknownAddress = 3
}AddressTypeForPickUpLocation;

typedef enum {
    KPending = 1,
    KConfirm = 2,
    Kongoing = 6
}BookingStatus;



#pragma mark - Constants
//eg: give prifix kPMD

extern NSString *const kPMDPublishStreamChannel;
extern NSString *const kPMDPubNubPublisherKey;
extern NSString *const kPMDPubNubSubcriptionKey;
extern NSString *const kPMDGoogleMapsAPIKey;
extern NSString *const kPMDServerGoogleMapsAPIKey;
extern NSString *const kPMDFlurryId;
extern NSString *const kPMDTestDeviceidKey;
extern NSString *const kPMDDeviceIdKey;
extern NSString *const KUBERCarArrayKey;
extern NSString *const kPMDStripeTestKey;
extern NSString *const kPMDStripeLiveKey;


#pragma mark - mark URLs

//Base URL

extern NSString *BASE_URL;
extern NSString *BASE_URL_SUPPORT;
extern NSString *BASE_URL_UPLOADIMAGE;
extern NSString *const   baseUrlForXXHDPIImage;
extern NSString *const   baseUrlForOriginalImage;
extern NSString *const   baseUrlForShipmentImage;
extern NSString *const   UrlForSupport;

extern NSString *const pricesURL;
extern NSString *const termsAndCondition;
extern NSString *const privacyPolicy;
extern NSString *const itunesURL;
extern NSString *const facebookURL;
extern NSString *const websiteLabel;
extern NSString *const websiteURL;
extern NSString *const imgLinkForSharing;
extern NSString *const imgLinkForAmazon;




//Methods
extern NSString *MethodPatientSignUp;
extern NSString *MethodPatientLogin;
extern NSString *MethodDriverUploadImage;
extern NSString *MethodPassengerLogout;
extern NSString *MethodFareCalculator;


#pragma mark - ServiceMethods
// eg : prifix kSM
extern NSString *const kSMLiveBooking;
extern NSString *const kSMTriggerBooking;
extern NSString *const kSMGetAppointmentDetial;
extern NSString *const kSMCancelAppointment;
extern NSString *const kSMCancelOngoingAppointment;
extern NSString *const kSMUpdateSlaveReview;
extern NSString *const kSMGetMasters ;


#pragma marks - Distance Km or Miles
extern double   const kPMDDistanceMetric;
extern NSString *const kPMDDistanceParameter;
extern NSString *const kPMDMPHorKMPH;

extern BOOL const kPMDPaymentType;
extern BOOL const kPMDCardOrCash;
extern BOOL const kPMDBookLater;
extern BOOL const kPMDWithDispatch;


//Request Params For SignUp
extern NSString *KDASignUpFirstName;
extern NSString *KDASignUpLastName;
extern NSString *KDASignUpMobile;
extern NSString *KDASignUpEmail;
extern NSString *KDASignUpPassword;
extern NSString *KDASignUpCountry;
extern NSString *KDASignUpCity;
extern NSString *KDASignUpDeviceType;
extern NSString *KDASignUpDeviceId;
extern NSString *KDASignUpAddLine1;
extern NSString *KDASignUpAddLine2;
extern NSString *KDASignUpPushToken;
extern NSString *KDASignUpZipCode;
extern NSString *KDASignUpAccessToken;
extern NSString *KDASignUpDateTime;
extern NSString *KDASignUpCreditCardNo;
extern NSString *KDASignUpCreditCardCVV;
extern NSString *KDASignUpCreditCardExpiry;
extern NSString *KDASignUpTandC;
extern NSString *KDASignUpPricing;
extern NSString *KDASignUpReferralCode;
extern NSString *KDASignUpLatitude;
extern NSString *KDASignUpLongitude;

extern NSString *KDAProfilePic;
//Request Params For Login

extern NSString *KDALoginEmail;
extern NSString *KDALoginPassword;
extern NSString *KDALoginDeviceType;
extern NSString *KDALoginDevideId;
extern NSString *KDALoginPushToken;
extern NSString *KDALoginUpDateTime;

//Request for Upload Image
extern NSString *KDAUploadDeviceId;
extern NSString *KDAUploadSessionToken;
extern NSString *KDAUploadImageName;
extern NSString *KDAUploadImageChunck;
extern NSString *KDAUploadfrom;
extern NSString *KDAUploadtype;
extern NSString *KDAUploadDateTime;
extern NSString *KDAUploadOffset;


//Shipment Booking
extern NSString * KDAZipCode;
extern NSString * KDAAdditionalInfo;
extern NSString * KDAFlatNumber;


//Request Params For Logout the user

extern NSString *KDALogoutSessionToken;
extern NSString *KDALogoutUserId;
extern NSString *KDALogoutDateTime;

//Parsms for checking user loged out or not

extern NSString *KDAcheckUserLogedOut;
extern NSString *KDAcheckUserSessionToken;
extern NSString *KDAgetPushToken;

//Params to store the Country & City.

extern NSString *KDACountry;
extern NSString *KDACity;
extern NSString *KDALatitude;
extern NSString *KDALongitude;

//params for firstname
extern NSString *KDAFirstName;
extern NSString *KDALastName;
extern NSString *KDAEmail;
extern NSString *KDAPhoneNo;
extern NSString *KDAPassword;


#pragma mark - NSUserDeafults Keys
//eg : give prefix kNSU

extern NSString *const kNSUPatientPubNubChannelkey;
extern NSString *const kNSUAppoinmentDriverDetialKey;
extern NSString *const kNSUPatientEmailAddressKey;
extern NSString *const kNSUMongoDataBaseAPIKey;
extern NSString *const kNSUIsPassengerBookedKey;
extern NSString *const kNSUPassengerBookingStatusKey;
extern NSString *const kNSUPatientCouponkey;
extern NSString *const kNSUPatientPaypalkey;
extern NSString *const SelectedLanguage;


#pragma mark - PushNotification Payload Keys
//eg : give prefix kPN
extern NSString *const kPNPayloadDriverNameKey;
extern NSString *const kPNPayloadAppoinmentTimeKey;
extern NSString *const kPNPayloadDistanceKey;
extern NSString *const kPNPayloadEstimatedTimeKey;
extern NSString *const kPNPayloadDriverEmailKey;
extern NSString *const kPNPayloadDriverContactNumberKey;
extern NSString *const kPNPayloadProfilePictureUrlKey;
extern NSString *const kPNPayloadStatusIDKey;
extern NSString *const kPNPayloadAppointmentIDKey;
extern NSString *const kPNPayloadStatusIDKey;



#pragma mark - Controller Keys

#pragma mark - Notification Name keys
extern NSString *const kNotificationNewCardAddedNameKey;
extern NSString *const kNotificationCardDeletedNameKey;
extern NSString *const kNotificationLocationServicesChangedNameKey;
extern NSString *const kNotificationBookingConfirmationNameKey;
extern NSString *const kNotificationReviewSubmitted;
extern NSString *const BOOKINGCONFIRMED;
extern NSString *const DROPADDRESSCHANGED;
extern NSString *const BACKGROUNDSUBSCRIBE;
extern NSString *const BACKGROUNDUNSUBSCRIBE;
extern NSString *const kNotificationChangeAddress;


#pragma mark - Network Error
extern NSString *const kNetworkErrormessage;

extern NSString *const KNUCurrentLat;
extern NSString *const KNUCurrentLong;
extern NSString *const KNUserCurrentCity;
extern NSString *const KNUserCurrentState;
extern NSString *const KNUserCurrentCountry;

extern NSString *const KUDriverEmail;
extern NSString *const KUBookingDate;
extern NSString *const KUBookingID;


