//
//  HelpViewController.h
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CSAnimationView;
@interface HelpViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIView *topView;

@property (strong, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomspaceConstraint;
@property (weak, nonatomic) IBOutlet UIButton *btnLanguageChange;



- (IBAction)signInButtonClicked:(id)sender;
- (IBAction)registerButtonClicked:(id)sender;

@end
