//
//  PickUpViewController.m
//  DoctorMapModule
//
//  Created by Rahul Sharma on 04/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PickUpViewController.h"
#import "ProgressIndicator.h"
#import "WebServiceHandler.h"
#import "fareCalculatorViewController.h"
#import "Database.h"
#import "SourceAddress.h"
#import "DestinationAddress.h"
#import "MapViewController.h"
#import "LanguageManager.h"
#import "AnimationsWrapperClass.h"
#import "ReceiverDetailViewController.h"
#import "AppointmentLocation.h"
#import "PickUpAddressFromMapController.h"

@interface PickUpViewController ()<pickupAddressFromMap>
{
    AppointmentLocation *ap;
}

@property(nonatomic,assign) BOOL isSearchResultCome;
@property NSTimer *autoCompleteTimer;
@property NSString *substring;
@property NSMutableArray *pastSearchWords;
@property NSMutableArray *pastSearchResults;

@end

@implementation PickUpViewController
@synthesize onCompletion;
@synthesize latitude;
@synthesize longitude;
@synthesize locationType;
@synthesize isSearchResultCome;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isSearchResultCome = NO;
    appDelegate = (PatientAppDelegate*)[UIApplication sharedApplication].delegate;
    context = [appDelegate managedObjectContext];
    
   // [self.navigationController.navigationBar
   //  setTitleTextAttributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0x565656e)}];
    
    if (locationType == kSourceAddress)
    {
         self.title =  NSLocalizedString(@"Pickup Location",@"Pickup Location");
        _searchBarController.placeholder = NSLocalizedString(@"Pickup Location",@"Pickup Location");
    }
    else
    {
         self.title =  NSLocalizedString(@"Drop Location",@"Drop Location");
        _searchBarController.placeholder = NSLocalizedString(@"Drop Location",@"Drop Location");
    }
    
    
    if (context!=nil)
    {
        arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getSourceAddressFromDataBase]];
    }

    /**
     *  if Address not present fetch all the cards form service
     */
    if (arrDBResult.count != 0) {
        _mAddress = [arrDBResult mutableCopy];
        
        // _mAddress = [[[arrDBResult reverseObjectEnumerator] allObjects] mutableCopy];
    }
    else {
        _mAddress = [NSMutableArray array];
    }
    
    self.searchBarController.searchBarStyle = UISearchBarStyleDefault;
    
    self.pastSearchWords = [NSMutableArray array];
    self.pastSearchResults = [NSMutableArray array];
    ap = [AppointmentLocation sharedInstance];
    
    [self createFooterViewForTable];
    [self createHeaderViewForTable];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    [[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0x565656)}];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    [self createNavLeftButton];

}

- (void)refresh:(UIRefreshControl *)refreshControl
{
    [refreshControl endRefreshing];
}

-(void)createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"signin_back_icon_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"signin_back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(dismissViewController:) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,30,30)];
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [navCancelButton setFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x,0.0f,30,30)];
    }
    
    
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)createNavRightButton
{
    UIButton *navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navNextButton setFrame:CGRectMake(-6,0,44,44)];
    
    //    NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [navNextButton setFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x+20,0.0f,44,44)];
    }
    
    [navNextButton addTarget:self action:@selector(doneButtonClickedAction:) forControlEvents:UIControlEventTouchUpInside];
    [Helper setButton:navNextButton Text:NSLocalizedString(@"Done", @"Done") WithFont:Hind_Regular FSize:17 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navNextButton setTitle:NSLocalizedString(@"Done", @"Done") forState:UIControlStateNormal];
    [navNextButton setTitle:NSLocalizedString(@"Done", @"Done") forState:UIControlStateSelected];
    [navNextButton setTitleColor:UIColorFromRGB(0xd53e30) forState:UIControlStateNormal];
    [navNextButton setTitleColor:UIColorFromRGB(0xd53e30) forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [_searchBarController becomeFirstResponder];
    //[self.view bringSubviewToFront:_searchBarController];
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissViewController:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                              subType:kCATransitionFromBottom
                                              forView:self.navigationController.view
                                         timeDuration:0.3];

    
    
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)backBtnAction:(id)sender
{
    [self.view endEditing:YES];
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                              subType:kCATransitionFromBottom
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController popViewControllerAnimated:NO];
}



#pragma mark -
#pragma mark UITableView DataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _mAddress.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"Cell";
    UITableViewCell *cell=nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.backgroundColor=[UIColor clearColor];
    
    if(cell==nil)
    {
        cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
        cell.backgroundColor=[UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:Hind_Regular size:15];
        cell.textLabel.textColor = UIColorFromRGB(0x000000);
        cell.detailTextLabel.font = [UIFont fontWithName:Hind_Regular size:13];
        cell.detailTextLabel.textColor = UIColorFromRGB(0xa0a0a0);
        cell.detailTextLabel.numberOfLines = 2;
    }
    
    id address =  _mAddress[indexPath.section];
    if ([address isKindOfClass:[SourceAddress class]]) {
        SourceAddress *add = (SourceAddress*)address;
        cell.textLabel.text = add.srcAddress;
        cell.detailTextLabel.text = add.srcAddress2;
    }
    else if ([address isKindOfClass:[DestinationAddress class]]) {
        DestinationAddress *add = (DestinationAddress*)address;
        cell.textLabel.text = add.desAddress;
        cell.detailTextLabel.text =add.desAddress2;
    }
    else {
        
        // NSDictionary *dict = (NSDictionary*)address;
        NSDictionary *searchResult = [_mAddress objectAtIndex:indexPath.section];
        cell.textLabel.text = [searchResult[@"terms"] objectAtIndex:0][@"value"];
        cell.detailTextLabel.text = searchResult[@"description"];
        
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return 25;
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"Result", @"Result");
}


-(void)createHeaderViewForTable
{
    UIView *headerView  = [[UIView alloc] initWithFrame:CGRectMake(0,0, [[UIScreen mainScreen] bounds].size.width,35)];
    headerView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, headerView.frame.size.width, headerView.frame.size.height)];
    
    [btn addTarget:self action:@selector(mapButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, headerView.frame.size.width, headerView.frame.size.height)];
    [Helper setToLabel:titleLabel Text:LS(@"Select location from map") WithFont:Hind_Regular FSize:14 Color:UIColorFromRGB(0x646464)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [headerView addSubview:titleLabel];
    [headerView addSubview:btn];
    self.tblView.tableHeaderView = headerView;
}

-(void)mapButtonClick:(id)sender
{
    if (locationType == kSourceAddress && _isComingFromMapVC)
    {
        [self backBtnAction:nil];
        return;
    }
    
    PickUpAddressFromMapController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"PickUpAddressFromMapController"];
    
    if (_isComingFromMapVC)
    {
        pickController.isFromMapVC = YES;
    }
  
    pickController.locationType = locationType;
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController pushViewController:pickController animated:NO];

}


- (void)createFooterViewForTable {
    
    UIView *footerView  = [[UIView alloc] initWithFrame:CGRectMake(0,0, [[UIScreen mainScreen] bounds].size.width,98/2)];
    footerView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.3];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"poweredby_google"]];
    imageView.frame = CGRectMake(0,0, [[UIScreen mainScreen] bounds].size.width,98/2);
    [footerView addSubview:imageView];
    self.tblView.tableFooterView = footerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Database *db = [[Database alloc]init];
    PMDReachabilityWrapper *reachable = [PMDReachabilityWrapper sharedInstance];
    if (reachable.isNetworkAvailable)
    {
        if(isSearchResultCome == YES  && reachable.isNetworkAvailable)
        {
            NSDictionary *searchResult = [self.mAddress objectAtIndex:indexPath.section];
            NSString *placeID = [searchResult objectForKey:@"reference"];
            [self.searchBarController resignFirstResponder];
            
            [self retrieveJSONDetailsAbout:placeID withCompletion:^(NSArray *place)
             {
                 NSDictionary *d = [place mutableCopy];
                 [db addSourceAddressInDataBase:d];
                 NSDictionary *dict = [NSDictionary dictionary];

                 NSString *add1 = [NSString stringWithFormat:@"%@",flStrForStr([place valueForKey:@"name"])];
                 NSArray *arr = [place valueForKey:@"address_components"];
                 NSString *add2 =@"";
                 NSString *zipCode = @"";
                 for (int addrCount=1 ; addrCount< [arr count]; addrCount++)
                 {
                     NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
                     if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
                     {
                         add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
                     }
                     if([[arrCountryType objectAtIndex:0] isEqualToString:@"postal_code"])
                     {
                         zipCode = flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"]);
                     }
                 }
                 if ([add2 hasPrefix:@", "]) {
                     add2 = [add2 substringFromIndex:2];
                 }
                 
                 if ([add2 hasSuffix:@", "]) {
                     add2 = [add2 substringToIndex:[add2 length]];
                 }
                 if (add1.length == 0)
                 {
                     NSArray *arr = [place valueForKey:@"address_components"];
                     NSString *add2 =@"";
                     for (int addrCount=0 ; addrCount< [arr count]; addrCount++)
                     {
                         NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
                         if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
                         {
                             add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
                         }
                         if([[arrCountryType objectAtIndex:0] isEqualToString:@"postal_code"])
                         {
                             zipCode = flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"]);
                         }
                     }
                     if ([add2 hasPrefix:@","]) {
                         add2 = [add2 substringFromIndex:1];
                     }
                     
                     if ([add2 hasSuffix:@", "]) {
                         add2 = [add2 substringToIndex:[add2 length]-2];
                     }
                     if(add2.length)
                     {
                         add1 = [NSString stringWithFormat:@"%@, %@",add1, add2];
                         add2 = @"";
                     }
                     else
                     {
                         add1 = [NSString stringWithFormat:@"%@ %@",add1, add2];
                         add2 = @"";
                     }
                 }
                 NSString *late = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lat"]];
                 NSString *longi = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lng"]];
                 NSString *keyId = [NSString stringWithFormat:@"%@",[place valueForKey:@"place_id"]];
                 
                 dict =  @{@"address1":flStrForObj(add1),
                           @"address2":flStrForObj(add2),
                           @"lat":flStrForObj(late),
                           @"lng":flStrForObj(longi),
                           @"keyId":flStrForObj(keyId),
                           @"zipCode":flStrForObj(zipCode),
                          };
                 
                 if (onCompletion)
                 {
                     onCompletion(dict,locationType);
                     [self backBtnAction:nil];
                 }
                 else
                 {
                      [self gotoViewController:dict];
                 }
             }];
        }
        else
        {
            
            NSDictionary *dict = [NSDictionary dictionary];
            
            arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getSourceAddressFromDataBase]];
            SourceAddress *add = (SourceAddress *)arrDBResult[indexPath.section];
            
            dict = @{ @"address1": flStrForObj(add.srcAddress),
                      @"address2":flStrForObj(add.srcAddress2),
                      @"lat":flStrForObj(add.srcLatitude),
                      @"lng":flStrForObj(add.srcLongitude),
                      @"keyId":flStrForStr(add.keyId),
                      @"zipCode":flStrForStr(add.zipCode),
                      };

            if (locationType == kSourceAddress)
            {
                if (onCompletion) {
                    onCompletion(dict,locationType);
                     [self backBtnAction:nil];
                }
            }
            else
            {
                [self gotoViewController:dict];
            }
        }
    }
}

-(void)gotoViewController:(NSDictionary *)dict {
    
    ap.desAddressLine1 = [NSString stringWithFormat:@"%@,%@",dict[@"address1"],dict[@"address2"]];

    ap.dropOffLatitude = [NSNumber numberWithFloat:[dict[@"lat"] floatValue]];
    
    ap.dropOffLongitude = [NSNumber numberWithFloat:[dict[@"lng"] floatValue]];
    
    ReceiverDetailViewController *rcdt = [self.storyboard instantiateViewControllerWithIdentifier:@"ReceiverDetailViewController"];
    
    [self.navigationController pushViewController:rcdt animated:YES];
    
}


#pragma mark -
#pragma mark Search Bar Delegates
#pragma mark - Autocomplete SearchBar methods
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [self.autoCompleteTimer invalidate];
    [self searchAutocompleteLocationsWithSubstring:self.substring];
    [self.searchBarController resignFirstResponder];
    [self.tblView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSString *searchWordProtection = [self.searchBarController.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (searchWordProtection.length != 0) {
        
        [self runScript];
        
    } else {
        
    }
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    self.substring = [NSString stringWithString:self.searchBarController.text];
    self.substring= [self.substring stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    self.substring = [self.substring stringByReplacingCharactersInRange:range withString:text];
    
    if ([self.substring hasPrefix:@"+"] && self.substring.length >1) {
        self.substring  = [self.substring substringFromIndex:1];
    }
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self.view endEditing:YES];
}

- (void)runScript{
    
    [self.autoCompleteTimer invalidate];
    self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.65f
                                                              target:self
                                                            selector:@selector(searchAutocompleteLocationsWithSubstring:)
                                                            userInfo:nil
                                                             repeats:NO];
}

- (void)searchAutocompleteLocationsWithSubstring:(NSString *)substring
{
    [self.mAddress removeAllObjects];
    [self.tblView reloadData];
    
    if (![self.pastSearchWords containsObject:self.substring]) {
        [self.pastSearchWords addObject:self.substring];
        [self retrieveGooglePlaceInformation:self.substring withCompletion:^(NSArray * results) {
            [self.mAddress addObjectsFromArray:results];
            isSearchResultCome = YES;
            NSDictionary *searchResult = @{@"keyword":self.substring,@"results":results};
            [self.pastSearchResults addObject:searchResult];
            [self.tblView reloadData];
            
        }];
        
    }else {
        
        for (NSDictionary *pastResult in self.pastSearchResults) {
            if([[pastResult objectForKey:@"keyword"] isEqualToString:self.substring]){
                [self.mAddress addObjectsFromArray:[pastResult objectForKey:@"results"]];
                [self.tblView reloadData];
            }
        }
    }
}


#pragma mark - Google API Requests


-(void)retrieveGooglePlaceInformation:(NSString *)searchWord withCompletion:(void (^)(NSArray *))complete{
    NSString *searchWordProtection = [searchWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (searchWordProtection.length != 0) {
        
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=establishment|geocode&location=%@,%@&radius=50000 &language=en&key=%@",searchWord,latitude,longitude,kPMDServerGoogleMapsAPIKey];
        
        NSLog(@"AutoComplete URL: %@",urlString);

        
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            NSDictionary *jSONresult;
            if (!data) {
                
            }
            else
            {
                jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            }
            NSArray *results = [jSONresult valueForKey:@"predictions"];
            
            if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
                if (!error){
                    NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                    NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                    complete(@[@"API Error", newError]);
                    return;
                }
                complete(@[@"Actual Error", error]);
                return;
            }else{
                complete(results);
            }
        }];
        
        [task resume];
    }
}

-(void)retrieveJSONDetailsAbout:(NSString *)place withCompletion:(void (^)(NSArray *))complete {
    
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&key=%@",place,kPMDServerGoogleMapsAPIKey];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        NSArray *results = [jSONresult valueForKey:@"result"];
        
        if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
            if (!error){
                NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                complete(@[@"API Error", newError]);
                return;
            }
            complete(@[@"Actual Error", error]);
            return;
        }else{
            complete(results);
        }
    }];
    
    [task resume];
}


@end
