//
//  SaveSelectedAddressViewController.m
//  iServe_AutoLayout
//
//  Created by Apple on 14/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "SaveSelectedAddressViewController.h"
#import "AddressManageViewController.h"
#import "Database.h"
#import "RecepientDetailsViewController.h"

@interface SaveSelectedAddressViewController ()<UITextFieldDelegate,UITextViewDelegate>
{
    NSInteger selectedTagButton;
    CGRect screenSize;
    NSString *dropoff;
    NSString *selectedTagAddress;
}

@end

@implementation SaveSelectedAddressViewController

#pragma mark - Initial Methods -
- (void)viewDidLoad {
    
    [super viewDidLoad];
    selectedTagButton = 3;
    self.otherButton.selected = YES;
    screenSize = [[UIScreen mainScreen]bounds];
    
    _labelHome.text = LS(@"HOME");
    _labelOffice.text = LS(@"OFFICE");
    _labelOther.text = LS(@"OTHER");
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([UIApplication sharedApplication].userInterfaceLayoutDirection == UIUserInterfaceLayoutDirectionRightToLeft) {
        
        _homeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        
        _otherButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        
        _officeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    
    if (self.isFromServiceDetailVC)
    {
        dropoff = @"1";
    }
    else
    {
        dropoff = @"0";
    }
    self.selectedAddressTextView.text = self.selectedAddressDetails[@"address"];
    [self setAddressTextFieldFrame];
}

-(void)setAddressTextFieldFrame
{
    self.selectedAddressTextViewHeightConstraint.constant = [self measureHeightTextField:self.selectedAddressTextView]+8;
}

#pragma mark - Custom Methods -

- (CGFloat)measureHeightTextField:(UITextView *)textView
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-20 , 9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:textView.font.fontName size:textView.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:textView.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    CGRect newFrame = textView.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}


#pragma mark - UIBUtton Action -

- (IBAction)tagAddressButtonAction:(id)sender
{
    UIButton *tagButton = (UIButton *)sender;
    for (UIView *button in tagButton.superview.subviews)
    {
        if ([button isKindOfClass:[UIButton class]])
        {
            [(UIButton *)button setSelected:NO];
        }
    }
    tagButton.selected = YES;
    selectedTagButton = tagButton.tag;
    
}

- (IBAction)navigationBackButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveAddressButtonAction:(id)sender
{
    if(self.selectedAddressTextView.text.length == 0)
    {
        [Helper showAlertWithTitle:LS(@"Message") Message:LS(@"Address should not be empty")];
    }
    else
    {
        [self.view endEditing:YES];
        [self sendRequestToSaveAddress];
    }
}

#pragma mark - KeyBoard Methods -

- (void)keyboardHiding:(__unused NSNotification *)inputViewNotification
{
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    self.scrollView.contentOffset = CGPointMake(0,0);
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.scrollView.scrollIndicatorInsets = ei;
        self.scrollView.contentInset = ei;
    }];
}

- (void)keyboardShowing:(__unused NSNotification *)inputViewNotification
{
    
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.scrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0,0.0, intersection.size.height, 0.0);
    self.scrollView.scrollIndicatorInsets = ei;
    self.scrollView.contentInset = ei;
}

#pragma mark - UITextField Delegate methods -

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - UITextView Delegate -

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        
        return NO;
    }
    
    return YES;
}



-(void)addBtncliked:(UIButton *)sender
{
   
}

-(void)backButtonAction:(id)sender
{
   
        NSDictionary *dicts = @{
                                @"address1":flStrForObj(_selectedAddressTextView.text),
                                @"tagAddress":selectedTagAddress,
                                @"flatNumber":flStrForObj(_flatNumberTextField.text),
                                @"latitude":[NSString stringWithFormat:@"%@",self.selectedAddressDetails[@"lat"]],
                                @"longitude":[NSString stringWithFormat:@"%@",self.selectedAddressDetails[@"log"]],
                                };
        
        if (_selectedAddressTextView.text.length>4) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RECEIPIENTADDRESS" object:nil userInfo:dicts];
        }
        
        
        
      //  NSArray *controllerArray = [self.navigationController viewControllers];
    
    
    for (UIViewController *controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[RecepientDetailsViewController class]])
        {
            [self.navigationController popToViewController:controller animated:YES];
            
            break;
        }
    }

    
}






#pragma mark - Web Service Call -

-(void)sendRequestToSaveAddress
{
    switch (selectedTagButton) {
        case 1:
            selectedTagAddress = @"HOME";
            break;
        case 2:
            selectedTagAddress = @"OFFICE";
            break;
        case 3:
            selectedTagAddress = @"OTHER";
            break;
            
        default:
            selectedTagAddress = @"OTHER";
            break;
    }
    
    
    if(_selectedAddressTextView.text.length == 0)
    {
        UIAlertView *alview =[[UIAlertView alloc] initWithTitle:nil message:@"Enter Address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alview.tag=10;
        
        [alview show];
        
    }
    
    else if ([_selectedAddressTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length < 15)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Sorry we could not generate valid geo co-ordinates for this address , please enter a new address again!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else
    {
        
        NSInteger randomNumber = arc4random();
        
        NSDictionary *add = @{
                              @"add_line1":flStrForObj(_selectedAddressTextView.text),
                              @"flatNumber":flStrForObj(_flatNumberTextField.text),
                              @"tagAddress":selectedTagAddress,
                              @"app_latitude":[NSString stringWithFormat:@"%@",self.selectedAddressDetails[@"lat"]],
                              @"app_longitude":[NSString stringWithFormat:@"%@",self.selectedAddressDetails[@"log"]],
                              @"randomNumber":[NSNumber numberWithInteger:randomNumber],
                              @"email":[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey],
                              };
        
        Database *db = [[Database alloc] init];
        BOOL isAdded = [db makeDataBaseEntryForAddress:add];
        
        if (isAdded)
        {
            [self backButtonAction:nil];
        }
        else
        {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Saving failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        
    }

}

#pragma mark - Web service Delegate -

//-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error{
//
//    [[ProgressIndicator sharedInstance]hideProgressIndicator];
//
//    if (error)
//    {
//        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
//        return;
//    }
//    
//    NSInteger errFlag = [response[@"errFlag"] integerValue];
//    NSInteger errNum = [response[@"errNum"] integerValue];
//    
//    switch (errFlag) {
//        case 1:
//        {
//            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
//            {
//                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
//            }
//            else
//            {
//                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
//            }
//        
//        }
//            break;
//            
//        case 0:
//        {
//            if(requestType == RequestTypeAddAddress)
//            {
//                NSString *selectedTagAddress;
//                switch (selectedTagButton) {
//                    case 1:
//                        selectedTagAddress = @"HOME";
//                        break;
//                    case 2:
//                        selectedTagAddress = @"OFFICE";
//                        break;
//                    case 3:
//                        selectedTagAddress = @"OTHER";
//                        break;
//                        
//                    default:
//                        selectedTagAddress = @"OTHER";
//                        break;
//                }
//
//                
//                NSDictionary *dict = @{
//                                       @"aid":response[@"addressid"],
//                                       @"address1":self.selectedAddressTextView.text,
//                                       @"suite_num":self.flatNumberTextField.text,
//                                       @"tag_address":selectedTagAddress,
//                                       @"lat":[NSString stringWithFormat:@"%@",self.selectedAddressDetails[@"lat"]],
//                                       @"long":[NSString stringWithFormat:@"%@",self.selectedAddressDetails[@"log"]],
//                                       @"dropoff":dropoff
//                                     };
//                [[Database sharedInstance]makeDataBaseEntryForManageAddress:dict];
//                [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
//                
//                if(self.isFromProviderBookingVC)
//                {
//                    for (UIViewController* viewController in self.navigationController.viewControllers) {
//                        
//                        if ([viewController isKindOfClass:[AddressManageViewController class]] )
//                        {
//                            [self.navigationController popToViewController:viewController animated:YES];
//                            return;
//                        }
//                    }
//                }
//                else
//                {
//                    [self.navigationController popToRootViewControllerAnimated:YES];
//                }
//                
//            }
//            
//        }
//            break;
//            
//        default:
//            break;
//    }

//}


@end
