//
//  PatientAppDelegate.h
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MapViewController;
@class ShipmentDetailsViewController;
@class CurrentOrderViewController;
@class OnTheWayViewController;

@protocol UIKeyboardDelegates <NSObject>

@optional

-(void)keyboardWillShown:(__unused NSNotification *)inputViewNotification;
-(void)keyboardWillHide:(__unused NSNotification *)inputViewNotification;

@end

@interface PatientAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic,strong) MapViewController *mapViewContoller;
@property (nonatomic,strong) CurrentOrderViewController *orderVC;
@property (nonatomic,strong) ShipmentDetailsViewController *shipmentVC;
@property (nonatomic,strong) OnTheWayViewController *onTheWayVC;


@property (nonatomic,assign) id keyboardDelegate;



- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@property (assign, nonatomic) CancelBookingReasons cancelReason;

@end
