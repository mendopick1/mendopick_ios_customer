//
//  PastPaymentDetailsViewController.h
//  MenDoPick
//
//  Created by Rahul Sharma on 17/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PastPaymentDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewContent;

@property (nonatomic,strong) NSDictionary *walletDetail;
@property (strong, nonatomic) IBOutlet UILabel *labelOpeningBalance;
@property (strong, nonatomic) IBOutlet UILabel *labelClosingBalance;
@property (strong, nonatomic) IBOutlet UILabel *labelTotalBooking;
@property (strong, nonatomic) IBOutlet UILabel *labelTotalShipmentValueDue;
@property (strong, nonatomic) IBOutlet UILabel *labelPgCommission;
@property (strong, nonatomic) IBOutlet UILabel *labelDeliveryValueToPaid;
@property (strong, nonatomic) IBOutlet UILabel *labelNetReceivable;
@property (strong, nonatomic) IBOutlet UILabel *labelPaidOut;


@property (strong, nonatomic) IBOutlet UILabel *labelCashBookingText;
@property (strong, nonatomic) IBOutlet UILabel *labelCashBooking;
@property (strong, nonatomic) IBOutlet UILabel *labelTotalCardBookingText;
@property (strong, nonatomic) IBOutlet UILabel *labelTotalCardBooking;


@end
