//
//  AcceptShipmentViewController.m
//  DeliveryPlus
//
//  Created by Rahul Sharma on 30/09/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import "ShipmentBookingViewController.h"
#import "NewOrderViewController.h"
#import "Database.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "AcceptShipmentViewController.h"
#import "NetworkHandler.h"

@interface ShipmentBookingViewController ()
{
    float actual;
    NSInteger driverCount;
    NSString *driverEmail;
    double  progresslength;
    BOOL bookingCancelled;
    UIWindow *window;
    NSString * paymenttype;
    NSString *dispatch;
    UIBackgroundTaskIdentifier bgTask;

}

@end

@implementation ShipmentBookingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    //    self.title = @"";
    //    self.navigationItem.hidesBackButton = YES;
    
     [Helper setCornerRadius:3.0 bordercolor:CLEAR_COLOR withBorderWidth:0 toview:_cancelButton];
    window = [[UIApplication sharedApplication] keyWindow];
}

-(void) viewWillAppear:(BOOL)animated{
    
    driverCount = 0;
    
    bookingCancelled = NO;
    
    progresslength = (double)1/(double)(60*_arrayOfDriverEmails.count);
    
    [self updateThreadProgressView];
    
    [self toGetDriverEmail];
    
    [self.progressBar setTransform:CGAffineTransformMakeScale(1.0, 3.0)];
    
    _addressLabel.text = _pickupAddress;
    
    
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelButtonAction:(id)sender {
    
    [self sendAppointmentRequestForLiveBookingCancellation];
}


-(void)updateThreadProgressView
{
    if (actual < 1)
    {
        _progressBar.progress = actual;
        actual = actual + progresslength;
        
        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateThreadProgressView) userInfo:nil repeats:NO];
    }
}

-(void)toGetDriverEmail
{
    if(driverCount < _arrayOfDriverEmails.count){
        
        driverEmail = _arrayOfDriverEmails[driverCount];
        
        driverCount = driverCount + 1;
        
        [self sendRequestToPickupNow];
    }
}

-(void)sendRequestToPickupNow{
    
     [self startBackgroundTask];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *dict =  @{
                            @"ent_sess_token":sessionToken,
                            @"ent_dev_id":deviceId,
                            @"ent_date_time":[Helper getCurrentDateTime],
                            @"ent_wrk_type":flStrForObj(_vehicleId),
                            @"ent_bid":_bId,
                            @"ent_dri_email":flStrForObj(driverEmail),
                            @"additional_info":flStrForObj(_additionalNotes),
                            };
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithDictionary:dict];
        
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:kSMTriggerBooking
                              paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             if (success) {
                                 
                                 [self getShipmentBookingResponse:response];
                                 
                             }
                             else if(response == nil){
                                 
                                 [self.navigationController popViewControllerAnimated:YES];
                                 
                             }
                         }];
}

-(void)getShipmentBookingResponse:(NSDictionary *)response{
    
    _cancelButton.userInteractionEnabled = YES;
    [self stopBackgroundTask];

    if(!response){
        return;
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 7)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:TitleMessage Message:[response objectForKey:@"errMsg"]];
        
    }
    else if(bookingCancelled == NO){
        
        // 39 ->livebooking, 78 -> laterbooking, 71 -> Notaccepted By Driver
        if( [response[@"errNum"] integerValue] == 71 || [response[@"errFlag"] integerValue] == 1){
            
            if(driverCount < _arrayOfDriverEmails.count){
                
                [self toGetDriverEmail];
                
            }
            else {
                [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Currently all Drivers are busy Please try after some time", @"Currently all Drivers are busy Please try after some time")];
                [self.navigationController popViewControllerAnimated:YES];
            }            
        }
        else {
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
            
            [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Thank you for booking on MenDoPick ! One of our Courier agents has picked up your request and we will get you all details on this page shortly!",@"Thank you for booking on MenDoPick ! One of our Courier agents has picked up your request and we will get you all details on this page shortly!")];
            
        }
    }
}

-(void)startBackgroundTask {
    UIApplication *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
        
    }];
}
-(void)stopBackgroundTask {
    UIApplication *app = [UIApplication sharedApplication];
    [app endBackgroundTask:bgTask];
}



-(void)sendAppointmentRequestForLiveBookingCancellation {
    
    [[ProgressIndicator sharedInstance] showPIOnWindow:window withMessge:TitleCancel];    //setup parameters
    
    
    if (driverEmail.length>0)
    {
        
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *dict = @{
                           @"ent_sess_token":sessionToken,
                           @"ent_dev_id":deviceID,
                           @"ent_dri_email":flStrForObj(driverEmail),
                           @"ent_appnt_dt":_apointmentTime,
                           @"ent_date_time":[Helper getCurrentDateTime],
                           };
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithDictionary:dict];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    if([ud boolForKey:@"laterBooking"] == YES) {
        [params setObject:[ud objectForKey:@"selectedDate"] forKey:@"ent_appnt_dt"];
    }
   // kSMCancelAppointment
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"cancelRequest"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       [self getLiveCancelResponse:response];
                                   }
                                   else{
                                       
                                   }
                               }];
    }
    else
    {
        
         [self.navigationController popViewControllerAnimated:YES];
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        
    }
}

-(void)getLiveCancelResponse:(NSDictionary *)responseDictionary{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    NSString *titleMsg = TitleMessage;
    NSString *errMsg = @"";
    
    bookingCancelled = YES;
    
    if (!responseDictionary)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleError message:[responseDictionary objectForKey: @"Message"] delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alertView show];
    }
    else if ([responseDictionary objectForKey:@"Error"])
    {
        errMsg = [responseDictionary objectForKey:@"errMsg"];
    }
    else if ([responseDictionary[@"errFlag"] intValue] == 1 && ([responseDictionary[@"errNum"] intValue] == 96 || [responseDictionary[@"errNum"] intValue] == 7)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:TitleMessage Message:[responseDictionary objectForKey:@"errMsg"]];
        
    }
    else
    {
        if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0)
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
        else if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 1)
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
        else
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
    }
    [Database deleteAllShipmentDetails];
    
    [Helper showAlertWithTitle:titleMsg Message:errMsg];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
}


@end
