//
//  CurrentOrderViewController.m
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 12/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "CurrentOrderViewController.h"
#import "NewOrderViewController.h"
#import "OnTheWayViewController.h"
#import "AcceptShipmentViewController.h"
#import "Database.h"
#import "LiveBookingTable.h"
#import "LiveBookingStatusUpdatingClass.h"
#import "LiveBookingShipmentDetails.h"
#import "LiveBookingOrderTracking.h"
#import "XDKAirMenuController.h"
#import "PatientAppDelegate.h"
#import "CustomNavigationBar.h"
#import "PatientViewController.h"
#import "BookingHistoryTableViewCell.h"

@interface CurrentOrderViewController ()<CustomNavigationBarDelegate,UITableViewDelegate,updateLiveBookingStatusDelegate,noNeedToUpdateStatus,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate,UIApplicationDelegate>
{
    NSArray *arrayOfLiveBooking;
    
 
        
    NSArray *pastitem;
    NSDictionary *itemList;
    BOOL networkCalls;
    NSString *appointmenttime;
    NSString *bid;
}

@property(nonatomic,strong) NSString *patientPubNubChannel;
@property(nonatomic,strong) NSString *serverPubNubChannel;
@property(nonatomic,strong) NSString *patientEmail;


@end
static CurrentOrderViewController *showOnGoingBookingVC = nil;


@implementation CurrentOrderViewController
@synthesize currentOrderTableView;

+ (instancetype) getSharedInstance
{
    return showOnGoingBookingVC;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
   
       
    [currentOrderTableView registerNib:[UINib nibWithNibName:@"BookingHistoryTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BookingHistoryTableViewCell"];
    
    currentOrderTableView.estimatedRowHeight = 100;
    currentOrderTableView.rowHeight = UITableViewAutomaticDimension;
    
    showOnGoingBookingVC = self;
    
    // Display a message when the table is empty
   
    self.currentOrderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

   
    
    _labelMessage.text = LS(@"Currently No Active Bookings......");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated {
    
    self.navigationController.navigationBarHidden = YES;
    //[self statusUpdatedReloadViews];
    
    currentOrderTableView.dataSource = self;
    currentOrderTableView.delegate = self;

}



-(void)getDataFromOrderVC:(NSDictionary *)order
{
    
    
}




-(void)reloadTableView
{
    [currentOrderTableView reloadData];
}

#pragma mark - Table View Data source -

-(NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section     //present in data souurce protocol
{
    if (_allshipmentDetails.count == 0) {
        self.currentOrderTableView.backgroundView = self.labelMessage;
    }
    return _allshipmentDetails.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
            NSString *CellIdentifier = @"BookingHistoryTableViewCell";    //must be indentical
            BookingHistoryTableViewCell *cell = (BookingHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
     [cell loadCellData:_allshipmentDetails[indexPath.row]];
    
       return cell;
}

#pragma mark - TableView delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(goToOngoingVc:and:)])
    {
        [_delegate goToOngoingVc:indexPath.row and:_allshipmentDetails[indexPath.row]];
    }
}


//update LiveBooking Status Delegate Method

-(void)updateLiveBookingStatusResponse:(NSInteger)statusValue{
    
//    apptStatus = statusValue; //[NSString stringWithFormat:@"%d",statusValue];;
//    
//    [self reloadTableView];
}

-(void)noNeedToUpdateBookingStatus:(NSInteger)status{
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 60)
    {
        
        if(buttonIndex == 1)//View Button Action
        {
            
           
            [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:TitleCancel];

                NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
                
                NSString *deviceID = @"";
                if (IS_SIMULATOR) {
                    deviceID = kPMDTestDeviceidKey;
                }
                else {
                    deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
                }
                
                NSDictionary *dict = @{
                                       @"ent_sess_token":sessionToken,
                                       @"ent_dev_id":deviceID,
                                       @"ent_appnt_dt":appointmenttime,
                                       @"ent_date_time":[Helper getCurrentDateTime],
                                       @"ent_bid":bid
                                       };
                
                NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithDictionary:dict];
                
            
                
            
                //setup request
                NetworkHandler *networHandler = [NetworkHandler sharedInstance];
                [networHandler composeRequestWithMethod:kSMCancelAppointment
                                                paramas:params
                                           onComplition:^(BOOL success, NSDictionary *response){
                                               
                                               if (success) { //handle success response
                                                   [self getLiveCancelResponse:response];
                                               }
                                               else{
                                                   
                                               }
                                           }];
            
           

            
            
        }
    }
    
}




-(void)getLiveCancelResponse:(NSDictionary *)responseDictionary{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    NSString *errMsg = @"";
    
    if (!responseDictionary)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleError message:[responseDictionary objectForKey: @"Message"] delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alertView show];
    }
    else if ([responseDictionary objectForKey:@"Error"])
    {
        errMsg = [responseDictionary objectForKey:@"errMsg"];
    }
    else if ([responseDictionary[@"errFlag"] intValue] == 1 && ([responseDictionary[@"errNum"] intValue] == 96 || [responseDictionary[@"errNum"] intValue] == 7)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:@"Message" Message:[responseDictionary objectForKey:@"errMsg"]];
        
    }
    else
    {
        if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0)
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
        else if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 1)
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
        else
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
    }
    
    [Database deleteAllLiveBookingDetails];
    [Database deleteAllLiveBookingShipmentDetails];
    
}

@end
