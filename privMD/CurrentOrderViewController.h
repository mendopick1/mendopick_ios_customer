//
//  CurrentOrderViewController.h
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 12/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CurrentOrderViewControllerDelegate <NSObject>

-(void)goToOngoingVc:(NSInteger)index and:(NSDictionary *)response;

@end

@interface CurrentOrderViewController : UIViewController

@property (strong, atomic) IBOutlet UITableView *currentOrderTableView;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;

@property (strong, nonatomic) NSMutableArray *allshipmentDetails;

@property (strong, nonatomic) NSArray *shipmentDetailsFromDB;

-(void)getDataFromOrderVC:(NSDictionary *)items;
+ (instancetype) getSharedInstance;

@property(nonatomic,strong) id<CurrentOrderViewControllerDelegate> delegate;
@end
