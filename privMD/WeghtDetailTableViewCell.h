//
//  WeghtDetailTableViewCell.h
//  MenDoPick
//
//  Created by Rahul Sharma on 19/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeghtDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelItemname;
@property (weak, nonatomic) IBOutlet UITextField *txtfieldItemValue;
@property (weak, nonatomic) IBOutlet UIButton *btnPaidByCustomer;
@property (weak, nonatomic) IBOutlet UILabel *labelPaidByCustomer;
@property (strong, nonatomic) IBOutlet UILabel *labelQuantityText;
@property (strong, nonatomic) IBOutlet UITextField *textfieldQuantity;
@property (weak, nonatomic) IBOutlet UIImageView *imgviewPaidByCustomer;
-(void)loadCellData:(NSArray *)itemArray :(NSInteger)row;
@end
