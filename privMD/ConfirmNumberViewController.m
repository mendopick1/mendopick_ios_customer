//
//  ConfirmNumberViewController.m
//  DeliveryPlus
//
//  Created by Rahul Sharma on 06/10/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import "ConfirmNumberViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "UploadFiles.h"
#import "AmazonTransfer.h"
#import "BookingListViewController.h"
#import "ViewController.h"

@interface ConfirmNumberViewController ()<UITextFieldDelegate>
{
    NSString *url;
    NSString *verificationCode;
    UIWindow *window;
    ProgressIndicator *progressIndicator;
    BOOL isFromResend;
}
@end



@implementation ConfirmNumberViewController

@synthesize signUpDetails;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    [self createNavLeftButton];
    [self createNavView];
    progressIndicator = [ProgressIndicator sharedInstance];
    window = [[UIApplication sharedApplication] keyWindow];
    // Do any additional setup after loading the view.
    
     [Helper setCornerRadius:3.0 bordercolor:CLEAR_COLOR withBorderWidth:0 toview:_resendSmsButton];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewWillAppear:(BOOL)animated{
    
    isFromResend = NO;
    [self sendCodeToMobile];
    [_numberTextfield becomeFirstResponder];
    
}
-(void)createNavView
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80,-10, 160, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(10,8, 160, 30)];
   // navTitle.text = @"Confirm Number";
    navTitle.text = NSLocalizedString(@"Verify Phone Number", @"Verify Phone Number");
    navTitle.textColor = UIColorFromRGB(0x565656);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:Hind_Medium size:17];
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;
    
}

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"signin_back_icon_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"signin_back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,30,30)];
    
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

-(void)backButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.text.length < 9){
        
        
    }
    else {
        
        verificationCode = [_numberTextfield.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        [self verifyMobileCode];
        
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger length = [textField.text length] + [string length] - range.length;
    
    if (length < 11) {
        
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString *lastString = [NSString stringWithFormat:@"%c",[newString characterAtIndex:newString.length-1]];
        
        if(![lastString isEqualToString:@" "] && ![string isEqualToString:@""])
            newString = [newString stringByAppendingString:@" "];
        else {
            
            NSRange tempR = NSMakeRange(range.location-1, 2);
            
            newString = [textField.text stringByReplacingCharactersInRange:tempR withString:@""];
        }
        
        textField.text = newString;
    }
    if(textField.text.length == 10){
        [textField resignFirstResponder];
    }
    
    return NO;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_numberTextfield resignFirstResponder];
    
    return YES;
}


- (void)sendCodeToMobile
{
    if(isFromResend == YES){
        
        [progressIndicator showPIOnWindow:window withMessge:NSLocalizedString(@"Resending OTP...", @"Resending OTP...")];
        
    }
    
    NSDictionary *params = @{
                             
                             @"ent_mobile":[signUpDetails objectForKey:KDASignUpMobile],
                             
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getVerificationCode"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if(isFromResend == YES){
                                       
                                       [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                       
                                   }
                                   if (success) { //handle success response
                                       
                                       [self sendVerificationCodeResponse:response];
                                       
                                   }
                               }];
}

-(void)sendVerificationCodeResponse:(NSDictionary *)response{
    
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleError message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:TitleMessage Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            if(isFromResend == YES){
                
                [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"You will receive an OTP again shortly", @"You will receive an OTP again shortly")];
            }
            
        }
        
    }
    
    
}
- (void)verifyMobileCode
{
    
    [progressIndicator showPIOnWindow:window withMessge:NSLocalizedString(@"Verifying...",@"Verifying...")];
    
    NSDictionary *params = @{
                             
                             @"ent_phone":[signUpDetails objectForKey:KDASignUpMobile],
                             @"ent_code": verificationCode,
                             
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"verifyPhone"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   
                                   if (success) { //handle success response
                                       
                                       [self verifyMobileCodeResponse:response];
                                       
                                   }
                               }];
}

-(void)verifyMobileCodeResponse:(NSDictionary *)response{
    
    if (!response)
    {
        [progressIndicator hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleError message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [progressIndicator hideProgressIndicator];
        [Helper showAlertWithTitle:TitleMessage Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [self uploadImageinAmazonServer];
            
        }
        else{
            
            [progressIndicator hideProgressIndicator];
            _numberTextfield.text = @"";
            [_numberTextfield becomeFirstResponder];
            [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Please Enter Valid Verification Code!", @"Please Enter Valid Verification Code!")];
             //[self signUp];
        }
        
    }
    
    
}

-(void)uploadImageinAmazonServer{
    
    [progressIndicator changePIMessage:NSLocalizedString(@"Signing Up...",@"Signing Up...")];

    if(_pickedImage){
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyMMddhhmmssa"];
        
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[formatter stringFromDate:[NSDate date]]]];
        
        
        
        NSData *data = UIImageJPEGRepresentation(_pickedImage,1.0);
        [data writeToFile:getImagePath atomically:YES];
        
        
        NSString *name = [NSString stringWithFormat:@"%@%@.jpg",@"image",[Helper getCurrentTime]];
        //    NSString *uid = [[NSUserDefaults standardUserDefaults] objectForKey:];
        NSString *fullImageName = [NSString stringWithFormat:@"%@/ProfilePhotos/%@",@"ShyprImages",name];
        
        [AmazonTransfer upload:getImagePath
                       fileKey:fullImageName
                      toBucket:Bucket
                      mimeType:@"image/jpeg"
                 progressBlock:^(NSInteger progressSize, NSInteger expectedSize) {
                     
                 } completionBlock:^(BOOL success, id result, NSError *error) {
                     
                     
                     url = [NSString stringWithFormat:@"%@%@/%@",imgLinkForAmazon,Bucket,fullImageName];
                     
                     
                     [[NSUserDefaults standardUserDefaults] setObject:url forKey:KDAProfilePic];
                     [[NSUserDefaults standardUserDefaults]synchronize];
                     
                     [self signUp];
                     
                 }];
    }
    else
        
        [self signUp];
}



-(void) signUp{
    
    if(url == nil){
        url = @" ";
    }
    [signUpDetails setObject:url forKey:@"ent_profile_pic"];
    
    //[progressIndicator showPIOnWindow:window withMessge:@"Sign Up..."];
    
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodPatientSignUp
                                    paramas:signUpDetails
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       
                                     //  TELogInfo(@"response %@",response);
                                       [progressIndicator hideProgressIndicator];
                                       
                                       [self signupResponse:response];
                                       
                                   }
                               }];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:[signUpDetails objectForKey:KDASignUpFirstName]forKey:KDAFirstName];
    [[NSUserDefaults standardUserDefaults] setObject:[signUpDetails objectForKey:KDASignUpEmail] forKey:KDAEmail];
    [[NSUserDefaults standardUserDefaults] setObject:[signUpDetails objectForKey:KDASignUpMobile] forKey:KDAPhoneNo];
    [[NSUserDefaults standardUserDefaults] setObject:[signUpDetails objectForKey:KDASignUpPassword] forKey:KDAPassword];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


-(void)signupResponse:(NSDictionary*)response
{
    
    //check if response is not null
    if(response == nil)
    {
        //hide progress indicator
        return;
    }
    
    //check for network error
    if (response[@"Error"])
    {
        //hide progress indicator
        [Helper showAlertWithTitle:TitleError Message:response[@"Error"]];
    }
    else
    {
        
        //handle response here
        NSDictionary *itemList = [[NSDictionary alloc]init];
        itemList = [response mutableCopy];
        
        if ([itemList[@"errFlag"] intValue] == 1) { //error
            
            [Helper showAlertWithTitle:TitleError Message:itemList[@"errMsg"]];
        }
        else {
            
           // TELogInfo(@"ItemList DIct %@ ,",itemList);
            
            NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
            [ud setObject:[itemList objectForKey:@"token"] forKey:KDAcheckUserSessionToken];
            [ud setObject:itemList[@"chn"] forKey:kNSUPatientPubNubChannelkey];
            [ud setObject:itemList[@"email"] forKey:kNSUPatientEmailAddressKey];
            [ud setObject:itemList[@"apiKey"] forKey:kNSUMongoDataBaseAPIKey];
            [ud setObject:itemList[@"serverChn"] forKey:kPMDPublishStreamChannel];
            [ud setObject:itemList[@"coupon"] forKey:kNSUPatientCouponkey];
            [ud setObject:itemList[@"presenseChn"] forKey:@"presencechannel"];
            [ud synchronize];
            
            NSMutableArray *carTypes = [[NSMutableArray alloc]initWithArray:itemList[@"types"]];
            
            
            if (!carTypes || !carTypes.count) {
              //  TELogInfo(@"no data in array car types");
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:carTypes forKey:KUBERCarArrayKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            
            if(![ud objectForKey:KDAcheckUserSessionToken])
            {
                return;
            }
            
            else
            {
                for(UIView *view in window.subviews){
                    if([view isKindOfClass:[UIProgressView class]])
                        [view removeFromSuperview];
                }
                
                [self moveToHome];
            }
        }
        
    }
}


-(void)moveToHome
{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//    BookingListViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"BookingListViewController"];
//    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
//    if ([[segue identifier] isEqualToString:@"toCardVC"])
//    {
//        CardLoginViewController *cardVC = (CardLoginViewController*)[segue destinationViewController];
//        cardVC.isComingFromPayment = 3;
//        
//    }
}


- (IBAction)resendSmsButtonAction:(id)sender {
    isFromResend = YES;
    [self sendCodeToMobile];
    //[self performSegueWithIdentifier:@"toCardVC" sender:self];
}
@end
