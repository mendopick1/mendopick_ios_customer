//
//  InviteViewController.m
//  UBER
//
//  Created by Rahul Sharma on 06/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "InviteViewController.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"



@interface InviteViewController ()<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,CustomNavigationBarDelegate,FBSDKSharingDelegate>
{
    MFMailComposeViewController *mailer;
    NSString *shareText;
}
@property (strong, nonatomic) IBOutlet UILabel *shareLable;
@property (strong, nonatomic) IBOutlet UILabel *promoCodeMsgShowingLabel;
@property (strong, nonatomic) IBOutlet UILabel *promoCodeLablel;
@property (strong, nonatomic) IBOutlet UIButton *fbButton;
@property (strong, nonatomic) IBOutlet UILabel *fbShareLabel;
@property (strong, nonatomic) IBOutlet UIButton *twitterButton;
@property (strong, nonatomic) IBOutlet UILabel *twitterShareLAble;
@property (strong, nonatomic) IBOutlet UIButton *smsButton;
@property (strong, nonatomic) IBOutlet UILabel *smsShareLabel;
@property (strong, nonatomic) IBOutlet UIButton *emailButton;
@property (strong, nonatomic) IBOutlet UILabel *emailShareLable;
@property (strong, nonatomic) IBOutlet UILabel *shareyourcodeLabel;

- (IBAction)facebookButtonClicked:(id)sender;
- (IBAction)twitterButtonClicked:(id)sender;
- (IBAction)smsButtonClicked:(id)sender;
- (IBAction)emailButtonClicked:(id)sender;

@end

@implementation InviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addCustomNavigationBar];
    NSString *pCode = flStrForObj([[NSUserDefaults standardUserDefaults]objectForKey:kNSUPatientCouponkey]);
    [Helper setToLabel:_shareLable Text:NSLocalizedString(@"SHARE MEN DO PICK", @"SHARE MEN DO PICK") WithFont:OpenSans_SemiBold FSize:12 Color:UIColorFromRGB(0x656565)];
    NSString *str1 = NSLocalizedString(@"Get a discount if your contacts sign up using your invite code", @"Get a discount if your contacts sign up using your invite code");
    
    shareText = [NSString stringWithFormat:@"%@\n",str1];
    [Helper setToLabel:_promoCodeMsgShowingLabel Text:shareText WithFont:OpenSans_Light FSize:14 Color:UIColorFromRGB(0x333333)];

   

    shareText = nil;
    shareText = NSLocalizedString(@"Use my referral code, ", @"Use my referral code, ");
    NSString *str2 = NSLocalizedString(@"to signup on MenDoPick and earn a discount on your first booking!", @"to signup on MenDoPick and earn a discount on your first booking!");
    shareText = [shareText stringByAppendingString:pCode];
    shareText = [NSString stringWithFormat:@"%@ , %@", shareText,str2];
    
    [Helper setToLabel:_promoCodeLablel Text:pCode WithFont:OpenSans_Light FSize:33 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_shareyourcodeLabel Text:NSLocalizedString(@"SHARE YOUR CODE", @"SHARE YOUR CODE") WithFont:OpenSans_Light FSize:12 Color:UIColorFromRGB(0x333333)];

    [Helper setToLabel:_fbShareLabel Text:NSLocalizedString(@"FACEBOOK", @"FACEBOOK") WithFont:OpenSans_Light FSize:12 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_twitterShareLAble Text:NSLocalizedString(@"TWITTER", @"TWITTER") WithFont:OpenSans_Light FSize:12 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_smsShareLabel Text:NSLocalizedString(@"MESSAGE", @"MESSAGE") WithFont:OpenSans_Light FSize:12 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_emailShareLable Text:NSLocalizedString(@"EMAIL", @"EMAIL") WithFont:OpenSans_Light FSize:12 Color:UIColorFromRGB(0x333333)];
}

#pragma mark- Custom Methods

- (void) addCustomNavigationBar{
    
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    UILabel *bglabel = [[UILabel alloc]initWithFrame:CGRectMake(0, customNavigationBarView.frame.size.height-1, customNavigationBarView.frame.size.width, 1)];
    [customNavigationBarView addSubview:bglabel];
    bglabel.backgroundColor = UIColorFromRGB(0xcccccc);
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Share", @"Share")];
    [self.view addSubview:customNavigationBarView];
    
}
-(void)leftBarButtonClicked:(UIButton *)sender{

    [self menuButtonclicked];

}

- (void)menuButtonclicked
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)facebookButtonClicked:(id)sender {
    
    [self shareOnFacebook];
}

- (IBAction)twitterButtonClicked:(id)sender {
    [self shareOnTwitter];
}

- (IBAction)smsButtonClicked:(id)sender {
    
    [self shareViaSMS];
}

- (IBAction)emailButtonClicked:(id)sender {
    [self EmaiSharing];
}



-(void)shareViaSMS {
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:TitleAlert message:NSLocalizedString(@"Your device doesn't support SMS!", @"Your device doesn't support SMS!") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *message = [NSString stringWithFormat:@"%@",shareText];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    //[messageController setRecipients:recipents];
    [messageController setBody:message];
   
    
    
    if([messageController.navigationBar respondsToSelector:@selector(barTintColor)]) {
        // iOS7
        // Set Navigation Bar Title Color
        [messageController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:OpenSans_SemiBold size:14], NSFontAttributeName, [UIColor blackColor], NSForegroundColorAttributeName, nil]];
        
        // Set back button color
        
    } 

    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
    
    
    
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:TitleError message:NSLocalizedString(@"Failed to send !", @"Failed to send !") delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)shareOnTwitter
{
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
           NSString *message = [NSString stringWithFormat:@"%@",shareText];
        
        [tweetSheet setInitialText:message];
        
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgLinkForSharing]]];
        [tweetSheet addImage:image];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:TitleMessage
                                  message:NSLocalizedString(@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup", @"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup")
                                  delegate:self
                                  cancelButtonTitle:TitleOk
                                  otherButtonTitles:nil];
        [alertView show];
    }
    
}


-(void)shareOnFacebook
{
    [self publishWithWebDialog];
}

/*
 * Share using the Web Dialog
 */
- (void) publishWithWebDialog {
    
    FBSDKShareLinkContent  *content = [[FBSDKShareLinkContent  alloc]init];
    content.contentURL = [NSURL URLWithString:@"http://MenDoPick.in"];
    content.contentTitle = NSLocalizedString(@"MenDoPick", @"MenDoPick");
    content.contentDescription = shareText;
    content.imageURL = [NSURL URLWithString:imgLinkForSharing];
    content.quote = shareText;
    
    FBSDKShareDialog* dialog = [[FBSDKShareDialog alloc] init];
    dialog.mode = FBSDKShareDialogModeAutomatic;
    dialog.shareContent = content;
    [dialog setDelegate:self];
    dialog.fromViewController = self;
    [dialog show];
    
}

#pragma mark - FBSDKSharingDelegate

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    
}

-(void)EmaiSharing
{
    if ([MFMailComposeViewController canSendMail])
    {
        
        mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        
        if([mailer.navigationBar respondsToSelector:@selector(barTintColor)]) {
            // Set Navigation Bar Title Color
            [mailer.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:OpenSans_SemiBold size:14], NSFontAttributeName, [UIColor blackColor], NSForegroundColorAttributeName, nil]];
            
            
        }

        NSString *shareUrl =  [NSString stringWithFormat:@"%@",shareText];
        [mailer setSubject:NSLocalizedString(@"Signup on MenDoPick and  Earn a Discount", @"Signup on MenDoPick and  Earn a Discount")];
       
        NSArray *toRecipents;
        NSMutableString* message =[[NSMutableString alloc] init];
        [message appendString:shareUrl];
        toRecipents = [NSArray arrayWithObject:@""];
        [mailer setMessageBody:message isHTML:NO];
        [mailer setToRecipients:toRecipents];
        NSString *strURL = [NSString stringWithFormat:@"%@",imgLinkForSharing];
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]]];
        
        NSData *photoData = UIImageJPEGRepresentation(image,1);
        [mailer addAttachmentData:photoData mimeType:@"image/jpg" fileName:[NSString stringWithFormat:@"photo.png"]];
        
         [self presentViewController:mailer animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Failure", @"Failure")
                                                        message:NSLocalizedString(@"Your device doesn't support the composer sheet", @"Your device doesn't support the composer sheet")
                                                       delegate:nil
                                              cancelButtonTitle:TitleOk
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - MFMailComposeViewController Delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
