//
//  RateViewController.h
//  privMD
//
//  Created by Rahul Sharma on 10/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView/AXRatingView.h>

@interface RateViewController : UIViewController<UITextViewDelegate>

@property (nonatomic,strong) MapViewController *mapViewContoller;
@property (weak, nonatomic) IBOutlet UIScrollView *mainscrollView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImage;
@property (weak, nonatomic) IBOutlet UILabel *labelDriverName;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalCost;
@property (weak, nonatomic) IBOutlet AXRatingView *ratingStarView;
@property (weak, nonatomic) IBOutlet UIView *viewDetails;
@property (weak, nonatomic) IBOutlet UITextField *textfieldComment;
@property (weak, nonatomic) IBOutlet UILabel *labelPickupAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelReceiverAddress;

@property (strong,nonatomic) NSDictionary *appointmentData;
@property (strong,nonatomic) NSString *shipmentId;
@property (strong,nonatomic) NSString *customerEmail;
@property (strong,nonatomic) NSString *appointmentDate;
@property (weak, nonatomic) IBOutlet UILabel *rateyourLabel;

- (IBAction)submitRatingButtonAction:(id)sender;

@end
