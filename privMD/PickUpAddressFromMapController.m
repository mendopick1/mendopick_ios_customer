//
//  Map1ViewController.m
//  Servodo
//
//  Created by Rahul Sharma on 10/09/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PickUpAddressFromMapController.h"
#import "HelpViewController.h"
#import "AppointmentLocation.h"
#import "Database.h"
#import "AddressManageViewController.h"
#import "PickAddressContainerView.h"
#import "RecepientDetailsViewController.h"
#import "Fonts.h"
#import "SaveSelectedAddressViewController.h"
#import "PickUpViewController.h"
#import "LanguageManager.h"
#import "ReceiverDetailViewController.h"
#import "AnimationsWrapperClass.h"
#import "ReceiverDetailViewController.h"

#define middleBtnTag 2000
#define curLocBtnTag 3000
#define myCustomMarkerTag 5000
#define topViewTag 90
#define pickupAddressTag 2500

@interface PickUpAddressFromMapController ()<UITextFieldDelegate,PickAddressDelegate,GMSMapViewDelegate,CLLocationManagerDelegate,AddressManageDelegate>
{
    GMSGeocoder *geocoder_;
    BOOL isCustomMarkerSelected;
    NSString *srcAddr;
    NSString *city;
    CGRect screenSize;
    BOOL isAddressManuallyPicked;
}
@property(nonatomic,strong) PickAddressContainerView *bookingControllerVC;

@property(nonatomic,strong) UIView *PikUpViewForConfirmScreen;
@property(nonatomic,assign) double currentLatitude;
@property(nonatomic,assign) double currentLongitude;
@property (strong, nonatomic) NSString *patientAddressLine2;
@property (assign, nonatomic) float patientAddressLong;
@property (assign, nonatomic) float patientAddressLati;
@property(nonatomic,strong) NSString *zipCode;
@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,assign) BOOL isUpdatedLocation;
@property (strong, nonatomic) UILabel *textFeildAddress;
@property AppointmentLocation *apptLocation;
@property(nonatomic,assign) BOOL isMarkerClicked;
@end

@implementation PickUpAddressFromMapController {
    
    NSArray *_tableData; // active list of data to show in table at the moment
    UIView* _searchBarWrapper;
    UIView *_shadowView;
   
}

NSString *const apiKeysd = @"AIzaSyDzFpzMhM014fzTzOErkt3M7nKRnzwz1hs";

@synthesize mapView_;




@synthesize apptLocation,PikUpViewForConfirmScreen;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    screenSize = [[UIScreen mainScreen] bounds];
    apptLocation = [AppointmentLocation sharedInstance];
    
    [self.navigationController.navigationBar setHidden:NO];
    
     [[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    
    [_currentLocationbutton addTarget:self action:@selector(getCurrentLocation:) forControlEvents:UIControlEventTouchUpInside];
    
   
    
//    if(_isFromMapVC == YES){
//        _currentLatitude = _latitude;
//        _currentLongitude = _longitude;
//        _isUpdatedLocation = YES;
//    }
//    else{
        _currentLatitude = [[[NSUserDefaults standardUserDefaults]objectForKey:KNUCurrentLat] doubleValue];
    
        _currentLongitude =  [[[NSUserDefaults standardUserDefaults]objectForKey:KNUCurrentLong] doubleValue];
        [self getCurrentLocation];
 //   }
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_currentLatitude longitude:_currentLongitude zoom:15];
    
    
    
     NSString *str = NSLocalizedString(@"Place the pin on exact location", @"Place the pin on exact location");
    
     NSString *str1 = NSLocalizedString(@"or Allow location services", @"or Allow location services");
    
    _messageLabel.text = str;
    _messageLabel1.text = str1;
    mapView_.camera = camera; // = [GMSMapView mapWithFrame:[self.view bounds] camera:camera];
    mapView_.delegate = self;
    [mapView_ clear];
    geocoder_ = [[GMSGeocoder alloc] init];
   
        UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
        [_topView addGestureRecognizer:tapgesture];
        [mapView_ addGestureRecognizer:tapgesture];
    self.topView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"pinit_location_small_white_background"]];
    [self createNavLeftButton];
    _addressLine1Label.textColor = UIColorFromRGB(0x333333);
   
    [Helper setCornerRadius:3.0 bordercolor:CLEAR_COLOR withBorderWidth:0 toview:_btnConfirmLocation];

}

- (void)getCurrentLocation:(UIButton *)sender{
    
    CLLocation *location = mapView_.myLocation;
    _currentLatitude = location.coordinate.latitude;
    _currentLongitude = location.coordinate.longitude;
    
    if (location) {
        
        GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude) zoom:15];
        [mapView_ animateWithCameraUpdate:zoomCamera];
    }
    
    
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [_flatNumberTextFiewld resignFirstResponder];
    [_addressLine1Label resignFirstResponder];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    [self dismissKeyboard];
    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    city = @"";
    
    if (_locationType == kSourceAddress)
    {
        self.navigationItem.title =  NSLocalizedString(@"Pickup Location",@"Pickup Location");
    }
    else
    {
        self.navigationItem.title =  NSLocalizedString(@"Drop Location",@"Drop Location");
    }

    if (_isFromMapVC) {
        self.navigationItem.title =  NSLocalizedString(@"Drop Location",@"Drop Location");
    }
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : UIColorFromRGB(0x565656)}];

    mapView_.myLocationEnabled = YES;
    mapView_.settings.compassButton = YES;
}

- (void) viewDidAppear:(BOOL)animated{

    if (IS_SIMULATOR) {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord zoom:15];
    }
}


-(void)viewWillDisappear:(BOOL)animated{
    
    _isFromHomeVC = NO;
    _isFromRecepientVC = NO;
}

/*-------------------------------- Navigation Bar ----------------------------------*/

-(void) createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIImage *buttonImage = [UIImage imageNamed:@"signin_back_icon_off"];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    UIImage *buttonImage1 = [UIImage imageNamed:@"signin_back_icon_on"];
    [navCancelButton setBackgroundImage:buttonImage1 forState:UIControlStateHighlighted];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        
        [navCancelButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 20,0.0f,buttonImage.size.width,buttonImage.size.height)];
    }

    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

- (void)getAddress:(CLLocationCoordinate2D)coordinate {
    
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response,
                                          NSError *error) {
        if (response && response.firstResult) {
            
            GMSAddress *address = response.firstResult;
            NSLog(@"Adress %@ ",address);
            NSLog(@"zipcode %@",address.postalCode);
            
            NSString *addresstext = [address.lines componentsJoinedByString:@","];

            _patientAddressLong =  response.firstResult.coordinate.longitude;
            
            _addressLine1Label.text =[NSString stringWithFormat:@"%@",addresstext];
            
            city = address.locality;
            
            
            if ([city isEqualToString:@"(null)"]) {
                city = @"";
                
            }

            
            
            if ([_addressLine1Label.text isEqualToString:@"(null)"]) {
                _addressLine1Label.text = @"";
            }
            
            
            if ([_flatNumberTextFiewld.text isEqualToString:@"(null)"]) {
                _flatNumberTextFiewld.text = @"";
            }
            
            CLLocationCoordinate2D addressCoordinates = address.coordinate;
            
            _currentLatitude = addressCoordinates.latitude;
            _currentLongitude = addressCoordinates.longitude;

        }
    };
    
    [geocoder_ reverseGeocodeCoordinate:coordinate
                      completionHandler:handler];
    
}


-(void)backButtonAction:(id)sender
{
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                              subType:kCATransitionFromBottom
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)getCurrentLocation{
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        }
        [_locationManager startUpdatingLocation];
        
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service" message:@"Unable to find your location,Please enable location services." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
#pragma mark - CLaddaddressDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {

    if (!_isUpdatedLocation) {
        
        [_locationManager stopUpdatingLocation];
        //change map camera postion to current location
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                                longitude:newLocation.coordinate.longitude
                                                                     zoom:15];
        [mapView_ setCamera:camera];
        //add marker at current location
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        //save current location to plot direciton on map
        _currentLatitude = newLocation.coordinate.latitude;
        _currentLongitude =  newLocation.coordinate.longitude;
        //get address for current location
        [self getAddress:position];
        //change flag that we have updated location once and we don't need it again
        _isUpdatedLocation = YES;
        
        NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
        NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied) {
        // The user denied your app access to location information.
        
    }
    NSLog(@"locationManager failed to update location : %@",[error localizedDescription]);
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showMessage:[error localizedDescription] On:self.view];
}

#pragma mark GMSMapviewDelegate -

- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture{
    
    NSLog(@"willMove");
    
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position{
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    NSLog(@"idleAtCameraPosition");
    
    if (_isMarkerClicked) {
        _isMarkerClicked = NO;
        return;
        
    }
    
    if (_isUpdatedLocation) {
        
        CGPoint point1 = mapView_.center;
        CLLocationCoordinate2D coor = [mapView_.projection coordinateForPoint:point1];
        NSLog(@"center Coordinate idleAtCameraPosition :%f %f",coor.latitude,coor.longitude);
        _currentLatitude = coor.latitude;
        _currentLongitude = coor.longitude;
        
        if (isAddressManuallyPicked)
        {
            isAddressManuallyPicked = NO;
            return;
        }

        else{
            
            [self getAddress:coor];
        }
    }
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    
    NSLog(@"marker userData %@",marker.userData);
    
    return nil;
}
- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didBeginDraggingMarker");
}
- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didEndDraggingMarker");
    
    
}


-(CLLocationCoordinate2D) getLocationFromAddressString:(NSString*) addressStr {
    
    CLLocationCoordinate2D center;
    
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    
    NSData *responseData = [[NSData alloc] initWithContentsOfURL:
                            [NSURL URLWithString:req]];    NSError *error;
    
    NSMutableDictionary *responseDictionary = [NSJSONSerialization
                                               JSONObjectWithData:responseData
                                               options:nil
                                               error:&error];
    if( error )
    {
        NSLog(@"%@", [error localizedDescription]);
        center.latitude = 0;
        center.longitude = 0;
        return center;
    }
    else {
        NSArray *results = (NSArray *) responseDictionary[@"results"];
        
        if (results.count>0) {
            NSDictionary *firstItem = (NSDictionary *) [results objectAtIndex:0];
            NSDictionary *geometry = (NSDictionary *) [firstItem objectForKey:@"geometry"];
            NSDictionary *location = (NSDictionary *) [geometry objectForKey:@"location"];
            NSNumber *lat = (NSNumber *) [location objectForKey:@"lat"];
            NSNumber *lng = (NSNumber *) [location objectForKey:@"lng"];
            
            center.latitude = [lat doubleValue];
            center.longitude = [lng doubleValue];
            return center;
        }
        else
        {
            center.latitude = 0;
            center.longitude = 0;
        }
    }
    return center;
}

#pragma textfield delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    NSLog(@"textFieldShouldReturn:");
    
    if (textField == _flatNumberTextFiewld) {
        
        
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}

-(void)pickedAddressWithLat:(float)latitude andLong:(float)longitude andAddress:(NSDictionary *)addressText
{

    NSString *add1 = addressText[@"add1"];
    NSString *add2 = [addressText objectForKey:@"add2"];
    
    if(add1.length > add2.length || add1.length > 35){
        
        _addressLine1Label.text = [NSString stringWithFormat:@"%@,%@",addressText[@"add1"],addressText[@"add2"]];
        
    }
    else{
        
        _addressLine1Label.text = addressText[@"add2"];
        
    }
    
    isAddressManuallyPicked = YES;
    _currentLatitude = [addressText[@"lat"] doubleValue];
    _currentLongitude = [addressText[@"long"] doubleValue];
    CLLocation *location = [[CLLocation alloc]initWithLatitude:_currentLatitude longitude:_currentLongitude];
    mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:15];
    [self showContainerViewControllerWithAnimation:NO];
}






#pragma mark - Google API Requests


-(void)retrieveGooglePlaceInformation:(NSString *)searchWord withCompletion:(void (^)(NSArray *))complete{
    NSString *searchWordProtection = [searchWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (searchWordProtection.length != 0) {
        
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=establishment|geocode&location=%f,%f&radius=500po &language=en&key=%@",searchWord,_currentLatitude,_currentLongitude,apiKeysd];
        NSLog(@"AutoComplete URL: %@",urlString);
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSArray *results = [jSONresult valueForKey:@"predictions"];
            
            if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
                if (!error){
                    NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                    NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                    complete(@[@"API Error", newError]);
                    return;
                }
                complete(@[@"Actual Error", error]);
                return;
            }else{
                complete(results);
            }
        }];
        
        [task resume];
    }
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"toSaveAddressVC"])
    {
        SaveSelectedAddressViewController *saveAddressVC = [segue destinationViewController];
        
        saveAddressVC.selectedAddressDetails = @{
                                                 @"address":flStrForStr(self.addressLine1Label.text),
                                                 @"lat":[NSNumber numberWithDouble:_currentLatitude],
                                                 @"log":[NSNumber numberWithDouble:_currentLongitude],
                                                 };
        
    }

    
    
}

- (void)showContainerViewControllerWithAnimation:(BOOL)isShowing {
    
    if (isShowing) {
        _shadowView.hidden = NO;
        _topView.hidden = YES;
        
    _arrayOfAddress = [[NSMutableArray alloc]init];
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options: UIViewAnimationOptionTransitionNone|UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                     }
     
                     completion:^(BOOL finished){
                         
                     }];

    }
    else {
        _topView.hidden = NO;
        
        [UIView animateWithDuration:0.5f
                              delay:0.0f
                            options: UIViewAnimationOptionTransitionNone|UIViewAnimationOptionCurveEaseIn
                         animations:^{
                         }
                         completion:^(BOOL finished){
                             
                         }];
        _shadowView.hidden = YES;
    }
    
}
- (IBAction)confirmLocation:(id)sender
{
    if (_isFromMapVC)
    {
        [self gotoViewController:nil];
    }
    else
    {
        NSDictionary *dict = @{@"lat":[NSNumber numberWithDouble:_currentLatitude],
                               @"lng":[NSNumber numberWithDouble:_currentLongitude],
                               @"address":flStrForStr(self.addressLine1Label.text),
                               @"locationtype":[NSNumber numberWithInteger:_locationType]
                               };
        
         [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationChangeAddress object:nil userInfo:dict];
        
        
        for (UIViewController* viewController in self.navigationController.viewControllers) {
            if ([viewController isKindOfClass:[ReceiverDetailViewController class]] )
            {
                [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                                          subType:kCATransitionFromTop
                                                          forView:self.navigationController.view
                                                     timeDuration:0.3];
                
                [self.navigationController popToViewController:viewController animated:NO];
                return;
            }
        }
    }
}

-(void)gotoViewController:(NSDictionary *)dict {
    
    AppointmentLocation *ap = [AppointmentLocation sharedInstance];
    
    ap.desAddressLine1 = flStrForStr(self.addressLine1Label.text);
    
    ap.dropOffLatitude = [NSNumber numberWithDouble:_currentLatitude];
    
    ap.dropOffLongitude = [NSNumber numberWithDouble:_currentLongitude];
    
    ReceiverDetailViewController *rcdt = [self.storyboard instantiateViewControllerWithIdentifier:@"ReceiverDetailViewController"];
    
    [self.navigationController pushViewController:rcdt animated:YES];
    
}


- (IBAction)goToChooseAddress:(id)sender
{
    [self goToPickAddress];
}

-(void)goToPickAddress
{
//    PickUpViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"pick"];
////    if( sender.tag == 91){
//        pickController.locationType = kSourceAddress;
//        pickController.isComingFromMapVCFareButton = NO;
////    }
//    
////    pickController.typeID = vehicleTypeTag;
//    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
//    
//    pickController.latitude =  [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.latitude];
//    pickController.longitude = [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.longitude];
//    pickController.onCompletion = ^(NSDictionary *dict,NSInteger locationType)
//    {
//        NSString *addressText = flStrForStr(dict[@"address1"]);
//        if ([dict[@"address2"] rangeOfString:addressText].location == NSNotFound)
//        {
//            addressText = [addressText stringByAppendingString:@","];
//            addressText = [addressText stringByAppendingString:flStrForStr(dict[@"address2"])];
//        }
//        else
//        {
//            addressText = flStrForStr(dict[@"address2"]);
//        }
//        if(locationType == 1)
//        {
//            _addressLine1Label.text = addressText;
//            _zipCodeTextField.text = @"";
//            isAddressManuallyPicked = YES;
//            _currentLatitude = [dict[@"lat"] doubleValue];
//            _currentLongitude = [dict[@"lng"] doubleValue];
//            [_searchBar resignFirstResponder];
//            CLLocation *location = [[CLLocation alloc]initWithLatitude:_currentLatitude longitude:_currentLongitude];
//            mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:15];
//
//        }
//    };
//    
//    [self presentViewController:navBar animated:YES completion:nil];
}


@end
