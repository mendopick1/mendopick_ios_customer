//
//  NewOrderViewController.h
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 11/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NewOrderViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *labelMessage;

+(instancetype)getSharedInstance;
-(void) orderSummary;
@end
