//
//  ShipmentAddViewController.m
//  MenDoPick
//
//  Created by Rahul Sharma on 18/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ShipmentAddViewController.h"
#import "CustomNavigationBar.h"
#import "ShipmentNoteTableViewCell.h"
#import "WeghtDetailTableViewCell.h"
#import "RecepientListViewController.h"
#import "PatientGetLocalCurrency.h"
#import "LanguageManager.h"
#import "AppointmentLocation.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "AmazonTransfer.h"
#import "Database.h"
#import "ShipmentValueTableViewCell.h"


typedef enum : NSUInteger {
    CellShipmentDetails=0,
    CellShipmentItem=1,
    CellShipmentValue = 2,
} addShipmentBookingCellNo;


//Localization strings
#define weight NSLocalizedString(@"Weight", @"Weight")
#define quantity NSLocalizedString(@"Quantity", @"Quantity")
#define shipmentValue NSLocalizedString(@"Shipment Value", @"Shipment Value")
#define AddshipmentPlaceholder NSLocalizedString(@"Please add shipment details here..",@"Please add shipment details here..")


@interface ShipmentAddViewController ()<UITableViewDelegate,UITableViewDataSource,CustomNavigationBarDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate>
{
    NSMutableArray *itemArray;
           
    WeghtDetailTableViewCell *wcell;
    AppointmentLocation *ap;
    NSUserDefaults *ud;
    UIImage *_pickedImage;
    NSString *shipmentImageUrl;
    
    NSInteger selectedWeight1;
    NSInteger selectedWeight2;
    NSInteger selectedQuantity;
    
    BOOL isWeightSelected;
    BOOL isQuantitySelected;
    BOOL isPickerOpened;
    
    NSString *paidByCustomer;
    NSString *shipmentvalue;
    
}
@property (strong,nonatomic)  UITextField *activeTextField;
@property (strong,nonatomic)  UITextView *activeTextView;
@end

@implementation ShipmentAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialiseVariableAndUI];

    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated
{
    [self regiesterNotification];
    self.navigationController.navigationBarHidden = NO;
    [[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    [self createNavView];
    ap = [AppointmentLocation sharedInstance];
    self.navigationItem.hidesBackButton = YES;
    [self createNavLeftButton];
    _constraintPickerViewTop.constant = -self.view.frame.size.height *0.37;
}

-(void)viewWillDisappear:(BOOL)animated
{
     [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)regiesterNotification
{
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
   // tapGesture.delegate = self;
    [self.view addGestureRecognizer:tapGesture];
}

-(void)initialiseVariableAndUI
{
    
    itemArray = [[NSMutableArray alloc]initWithObjects:weight,nil];
    ud = [NSUserDefaults standardUserDefaults];
    ap = [AppointmentLocation sharedInstance];
    _labelDeliveryFee.text = [PatientGetLocalCurrency getCurrencyLocal:[ap.deliveryFee floatValue]];
    paidByCustomer = @"0";
    shipmentvalue = @"";
    shipmentImageUrl = @"";
    
    _labelDeliveryFeeText.text = LS(@"Delivery Fee");
    
    _labelSendToCustomerText.text = LS(@"Send To Customer");
    
    _labelAddPhotos.text = LS(@"Add Photos");
    
   if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {       _constraintHorizontallyCenterSendToCustomerLabel.constant = -45;
   }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (_activeTextField) {
        _activeTextField = nil;
        [self dismissKeyboard];
    }
    else if (isPickerOpened)
    {
        return NO;
    }
    
    if ([touch.view isDescendantOfView:_tableviewShipment]) {
        
        return NO;
    }
    return YES;
}


#pragma -mark - navigationbar delegate -

-(void)createNavView
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80,-10, 160, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(10,10, 147, 30)];
    navTitle.text = NSLocalizedString(@"Add Shipment",@"Add Shipment");
    navTitle.textColor = UIColorFromRGB(0x565656);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:Hind_Medium size:17];
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;
}

-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"signin_back_icon_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"signin_back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    
   if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
       [navCancelButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 40,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    }
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"toShowRecepientsListVC"])
    {
        RecepientListViewController *rlVC  = [segue destinationViewController];
        rlVC =[segue destinationViewController];
        rlVC.shipmentData = sender;
    }
}

#pragma mark - tableview Delegates and Datasource -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([paidByCustomer isEqualToString:@"0"] && section == CellShipmentValue) {
        return 0;
    }
        return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case CellShipmentDetails:
        {
            return 95;
         break;
        }
        case CellShipmentItem:
        {
            return 110;
        }
         break;
            default:
            return 70;
            break;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *tableidentifier = @"ShipmentNoteTableViewCell";
    
    
    switch (indexPath.section) {
        case CellShipmentDetails:
        {
            ShipmentNoteTableViewCell *cell = (ShipmentNoteTableViewCell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
            if([cell.txtviewShipmentDetail.text isEqualToString:@""])
            {
                cell.txtviewShipmentDetail.text = AddshipmentPlaceholder;
            }
            else if (![cell.txtviewShipmentDetail.text isEqualToString:AddshipmentPlaceholder]) {
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
            break;
            case CellShipmentItem:
        {
            
            tableidentifier = @"WeghtDetailTableViewCell";
            wcell = (WeghtDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
            wcell.selectionStyle = UITableViewCellSelectionStyleNone;
            [wcell loadCellData:itemArray :indexPath.row];
            wcell.txtfieldItemValue.tag = indexPath.row;
            [wcell.btnPaidByCustomer addTarget:self action:@selector(paidBycustomerCheckAction:) forControlEvents:UIControlEventTouchUpInside];
            return wcell;
            
        }
            break;
            
            case CellShipmentValue:
        {
            tableidentifier = @"ShipmentValueTableViewCell";
            ShipmentValueTableViewCell *cell = (ShipmentValueTableViewCell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;

        }
            break;
            
        default:
            return nil;
            break;
    }
    
}


-(void)reloadParticulaeSection
{
    NSRange range = NSMakeRange(1, 0);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableviewShipment reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
}

-(void)paidBycustomerCheckAction:(UIButton *)btn
{
    if (isPickerOpened)
    {
        [self pickerdownMethod];
    }
    else if (_activeTextView || _activeTextField)
    {
        [_activeTextField resignFirstResponder];
        [_activeTextView resignFirstResponder];
        _activeTextView = nil;
        _activeTextField = nil;
    }
    [self.view endEditing:YES];
        if (btn.isSelected) {
            btn.selected = NO;
            paidByCustomer = @"0";
            [itemArray removeLastObject];
            [_tableviewShipment reloadData];
        } else {
            btn.selected = YES;
            paidByCustomer = @"1";
            [itemArray addObject:shipmentValue];
            [_tableviewShipment reloadData];
           // [self scrollToPosition:ind4];
        }
}

#pragma - mark - TextfieldDelegate -

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    NSArray  *arrayOfString = [newString componentsSeparatedByString:@"."];
    
    if ([arrayOfString count]>1) {
    if ([arrayOfString[1] length] > 2)
    {
        return NO;
    }
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (_activeTextField && _activeTextField.tag == 2) {
        [_activeTextField resignFirstResponder];
        _activeTextField = nil;
        return NO;
    }
    else if (isPickerOpened)
    {
        [self pickerdownMethod];
    }
    
    self.activeTextField = textField;
    if (textField.tag != 2) {
        [self dismissKeyboard];
        WeghtDetailTableViewCell  * currentCell = (WeghtDetailTableViewCell *) textField.superview.superview;
        NSIndexPath * currentIndexPath = [self.tableviewShipment indexPathForCell:currentCell];
        [self.tableviewShipment scrollToRowAtIndexPath:currentIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        [_tableviewShipment layoutIfNeeded];
        [self.view layoutIfNeeded];
        
        [self pickerAnimationMethod];
        return NO;
    }
    else
    {
        _activeTextField = textField;        
        return YES;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == 2 ) {
        shipmentvalue = textField.text;
        
        if(textField.text.length>0){
        textField.text = [NSString stringWithFormat:@"%@",[PatientGetLocalCurrency getCurrencyLocal:[textField.text floatValue]]];
    }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - TextView Delegate methods -

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if (_activeTextField) {
        [self dismissKeyboard];
        _activeTextField = nil;
        return NO;
    }
    else if (isPickerOpened)
    {
        [self pickerdownMethod];
    }

    self.activeTextView = textView;
    if ([textView.text isEqualToString:AddshipmentPlaceholder]) {
        textView.text = @"";
        textView.textColor = UIColorFromRGB(0x646464);
    }
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    if(textView.text.length == 0){
        textView.textColor = UIColorFromRGB(0xC7C7CD);
        textView.text = AddshipmentPlaceholder;
        [textView resignFirstResponder];
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    if (![textView.text isEqualToString:AddshipmentPlaceholder]) {
    }
    if(textView.text.length != 0)
    {
       // _messageToDriver = textView.text;
        
    }
    else if(textView.text.length == 0){
        textView.textColor = UIColorFromRGB(0xC7C7CD);
        textView.text = AddshipmentPlaceholder;
        
    }
    
    [textView resignFirstResponder];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if(textView.text.length == 0){
            textView.textColor = UIColorFromRGB(0xC7C7CD);
            textView.text = AddshipmentPlaceholder;
        }
        return NO;
    }
    return YES;
}

-(void)dismissKeyboard
{
    if (_activeTextField || _activeTextView) {
        [self.view endEditing:YES];
        [_activeTextField resignFirstResponder];
        [_activeTextView resignFirstResponder];
    }
}

#pragma mark - Action methods -

- (IBAction)addShipmentAction:(id)sender
{
    [self.view endEditing:YES];
    if (isPickerOpened) {
        [self pickerdownMethod];
    }
    
    UIActionSheet *actionSheet;
    if (_pickedImage != nil) {
        
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture")
                                                  delegate:self
                                         cancelButtonTitle:TitleCancel
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:TitlePhoto,
                       TitlePhotoLibrary,
                       TitleRemovePhoto,nil];
        actionSheet.tag = 200;
    }
    else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture")
                                                  delegate:self
                                         cancelButtonTitle:TitleCancel
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:TitlePhoto,
                       TitlePhotoLibrary,nil];
        actionSheet.tag = 100;
}
    [actionSheet showInView:self.view];
}


-(void)deSelectProfileImage
{
    _imageviewShipment.image =  [UIImage imageNamed:@"add_shipment_default_image_frame"];
    [_tableviewHeaderView bringSubviewToFront:_btnAddphoto];
    _pickedImage = nil;
}

-(void)cameraButtonClicked
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TitleMessage message: NSLocalizedString(@"Camera is not available", @"Camera is not available") delegate:nil cancelButtonTitle:TitleOk otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)libraryButtonClicked
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (IBAction)sendToCustomerBtnAction:(id)sender
{
    [self dismissKeyboard];
    NSIndexPath *ind2 = [NSIndexPath indexPathForRow:0 inSection:1];
    
    WeghtDetailTableViewCell *cell2 = (WeghtDetailTableViewCell *)[self.tableviewShipment cellForRowAtIndexPath:ind2];
    
    if(cell2.txtfieldItemValue.text.length == 0) {
        
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Enter Weight",@"Enter Weight")];
        return;
        
    }
    else if(cell2.textfieldQuantity.text.length == 0) {
        
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Enter Quantity",@"Enter Quantity")];
        return;
        
    }
    else if([paidByCustomer isEqualToString:@"1"]  && shipmentvalue.length == 0) {
        
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Enter Shipment Value",@"Enter Shipment Value")];
        return;
    }
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Confirming your order...",@"Confirming your order...")];
    
    if (_pickedImage != nil) {
        
        NSString *name = [NSString stringWithFormat:@"%@%@.jpg",@"image",[Helper getCurrentTime]];
        NSString *fullImageName = [NSString stringWithFormat:@"%@/ShipmentImage/%@",@"MenDoImages",name];
        NSString *getImagePath = [[NSUserDefaults standardUserDefaults] objectForKey:@"imagePath"];
        NSData *data = UIImageJPEGRepresentation(_pickedImage,0.6);
        [data writeToFile:getImagePath atomically:YES];
        
        [AmazonTransfer upload:getImagePath
                       fileKey:fullImageName
                      toBucket:Bucket
                      mimeType:@"image/jpeg"
                 progressBlock:^(NSInteger progressSize, NSInteger expectedSize) {
                     
                 } completionBlock:^(BOOL success, id result, NSError *error) {
                     
                     shipmentImageUrl = [NSString stringWithFormat:@"%@%@/%@",imgLinkForAmazon,Bucket,fullImageName];
                     [self createOrder:cell2.txtfieldItemValue.text and:cell2.textfieldQuantity.text];
                 }];
    }
    else
    {
         [self createOrder:cell2.txtfieldItemValue.text and:cell2.textfieldQuantity.text];
    }
}

#pragma mark - Web Service Call

-(void)createOrder:(NSString *)weigh and:(NSString *)quant
{
    NSIndexPath *ind1 = [NSIndexPath indexPathForRow:0 inSection:0];
    ShipmentNoteTableViewCell *cell1 = (ShipmentNoteTableViewCell *)[self.tableviewShipment cellForRowAtIndexPath:ind1];
    
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }

    NSString *shipmentdetails = cell1.txtviewShipmentDetail.text;
    if ([cell1.txtviewShipmentDetail.text isEqualToString:AddshipmentPlaceholder] || [shipmentdetails stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        shipmentdetails =@"";
    }
    else
    {
        shipmentdetails = cell1.txtviewShipmentDetail.text;
    }
    NSMutableString *countryCodeWithPhone = [NSMutableString stringWithFormat:@"%@",ap.receiverPhoneCountryCode];
    
    [countryCodeWithPhone appendString:ap.receiverPhone];

    
    NSDictionary *shipmentDetail = @{
                                     
                                     @"ent_receiver_name":flStrForObj(ap.receiverName),
                                     @"ent_receiver_mobile":flStrForObj(countryCodeWithPhone),
                                     @"ent_receiver_email":flStrForObj(ap.receiverEmail),
                                     @"ent_receiver_address":flStrForObj(ap.desAddressLine1),
                                     @"ent_receiver_landmark":@"",
                                     @"ent_receiver_flat_number":@"",
                                     @"ent_receiver_zip":@"",
                                     @"ent_receiver_city":@"",
                                     @"photo":shipmentImageUrl,
                                     @"quantity":quant,
                                     @"weight":weigh,
                                     @"volume":@"12",
                                     @"length":@"123",
                                     @"width":@"324",
                                     @"height":@"23",
                                     @"ent_drop_lat":flStrForObj(ap.dropOffLatitude),
                                     @"ent_drop_long":flStrForObj(ap.dropOffLongitude),
                                     @"passanger_chn":flStrForObj([ud objectForKey:kNSUPatientPubNubChannelkey]),
                                     @"ent_dist":shipmentvalue,
                                     @"ent_Approxcost":flStrForObj(ap.deliveryFee),
                                     @"ent_Value_paidBy":paidByCustomer,
                                     @"ent_shipment_Value":shipmentvalue,
                                     @"product_name":shipmentdetails,
                                     @"additional_info":shipmentdetails,
                                     
                                     };
    
    NSMutableArray *shipmentArray = [[NSMutableArray alloc]init];
    [shipmentArray addObject:shipmentDetail];
    
    NSDictionary *dict1 =  @{
                             @"ent_sess_token":[ud objectForKey:KDAcheckUserSessionToken],
                             @"ent_dev_id":deviceId,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             @"ent_wrk_type":flStrForObj([NSNumber numberWithInteger:ap.typeID]),
                             @"ent_addr_line1":flStrForObj(ap.srcAddressLine1),
                             @"ent_lat":flStrForObj(ap.pickupLatitude),
                             @"ent_long":flStrForObj(ap.pickupLongitude),
                             @"ent_payment_type":@"0",
                             @"additional_info":@"",
                             @"shipemnt_details":shipmentArray,
                             };
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:kSMLiveBooking
                              paramas:dict1
                         onComplition:^(BOOL success , NSDictionary *response){
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             if (success)
                             {
                                 [self shipmentBookingResponse:response];
                             }
                             else if(response == nil)
                             {
                                 [self.navigationController popToRootViewControllerAnimated:YES];
                             }
                         }];
}

-(void)shipmentBookingResponse:(NSDictionary *)response
{
    if(!response)
    {
        return;
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 7)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:@"Message" Message:[response objectForKey:@"errMsg"]];
    }
    else {
        
        // 39 ->livebooking, 78 -> laterbooking, 71 -> Notaccepted By Driver
        if( [response[@"errNum"] integerValue] == 71 || [response[@"errFlag"] integerValue] == 1){
            [self.navigationController popViewControllerAnimated:YES];
        }
        else {
            
            [self storeRecepientDetails];
            [self.navigationController popToRootViewControllerAnimated:YES];
            [Helper showAlertWithTitle:@"Message" Message:NSLocalizedString(@"The booking has been sent to the customer for confirmation.", @"The booking has been sent to the customer for confirmation.")];
        }
    }
}

//store receiver data in local
-(void)storeRecepientDetails
{

    Database *db = [[Database alloc] init];
    NSArray * ert = [Database getParticularRecepientsDetailsUsingMobile:ap.receiverPhone];
    
    if (ert.count == 0 ) {
    
        NSInteger randomNumber = arc4random();
        
        NSDictionary *add = @{
                              @"address":flStrForObj(ap.desAddressLine1),
                              @"recepientEmail":flStrForObj(ap.receiverEmail),
                              @"phoneNumber":flStrForObj(ap.receiverPhone),
                              @"name":flStrForObj(ap.receiverName),
                              @"randomNumber":[NSNumber numberWithInteger:randomNumber],
                              @"passengerEmail":[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey],
                              @"landMark":@"",
                              @"city":@"",
                              @"flatNumber":@"",
                              @"zipCode":@"",
                              @"dropLatitude":ap.dropOffLatitude,
                              @"dropLongitude":ap.dropOffLongitude,
                              };
        
    
        BOOL isAdded = [db makeDataBaseEntryForRecepients:add];
        
        if (isAdded)
        {
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Saving failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
    }
}

#pragma mark - image picker -

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    _pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    picker.allowsEditing = YES;
    
    _pickedImage = [self imageWithImage:_pickedImage scaledToSize:[self makeSize:_pickedImage.size fitInSize:CGSizeMake(304, 242)]];
    
    _imageviewShipment.image = _pickedImage;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddhhmmssa"];
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[formatter stringFromDate:[NSDate date]]]];
    
    [[NSUserDefaults standardUserDefaults]setObject:getImagePath forKey:@"imagePath"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [_tableviewHeaderView bringSubviewToFront:_imageviewShipment];
}

- (CGSize)makeSize:(CGSize)originalSize fitInSize:(CGSize)boxSize
{
    float widthScale = 0;
    float heightScale = 0;
    
    widthScale = boxSize.width/originalSize.width;
    heightScale = boxSize.height/originalSize.height;
    
    float scale = MIN(widthScale, heightScale);
    
    CGSize newSize = CGSizeMake(originalSize.width * scale, originalSize.height * scale);
    
    return newSize;
}


-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage*)imageWithImage:(UIImage *)image
{
    CGSize newSize = CGSizeMake(150,120);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
-(void)scrollToPosition:(NSIndexPath *)path
{
  [self.tableviewShipment scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

#pragma mark - actionsheet delegate - 

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked];
                break;
            }
            default:
                break;
        }
    }
    else  if (actionSheet.tag == 200) {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked];
                break;
            }
            case 2:
            {
                [self deSelectProfileImage];
                break;
            }
                
            default:
                break;
        }
    }
}

#pragma mark - PickerView DataSource -
//Method to define how many columns/dials to show
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
        return 1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 320;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *retval = (id)view;
    if (!retval)
    {
        retval= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)];
    }

        retval.textAlignment = NSTextAlignmentCenter;
        [Helper setToLabel:retval Text:[NSString stringWithFormat:@"%ld",(long)(row+1)] WithFont:Hind_Regular FSize:15 Color:BLACK_COLOR];
    return retval;
}

// Method to define the numberOfRows in a component using the array.
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent :(NSInteger)component
{
        return 50;
}

#pragma -mark - Picker Animation -
-(void)pickerAnimationMethod
{
    [_pickerView reloadAllComponents];
    _pickerView.dataSource = self;
    _pickerView.delegate = self;
    if(_activeTextField.tag == 0){
        _pickerTitle.text = NSLocalizedString(@"Select Weight", @"Select Weight");
        if(isWeightSelected == YES) {
            [_pickerView selectRow:selectedWeight1 inComponent:0 animated:YES];
        } else {
            [_pickerView selectRow:0 inComponent:0 animated:YES];
        }
    } else {
        _pickerTitle.text = NSLocalizedString(@"Select Quantity",@"Select Quantity");
        if(isQuantitySelected == YES) {
            
            [_pickerView selectRow:selectedQuantity inComponent:0 animated:YES];
            
        } else {
            [_pickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
    isPickerOpened = YES;
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.6f animations:^{
    _constraintPickerViewTop.constant = 0;
    [self.viewPickerBox layoutIfNeeded];
    [self.view layoutIfNeeded];
        
    }];
}

-(void)pickerdownMethod
{
    isPickerOpened = NO;
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.6f animations:^{
        _constraintPickerViewTop.constant = -self.view.frame.size.height *0.35;
        [self.view layoutIfNeeded];
        [self.viewPickerBox layoutIfNeeded];
        
    }];
}

- (IBAction)doneButtonAction:(id)sender {
    
    NSInteger row = [_pickerView selectedRowInComponent:0];
    if(_activeTextField.tag == 0){
        
        _activeTextField.text = [NSString stringWithFormat:@"%ld Kg",(long)row+1];
        isWeightSelected = YES;
        selectedWeight1 = row;
    }
    else{
        _activeTextField.text = [NSString stringWithFormat:@"%ld",(long)(row+1)];
        isQuantitySelected = YES;
        selectedQuantity = row;
    }
    [self pickerdownMethod];
}

- (IBAction)cancelButtonAction:(id)sender {
    [self pickerdownMethod];
}

#pragma mark - Keyboard methods -

- (void)keyboardWillHide:(__unused NSNotification *)inputViewNotification
{
    if(_activeTextField)
    {
    [self moveViewDown];
    }
    _activeTextField = nil;
    _activeTextView = nil;
}

- (void)keyboardWillShow:(__unused NSNotification *)inputViewNotification
{
    CGSize keyboardSize = [[[inputViewNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    float height = MIN(keyboardSize.height,keyboardSize.width);
    if (_activeTextField) {
        
        [self moveViewUp:_activeTextField andKeyboardHeight:height];
    }
}

- (void)moveViewUp:(UIView *)textfield andKeyboardHeight:(float)height
{
    
    float textfieldMaxY = CGRectGetMaxY(textfield.frame);
    UIView *view = [textfield superview];
    
    while (view != [self.view superview]) {
        textfieldMaxY += CGRectGetMinY(view.frame);
        view = [view superview];
    }
    
    float remainder = CGRectGetHeight(self.view.window.frame) - (textfieldMaxY + height+64);
    if (remainder >= 0) {
        
    }
    else {
        [UIView animateWithDuration:0.4
                         animations:^{
                             _tableviewShipment.contentOffset = CGPointMake(0, -remainder);
                         }];
    }
}

- (void)moveViewDown {
    self.tableviewShipment.contentOffset = CGPointMake(0, 0);
}



@end
