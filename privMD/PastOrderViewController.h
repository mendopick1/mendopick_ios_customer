//
//  PastOrderViewController.h
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 12/06/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PastOrderViewControllerDelegate <NSObject>

-(void)goToCompletedVc:(NSInteger)index and:(NSDictionary *)response;

@end

@interface PastOrderViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *pastOrderTableView;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;

@property (strong, nonatomic) NSMutableArray *allshipmentDetails;

@property(nonatomic,strong) id<PastOrderViewControllerDelegate> delegate;

-(void)getDataFromOrderVC:(NSArray *)item;
+ (instancetype) getSharedInstance;
@end
