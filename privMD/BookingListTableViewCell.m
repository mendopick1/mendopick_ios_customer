//
//  BookingListTableViewCell.m
//  MenDoPick
//
//  Created by Rahul Sharma on 18/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "BookingListTableViewCell.h"
#import "PatientGetLocalCurrency.h"


@implementation BookingListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [Helper setCornerRadius:4.0 bordercolor:UIColorFromRGB(0xD2D2D2) withBorderWidth:1.0 toview:_viewContent];
    
  //  [Helper setCornerRadius:4.0 bordercolor:UIColorFromRGB(0xb6b6b6) withBorderWidth:0.9 toview:_viewContent];
    
    [Helper makeShadowForView:_viewContent];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)loadCellData:(NSDictionary *)shipmentValue
{
    self.labelShipmentValue.text = [PatientGetLocalCurrency getCurrencyLocal:[shipmentValue[@"shipementValue"] floatValue]];
    self.labelName.text = shipmentValue[@"name"];
    self.labelFareValue.text = [PatientGetLocalCurrency getCurrencyLocal:[shipmentValue[@"ApproxFare"] floatValue]];

    NSString *str = LS(@"Job Id");
    self.labelBid.text = [NSString stringWithFormat:@"%@: %@",str,shipmentValue[@"bookingId"]];
    
    NSString *myString = shipmentValue[@"appointment_date"];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"dd MMM yyyy";
    
    self.labelDate.text = [dateFormatter stringFromDate:yourDate];
    
    switch ([shipmentValue[@"status"] integerValue]) {
        case KPending:
            [Helper setCornerRadius:0.0 bordercolor:UIColorFromRGB(0xD43E30) withBorderWidth:0.7 toview:_viewBox];
            _labelType.text = LS(@"New");
            _labelType.textColor = UIColorFromRGB(0xD43E30);
            break;
        case KConfirm:
            _labelType.text = LS(@"Confirmed");
            [Helper setCornerRadius:0.0 bordercolor:UIColorFromRGB(0x00AEEF) withBorderWidth:0.7 toview:_viewBox];
            _labelType.textColor = UIColorFromRGB(0x00AEEF);
            break;
        case Kongoing:
            _labelType.text = LS(@"OnGoing");
            break;
        default:
            break;
    }
    
}




@end
