//
//  XDKAirMenuController.m
//  XDKAirMenu
//
//  Created by Xavier De Koninck on 29/12/2013.
//  Copyright (c) 2013 XavierDeKoninck. All rights reserved.
//

#import "XDKAirMenuController.h"
#import "XDKMenuCell.h"
#import "User.h"
#import "PatientViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "LanguageManager.h"

#define WIDTH_OPENED (80.f)
#define MIN_SCALE_CONTROLLER (1.0f)
#define MIN_SCALE_TABLEVIEW (0.8f)
#define MIN_ALPHA_TABLEVIEW (0.01f)
#define DELTA_OPENING (65.f)

#define NewDelivery NSLocalizedString(@"Home",@"Home")

#define MyOrder NSLocalizedString(@"My Order",@"My Order")

#define Payment NSLocalizedString(@"Payment Activity",@"Payment Activity")

#define Setting NSLocalizedString(@"Settings",@"Settings")

#define Support NSLocalizedString(@"Contact us",@"Contact us")

#define About NSLocalizedString(@"About Mendopick",@"About Mendopick")

#define Share NSLocalizedString(@"Invite",@"Invite")

#define Profile NSLocalizedString(@"Profile",@"Profile")

@interface XDKAirMenuController ()<UITableViewDelegate,UIGestureRecognizerDelegate,UITableViewDataSource,UserDelegate>

@property (nonatomic, strong) NSArray *images_off;
@property (nonatomic, strong) NSArray *images_on;
@property (nonatomic, strong) NSArray *menuTitle;

@property (nonatomic, assign) CGPoint startLocation;
@property (nonatomic, assign) CGPoint lastLocation;
@property (nonatomic, weak) UITableView *tableView;

@property (nonatomic, assign) CGFloat widthOpened;
@property (nonatomic, assign) CGFloat minScaleController;
@property (nonatomic, assign) CGFloat minScaleTableView;
@property (nonatomic, assign) CGFloat minAlphaTableView;
@property UIPanGestureRecognizer *panGesture, *panGesture1;

@end


@implementation XDKAirMenuController
@synthesize panGesture,panGesture1;

static XDKAirMenuController *controller = nil;

+ (id)sharedMenu {
    
    if (!controller) {
        controller  = [[XDKAirMenuController alloc] init];
    }
    
    return controller;
}
-(void)sharedRelease {
    
    for (UIGestureRecognizer *recognizer in self.currentViewController.view.gestureRecognizers) {
        if ([recognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
            
        }else {
            NSLog(@"removing gesture : %@", recognizer);
            recognizer.delegate = nil;
            [self.currentViewController.view removeGestureRecognizer:recognizer];
        }
    }
    
}

+(BOOL)relese {
    
    [controller.panGesture.view removeGestureRecognizer:controller.panGesture];
    [controller.panGesture1.view removeGestureRecognizer:controller.panGesture1];
    controller.panGesture.delegate = nil;
    controller.panGesture = nil;
    controller.panGesture1.delegate = nil;
    controller.panGesture1 = nil;
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    
    for (UIGestureRecognizer *recognizer in frontWindow.gestureRecognizers) {
        [frontWindow removeGestureRecognizer:recognizer];
    }
    
    
    NSArray *gestrue =  [[[XDKAirMenuController sharedMenu] view] gestureRecognizers];
    for (UIGestureRecognizer *recognizer in gestrue) {
        [[[XDKAirMenuController sharedMenu] view] removeGestureRecognizer:recognizer];
    }
    
    [[self sharedMenu] sharedRelease];
    controller.airDelegate = Nil;
    controller = Nil;
    return YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (panGesture) {
        [panGesture.view removeGestureRecognizer:panGesture];
        panGesture.delegate = nil;
        panGesture = nil;
    }
    
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)];
    panGesture.delegate = self;
    
     if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
    
        [frontWindow addGestureRecognizer:panGesture];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self sendServiceToGetProfileData];
    
    _images_off = [[NSArray alloc]initWithObjects:@"menu_home",@"menu_home_btn_unselector",@"menu_my_order_btn_unselector",@"menu_payment_btn_unselector",@"menu_support_btn_unselector",@"menu_about_btn_unselector",@"menu_invite_btn_unselector",@"menu_setting_btn_unselector", nil];

    _images_on = [[NSArray alloc]initWithObjects:@"menu_home_on",@"menu_home_btn_selector",@"menu_my_order_btn_selector",@"menu_payment_btn_selector",@"menu_support_btn_selector",@"menu_about_btn_selector",@"menu_invite_btn_selector",@"menu_setting_btn_selector", nil];
    
    _menuTitle = [[NSArray alloc]initWithObjects:Profile,NewDelivery,MyOrder,Payment,Support,About,Share,Setting,nil];
    
    if ([self.airDelegate respondsToSelector:@selector(tableViewForAirMenu:)])
    {
        self.tableView = [self.airDelegate tableViewForAirMenu:self];
        self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"side_menu_bg"]];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self.tableView setScrollEnabled:NO];
    }
    
     if ([UIScreen mainScreen].bounds.size.height == 568) {
        [self.tableView setScrollEnabled:NO];
     }
     else if ([UIScreen mainScreen].bounds.size.height == 480){
         
         [self.tableView setScrollEnabled:YES];
     }
              
    [_tableView setShowsHorizontalScrollIndicator:NO];
    [_tableView setShowsVerticalScrollIndicator:NO];
    
    self.widthOpened = WIDTH_OPENED;
    self.minAlphaTableView = MIN_ALPHA_TABLEVIEW;
    self.minScaleTableView = MIN_SCALE_TABLEVIEW;
    self.minScaleController = MIN_SCALE_CONTROLLER;
    
    if ([self.airDelegate respondsToSelector:@selector(widthControllerForAirMenu:)])
        self.widthOpened = [self.airDelegate widthControllerForAirMenu:self];
    
    if ([self.airDelegate respondsToSelector:@selector(minAlphaTableViewForAirMenu:)])
        self.minAlphaTableView = [self.airDelegate minAlphaTableViewForAirMenu:self];
    
    if ([self.airDelegate respondsToSelector:@selector(minScaleTableViewForAirMenu:)])
        self.minScaleTableView = [self.airDelegate minScaleTableViewForAirMenu:self];
    
    if ([self.airDelegate respondsToSelector:@selector(minScaleControllerForAirMenu:)])
        self.minScaleController = [self.airDelegate minScaleControllerForAirMenu:self];
    
    
    _isMenuOpened = FALSE;
    
    [self openViewControllerAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tableViewReload) name:@"proPicChange" object:nil];
}
-(void)tableViewReload {
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (self.isMenuOpened)
        [self closeMenuAnimated];
}


-(void)sendServiceToGetProfileData
{
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    if (sessionToken != nil)
    {
        
    NSDictionary *params =  @{ @"ent_sess_token":sessionToken,
                               @"ent_dev_id": deviceId,
                               @"ent_date_time":[Helper getCurrentDateTime]
                               };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getProfile" paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                                        if (success) {
                                
                                                [self getProfileResponse:response];
                                        }
                                }];
    }
}

-(void)getProfileResponse:(NSDictionary *)response
{
   
    
  //  TELogInfo(@"response:%@",response);
    if (!response)
    {
        
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 96)) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [Helper showAlertWithTitle:@"Message" Message:[response objectForKey:@"errMsg"]];
        
    }

    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            
            
            [[NSUserDefaults standardUserDefaults] setObject:dictResponse[@"fName"] forKey:KDAFirstName];
           
            [[NSUserDefaults standardUserDefaults] setObject:dictResponse[@"email"] forKey:KDAEmail];
            [[NSUserDefaults standardUserDefaults] setObject:dictResponse[@"phone"] forKey:KDAPhoneNo];
            [[NSUserDefaults standardUserDefaults]setObject:dictResponse[@"pPic"] forKey:KDAProfilePic];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"proPicChange" object:nil userInfo:nil];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
        }
        
        
    }
}



#pragma mark - Gestures

- (void)panGesture:(UIPanGestureRecognizer *)sender {
    
    if (sender.state == UIGestureRecognizerStateBegan)
        self.startLocation = [sender locationInView:self.view];
    else if (sender.state == UIGestureRecognizerStateEnded
             && (self.isMenuOpened
                 || ((self.startLocation.x < DELTA_OPENING && !self.isMenuOnRight)
                     || (self.startLocation.x > self.currentViewController.view.frame.size.width -DELTA_OPENING && self.isMenuOnRight))))
    {
        CGFloat dx = self.lastLocation.x - self.startLocation.x;
        
        if (self.isMenuOnRight)
        {
            if ((self.isMenuOpened && dx > 0.f) || self.view.frame.origin.x > 3*self.view.frame.size.width / 4)
                [self closeMenuAnimated];
            else
                [self openMenuAnimated];
        }
        else
        {
            if ((self.isMenuOpened && dx < 0.f) || self.view.frame.origin.x < self.view.frame.size.width / 4)
                [self closeMenuAnimated];
            else
                [self openMenuAnimated];
        }
        
    }
    else if (sender.state == UIGestureRecognizerStateChanged
             && (self.isMenuOpened
                 || ((self.startLocation.x < DELTA_OPENING && !self.isMenuOnRight)
                     || (self.startLocation.x > self.currentViewController.view.frame.size.width -DELTA_OPENING && self.isMenuOnRight))))
        [self menuDragging:sender];
    
}

- (void)menuDragging:(UIPanGestureRecognizer *)sender
{
    CGPoint stopLocation = [sender locationInView:self.view];
    self.lastLocation = stopLocation;
    
    CGFloat dx = stopLocation.x - self.startLocation.x;
    
    CGFloat distance = dx;
    
    CGFloat width = (self.isMenuOnRight) ? (-self.view.frame.size.width + self.widthOpened) : (self.view.frame.size.width - self.widthOpened);
    
    
    CGFloat scaleController = 1 - ((self.view.frame.origin.x / width) * (1-self.minScaleController));
    
    CGFloat scaleTableView = 1 - ((1 - self.minScaleTableView) + ((self.view.frame.origin.x / width) * (-1+self.minScaleTableView)));
    
    CGFloat alphaTableView = 1 - ((1 - self.minAlphaTableView) + ((self.view.frame.origin.x / width) * (-1+self.minAlphaTableView)));
    
    
    if (scaleTableView < self.minScaleTableView)
        scaleTableView = self.minScaleTableView;
    
    if (scaleController > 1.f)
        scaleController = 1.f;
    
    self.tableView.transform = CGAffineTransformMakeScale(scaleTableView, scaleTableView);
    
    self.tableView.alpha = alphaTableView;
    
    self.currentViewController.view.transform = CGAffineTransformMakeScale(scaleController, scaleController);
    
    CGRect frame = self.view.frame;
    frame.origin.x = frame.origin.x + distance;
    
    if (self.isMenuOnRight)
    {
        if (frame.origin.x < -frame.size.width)
            frame.origin.x = -frame.size.width;
        if (frame.origin.x > 0.f)
            frame.origin.x = 0.f;
    }
    else
    {
        if (frame.origin.x < 0.f)
            frame.origin.x = 0.f;
        if (frame.origin.x > (frame.size.width))
            frame.origin.x = (frame.size.width);
    }
    
    self.view.frame = frame;
    
    frame = self.currentViewController.view.frame;
    if (self.isMenuOnRight)
        frame.origin.x = self.view.frame.size.width - frame.size.width;
    else
        frame.origin.x = 0.f;
    self.currentViewController.view.frame = frame;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (self.isMenuOpened)
        return TRUE;
    return FALSE;
}


#pragma mark - Menu

- (void)openViewControllerAtIndexPath:(NSIndexPath*)indexPath {
    if ([self.airDelegate respondsToSelector:@selector(airMenu:viewControllerAtIndexPath:)]) {
        BOOL firstTime = FALSE;
        if (self.currentViewController == nil)
            firstTime = TRUE;
        _currentViewController = [self.airDelegate airMenu:self viewControllerAtIndexPath:indexPath];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeMenuAnimated)];
        tapGesture.delegate = self;
        [self.currentViewController.view addGestureRecognizer:tapGesture];
        CGRect frame = self.view.frame;
        frame.origin.x = 0.f;
        frame.origin.y = 0.f;
        self.currentViewController.view.frame = frame;
        frame = self.view.frame;
        frame.origin.x = 0.f;
        frame.origin.y = 0.f;
        self.view.frame = frame;
        self.currentViewController.view.autoresizesSubviews = TRUE;
        self.currentViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        for (UIView *subView in self.view.subviews) {
            if (self.view.subviews.count > 1) {
                [subView removeFromSuperview];
            }
        }
        [self.view addSubview:self.currentViewController.view];
        [self addChildViewController:self.currentViewController];
        if (panGesture1) {
            [panGesture1.view removeGestureRecognizer:panGesture1];
            panGesture1.delegate = nil;
            panGesture1 = nil;
        }
        panGesture1 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)];
        if (indexPath.row != 3) {
            
         //   NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
            
            if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
                 [self.view addGestureRecognizer:panGesture1];
            }
           
        }
        if (!firstTime)
            [self openingAnimation];
    }
}

- (void)openingAnimation {
    self.currentViewController.view.transform = CGAffineTransformMakeScale(self.minScaleController, self.minScaleController);
    CGRect frame = self.view.frame;
   // NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
    
   if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        self.isMenuOnRight = YES;
        frame.origin.x = -frame.size.width + self.widthOpened;
        
    } else {
        self.isMenuOnRight = NO;
        frame.origin.x = frame.size.width - self.widthOpened;
    }
    
    self.view.frame = frame;
    self.tableView.alpha = 1.f;
    self.tableView.transform = CGAffineTransformMakeScale(1.f, 1.f);
    frame = self.currentViewController.view.frame;
    if (self.isMenuOnRight)
        frame.origin.x = self.view.frame.size.width - frame.size.width;
    else
        frame.origin.x = 0.f;
    self.currentViewController.view.frame = frame;
    [self closeMenuAnimated];
}


#pragma mark - Actions -

- (void)openMenuAnimated {
    [UIView animateWithDuration:0.3f animations:^{
        self.currentViewController.view.transform = CGAffineTransformMakeScale(self.minScaleController, self.minScaleController);
        
        CGRect frame = self.view.frame;
        
        
        if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
            
            self.isMenuOnRight = YES;
            frame.origin.x = -frame.size.width + self.widthOpened;
            
        } else {
            self.isMenuOnRight = NO;
            frame.origin.x = frame.size.width - self.widthOpened;
        }
        
        //        if (self.isMenuOnRight)
        //        else
        self.view.frame = frame;
        self.tableView.alpha = 1.f;
        self.tableView.transform = CGAffineTransformMakeScale(1.f, 1.f);
        frame = self.currentViewController.view.frame;
        if (self.isMenuOnRight)
            frame.origin.x = self.view.frame.size.width - frame.size.width;
        else
            frame.origin.x = 0.f;
        self.currentViewController.view.frame = frame;
    }];
    _isMenuOpened = TRUE;
    
    [self addBlurView];
}

-(void)addBlurView
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4.0), dispatch_get_main_queue(), ^{

        UIView *blurView = [[UIView alloc] initWithFrame:self.currentViewController.view.frame];
        blurView.backgroundColor = [UIColorFromRGB(0x333333) colorWithAlphaComponent:0.6];
        blurView.tag = 12345;
        [self.currentViewController.view addSubview:blurView];
    });
}


- (void)closeMenuAnimated {
    [UIView animateWithDuration:0.3f animations:^{
        self.currentViewController.view.transform = CGAffineTransformMakeScale(1.f, 1.f);
        
        UIView *cView = [self.currentViewController.view viewWithTag:12345];
        if (cView)
        {
            [cView removeFromSuperview];
        }
        
        CGRect frame = self.view.frame;
        frame.origin.x = 0.f;
        self.view.frame = frame;
        self.tableView.alpha = self.minAlphaTableView;
        self.tableView.transform = CGAffineTransformMakeScale(self.minScaleTableView, self.minScaleTableView);
        frame = self.currentViewController.view.frame;
        frame.origin.x = 0.f;
        self.currentViewController.view.frame = frame;
    }];
    _isMenuOpened = FALSE;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//     if (indexPath.row ==7){
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirm",@"Confirm") message:NSLocalizedString(@"Are you sure you want to logout?",@"Are you sure you want to logout?") delegate:self cancelButtonTitle:TitleNo otherButtonTitles:TitleYes, nil];
//        [alertView show];
//        [self openingAnimation];
//    }
//    else
//    {
        [self.currentViewController.view removeFromSuperview];
        [self.currentViewController removeFromParentViewController];
        [self openViewControllerAtIndexPath:indexPath];
 //   }
}

#pragma mark - Setters

- (void)setIsMenuOnRight:(BOOL)isMenuOnRight
{
    if (self.view.superview == nil)
        _isMenuOnRight = isMenuOnRight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _menuTitle.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    XDKMenuCell *cell = nil;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;

    if (cell == nil) {
        cell = [[XDKMenuCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = UIColorFromRGB(0xffffff);
    }
    
    if (indexPath.row == 0) {
        
        UIButton *setMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [setMenuButton setBackgroundColor:[UIColor clearColor]];
        
        UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, 110)];
        
        UIImageView *profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth - (WIDTH_OPENED + 88),bgView.frame.origin.y+70,78,78)];
        
        bgView.backgroundColor = UIColorFromRGB(0xd53e30);
    
        profileImageView.image = [UIImage imageNamed:@"menu_profile_default_image"];
        
        NSString *profilePic = [[NSUserDefaults standardUserDefaults] objectForKey:KDAProfilePic];
        
         NSString *strImageUrl = [NSString stringWithFormat:@"%@",profilePic];
        
        [profileImageView sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                          placeholderImage:[UIImage imageNamed:@"menu_profile_default_image"]
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                     
                                     if(image != nil)
                                     {
                                       //  profileImageView.frame = CGRectMake(68, 25, 83, 83);
                                         
                                         profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2;
                                         [profileImageView setClipsToBounds:YES];
                                         profileImageView.layer.masksToBounds = YES;
                                         
                                         profileImageView.layer.borderWidth = 2.5;
                                         profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
                                         [profileImageView layoutIfNeeded];

                                         
                                     }
                                 }];
        
        UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(20,60,130,20)];
        
        UILabel *mobileLabel = [[UILabel alloc]initWithFrame:CGRectMake(20,nameLabel.frame.size.height + 60, 130, 20)];
        
        NSString *name = [[NSUserDefaults standardUserDefaults]objectForKey:KDAFirstName];
      
        [Helper setToLabel:nameLabel Text:name WithFont:Hind_Medium FSize:20 Color:UIColorFromRGB(0xffffff)];
        
        NSString *mobileno = [Helper appendToString:[[NSUserDefaults standardUserDefaults]objectForKey:KDAPhoneNo]];
        
        [Helper setToLabel:mobileLabel Text:mobileno   WithFont:Hind_Regular FSize:15 Color:UIColorFromRGB(0xffffff)];
        
        nameLabel.textAlignment = NSTextAlignmentLeft;
        mobileLabel.textAlignment = NSTextAlignmentLeft;
        
        setMenuButton.frame = CGRectMake(0,0,200,100);
        [setMenuButton setBackgroundColor:[UIColor clearColor]];
    
        //cell.backgroundColor = [UIColor greenColor];
        
       if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang])
       {
           bgView = [[UIView alloc]initWithFrame:CGRectMake(WIDTH_OPENED, 0, cell.contentView.frame.size.width, 110)];
           bgView.backgroundColor = UIColorFromRGB(0xd53e30);
           profileImageView.frame = CGRectMake(WIDTH_OPENED+20 , bgView.frame.origin.y + 70, 80, 80);
           nameLabel.frame = CGRectMake(screenWidth-150, 50, 130, 30);
           
           //nameLabel.backgroundColor = [UIColor blueColor];
           mobileLabel.frame = CGRectMake(nameLabel.frame.origin.x,nameLabel.frame.size.height+45,130, 20);
           //mobileLabel.backgroundColor = [UIColor lightGrayColor];
           mobileLabel.textAlignment = NSTextAlignmentRight;
           nameLabel.textAlignment = NSTextAlignmentRight;
        }
        
        [cell.contentView addSubview:bgView];
        [cell.contentView addSubview:profileImageView];
        [cell.contentView  addSubview:nameLabel];
        [cell.contentView addSubview:mobileLabel];
    }
    
    else {
        
        UIImage *cellimageNormal = [UIImage imageNamed:_images_off[indexPath.row]];
        UIImage *cellimageSelected = [UIImage imageNamed:_images_on[indexPath.row]];
        UIButton *setMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [setMenuButton setBackgroundColor:[UIColor clearColor]];
        
        
       
        if ([Helper isIphone5])
        {
            setMenuButton.frame = CGRectMake(0, 0, screenWidth - (WIDTH_OPENED-10),50);
        }
        else
        {
            setMenuButton.frame = CGRectMake(0, 0, screenWidth - (WIDTH_OPENED+25),50);
        }
        
        if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
            
            setMenuButton.frame = CGRectMake(70, 0, screenWidth - WIDTH_OPENED,50);
            setMenuButton.transform = CGAffineTransformMakeScale(-1, 1);
            setMenuButton.titleLabel.transform = CGAffineTransformMakeScale(-1, 1);
            
        } else {
            setMenuButton.transform = CGAffineTransformMakeScale(1, 1);
        }
        

        [Helper setButton:setMenuButton Text:_menuTitle[indexPath.row] WithFont:Ubuntu_Regular FSize:15 TitleColor:UIColorFromRGB(0x444444)
              ShadowColor:UIColorFromRGB(0xffffca)];
        [setMenuButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
        [setMenuButton addTarget:self action:@selector(buttonPressedAction:) forControlEvents:UIControlEventTouchUpInside];
        setMenuButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        setMenuButton.titleLabel.numberOfLines = 0;
        [setMenuButton setBackgroundImage:cellimageNormal forState:UIControlStateNormal];
        [setMenuButton setBackgroundImage:cellimageSelected forState:UIControlStateHighlighted];
        setMenuButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        setMenuButton.titleEdgeInsets = UIEdgeInsetsMake(0,70, 0, 0);
        [setMenuButton setTag:indexPath.row + 100];
        UILabel *lineLabel;
        lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 45, screenWidth - WIDTH_OPENED - 10, 0)];
        
        if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang])
        {
            lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 45, screenWidth - WIDTH_OPENED - 10, 0)];
        }
        
        lineLabel.backgroundColor = UIColorFromRGB(0xE4E7F0);
        [cell.contentView addSubview:lineLabel];
        [cell.contentView addSubview:setMenuButton];
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        return 152;
        
    }
    else {
        
        return 50;
    }
    
}

#pragma mark- ButtonAction
-(void)buttonPressedAction:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        [self tableView:_tableView didSelectRowAtIndexPath:indexPath];
    }
}


#pragma mark- UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
   
    if (buttonIndex == 1) { // logout
        
        [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Logging out..",@"Logging out..")];
        
        User *user = [[User alloc] init];
        user.delegate = self;
        
        [user logout];
    }
}

#pragma mark - UserDelegate
-(void)userDidLogoutSucessfully:(BOOL)sucess{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
}
-(void)userDidFailedToLogout:(NSError *)error{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
}

@end
