//
//  Addresscell.h
//  RoadyoPassenger
//
//  Created by Rahul Sharma on 15/04/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Addresscell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *addressLabel1;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel2;
@property (strong, nonatomic) IBOutlet UIButton *removeAddressButton;
@property (weak, nonatomic) IBOutlet UIImageView *addressPin;
@property (weak, nonatomic) IBOutlet UIView *cellDivider;
@end
