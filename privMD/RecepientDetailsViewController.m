//
//  RecepientDetailsViewController.m
//  DeliveryPlus
//
//  Created by Rahul Sharma on 05/10/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import "RecepientDetailsViewController.h"
#import "PickUpAddressFromMapController.h"
#import "AddressManageViewController.h"
#import "Database.h"
#import "UploadFiles.h"
#import "AmazonTransfer.h"
#import "CountryNameTableViewController.h"
#import "CountryPicker.h"
#import "LanguageManager.h"

@interface RecepientDetailsViewController ()<AddressManageDelegate,pickupAddressFromMap>
{
    NSString *shipmentImageUrl;
    UIImage *shipmentImage;
    NSString *imageAdded;
    double dropLatitude;
    double dropLongitude;
    NSString *landMark;
    NSString *city;
    NSString *zipCode;
    NSString *flatNumber;
    BOOL isEmailEntered;
    UIWindow *window;
    NSString *fulladdress;
}
@property(nonatomic,strong) NSDictionary *addressDetails;
@property (strong,nonatomic)  UITextField *activeTextField;


@end

@implementation RecepientDetailsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    isEmailEntered = NO;
    [_phoneNumberTextField setValue:UIColorFromRGB(0x9e9e9e)
                         forKeyPath:@"_placeholderLabel.textColor"];
    [_recepientNameTextField setValue:UIColorFromRGB(0x9e9e9e)
                           forKeyPath:@"_placeholderLabel.textColor"];
    [_emailTextField setValue:UIColorFromRGB(0x9e9e9e)
                   forKeyPath:@"_placeholderLabel.textColor"];
    _phoneNumberTextField.textColor = UIColorFromRGB(0x333333);
    _recepientNameTextField.textColor = UIColorFromRGB(0x333333);
    _emailTextField.textColor = UIColorFromRGB(0x333333);
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    
    NSDictionary *dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
    
    NSString *stringCountryCode = [dictionaryWithCode objectForKey:countryCode];
    
    _countryCodeLabel.text = [NSString stringWithFormat:@"+%@",stringCountryCode];
    UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapgesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pickupAddressFromSearch:) name:@"RECEIPIENTADDRESS" object:nil];
    // Do any additional setup after loading the view.
}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [_emailTextField resignFirstResponder];
    [_phoneNumberTextField resignFirstResponder];
    [_recepientNameTextField resignFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = NO;
    [[[self navigationController]navigationBar]setBackgroundImage:[UIImage imageNamed:@"home_navigation_bar"] forBarMetrics:UIBarMetricsDefault];
    
    [self createNavView];
    self.navigationItem.hidesBackButton = YES;
    
    [self createNavLeftButton];
    [self createNavRightButton];
    window = [[UIApplication sharedApplication] keyWindow];
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)createNavView
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80,-10, 160, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(10,10, 147, 30)];
//    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(50, 30, 70, 15)];
//    image.image = [UIImage imageNamed:@"add_shipment_second_progress"];
//    [navView addSubview:image];
    navTitle.text = NSLocalizedString(@"Add Recipients", @"Add Recipients");
    navTitle.textColor = UIColorFromRGB(0x565656);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:Hind_Medium size:17];
    [navView addSubview:navTitle];
    self.navigationItem.titleView = navView;
    
}


-(void) createNavLeftButton
{
    UIImage *buttonImageOn = [UIImage imageNamed:@"signin_back_icon_on"];
    UIImage *buttonImageOff = [UIImage imageNamed:@"signin_back_icon_off"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(backButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    
  //  NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
    
   if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [navCancelButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 20,0.0f,buttonImageOff.size.width,buttonImageOff.size.height)];
    }
    
    
    [navCancelButton setTitle:@"" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"" forState:UIControlStateSelected];
    
    [navCancelButton setBackgroundImage:buttonImageOff forState:UIControlStateNormal];
    [navCancelButton setBackgroundImage:buttonImageOn forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)createNavRightButton
{
    UIButton *navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navNextButton setFrame:CGRectMake(-6,0,44,44)];
    
//    NSString *language = [[[[NSLocale preferredLanguages] objectAtIndex:0] componentsSeparatedByString:@"-"] firstObject];
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang]) {
        [navNextButton setFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x+20,0.0f,44,44)];
    }
    
    [navNextButton addTarget:self action:@selector(doneButtonClickedAction:) forControlEvents:UIControlEventTouchUpInside];
    [Helper setButton:navNextButton Text:NSLocalizedString(@"Done", @"Done") WithFont:Hind_Regular FSize:17 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navNextButton setTitle:NSLocalizedString(@"Done", @"Done") forState:UIControlStateNormal];
    [navNextButton setTitle:NSLocalizedString(@"Done", @"Done") forState:UIControlStateSelected];
    [navNextButton setTitleColor:UIColorFromRGB(0xd53e30) forState:UIControlStateNormal];
    [navNextButton setTitleColor:UIColorFromRGB(0xd53e30) forState:UIControlStateHighlighted];
    
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)backButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)doneButtonClickedAction:(id)sender
{
    [self dismissKeyboard];
//    imageAdded = [[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
    
    if(_emailTextField.text.length > 0){
        
        if ([Helper emailValidationCheck:_emailTextField.text] == 0)
        {
            [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Invalid Email Id", @"Invalid Email Id")];
            _emailTextField.text = @"";
            [_emailTextField becomeFirstResponder];
            return;
        }
        
    }
    
    
    if([_selectedAddressLabel.text isEqualToString:NSLocalizedString(@"Select an Address *",@"Select an Address *")]){
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Please Select an Address",@"Please Select an Address")];
    }
    
    else if(_recepientNameTextField.text.length == 0){
        
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Please Enter Recipient Name",@"Please Enter Recipient Name")];
        [_recepientNameTextField becomeFirstResponder];
    }
    
    else if ((unsigned long)_phoneNumberTextField.text.length == 0)
    {
        [Helper showAlertWithTitle:TitleMessage Message:NSLocalizedString(@"Please Enter Recipient Phone Number",@"Please Enter Recipient Phone Number")];
        [_phoneNumberTextField becomeFirstResponder];
    }
    
    else{
        //[self sendRequestgetETAnDistance];
        
        if(flatNumber == nil){
            
            flatNumber = @"";
        }
        
        if(zipCode == nil){
            
            zipCode = @"";
        }
        
        if(landMark == nil){
            
            landMark = @"";
        }
        
        if(city == nil){
            
            city = @"";
        }

        
        
        NSString *mobNo = [_countryCodeLabel.text stringByAppendingString:_phoneNumberTextField.text];
        NSInteger randomNumber = arc4random();
        
        NSDictionary *add = @{
                              @"address":flStrForObj(fulladdress),
                              @"recepientEmail":flStrForObj(_emailTextField.text),
                              @"phoneNumber":flStrForObj(mobNo),
                              @"name":flStrForObj(_recepientNameTextField.text),
                              @"randomNumber":[NSNumber numberWithInteger:randomNumber],
                              @"passengerEmail":[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey],
                              @"landMark":flStrForObj(landMark),
                              @"city":flStrForObj(city),
                              @"flatNumber":flStrForObj(flatNumber),
                              @"zipCode":flStrForObj(zipCode),
                              @"dropLatitude":[NSString stringWithFormat:@"%f",dropLatitude],
                              @"dropLongitude":[NSString stringWithFormat:@"%f",dropLongitude],
                            };
        
        Database *db = [[Database alloc] init];
        BOOL isAdded = [db makeDataBaseEntryForRecepients:add];
        
        if (isAdded)
        {
            
                [self backButtonClicked:nil];
            
        }
        else
        {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Saving failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }

        }
}



-(void)addShipmentToDataBase{
    
    NSInteger randomNumber = arc4random();
    
    if(flatNumber == nil){
        
        flatNumber = @"";
    }
    
    if(zipCode == nil){
        
        zipCode = @"";
    }
    
    if(landMark == nil){
        
        landMark = @"";
    }
    
    if(city == nil){
        
        city = @"";
    }
    
    NSString *mobNo = [_countryCodeLabel.text stringByAppendingString:_phoneNumberTextField.text];
    
    NSDictionary *dict = @{
                           @"image":flStrForObj(shipmentImageUrl),
                           @"shipmentDetails":_shipmentData[@"shipmentDetails"],
                           @"weight":_shipmentData[@"shipmentWeight"],
                           @"quantity":_shipmentData[@"shipmentQuantity"],
                           @"name":_recepientNameTextField.text,
                           @"phoneNumber":flStrForObj(mobNo),
                           @"address":_selectedAddressLabel.text,
                           @"email":_emailTextField.text,
                           @"randomNumber":[NSNumber numberWithInteger:randomNumber],
                           @"landMark":flStrForObj(landMark),
                           @"city":flStrForObj(city),
                           @"flatNumber":flStrForObj(flatNumber),
                           @"zipCode":flStrForObj(zipCode),
                           @"dropLatitude":[NSString stringWithFormat:@"%f",dropLatitude],
                           @"dropLongitude":[NSString stringWithFormat:@"%f",dropLongitude],
                        };
    
    
    
    Database *db = [[Database alloc]init];
    [db makeDataBaseEntryForShipments:dict];
    
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    PatientAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    ShipmentDetailsViewController *sdVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"shipmentDetailsVC"];
//    
//    sdVC.isFromRecepientVC = YES;
//    sdVC.isFromMapVC = YES;
//    UINavigationController *naviVC =(UINavigationController*) appDelegate.window.rootViewController;
//    [naviVC pushViewController:sdVC animated:YES];
    
    
}


- (IBAction)nextButtonAction:(id)sender {

    [self performSegueWithIdentifier:@"toAddAddress" sender:sender];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"toAddAddress"])
    {
        PickUpAddressFromMapController *obj1 = (PickUpAddressFromMapController *)[segue destinationViewController];
        obj1.isFromHomeVC = NO;
        obj1.isFromRecepientVC = YES;
        
    }
    
    
}

//get address from Favourite adresses
-(void)pickupAddressFromSearch:(NSNotification *)addressDetails {
    
    _addressDetails = [[addressDetails userInfo]mutableCopy];
    
    landMark =  _addressDetails[@"tagAddress"];
    city = _addressDetails[@"city"];
    flatNumber = _addressDetails[@"flatNumber"];
    zipCode = _addressDetails[@"zipCode"];
    
    dropLatitude = [_addressDetails[@"latitude"] doubleValue];
    dropLongitude = [_addressDetails[@"longitude"]doubleValue];
    NSString *str;
    if(flatNumber.length > 0 ){
        
        str = [NSString stringWithFormat:@"%@,%@",flatNumber,_addressDetails[@"address1"]];
        
    }
    else{
        
        str = [NSString stringWithFormat:@"%@",_addressDetails[@"address1"]];
        
    }
    
    fulladdress = str;
    
    if (landMark.length > 0) {
        str = [NSString stringWithFormat:@"%@,%@",landMark,str];
    }
    
    
    _selectedAddressLabel.text =  str;
}

-(void)getAddressFromDatabase:(NSDictionary *)addressDetails
{
    
    _addressDetails = [addressDetails mutableCopy];
    landMark =  _addressDetails[@"tagAddress"];
    city = _addressDetails[@"city"];
    flatNumber = _addressDetails[@"flatNumber"];
    zipCode = _addressDetails[@"zipCode"];
    dropLatitude = [_addressDetails[@"latitude"] doubleValue];
    dropLongitude = [_addressDetails[@"longitude"]doubleValue];
    NSString *str;
    if(flatNumber.length > 0){
        
        str = [NSString stringWithFormat:@"House No:%@,%@",flatNumber,_addressDetails[@"address1"]];
        
    }
    else{
        
        str = [NSString stringWithFormat:@"%@",_addressDetails[@"address1"]];
        
    }
    _selectedAddressLabel.text =  str;
}

#pragma mark - TextFields

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
    if (textField == _recepientNameTextField) {
        _divider1.backgroundColor = UIColorFromRGB(0xef4836);
        _divider2.backgroundColor = UIColorFromRGB(0x444444);
        _divider3.backgroundColor = UIColorFromRGB(0x444444);
    }
    else if (textField == _phoneNumberTextField)
    {
        
        _divider1.backgroundColor = UIColorFromRGB(0x444444);
        _divider2.backgroundColor = UIColorFromRGB(0xef4836);
        _divider3.backgroundColor = UIColorFromRGB(0x444444);
        
        
    }
    else
    {
        _divider1.backgroundColor = UIColorFromRGB(0x444444);
        _divider2.backgroundColor = UIColorFromRGB(0x444444);
        _divider3.backgroundColor = UIColorFromRGB(0xef4836);
    }
    
    
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _recepientNameTextField) {
        
        _divider1.backgroundColor = UIColorFromRGB(0x444444);
        
    }
    else if (textField == _phoneNumberTextField)
    {
        
        _divider2.backgroundColor = UIColorFromRGB(0x444444);
        
    }
    else
    {
        _divider3.backgroundColor = UIColorFromRGB(0x444444);
        
    }
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    if (textField == _recepientNameTextField)
    {
        [_phoneNumberTextField becomeFirstResponder];
    }
    else if (textField == _phoneNumberTextField)
    {
        [_emailTextField becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        [self doneButtonClickedAction:nil];
    }
    return YES;
    
}

- (void)keyboardWillHide:(__unused NSNotification *)inputViewNotification
{
    UIEdgeInsets ei = UIEdgeInsetsMake(48.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.scrollView.scrollIndicatorInsets = ei;
        self.scrollView.contentInset = ei;
        self.scrollView.contentOffset = CGPointZero;
    }];
}

- (void)keyboardWillShow:(__unused NSNotification *)inputViewNotification
{
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.scrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.scrollView.scrollIndicatorInsets = ei;
    self.scrollView.contentInset = ei;
}



@end
