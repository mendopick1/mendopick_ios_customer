//
//  ContactsTableViewCell.m
//  Speedy
//
//  Created by Rahul Sharma on 21/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "ContactsTableViewCell.h"
#import "Helper.h"

@implementation ContactsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [Helper setCornerRadius:_imageviewProfile.frame.size.width/2 bordercolor:CLEAR_COLOR withBorderWidth:0 toview:_imageviewProfile];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
