//
//  ShipmentDetails.h
//  DeliveryPlus
//
//  Created by Rahul Sharma on 30/10/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface ShipmentDetails : NSManagedObject


@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * cost;
@property (nonatomic, retain) NSNumber * miles;
@property (nonatomic ,retain) NSNumber * randomNumber;
@property (nonatomic, retain) NSString * shipmentDetails;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * weight;
@property (nonatomic ,retain) NSString * phoneNumber;
@property (nonatomic ,retain) NSNumber * quantity;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * height;
@property (nonatomic, retain) NSString * width;
@property (nonatomic, retain) NSString * length;
@property (nonatomic, retain) NSString * flatNumber;
@property (nonatomic, retain) NSString * productName;
@property (nonatomic, retain) NSString * volume;
@property (nonatomic, retain) NSString * landMark;
@property (nonatomic, retain) NSString * zipCode;
@property (nonatomic, retain) NSString * dropLatitude;
@property (nonatomic, retain) NSString * dropLongitude;

@end
