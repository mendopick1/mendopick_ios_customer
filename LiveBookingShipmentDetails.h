//
//  LiveBookingShipmentDetails.h
//  DeliveryPlus
//
//  Created by Rahul Sharma on 19/11/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import <CoreData/CoreData.h>
@class LiveBookingTable;

@interface LiveBookingShipmentDetails : NSManagedObject

@property (nonatomic, retain) NSString * bid;
@property (nonatomic, retain) NSString * shipmentStatus;
@property (nonatomic, retain) NSString * shipmentId;
@property (nonatomic, retain) NSString * shipmentAddress;
@property (nonatomic, retain) NSString * shipmentPhoneNumber;
@property (nonatomic, retain) NSString * shipmentImage;
@property (nonatomic, retain) NSString * dropLatitude;
@property (nonatomic, retain) NSString * dropLongitude;
@property (nonatomic, retain) NSString * recepientName;

@end
