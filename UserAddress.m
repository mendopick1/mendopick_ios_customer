//
//  UserAddress.m
//  Servodo
//
//  Created by Rahul Sharma on 15/09/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "UserAddress.h"


@implementation UserAddress

@dynamic addressLine1;
@dynamic tagAddress;
@dynamic identity;
@dynamic flatNumber;
@dynamic longitude;
@dynamic latitude;
@dynamic randomNumber;
@dynamic city;
@dynamic email;

@end
