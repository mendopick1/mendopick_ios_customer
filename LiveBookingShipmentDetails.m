//
//  LiveBookingShipmentDetails.m
//  DeliveryPlus
//
//  Created by Rahul Sharma on 19/11/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import "LiveBookingShipmentDetails.h"

@implementation LiveBookingShipmentDetails

@dynamic shipmentId;
@dynamic shipmentStatus;
@dynamic bid;
@dynamic shipmentAddress;
@dynamic shipmentImage;
@dynamic shipmentPhoneNumber;
@dynamic dropLatitude;
@dynamic dropLongitude;
@dynamic recepientName;

@end
