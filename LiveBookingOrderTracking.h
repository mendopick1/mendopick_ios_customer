//
//  LiveBookingOrderTracking.h
//  DeliveryPlus
//
//  Created by Rahul Sharma on 26/11/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface LiveBookingOrderTracking : NSManagedObject

@property (nonatomic, retain) NSString * bid;
@property (nonatomic, retain) NSString * shipmentId;
@property (nonatomic, retain) NSString * onthewayTime;
@property (nonatomic, retain) NSString * pickedupTime;
@property (nonatomic, retain) NSString * jobstartedTime;
@property (nonatomic, retain) NSString * jobcompletedTime;

@end
