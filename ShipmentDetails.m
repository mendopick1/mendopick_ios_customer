//
//  ShipmentDetails.m
//  DeliveryPlus
//
//  Created by Rahul Sharma on 30/10/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import "ShipmentDetails.h"

@implementation ShipmentDetails

@dynamic address;
@dynamic name;
@dynamic cost;
@dynamic miles;
@dynamic image;
@dynamic randomNumber;
@dynamic shipmentDetails;
@dynamic quantity;
@dynamic weight;
@dynamic phoneNumber;
@dynamic email;
@dynamic city;
@dynamic landMark;
@dynamic flatNumber;
@dynamic width;
@dynamic length;
@dynamic height;
@dynamic productName;
@dynamic zipCode;
@dynamic volume;
@dynamic dropLatitude;
@dynamic dropLongitude;

@end
