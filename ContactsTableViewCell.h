//
//  ContactsTableViewCell.h
//  Speedy
//
//  Created by Rahul Sharma on 21/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelMobileNo;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnContactSelect;

@end
