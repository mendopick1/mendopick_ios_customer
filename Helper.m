//
//  Helper.m
//  Restaurant
//
//  Created by 3Embed on 14/09/12.
//
//

#import "Helper.h"
#import "LanguageManager.h"

@implementation Helper


static Helper *helper;
@synthesize _latitude;
@synthesize _longitude;
@synthesize menu_valueChanged;
@synthesize location;
@synthesize locate_ValueChanged;







+ (id)sharedInstance {
	if (!helper) {
		helper  = [[self alloc] init];
	}
	
	return helper;
}


+(void)setToLabel:(UILabel*)lbl Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size Color:(UIColor*)color
{
   
    lbl.textColor = color;

    if (txt != nil) {
        lbl.text = txt;
    }
    
    
    if (font != nil) {
        lbl.font = [UIFont fontWithName:font size:_size];
    }
    
    lbl.backgroundColor = [UIColor clearColor];
}



+(void)setButton:(UIButton*)btn Text:(NSString*)txt WithFont:(NSString*)font FSize:(float)_size TitleColor:(UIColor*)t_color ShadowColor:(UIColor*)s_color
{
    [btn setTitle:txt forState:UIControlStateNormal];
    
    [btn setTitleColor:t_color forState:UIControlStateNormal];
    
    if (s_color != nil) {
        [btn setTitleShadowColor:s_color forState:UIControlStateNormal];
    }
    
    
    if (font != nil) {
        btn.titleLabel.font = [UIFont fontWithName:font size:_size];
    
    }
    else
    {
        btn.titleLabel.font = [UIFont systemFontOfSize:_size];
    }
}

+(void)showAlertWithTitle:(NSString*)title Message:(NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LS(title) message:LS(message) delegate:nil cancelButtonTitle:LS(@"OK") otherButtonTitles:nil];
    [alert show];
}

+(void)showErrorFor:(int)errorCode
{
}

+ (NSString *)removeWhiteSpaceFromURL:(NSString *)url
{
	NSMutableString *string = [[NSMutableString alloc] initWithString:url] ;
	[string replaceOccurrencesOfString:@" " withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
    
  	return string;
}


+ (NSString *)stripExtraSpacesFromString:(NSString *)string
{
	NSCharacterSet *whitespaces = [NSCharacterSet whitespaceCharacterSet];
	NSPredicate *noEmptyStrings = [NSPredicate predicateWithFormat:@"SELF != ''"];
	
	NSArray *parts = [string componentsSeparatedByCharactersInSet:whitespaces];
	NSArray *filteredArray = [parts filteredArrayUsingPredicate:noEmptyStrings];
	
	return [filteredArray componentsJoinedByString:@" "];
}


+(NSString*)getCurrentDate
{
    // Get current date time
    
    NSDate *currentDateTime = [NSDate date];
    
    // Instantiate a NSDateFormatter
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the dateFormatter format
    
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale: usLocale];

    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    // Get the date time in NSString
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];

    return dateInStringFormated;
   
    
    // Release the dateFormatter
    
    //[dateFormatter release];
}


+(NSString*)getDueTime :(NSString *)deliverytime
{
    // Get current date time
    
    NSDate *currentDateTime = [NSDate date];
    
    // Instantiate a NSDateFormatter
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the dateFormatter format
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    
    
    
    // Get the date time in NSString
    
    //NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    
    NSDate *toDate = [dateFormatter dateFromString:deliverytime];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit
                                               fromDate:toDate
                                                 toDate:currentDateTime
                                                options:0];
    
    
    NSString * stringDue = [NSString stringWithFormat:@"Day-%li Hours-%li Mins-%li Secs-%li",(long)components.day, (long)components.hour, (long)components.minute,(long)components.second];
    return stringDue;
 //   TELogInfo(@"day%@", stringDue);
    
    // Release the dateFormatter
    
    //[dateFormatter release];
}



+(NSString*)getCurrentTime
{
    NSDate *currentDateTime = [NSDate date];
    
    // Instantiate a NSDateFormatter
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the dateFormatter format
    
    
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale: usLocale];
    
    [dateFormatter setDateFormat:@"HH:MM:SS"];
    
    // Get the date time in NSString
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    
    return dateInStringFormated;

}



+(NSString*)getDayDate
{
    NSDate *currentDateTime = [NSDate date];
    
    // Instantiate a NSDateFormatter
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the dateFormatter format
    
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    
    [dateFormatter setDateFormat:@"EEEE"];
    
    // Get the date time in NSString
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    
    return dateInStringFormated;
}

+(NSString*)getDay:(NSDate *)date
{
    NSDate *currentDateTime = [NSDate date];
    
    // Instantiate a NSDateFormatter
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Set the dateFormatter format
    
    //[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    // or this format to show day of the week Sat,11-12-2011 23:27:09
    
    [dateFormatter setDateFormat:@"EEEE"];
    
    // Get the date time in NSString
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:currentDateTime];
    
    return dateInStringFormated;
}

+(NSString *)getCurrentDateTime
{
    
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale: usLocale];

    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:now];
    
       
    return dateInStringFormated;

}


+ (UIColor *)getColorFromHexString:(NSString *)hexString
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


+(NSDate *)convertGMTtoLocal:(NSString *)gmtDateStr
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSTimeZone *gmt = [NSTimeZone systemTimeZone];
    
    [formatter setTimeZone:gmt];
    
    NSDate *localDate = [formatter dateFromString:gmtDateStr]; // get the date
    
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMT]; // You could also use the systemTimeZone method
    
    NSTimeInterval localTimeInterval = [localDate timeIntervalSinceReferenceDate] + timeZoneOffset;
    
    NSDate *localCurrentDate = [NSDate dateWithTimeIntervalSinceReferenceDate:localTimeInterval];
    return localCurrentDate;
    }



+(NSDate *)convertLocalToGMT:(NSString *)localDateStr
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *localDate = [formatter dateFromString: localDateStr];
    NSTimeInterval timeZoneOffset = [[NSTimeZone defaultTimeZone] secondsFromGMT]; // You could also use the systemTimeZone method
    NSTimeInterval gmtTimeInterval = [localDate timeIntervalSinceReferenceDate] - timeZoneOffset;
    NSDate *gmtDate = [NSDate dateWithTimeIntervalSinceReferenceDate:gmtTimeInterval];
    return gmtDate;
}

+(void)setCornerRadius:(float)corner bordercolor:(UIColor *)color withBorderWidth:(float)width toview:(UIView *)view
{
    view.layer.cornerRadius = corner;
    view.layer.borderColor = color.CGColor;
    view.layer.borderWidth = width;
}

+(void)makeCircular:(UIView *)view
{
    [view layoutIfNeeded];
    view.layer.cornerRadius = view.frame.size.width/2;
    view.clipsToBounds  = YES;
}



+(void)makeShadowForView:(UIView *)viewContent
{
    viewContent.backgroundColor = WHITE_COLOR;
    viewContent.layer.masksToBounds = NO;
    viewContent.layer.shadowColor = UIColorFromRGB(0x646464).CGColor;
    viewContent.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    viewContent.layer.shadowOpacity = 0.2;
    viewContent.layer.shadowRadius = 2.0f;
}

+(NSString *)appendToString:(NSString *)str
{
    if(str != nil)
    {
    NSString *plusCode = @"+";
    plusCode = [plusCode stringByAppendingString:[Helper stripExtraSpacesFromString:str]];
    return plusCode;
}
    else
    {
        return @"";
    }
}

+(NSMutableAttributedString *)makeAttributedTitlle:(NSString *)date
{
    
    NSDictionary *attrs = @{ NSForegroundColorAttributeName :UIColorFromRGB(0x565656),
                             NSFontAttributeName:[UIFont fontWithName:Hind_Medium size:17]
                             };
    NSAttributedString *attrStr1 = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n",NSLocalizedString(@"Your Last Booking", @"Your Last Booking")] attributes:attrs];
    NSDictionary *attrs2 = @{ NSForegroundColorAttributeName : UIColorFromRGB(0x565656),
                              NSFontAttributeName:[UIFont fontWithName:Hind_Medium size:15]
                              };
    
    
    NSString *myString = date;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"dd MMM yyyy";
    
    NSMutableAttributedString *title1 = [[NSMutableAttributedString alloc]initWithString:[dateFormatter stringFromDate:yourDate] attributes:attrs2];
    
    NSMutableAttributedString *ti = [attrStr1 mutableCopy];
    [ti appendAttributedString:title1];

    return ti;
}

+(NSString *)togetTimeDifference:(NSString *)time1
{
    
    NSDateFormatter *df=[[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [df setDateFormat:@"dd MMM yyyy hh:mm a"];
    NSDate *date1= [df dateFromString:time1];
    
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *yourDate = [dateFormatter dateFromString:time1];
    dateFormatter.dateFormat = @"dd MMM yyyy hh:mm a";

    
    
    NSString *result = [df stringFromDate:yourDate];
    return result;
}


/*
 *  Validates an eMail Address.
 */
+ (BOOL) emailValidationCheck: (NSString *) emailToValidate
{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}


+(BOOL)isIphone5{
    
    if([[UIScreen mainScreen] bounds].size.height == 568)
        return YES;
    else
        return NO;
}


@end
