//
//  ContactTableViewController.h
//  Speedy
//
//  Created by Rahul Sharma on 21/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactTableViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITableView *tableviewContact;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic,copy) void (^oncomplete)(NSDictionary * code);
@end
