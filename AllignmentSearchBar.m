//
//  AllignmentSearchBar.m
//  MenDoPick
//
//  Created by Rahul Sharma on 28/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "AllignmentSearchBar.h"
#import "LanguageManager.h"


@implementation AllignmentSearchBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setTextAlignmentForLeftAlignedTextView];
    
    
}

- (void)setTextAlignmentForLeftAlignedTextView {
    
    UITextField *searchTextField = [self valueForKey:@"_searchField"];
    
    if([[LanguageManager currentLanguageString] isEqualToString:ArabicLang])
    {
        [self setValue:TitleCancel forKey:@"_cancelButtonText"];
        [searchTextField setTextAlignment:NSTextAlignmentRight];
    }
    else
    {
        [self setValue:TitleCancel forKey:@"_cancelButtonText"];
        [searchTextField setTextAlignment:NSTextAlignmentLeft];
    }

}

@end
