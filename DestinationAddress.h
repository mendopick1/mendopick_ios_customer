//
//  DestinationAddress.h
//  DeliveryPlus
//
//  Created by Rahul Sharma on 07/10/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface DestinationAddress : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "DestinationAddress+CoreDataProperties.h"
