//
//  LiveBookingStatusUpdatingClass.m
//  DeliveryPlus
//
//  Created by Rahul Sharma on 21/11/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import "LiveBookingStatusUpdatingClass.h"
#import "Database.h"
#import "LiveBookingTable.h"
#import "OnTheWayViewController.h"
#import "NewOrderViewController.h"
#import "PatientAppDelegate.h"
#import "LiveBookingTable.h"
#import "LiveBookingShipmentDetails.h"
#import "XDKAirMenuController.h"
#import "CurrentOrderViewController.h"
#import "RateViewController.h"
#import "AcceptShipmentViewController.h"
#import "OrderSummaryViewController.h"

#define our agent NSLocalizedString(@"our agent",@"our agent")

#define TitleAgent LS(@"Our Agent")
#define TitleCancel1 LS(@"Vehicle Break Down")
#define TitleCancel2 LS( @"No One At Pickup Point")
#define TitleCancel3 LS(@"Other Reasons")
#define TitleCancel4 LS(@"Driver Cancelled Booking for")

#define TitleOntheWay LS(@"is on the way to pickup your order.")

#define TitleArrived LS(@"has arrived , please keep your stuff ready for him to pickup")

#define TitleToDeliveryLocation LS(@"is on the way to deliver your order.")

#define TitleToDeliveryUnloaded LS(@"has arrived at drop location and unloaded the vehicle.")

#define TitleToDeliveryCompleted LS(@"has completed the Booking!")

static LiveBookingStatusUpdatingClass *updateLiveBookingStatus;


@interface LiveBookingStatusUpdatingClass()<UIAlertViewDelegate>{
    
    UIAlertView *alertViews;
    PatientAppDelegate *appDelegate;
    UIStoryboard *mainstoryboard;
    UINavigationController *naviVC;
    NSArray *controllerArray;
    LiveBookingShipmentDetails *lbsd;
    OrderSummaryViewController *orderSummary;
    NewOrderViewController *newOrder;
    BOOL bookingCompleted;
    
}

@end


@implementation LiveBookingStatusUpdatingClass
{
    
    NSString *statusFromResponse;
    NSInteger statusChanged;
    NSDictionary *appointmentDetails;
    NSString *shipmentId;
    LiveBookingTable *liveBookingTB;
    NSArray *appntDetails;
    
}

+(instancetype)sharedInstance
{
    if (!updateLiveBookingStatus) {
        
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            
            updateLiveBookingStatus = [[self alloc] init];
            
        });
    }
    
    return updateLiveBookingStatus;
}


-(NSString*)getCancelStatus:(CancelBookingReasons)statusCancel {
    
    NSString *cancelStatus;
  //  TELogInfo(@"status of cancel %d",statusCancel);
    switch (statusCancel) {
        case CBRVehicleBreakDown:
            cancelStatus = TitleCancel1;
            break;
        case CBRNoOneAtPickUpPoint:
            cancelStatus = TitleCancel2;
            break;
        case CBROtherReasons:
            cancelStatus = TitleCancel3;
            break;
        default:
            cancelStatus = TitleCancel3;
            break;
    }
    
    return cancelStatus;
}


-(void)updateLiveBookingStatusAndShowVC:(NSDictionary *)appointmentResponse
{
    
    bookingCompleted = NO;
    appDelegate = [UIApplication sharedApplication].delegate;
    mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    naviVC =(UINavigationController*) appDelegate.window.rootViewController;
    controllerArray = naviVC.viewControllers;
    appntDetails = [[NSArray alloc]init];
    
    appointmentDetails = appointmentResponse;
    
    if([appointmentResponse[@"a"] integerValue] == kNotificationTypeBookingCancelled || [appointmentResponse[@"nt"] integerValue] == 10) {
        
        _cancelReason  = [appointmentResponse[@"r"] integerValue];
        
        NSString *cancelStatus = [self getCancelStatus:_cancelReason];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:TitleMessage message:[NSString stringWithFormat:@"Driver Cancelled Booking for %@",cancelStatus] delegate:self cancelButtonTitle:TitleOk otherButtonTitles:nil, nil];
        alertView.tag = 50;
        [alertView show];
        return;
        
    }
    
    //checking for status 21 and 22
    else if([appointmentResponse[@"a"] integerValue] == kNotificationTypeIndividualBooking ||[appointmentResponse[@"a"] integerValue]== kNotificationTypeBookingClosed||[appointmentResponse[@"nt"] integerValue] == kNotificationTypeIndividualBooking ||[appointmentResponse[@"nt"] integerValue]== kNotificationTypeBookingClosed){
        
        shipmentId = appointmentResponse[@"sub_id"];
        
        if (!shipmentId)
        {
            shipmentId = @"1";
        }
        
        appntDetails = [Database getLiveBookingEachShipmentDetails:appointmentResponse[@"bid"] shipmentId:shipmentId];
        
    }
    else if ([appointmentResponse[@"a"] integerValue] == kNotificationTypeBookingAccepted ||[appointmentResponse[@"nt"] integerValue] == kNotificationTypeBookingAccepted || [appointmentResponse[@"a"] integerValue] == kNotificationTypeBookingAccept || [appointmentResponse[@"nt"] integerValue] == kNotificationTypeBookingAccept)
    {
        return;
    }
    else{
        
        shipmentId = @"1";
        appntDetails = [Database getLiveBookingEachShipmentDetails:appointmentResponse[@"bid"]];
        
    }
    
    if(appntDetails.count!= 0){//Checking The Appointment Details Availble in DB
        
        if(appntDetails.count == 1){//UpdatingStaus for only one Shipment
            
            lbsd = appntDetails[0];
            _status = lbsd.shipmentStatus;
            statusFromResponse = appointmentResponse[@"a"];
            
            if(statusFromResponse == nil)
            {
                statusFromResponse = appointmentResponse[@"nt"];
            }
            if ([_status integerValue] == 11) {
                
                _status = @"2";
            }
            
            if( _status.integerValue < statusFromResponse.integerValue){//Checking the Status is Changed or Not
                _status = statusFromResponse;
                [lbsd setShipmentStatus:statusFromResponse];
                [self showOngoingBookingViews:appointmentResponse];//to Show Alert
            }
            else if (_status.integerValue == statusFromResponse.integerValue && _status.integerValue == kNotificationTypeBookingClosed)
            {
            }
        }
    }
    else{
        
        [naviVC popToRootViewControllerAnimated:YES];
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    }
    
    
}

#pragma mark - alertview delegate -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 60){
        
        if(buttonIndex == 1)//View Button Action
        {
            controllerArray = naviVC.viewControllers;
            
            if(![controllerArray[controllerArray.count-1] isKindOfClass:[OnTheWayViewController class]])
            {
                
                [self sendRequestToGetAppointmentDetails];
                
            }
            else{
                
                OnTheWayViewController *onTheWayVC = controllerArray[controllerArray.count-1];
                [onTheWayVC statusUpdatedResponse:appointmentDetails and:shipmentId];
                
            }
            
        }
        if(buttonIndex == 0){//Ok Button Action
            
            
            if (self.updateDelegate && [self.updateDelegate respondsToSelector:@selector(updateLiveBookingStatusResponse:)]) {
                
                [self.updateDelegate updateLiveBookingStatusResponse:_status.integerValue];
            }
        }
    }
    else if(alertView.tag == 70)
    {
        [self toShowInvoicePopUp];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    controllerArray = naviVC.viewControllers;
    if (alertView.tag == 50){
        
         if([[controllerArray lastObject] isKindOfClass:[NewOrderViewController class]])
        {
            newOrder = [NewOrderViewController getSharedInstance];
            [newOrder orderSummary];
        }
    }
}

-(void)sendRequestToGetAppointmentDetails {
    
//        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
//        [pi showPIOnView: withMessage:@"Loading Appointment Detail"];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *emailId;
    NSString *appointmentDate;
    
   
        
        emailId = liveBookingTB.passengerEmail;
        appointmentDate = liveBookingTB.appointmentDate;
    
    NSString *currentDate = [Helper getCurrentDateTime];
    
    @try {
        NSDictionary *params = @{
                                 
                                 @"ent_sess_token":flStrForObj(sessionToken),
                                 @"ent_dev_id":flStrForObj(deviceID),
                                 @"ent_email":flStrForObj(emailId),
                                 @"ent_user_type":flStrForObj(@"2"),
                                 @"ent_appnt_dt":flStrForObj(appointmentDate),
                                 @"ent_date_time":flStrForObj(currentDate),
                                 
                                 };
        
        //setup request
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:kSMGetAppointmentDetial
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success) { //handle success response
                                           //    TELogInfo(@"response %@",response);
                                           [self parseAppointmentDetailResponse:response];
                                       }
                                   }];
    }
    @catch (NSException *exception) {
        //  TELogInfo(@"Invoice Exception : %@",exception);
    }
    
    
}

#pragma mark - WebService Response
-(void)parseAppointmentDetailResponse:(NSDictionary*)response{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil) {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:TitleError Message:[response objectForKey:@"Error"]];
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 81 || [response[@"errNum"] intValue] == 78 )) { //session Expired
        
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            OrderSummaryViewController *onthewayVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"OrderSummaryViewController"];
            onthewayVC.shipmentDetailsBookingList = response;
            onthewayVC.BookingStatus = [_status integerValue];
            onthewayVC.apptDate = response[@"apptDt"];
            onthewayVC.bid = [[response objectForKey:@"bid"] integerValue];
            [naviVC pushViewController:onthewayVC animated:YES];
        }
        else
        {
            [Helper showAlertWithTitle:TitleMessage Message:@"errMsg"];
        }
        
    }
}




#pragma mark - custom methods -

-(void)toShowInvoicePopUp {
    
    [alertViews dismissWithClickedButtonIndex:0 animated: TRUE];
    
    appntDetails = [[NSArray alloc]init];
    
    appntDetails = [Database getLiveBookingDetails:appointmentDetails[@"bid"]];
    if(appntDetails.count > 0){
        
        liveBookingTB = appntDetails[0];
        controllerArray = naviVC.viewControllers;
        if(controllerArray.count > 0){
            
            UIViewController *currentVC = controllerArray[controllerArray.count-1];
            RateViewController *invoiceVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"rateVC"];
            invoiceVC.customerEmail = liveBookingTB.passengerEmail;
            invoiceVC.appointmentDate = liveBookingTB.appointmentDate;
            currentVC.definesPresentationContext = YES;
            invoiceVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
            invoiceVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [naviVC presentViewController:invoiceVC animated:YES completion:nil];
    }
    }
    else
    {
        
        [naviVC popToRootViewControllerAnimated:YES];
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    }
}

-(void)showOngoingBookingViews:(NSDictionary *)appointmentResponse{
    
    NSString *message;
    
    appntDetails = [[NSArray alloc]init];
    
    appntDetails = [Database getLiveBookingDetails:appointmentResponse[@"bid"]];
    liveBookingTB = appntDetails[0];
    
    if (_status.integerValue == kNotificationTypeBookingClosed){//Checking the Booking is Completed or not
        
        NSArray *shipments = [Database getLiveBookingEachShipmentDetails:appointmentResponse[@"bid"]];
        NSInteger count = 0;
        for(int i= 0;i<shipments.count;i++){//to check for all shipments are completed
            
            lbsd = shipments[i];
            if(lbsd.shipmentStatus.integerValue == kNotificationTypeBookingClosed ){
                
                count++;
            }
        }
        if(count == shipments.count){
            
            //show Invoice
            bookingCompleted = YES;
            
        }
    }
    
    NSString *passengerName = [[NSUserDefaults standardUserDefaults]objectForKey:KDAFirstName];
    NSString *driverName = liveBookingTB.driverFirstName;
    
    if(_status.integerValue == kNotificationTypeBookingOnMyWay){
        
        message = [NSString stringWithFormat:@"%@, %@ %@ %@",passengerName,TitleAgent,driverName,TitleOntheWay];
        
    }
    else if (_status.integerValue == kNotificationTypeBookingReachedLocation){
        
        message = [NSString stringWithFormat:@"%@, %@ %@ %@",passengerName,TitleAgent,driverName,TitleArrived];
        
    }
    else if (_status.integerValue == kNotificationTypeBookingStarted){
        
        message = [NSString stringWithFormat:@"%@, %@ %@ %@",passengerName,TitleAgent,driverName,TitleToDeliveryLocation];
    }
    else if (_status.integerValue == kNotificationTypeIndividualBooking){
        
        message = [NSString stringWithFormat:@"%@, %@ %@ %@",passengerName,TitleAgent,driverName,TitleToDeliveryUnloaded];
    }
    else if (_status.integerValue == kNotificationTypeBookingClosed){
        
        message = [NSString stringWithFormat:@"%@, %@ %@ %@",passengerName,TitleAgent,driverName,TitleToDeliveryCompleted];
    }
    
    
    if(![[controllerArray lastObject] isKindOfClass:[OrderSummaryViewController class]] && ![[controllerArray lastObject] isKindOfClass:[OnTheWayViewController class]])
    {
        [self showAlertMessages:message];
    }
    
    else if([[controllerArray lastObject] isKindOfClass:[OrderSummaryViewController class]])
    {

        if (bookingCompleted)
        {
            [self toShowInvoicePopUp];
        }
        else
        {
        [self showAlertMessagesForSummary:message];
        }
    }
    else
      {
            if(bookingCompleted == YES){
                
                
                
                [self toShowInvoicePopUp];
            }
            else
            {
                [self showAlertMessagesForSummary:message];
            }
        }
}

-(void)showAlertMessages:(NSString *)message{
    
    [alertViews dismissWithClickedButtonIndex:0 animated: TRUE];
    if(bookingCompleted == YES){
        
        alertViews = [[UIAlertView alloc] initWithTitle:LS(@"Message") message:[NSString stringWithFormat:@"%@",message] delegate:self cancelButtonTitle:LS(@"OK") otherButtonTitles:nil, nil];
        alertViews.tag = 70;
        
        [alertViews show];
        
    }
    else{
        
        alertViews = [[UIAlertView alloc] initWithTitle:LS(@"Message") message:[NSString stringWithFormat:@"%@",message] delegate:self cancelButtonTitle:LS(@"OK") otherButtonTitles:LS(@"VIEW"), nil];
        alertViews.tag = 60;
        [alertViews show];
        
    }
    
}

-(void)showAlertMessagesForSummary:(NSString *)message
{
    [alertViews dismissWithClickedButtonIndex:0 animated: TRUE];
    
    alertViews = [[UIAlertView alloc] initWithTitle:LS(@"Message") message:[NSString stringWithFormat:@"%@",message] delegate:self cancelButtonTitle:LS(@"OK") otherButtonTitles:nil, nil];
    
    [alertViews show];
}

-(void)showSameBookingView:(NSDictionary *)appointmentResponse{
    
    
}

-(void)gotoMyOrderViewController:(NSDictionary *)appointmentResponse{
    
    
}


@end
