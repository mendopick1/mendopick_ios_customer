//
//  LiveBookingTable.h
//  DeliveryPlus
//
//  Created by Rahul Sharma on 19/11/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//

#import <CoreData/CoreData.h>
@class LiveBookingShipmentDetails;
@interface LiveBookingTable : NSManagedObject


@property (nonatomic, retain) NSString * bid;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * passengerEmail;
@property (nonatomic, retain) NSString * pickupAddress;
@property (nonatomic, retain) NSString * pickupLat;
@property (nonatomic, retain) NSString * pickupLong;
@property (nonatomic, retain) NSString * passengerMobile;
@property (nonatomic, retain) NSString * appointmentDate;
@property (nonatomic, retain) NSString * driverFirstName;
@property (nonatomic, retain) NSString * driverLastName;
@property (nonatomic, retain) NSString * driverPpic;

@end
